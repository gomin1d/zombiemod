package yt.dms.api.bungee;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import yt.dms.api.annotation.BungeeOnly;
import yt.dms.api.both.MessagesSender;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by k.shandurenko on 31.07.2018
 */
@BungeeOnly
public interface BMessagesSender extends MessagesSender {

    /**
     * Отправить сообщение (message) от имени sender всем игрокам сервера.
     * Если отправитель не в сети, ничего не делает.
     * @param sender отправитель.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    default void broadcastMessage(ProxiedPlayer sender, String message) {
        broadcastMessage(sender.getName(), message);
    }

    /**
     * Отправить сообщения (messages) от имени sender всем игрокам сервера.
     * Если отправитель не в сети, ничего не делает.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void broadcastMessages(ProxiedPlayer sender, Collection<String> messages) {
        broadcastMessages(sender.getName(), messages);
    }

    /**
     * Отправить сообщение (message) от имени sender игроку receiver.
     * Если один из игроков не в онлайне, ничего не делает.
     * @param receiver получатель.
     * @param sender отправитель.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    default void sendMessage(ProxiedPlayer receiver, ProxiedPlayer sender, String message) {
        sendMessage(receiver.getName(), sender.getName(), message);
    }

    /**
     * Отправить сообщения (messages) от имени sender игроку receiver.
     * Если один из игроков не в онлайне, ничего не делает.
     * @param receiver получатель.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void sendMessages(ProxiedPlayer receiver, ProxiedPlayer sender, Collection<String> messages) {
        sendMessages(receiver.getName(), sender.getName(), messages);
    }

    /**
     * Отправить сообщение (message) от имени sender игрокам receivers.
     * Если отправителя нет в сети или ни один из получателей не онлайн, ничего не делает.
     * @param receivers получатели.
     * @param sender отправитель.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    default void sendMessage(Collection<ProxiedPlayer> receivers, ProxiedPlayer sender, String message) {
        sendMessage(receivers.stream().map(ProxiedPlayer::getName).collect(Collectors.toList()), sender.getName(), message);
    }

    /**
     * Отправить сообщения (messages) от имени sender игрокам receivers.
     * Если отправителя нет в сети или ни один из получателей не онлайн, ничего не делает.
     * @param receivers получатели.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void sendMessages(Collection<ProxiedPlayer> receivers, ProxiedPlayer sender, Collection<String> messages) {
        sendMessages(receivers.stream().map(ProxiedPlayer::getName).collect(Collectors.toList()), sender.getName(), messages);
    }

}
