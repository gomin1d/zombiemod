package yt.dms.api.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Отметка означает, что этот функционал еще не реализован и использовать его не нужно.
 * Created by RINES on 03.06.2018.
 */
@Retention(RetentionPolicy.SOURCE)
public @interface NotImplementedYet {
}
