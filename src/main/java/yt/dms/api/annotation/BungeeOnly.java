package yt.dms.api.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Отметка означает, что метод/класс имеет смысл использовать только на стороне BungeeCord'a.
 * Created by RINES on 03.06.2018.
 */
@Retention(RetentionPolicy.SOURCE)
public @interface BungeeOnly {
}
