package yt.dms.api.party;

import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.player.DmsPlayer;

import java.util.UUID;

/**
 * В отличие от обычного {@link DmsParty}, использует {@link DmsAutoParty}, которые
 * динамически обновляются в случае включения данного регистри, что позволяет работать
 * с ними более-менее на постоянной основе (например, в onDamage и onMove хендлерах).
 * Created by k.shandurenko on 08.12.2018
 */
@SpigotOnly
public interface DmsPartyRegistry {

    /**
     * Делает возможным использование данного регистри.
     * Более формально, начинает обрабатывать группы игроков в автоматическом режиме,
     * динамически подгружая данные с координатора.
     * @throws IllegalStateException если этот метод уже был вызван.
     */
    void enable() throws IllegalStateException;

    /**
     * Получить группу по указанному uuid.
     * @param uuid уникальный идентификатор группы.
     * @return группу, если она существует, или null.
     * @throws IllegalStateException если метод {@link DmsPartyRegistry#enable()} не был вызван.
     */
    DmsAutoParty getParty(UUID uuid) throws IllegalStateException;

    /**
     * Получить группу игрока с указанным ником.
     * @param playerName ник игрока.
     * @return группу, если игрок в ней состоит, или null.
     * @throws IllegalArgumentException если игрока с указанным ником нет в онлайне на данном spigot-сервере.
     * @throws IllegalStateException если метод {@link DmsPartyRegistry#enable()} не был вызван.
     */
    DmsAutoParty getParty(String playerName) throws IllegalArgumentException, IllegalStateException;

    /**
     * Получить группу указанного игрока.
     * @param player игрок.
     * @return группу, если игрок в ней состоит, или null.
     * @throws IllegalArgumentException если указанного игрока нет в онлайне на данном spigot-сервере.
     * @throws IllegalStateException если метод {@link DmsPartyRegistry#enable()} не был вызван.
     */
    default DmsAutoParty getParty(DmsPlayer player) throws IllegalArgumentException, IllegalStateException {
        return getParty(player.getName());
    }

}
