package yt.dms.api.party;

import yt.dms.api.DMS;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.player.DmsSpigotPlayer;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by k.shandurenko on 08.12.2018
 */
@SpigotOnly
public interface DmsAutoParty {

    /**
     * Получить уникальный идентификатор этой группы.
     * @return уникальный идентификатор этой группы.
     */
    UUID getUUID();

    /**
     * Проверить, существует ли группа по данному инстансу.
     * @return true/false.
     */
    boolean exists();

    /**
     * Получить ник лидера этой группы.
     * @return ник лидера этой группы в нижнем регистре.
     * @throws IllegalStateException если группа не существует.
     */
    String getLeaderName() throws IllegalStateException;

    /**
     * Получить лидера этой группы.
     * @return лидера этой группы.
     * @throws IllegalStateException если группа не существует.
     */
    default DmsSpigotPlayer getLeader() throws IllegalStateException {
        String leaderName = getLeaderName();
        return leaderName == null ? null : DMS.getPlayerRegistry().get(leaderName).spigot();
    }

    /**
     * Получить ники всех членов этой группы.
     * @return ники всех членов этой группы в нижнем регистре.
     * @throws IllegalStateException если группа не существует.
     */
    List<String> getMemberNames() throws IllegalStateException;

    /**
     * Получить всех членов этой группы.
     * @return всех членов этой группы.
     * @throws IllegalStateException если группа не существует.
     */
    default List<DmsSpigotPlayer> getMembers() throws IllegalStateException {
        return getMemberNames().stream().map(name -> DMS.getPlayerRegistry().get(name).spigot()).collect(Collectors.toList());
    }

}
