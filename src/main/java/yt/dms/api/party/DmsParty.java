package yt.dms.api.party;

import com.google.common.util.concurrent.ListenableFuture;
import yt.dms.api.DMS;
import yt.dms.api.player.DmsPlayer;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @deprecated т.к. теперь можно использовать {@link DmsPartyRegistry} и {@link DmsAutoParty}
 * Created by k.shandurenko on 04.08.2018
 */
@Deprecated
public interface DmsParty {

    /**
     * Проверить, что группа существует.
     * Этот метод отправит синхронный запрос к координатору.
     * @return true/false.
     */
    boolean existsSync();

    /**
     * Проверить, что группа существует.
     * Этот метод отправит асинхронный запрос к координатору.
     * @return фьючер на true/false.
     */
    ListenableFuture<Boolean> existsAsync();

    /**
     * Получить ник лидера группы.
     * Этот метод отправит синхронный запрос к координатору.
     * @return ник лидера группы или null, если группа не существует.
     */
    String getLeaderNameSync();

    /**
     * Получить ник лидера группы.
     * Этот метод отправит асинхронный запрос к координатору.
     * @return фьючер на ник лидера группы или null, если группа не существует.
     */
    ListenableFuture<String> getLeaderNameAsync();

    /**
     * Получить лидера группы.
     * Этот метод отправит синхронный запрос к координатору.
     * @return лидер группы или null, если группа не существует.
     */
    default DmsPlayer getLeaderSync() {
        String leaderName = getLeaderNameSync();
        return leaderName == null ? null : DMS.getPlayerRegistry().get(leaderName);
    }

    /**
     * Получить лидера группы.
     * Этот метод отправит асинхронный запрос к координатору.
     * @return фьючер на лидера группы или null, если группа не существует.
     */
    ListenableFuture<DmsPlayer> getLeaderAsync();

    /**
     * Получить список ников всех членов группы.
     * Этот метод отправит синхронный запрос к координатору.
     * @return список ников всех членов группы или пустой список, если группа не существует.
     */
    List<String> getMemberNamesSync();

    /**
     * Получить список ников всех членов группы.
     * Этот метод отправит асинхронный запрос к координатору.
     * @return фьючер на список ников всех членов группы или пустой список, если группа не существует.
     */
    ListenableFuture<List<String>> getMemberNamesAsync();

    /**
     * Получить список всех членов группы.
     * Этот метод отправит синхронный запрос к координатору.
     * @return список всех членов группы или пустой список, если группа не существует.
     */
    default List<DmsPlayer> getMembersSync() {
        return getMemberNamesSync().stream().map(DMS.getPlayerRegistry()::get).collect(Collectors.toList());
    }

    /**
     * Получить список всех членов группы.
     * Этот метод отправит асинхронный запрос к координатору.
     * @return фьючер на список всех членов группы или пустой список, если группа не существует.
     */
    ListenableFuture<List<DmsPlayer>> getMembersAsync();

    /**
     * Получить список всех членов группы, которые находятся на этом spigot-сервере.
     * Этот метод отправит синхронный запрос к координатору.
     * @return список всех членов группы, которые находятся на этом spigot-сервере,
     * или пустой список, если группа не существует.
     */
    List<DmsPlayer> getThisServerMembersSync();

    /**
     * Получить список всех членов группы, которые находятся на этом spigot-сервере.
     * Этот метод отправит асинхронный запрос к координатору.
     * @return фьючер на список всех членов группы, которые находятся на этом spigot-сервере,
     * или пустой список, если группа не существует.
     */
    ListenableFuture<List<DmsPlayer>> getThisServerMembersAsync();

}
