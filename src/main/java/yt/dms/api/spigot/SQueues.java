package yt.dms.api.spigot;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.minigame.shard.DmsMinigameShardBasis;
import yt.dms.api.player.DmsPlayer;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by RINES on 24.06.2018.
 */
@SpigotOnly
public interface SQueues {

    /**
     * Начать ожидание игроков из указанных очередей.
     * Как только наберется minPlayers, пойдет отсчет одной минуты, чтобы могло набраться не более maxPlayers игроков.
     * Если набралось maxPlayers игроков, игра сразу же стартует.
     * Как только игра стартует, никаких игроков ожидаться больше не будет, и выполняется gameStarter.
     * @param minPlayers минимальное количество игроков для начала игры.
     * @param maxPlayers максимальное количество игроков для начала игры.
     * @param queueNames имена очередей, из которых нужно ожидать игроков.
     * @param gameStarter раннабл запуска игры.
     */
    void awaitPlayers(int minPlayers, int maxPlayers, Collection<String> queueNames, Runnable gameStarter);

    /**
     * Начать ожидание игроков из указанной очереди.
     * Как только наберется minPlayers, пойдет отсчет одной минуты, чтобы могло набраться не более maxPlayers игроков.
     * Если набралось maxPlayers игроков, игра сразу же стартует.
     * Как только игра стартует, никаких игроков ожидаться больше не будет, и выполняется gameStarter.
     * @param minPlayers минимальное количество игроков для начала игры.
     * @param maxPlayers максимальное количество игроков для начала игры.
     * @param queueName имя очереди, из которой нужно ожидать игроков.
     * @param gameStarter раннабл запуска игры.
     */
    default void awaitPlayers(int minPlayers, int maxPlayers, String queueName, Runnable gameStarter) {
        awaitPlayers(minPlayers, maxPlayers, Collections.singleton(queueName), gameStarter);
    }

    void awaitPlayers(DmsMinigameShardBasis shard, int minPlayers, int maxPlayers, Collection<String> queueNames, Runnable gameStarter);

    default void awaitPlayers(DmsMinigameShardBasis shard, int minPlayers, int maxPlayers, String queueName, Runnable gameStarter) {
        awaitPlayers(shard, minPlayers, maxPlayers, Collections.singleton(queueName), gameStarter);
    }

    /**
     * Если по какой-то причине нужно перестать ожидать игроков.
     */
    void stopAwaitingPlayers();

    void stopAwaitingPlayers(DmsMinigameShardBasis shard);

    /**
     * Зарегистрировать игрока в указанную очередь.
     * Если игрок состоит в группе, то если он лидер, вся группа будет добавлена в очередь,
     * иначе игроку высветится ошибка, что только лидер группы может войти в очередь.
     * Если игрок состоял в регистрации на какую-либо очередь, поведение неопределено.
     * @param player игрок.
     * @param queueName имя очереди.
     */
    default void registerPlayerForQueue(DmsPlayer player, String queueName) {
        registerPlayerForQueue(player.getName(), queueName);
    }

    /**
     * Зарегистрировать игрока в указанную очередь.
     * Если игрок состоит в группе, то если он лидер, вся группа будет добавлена в очередь,
     * иначе игроку высветится ошибка, что только лидер группы может войти в очередь.
     * Если игрок состоял в регистрации на какую-либо очередь, поведение неопределено.
     * @param player игрок.
     * @param queueName имя очереди.
     */
    default void registerPlayerForQueue(Player player, String queueName) {
        registerPlayerForQueue(player.getName(), queueName);
    }

    /**
     * Зарегистрировать игрока в указанную очередь.
     * Если игрок состоит в группе, то если он лидер, вся группа будет добавлена в очередь,
     * иначе игроку высветится ошибка, что только лидер группы может войти в очередь.
     * Если игрок состоял в регистрации на какую-либо очередь, поведение неопределено.
     * @param playerName ник игрока.
     * @param queueName имя очереди.
     */
    void registerPlayerForQueue(String playerName, String queueName);

    /**
     * Вывести игрока из регистрации на любые очереди.
     * Если игрок состоял в группе, то если он лидер, вся группа будет выведена из очереди,
     * иначе игроку высветится ошибка, что только лидер группы может выйти из очереди.
     * @param player игрок.
     */
    default void unregisterPlayerFromAnyQueues(DmsPlayer player) {
        unregisterPlayerFromAnyQueues(player.getName());
    }

    /**
     * Вывести игрока из регистрации на любые очереди.
     * Если игрок состоял в группе, то если он лидер, вся группа будет выведена из очереди,
     * иначе игроку высветится ошибка, что только лидер группы может выйти из очереди.
     * @param player игрок.
     */
    default void unregisterPlayerFromAnyQueues(Player player) {
        unregisterPlayerFromAnyQueues(player.getName());
    }

    /**
     * Вывести игрока из регистрации на любые очереди.
     * Если игрок состоял в группе, то если он лидер, вся группа будет выведена из очереди,
     * иначе игроку высветится ошибка, что только лидер группы может выйти из очереди.
     * @param playerName ник игрока.
     */
    void unregisterPlayerFromAnyQueues(String playerName);

    /**
     * Добавить игрока в указанную очередь, если он в ней не состоит.
     * В противном случае убрать его из нее.
     * @param player игрок.
     * @param queueName имя очереди.
     */
    default void togglePlayerQueues(DmsPlayer player, String queueName) {
        togglePlayerQueues(player.getName(), queueName);
    }

    /**
     * Добавить игрока в указанную очередь, если он в ней не состоит.
     * В противном случае убрать его из нее.
     * @param player игрок.
     * @param queueName имя очереди.
     */
    default void togglePlayerQueues(Player player, String queueName) {
        togglePlayerQueues(player.getName(), queueName);
    }

    /**
     * Добавить игрока в указанную очередь, если он в ней не состоит.
     * В противном случае убрать его из нее.
     * @param playerName ник игрока.
     * @param queueName имя очереди.
     */
    void togglePlayerQueues(String playerName, String queueName);

}
