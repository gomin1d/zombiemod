package yt.dms.api.spigot;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;
import java.util.Set;

/**
 * Created by k.shandurenko on 19.10.2018
 */
@SpigotOnly
public interface SPermissions {

    /**
     * Получить все права игрока (работает как PEX).
     * @param player игрок.
     * @return права игрока.
     */
    Set<String> getPermissions(Player player);

    /**
     * Добавить право игроку (работает как PEX).
     * @param player игрок.
     * @param permission право.
     */
    void addPermission(Player player, String permission);

    /**
     * Добавить права игроку (работает как PEX).
     * @param player игрок.
     * @param permissions права.
     */
    void addPermissions(Player player, Collection<String> permissions);

    /**
     * Забрать право у игрока (работает как PEX).
     * @param player игрок.
     * @param permission право.
     */
    void removePermission(Player player, String permission);

    /**
     * Забрать права у игрока (работает как PEX).
     * @param player игрок.
     * @param permissions права.
     */
    void removePermissions(Player player, Collection<String> permissions);

}
