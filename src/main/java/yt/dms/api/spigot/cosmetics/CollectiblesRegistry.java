package yt.dms.api.spigot.cosmetics;

import org.bukkit.Color;
import org.bukkit.Material;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.player.PermissionGroup;

import java.util.List;

/**
 * Created by RINES on 29.06.2018.
 */
@SpigotOnly
public interface CollectiblesRegistry {

    /**
     * Зарегистрировать предмет брони.
     * @param name название предмета.
     * @param type тип предмета.
     * @param color цвет предмета.
     * @param enchanted зачарован или нет.
     * @param rarity редкость.
     * @param obtainReason способ получения.
     * @param permission минимальная группа для использования.
     * @throws IllegalArgumentException если указанный тип предмета не является типом брони.
     */
    void registerArmor(String name, Material type, Color color, boolean enchanted,
                           CollectiblesItemRarity rarity,
                           CollectiblesItemObtainReason obtainReason,
                           PermissionGroup permission) throws IllegalArgumentException;

    /**
     * Зарегистрировать полиморф.
     * @param name название предмета.
     * @param polymorphName имя енума полиморфа (MorphType).
     * @param rarity редкость.
     * @param obtainReason способ получения.
     * @param permission минимальная группа для использования.
     */
    void registerPolymorph(String name, String polymorphName,
                               CollectiblesItemRarity rarity,
                               CollectiblesItemObtainReason obtainReason,
                               PermissionGroup permission);

    /**
     * Зарегистрировать маунта (ездовое животное).
     * @param name название предмета.
     * @param mountName имя енума маунта (MountType).
     * @param rarity редкость.
     * @param obtainReason способ получения.
     * @param permission минимальная группа для использования.
     */
    void registerMount(String name, String mountName,
                           CollectiblesItemRarity rarity,
                           CollectiblesItemObtainReason obtainReason,
                           PermissionGroup permission);

    /**
     * Зарегистрировать эффект.
     * @param name название предмета.
     * @param effectName имя енума эффекта (ParticleEffectType).
     * @param rarity редкость.
     * @param obtainReason способ получения.
     * @param permission минимальная группа для использования.
     */
    void registerEffect(String name, String effectName,
                        CollectiblesItemRarity rarity,
                        CollectiblesItemObtainReason obtainReason,
                        PermissionGroup permission);

    /**
     * Зарегистрировать шапку.
     * @param name название предмета.
     * @param hatName имя енума шапки (Hat).
     * @param iconMaterial отображаемый материал иконки.
     * @param rarity редкость.
     * @param obtainReason способ получения.
     * @param permission минимальная группа для использования.
     */
    void registerHat(String name, String hatName,
                        Material iconMaterial,
                        CollectiblesItemRarity rarity,
                        CollectiblesItemObtainReason obtainReason,
                        PermissionGroup permission);

    /**
     * Зарегистрировать питомца.
     * @param name название предмета.
     * @param petName имя енума питомца (Pet).
     * @param rarity редкость.
     * @param obtainReason способ получения.
     * @param permission минимальная группа для использования.
     */
    void registerPet(String name, String petName,
                        CollectiblesItemRarity rarity,
                        CollectiblesItemObtainReason obtainReason,
                        PermissionGroup permission);

    /**
     * Зарегистрировать гаджет.
     * @param name название предмета.
     * @param description описание предмета.
     * @param gadgetName имя гаджета (GadgetType).
     * @param rarity редкость.
     * @param obtainReason способ получения.
     * @param permission минимальная группа для использования.
     */
    void registerGadget(String name, List<String> description, String gadgetName,
                        CollectiblesItemRarity rarity,
                        CollectiblesItemObtainReason obtainReason,
                        PermissionGroup permission);

    /**
     * @see CollectiblesRegistry#registerGadget(String, List, String, CollectiblesItemRarity, CollectiblesItemObtainReason, PermissionGroup)
     */
    default void registerGadget(String name, String gadgetName,
                                CollectiblesItemRarity rarity,
                                CollectiblesItemObtainReason obtainReason,
                                PermissionGroup permission) {
        registerGadget(name, null, gadgetName, rarity, obtainReason, permission);
    }

    /**
     * Зарегистрировать звание.
     * @param name название предмета.
     * @param description описание предмета или null.
     * @param iconMaterial материал иконки.
     * @param iconMaterialData метадата материала иконки.
     * @param rarity редкость.
     * @param obtainReason способ получения.
     * @param permission минимальная группа для использования.
     */
    void registerTitle(String name, List<String> description, Material iconMaterial,
                       int iconMaterialData, CollectiblesItemRarity rarity,
                       CollectiblesItemObtainReason obtainReason,
                       PermissionGroup permission);

    /**
     * @see CollectiblesRegistry#registerTitle(String, List, Material, int, CollectiblesItemRarity, CollectiblesItemObtainReason, PermissionGroup)
     */
    default void registerTitle(String name, List<String> description, Material iconMaterial,
                               CollectiblesItemRarity rarity,
                               CollectiblesItemObtainReason obtainReason,
                               PermissionGroup permission) {
        registerTitle(name, description, iconMaterial, 0, rarity, obtainReason, permission);
    }

    /**
     * @see CollectiblesRegistry#registerTitle(String, List, Material, int, CollectiblesItemRarity, CollectiblesItemObtainReason, PermissionGroup)
     */
    default void registerTitle(String name, Material iconMaterial, int iconMaterialData,
                               CollectiblesItemRarity rarity,
                               CollectiblesItemObtainReason obtainReason,
                               PermissionGroup permission) {
        registerTitle(name, null, iconMaterial, iconMaterialData, rarity, obtainReason, permission);
    }

    /**
     * @see CollectiblesRegistry#registerTitle(String, List, Material, int, CollectiblesItemRarity, CollectiblesItemObtainReason, PermissionGroup)
     */
    default void registerTitle(String name, Material iconMaterial,
                               CollectiblesItemRarity rarity,
                               CollectiblesItemObtainReason obtainReason,
                               PermissionGroup permission) {
        registerTitle(name, null, iconMaterial, 0, rarity, obtainReason, permission);
    }

    /**
     * Зарегистрировать компаньона.
     * @param name название предмета.
     * @param clientCompanionId клиентский идентификатор этого коллекционного предмета.
     */
    void registerCompanion(String name, int clientCompanionId);

    /**
     * Зарегистрировать клиентскую шапку.
     * @param name название предмета.
     * @param clientHatId клиентский идентификатор этого коллекционного предмета.
     */
    void registerClientHat(String name, int clientHatId);

    /**
     * Зарегистрировать клиентскую броню (или крылья).
     * @param name название предмета.
     * @param clientArmorId клиентский идентификатор этого коллекционного предмета.
     */
    void registerClientArmor(String name, int clientArmorId);

    /**
     * Зарегистрировать свечение.
     * @param name название предмета.
     * @param description описание предмета или null.
     * @param clientGlowingId клиентский идентификатор этого коллекционного предмета.
     * @param obtainable можно ли как-либо обычному игроку получить это свечение или нет.
     */
    void registerGlowing(String name, List<String> description, int clientGlowingId, boolean obtainable);

    /**
     * @see CollectiblesRegistry#registerGlowing(String, List, int, boolean)
     */
    default void registerGlowing(String name, int clientGlowingId, boolean obtainable) {
        registerGlowing(name, null, clientGlowingId, obtainable);
    }

}
