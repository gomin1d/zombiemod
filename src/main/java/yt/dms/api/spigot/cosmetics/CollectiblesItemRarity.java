package yt.dms.api.spigot.cosmetics;

/**
 * Created by RINES on 29.06.2018.
 */
public enum CollectiblesItemRarity {
    COMMON,
    UNCOMMON,
    RARE,
    EPIC,
    LEGENDARY,
    IMMORTAL
}
