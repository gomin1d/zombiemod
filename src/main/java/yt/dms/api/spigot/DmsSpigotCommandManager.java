package yt.dms.api.spigot;

import org.bukkit.command.CommandSender;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.command.DmsCommandExecutor;
import yt.dms.api.command.DmsCommandManager;

/**
 * Created by k.shandurenko on 11.09.2018
 */
@SpigotOnly
public interface DmsSpigotCommandManager extends DmsCommandManager {

    DmsCommandExecutor wrapExecutor(CommandSender sender);

}
