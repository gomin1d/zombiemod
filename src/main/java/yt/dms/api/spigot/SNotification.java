package yt.dms.api.spigot;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.player.DmsPlayer;

import java.util.Collection;
import java.util.List;

/**
 * Created by RINES on 05.06.2018.
 */
@SpigotOnly
public interface SNotification {

    /**
     * Отправить нотификацию всем игрокам сервера.
     * К заголовку и телу применяется замена цветовых кодов с &.
     * @param header заголовок объявления.
     * @param body тело объявления.
     */
    void broadcast(String header, List<String> body);

    /**
     * Отправить нотификацию всем указанным игрокам.
     * К заголовку и телу применяется замена цветовых кодов с &.
     * @param players список игроков, которым отправить объявление.
     * @param header заголовок объявления.
     * @param body тело объявления.
     */
    default void sendToDmsPlayers(Collection<DmsPlayer> players, String header, List<String> body) {
        players.forEach(player -> send(player.getName(), header, body));
    }

    /**
     * Отправить нотификацию всем указанным игрокам.
     * К заголовку и телу применяется замена цветовых кодов с &.
     * @param players список игроков, которым отправить объявление.
     * @param header заголовок объявления.
     * @param body тело объявления.
     */
    default void sendToSpigotPlayers(Collection<Player> players, String header, List<String> body) {
        players.forEach(player -> send(player.getName(), header, body));
    }

    /**
     * Отправить нотификацию указанному игроку.
     * К заголовку и телу применяется замена цветовых кодов с &.
     * @param player игрок, которому отправить объявление.
     * @param header заголовок объявления.
     * @param body тело объявления.
     */
    default void send(DmsPlayer player, String header, List<String> body) {
        send(player.getName(), header, body);
    }

    /**
     * Отправить нотификацию указанному игроку.
     * К заголовку и телу применяется замена цветовых кодов с &.
     * @param player игрок, которому отправить объявление.
     * @param header заголовок объявления.
     * @param body тело объявления.
     */
    default void send(Player player, String header, List<String> body) {
        send(player.getName(), header, body);
    }

    /**
     * Отправить нотификацию указанному игроку.
     * К заголовку и телу применяется замена цветовых кодов с &.
     * @param player игрок, которому отправить объявление.
     * @param header заголовок объявления.
     * @param body тело объявления.
     */
    void send(String player, String header, List<String> body);

}
