package yt.dms.api.spigot;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.player.DmsPlayer;

/**
 * Created by RINES on 05.06.2018.
 */
@SpigotOnly
public interface SBungee {

    /**
     * Отправить игрока на другой сервер.
     * @param player имя игрока.
     * @param serverName имя сервера.
     */
    default void sendPlayer(DmsPlayer player, String serverName) {
        sendPlayer(player.getName(), serverName);
    }

    /**
     * Отправить игрока на другой сервер.
     * @param player имя игрока.
     * @param serverName имя сервера.
     */
    default void sendPlayer(Player player, String serverName) {
        sendPlayer(player.getName(), serverName);
    }

    /**
     * Отправить игрока на другой сервер.
     * @param player имя игрока.
     * @param serverName имя сервера.
     */
    void sendPlayer(String player, String serverName);

    /**
     * Отправить игроков на другой сервер.
     * @param serverName имя сервера.
     */
    void sendAllPlayers(String serverName);

}
