package yt.dms.api.spigot.phantom.block;

import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;

/**
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomChunk {

    /**
     * Получить x-координату этого фантомного чанка.
     * @return x-координата этого фантомного чанка.
     */
    int getX();

    /**
     * Получить z-координату этого фантомного чанка.
     * @return z-координата этого фантомного чанка.
     */
    int getZ();

    /**
     * Получить фантомные блоки этого фантомного чанка.
     * @return фантомные блоки этого фантомного чанка.
     */
    Collection<PhantomBlock> getBlocks();

}
