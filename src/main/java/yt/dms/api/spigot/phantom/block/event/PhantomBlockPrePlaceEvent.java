package yt.dms.api.spigot.phantom.block.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.spigot.phantom.block.world.PhantomWorld;
import yt.dms.api.spigot.phantom.block.world.PhantomWorldBlockData;

/**
 * Created by k.shandurenko on 15.10.2018
 */
@Getter
@RequiredArgsConstructor
public class PhantomBlockPrePlaceEvent extends DmsEvent {

    private final PhantomWorld world;
    private final Player player;
    private final PhantomWorldBlockData block;
    private final PlacementData placementData;

    @Getter
    @RequiredArgsConstructor
    public static class PlacementData {

        private final int faceID;
        private final ItemStack heldItem;
        private final float cursorPositionX;
        private final float cursorPositionY;
        private final float cursorPositionZ;

        public BlockFace getFace() {
            switch(this.faceID) {
                case 0: return BlockFace.DOWN;
                case 1: return BlockFace.UP;
                case 2: return BlockFace.NORTH;
                case 3: return BlockFace.SOUTH;
                case 4: return BlockFace.WEST;
                default: return BlockFace.EAST;
            }
        }

    }

}
