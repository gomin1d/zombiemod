package yt.dms.api.spigot.phantom.block.world;

import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.spigot.phantom.block.PhantomChunk;

/**
 * Фантомный мир, который полностью проецирует на себя существующий мир.
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomMirrorWorld extends PhantomWorld {

    /**
     * Получить фантомный чанк по указанным координатам.
     * @param x x-координата фантомного чанка.
     * @param z z-координата фантомного чанка.
     * @return фантомный чанк.
     */
    PhantomChunk getPhantomChunk(int x, int z);

}
