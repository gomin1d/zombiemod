package yt.dms.api.spigot.phantom.block.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.spigot.phantom.block.world.PhantomWorld;
import yt.dms.api.spigot.phantom.block.world.PhantomWorldBlockData;

/**
 * Created by k.shandurenko on 14.10.2018
 */
@Getter
@AllArgsConstructor
public class PhantomBlockDigEvent extends DmsEvent {

    private final PhantomWorld world;
    private final Player player;
    private final PhantomWorldBlockData block;
    private final PhantomBlockPreDigEvent.DigType digType;

    /**
     * Был ли блок доломан?
     */
    private final boolean actualDestroy;

    /**
     * Нужно ли восстановить блок для игрока, его сломавшего (для других с блоком ничего не произойдет).
     */
    @Setter
    private boolean reset;

}
