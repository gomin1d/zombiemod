package yt.dms.api.spigot.phantom.block;

import lombok.Getter;
import org.bukkit.Material;
import yt.dms.api.annotation.SpigotOnly;

import java.util.Objects;

/**
 * Created by k.shandurenko on 14.10.2018
 */
@Getter
@SpigotOnly
public class PhantomBlock {

    private final int materialID;
    private final short materialData;
    private final int x, y, z;

    public PhantomBlock(int materialID, int x, int y, int z) {
        this(materialID, (short) 0, x, y, z);
    }

    public PhantomBlock(int materialID, short materialData, int x, int y, int z) {
        this.materialID = materialID;
        this.materialData = materialData;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @SuppressWarnings("deprecation")
    public Material getType() {
        return Material.getMaterial(this.materialID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhantomBlock that = (PhantomBlock) o;
        return materialID == that.materialID &&
                materialData == that.materialData &&
                x == that.x &&
                y == that.y &&
                z == that.z;
    }

    @Override
    public int hashCode() {
        return Objects.hash(materialID, materialData, x, y, z);
    }
}
