package yt.dms.api.spigot.phantom.block.world;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Фантомный мир, который проецирует на себя существующий мир, но имеет общие для всех игроков
 * фантомные блоки.
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomSharedWorld extends PhantomWorld, PhantomWorldVariation {

}
