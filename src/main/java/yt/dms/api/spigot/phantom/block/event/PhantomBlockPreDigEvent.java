package yt.dms.api.spigot.phantom.block.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.entity.Player;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.spigot.phantom.block.world.PhantomWorld;
import yt.dms.api.spigot.phantom.block.world.PhantomWorldBlockData;

/**
 * Created by k.shandurenko on 14.10.2018
 */
@Getter
@RequiredArgsConstructor
public class PhantomBlockPreDigEvent extends DmsEvent {

    private final PhantomWorld world;
    private final Player player;
    private final PhantomWorldBlockData block;
    private final DigType digType;

    /**
     * Был ли блок доломан?
     */
    @Setter
    private boolean actualDestroy;

    /**
     * Нужно ли восстановить блок для игрока, его сломавшего (для других с блоком ничего не произойдет).
     */
    @Setter
    private boolean reset;

    public enum DigType {
        START_DESTROY_BLOCK,
        ABORT_DESTROY_BLOCK,
        STOP_DESTROY_BLOCK,
        DROP_ALL_ITEMS,
        DROP_ITEM,
        RELEASE_USE_ITEM;

        public static DigType[] VALUES = values();

    }

}
