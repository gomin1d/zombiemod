package yt.dms.api.spigot.phantom.block;

import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;

/**
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomBlockConverter {

    /**
     * Преобразовать фантомный блок в фантомный чанк, состоящий из одного этого самого блока.
     * @param block фантомный блок.
     * @return фантомный чанк.
     */
    PhantomChunk convertBlockToChunk(PhantomBlock block);

    /**
     * Преобразовать фантомные блоки в фантомные чанки, состоящие из оных.
     * @param blocks фантомные блоки.
     * @return фантомные чанки.
     */
    Collection<PhantomChunk> convertBlocksToChunks(Collection<PhantomBlock> blocks);

}
