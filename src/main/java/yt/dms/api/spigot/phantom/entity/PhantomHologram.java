package yt.dms.api.spigot.phantom.entity;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by RINES on 03.07.2018.
 */
@SpigotOnly
public interface PhantomHologram extends PhantomEntity {

    /**
     * Получить текст голограммы.
     * @return текст голограммы.
     */
    String getText();

    /**
     * Установить текст голограммы (все & будут заменены на цветовые коды).
     * @param text текст голограммы.
     */
    void setText(String text);

}
