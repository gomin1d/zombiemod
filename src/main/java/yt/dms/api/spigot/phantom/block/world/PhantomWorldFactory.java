package yt.dms.api.spigot.phantom.block.world;

import org.bukkit.World;
import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;

import java.util.function.Function;

/**
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomWorldFactory {

    /**
     * Создать фантомный мир, который является полным отражением существующего bukkit-мира.
     * @param bukkitWorldName название bukkit-мира для создаваемого фантомного мира.
     * @param mirroredWorld bukkit-мир, который будет проецироваться создаваемым фантомным миром.
     * @return реализацию PhantomMirrorWorld.
     */
    PhantomMirrorWorld createMirrorWorld(String bukkitWorldName, World mirroredWorld);

    /**
     * Создать фантомный мир, который проецирует на себя существующий bukkit-мир, но имеет общие
     * для всех игроков фантомные блоки.
     * @param bukkitWorldName название bukkit-мира для создаваемого фантомного мира.
     * @param mirroredWorld bukkit-мир, который будет проецироваться создаваемым фантомным миром.
     * @return реализацию PhantomSharedWorld.
     */
    PhantomSharedWorld createSharedWorld(String bukkitWorldName, World mirroredWorld);

    /**
     * Создать фантомный мир, который проецирует на себя существующий bukkit-мир и имеет различные
     * вариации самого себя с разными фантомными блоками в каждой из них.
     * @param bukkitWorldName название bukkit-мира для создаваемого фантомного мира.
     * @param mirroredWorld bukkit-мир, который будет проецироваться создаваемым фантомным миром.
     * @param variationChooser функция, определяющая название вариации создаваемого фантомного мира, в которой
     *                         должен находиться переданный в нее игрок.
     * @return реализацию PhantomVariationalWorld.
     */
    PhantomVariationalWorld createVariationalWorld(String bukkitWorldName, World mirroredWorld, Function<Player, String> variationChooser);

}
