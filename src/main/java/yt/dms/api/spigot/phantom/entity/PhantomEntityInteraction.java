package yt.dms.api.spigot.phantom.entity;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by RINES on 03.07.2018.
 */
@SpigotOnly
public interface PhantomEntityInteraction {

    void onLeftClick(Player player);

    void onRightClick(Player player);

}
