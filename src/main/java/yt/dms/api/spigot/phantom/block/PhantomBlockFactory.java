package yt.dms.api.spigot.phantom.block;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomBlockFactory {

    /**
     * Получить утилиту для конвертации фантомных блоков в фантомные чанки.
     * @return конвертер фантомных блоков в чанки.
     */
    PhantomBlockConverter getConverter();

    /**
     * Отправить игрокам фантомный чанк.
     * @param players игроки.
     * @param chunk чанк.
     */
    void sendChunk(Collection<Player> players, PhantomChunk chunk);

    /**
     * Отправить игроку фантомный чанк.
     * @param player игрок.
     * @param chunk чанк.
     */
    default void sendChunk(Player player, PhantomChunk chunk) {
        sendChunks(Collections.singleton(player), Collections.singleton(chunk));
    }

    /**
     * Отправить игрокам фантомные чанки.
     * @param players игроки.
     * @param chunks чанки.
     */
    void sendChunks(Collection<Player> players, Collection<PhantomChunk> chunks);

    /**
     * Отправить игроку фантомные чанки.
     * @param player игрок.
     * @param chunks фантомный чанк.
     */
    default void sendChunks(Player player, Collection<PhantomChunk> chunks) {
        sendChunks(Collections.singleton(player), chunks);
    }

    /**
     * Отправить игрокам фантомный блок.
     * @param players игроки.
     * @param block фантомный блок.
     */
    void sendBlock(Collection<Player> players, PhantomBlock block);

    /**
     * Отправить игроку фантомный блок.
     * @param player игрок.
     * @param block фантомный блок.
     */
    default void sendBlock(Player player, PhantomBlock block) {
        sendBlock(Collections.singleton(player), block);
    }

    /**
     * Отправить игрокам фантомные блоки.
     * @param players игроки.
     * @param blocks фантомные блоки.
     */
    void sendBlocks(Collection<Player> players, Collection<PhantomBlock> blocks);

    /**
     * Отправить игроку фантомные блоки.
     * @param player игрок.
     * @param blocks фантомные блоки.
     */
    default void sendBlocks(Player player, Collection<PhantomBlock> blocks) {
        sendBlocks(Collections.singleton(player), blocks);
    }

}
