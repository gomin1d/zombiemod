package yt.dms.api.spigot.phantom.block.world;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;

/**
 * Фантомный мир, который проецирует на себя существующий мир и имеет различные
 * вариации самого себя с разными фантомными блоками в каждой из них.
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomVariationalWorld extends PhantomWorld {

    /**
     * Получить или создать (если еще не существует) вариацию этого фантомного мира с указанным названием.
     * @param variationName название вариации.
     * @return вариацию этого фантомного мира с указанным названием.
     */
    PhantomWorldVariation getOrCreateVariation(String variationName);

    /**
     * Получить название вариации, на которой в данный момент находится игрок (или null).
     * @param player игрок.
     * @return название вариации или null.
     */
    String getPlayerVariationName(Player player);

    /**
     * Удалить вариацию этого фантомного мира с указанным названием, если она существует.
     * @param variationName название вариации.
     */
    void removeVariation(String variationName);

    /**
     * Проверить, существует ли вариация этого фантомного мира с указанным названием.
     * @param variationName название вариации.
     * @return true/false.
     */
    boolean doesVariationExist(String variationName);

    /**
     * Переместить игрока, находящегося в этом фантомном мире, в его новую вариацию.
     * Если вариации не существует, она будет создана.
     * @param player игрок.
     * @param variationName название новой к перемещению вариации.
     */
    void changePlayerVariation(Player player, String variationName);

}
