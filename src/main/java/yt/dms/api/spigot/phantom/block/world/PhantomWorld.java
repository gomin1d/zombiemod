package yt.dms.api.spigot.phantom.block.world;

import org.bukkit.World;
import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomWorld {

    /**
     * Получить bukkit-мир данного фантомного мира.
     * @return bukkit-мир данного фантомного мира.
     */
    World getBukkitWorld();

    /**
     * Получить оригинальный bukkit-мир, который проецируется данным фантомным миром.
     * @return оригинальный bukkit-мир, который проецируется данным фантомным миром.
     */
    World getOriginalWorld();

    /**
     * Получить информацию о фантомном блоке по указанным координатам.
     * @param x x-координата блока.
     * @param y y-координата блока.
     * @param z z-координата блока.
     * @return информация о фантомном блоке по указанным координатам.
     */
    PhantomWorldBlockData getBlockData(int x, int y, int z);

}
