package yt.dms.api.spigot.phantom.entity;

import yt.dms.api.annotation.SpigotOnly;

import java.util.UUID;

/**
 * Created by RINES on 03.07.2018.
 */
@SpigotOnly
public interface PhantomPlayer extends PhantomEquippableEntity {

    /**
     * Получить UUID этого фантомного игрока.
     * @return UUID этого фантомного игрока.
     */
    UUID getUUID();

    /**
     * Получить имя этого фантомного игрока.
     * @return имя этого фантомного игрока.
     */
    String getName();

    /**
     * Получить ник скина этого фантомного игрока.
     * @return ник скина этого фантомного игрока.
     */
    String getSkinName();

    /**
     * Получить ник плаща этого фантомного игрока.
     * @return ник плаща этого фантомного игрока.
     */
    String getCapeName();

    /**
     * Обновить профиль этого фантомного игрока.
     * @param name новый ник.
     * @param skinName новый ник скина. Регистр важен!
     * @param capeName новый ник плаща. Регистр важен!
     */
    void updateProfile(String name, String skinName, String capeName);

    /**
     * Обновить скин (и плащ) этого фантомного игрока.
     * @param skinName новый ник скина. Регистр важен!
     * @param capeName новый ник плаща. Регистр важен!
     */
    default void setSkin(String skinName, String capeName) {
        updateProfile(getName(), skinName, capeName);
    }

    /**
     * Обновить имя этого фантомного игрока.
     * @param name новый ник.
     */
    default void setName(String name) {
        updateProfile(name, name, name);
    }

}
