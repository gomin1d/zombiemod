package yt.dms.api.spigot.phantom.block.world;

import org.bukkit.block.Block;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.spigot.phantom.block.PhantomBlock;

/**
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomWorldBlockData {

    /**
     * Если на данной позиции в фантомном мире находится фантомный блок, а не просто проекция блока
     * из оригинального bukkit-мира, данный метод вернет этот самый фантомный блок.
     * @return фантомный блок на данной позиции или null.
     */
    PhantomBlock getPhantomBlock();

    /**
     * Если на данной позиции в фантомном мире нет фантомного блока, то вернет блок из оригинального
     * bukkit-мира, который проецируется данным фантомным миром. В частности, это означает, что
     * {@link Block#getWorld()} вернет не bukkit-мир данного фантомного мира, но bukkit-мир
     * оригинального проецируемого мира.
     * @return блок на данной позиции в оригинальном проецируемом bukkit-мире или null.
     */
    Block getMirrorBlock();

    /**
     * Удостовериться, что на данной позиции в фантомном мире нет фантомного блока, и блок проецируется
     * с оригинального bukkit-мира.
     * @return true/false.
     */
    default boolean isReal() {
        return getPhantomBlock() == null;
    }

}
