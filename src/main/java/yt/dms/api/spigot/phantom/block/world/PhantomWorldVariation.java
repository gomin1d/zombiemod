package yt.dms.api.spigot.phantom.block.world;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.spigot.phantom.block.PhantomBlock;
import yt.dms.api.spigot.phantom.block.PhantomChunk;

import java.util.Collection;

/**
 * Вариация фантомного мира. По сути, является одной из версий мира, в каждой из которых
 * могут содержаться, при чем разные, фантомные блоки (помимо лишь проецируемых блоков из
 * другого, оригинального мира).
 * Created by k.shandurenko on 14.10.2018
 */
@SpigotOnly
public interface PhantomWorldVariation {

    /**
     * Получить информацию о фантомном блоке по указанным координатам.
     * @param x x-координата блока.
     * @param y y-координата блока.
     * @param z z-координата блока.
     * @return информация о фантомном блоке по указанным координатам.
     */
    PhantomWorldBlockData getBlockData(int x, int y, int z);

    /**
     * Добавить фантомный блок.
     * @param phantomBlock фантомный блок.
     */
    void addPhantomBlock(PhantomBlock phantomBlock);

    /**
     * Добавить фантомные блоки.
     * @param phantomBlocks фантомные блоки.
     */
    void addPhantomBlocks(Collection<PhantomBlock> phantomBlocks);

    /**
     * Удалить фантомный блок по указанным координатам. После выполнения этого действия
     * на данной позиции будет отображен блок из проецируемого мира.
     * @param x x-координата фантомного блока.
     * @param y y-координата фантомного блока.
     * @param z z-координата фантомного блока.
     */
    void removePhantomBlock(int x, int y, int z);

    /**
     * Удалить фантомный блок по координатам переданного фантомного блока. После выполнения этого
     * действия на данной позиции будет отображен блок из проецируемого мира.
     * @param phantomBlock фантомный блок.
     */
    default void removePhantomBlock(PhantomBlock phantomBlock) {
        removePhantomBlock(phantomBlock.getX(), phantomBlock.getY(), phantomBlock.getZ());
    }

    /**
     * Удалить фантомные блоки по координатам переданных фантомных блоков. После выполнения этого
     * действия на данной позиции будет отображен блок из проецируемого мира.
     * @param phantomBlocks фантомные блоки.
     */
    void removePhantomBlocks(Collection<PhantomBlock> phantomBlocks);

    /**
     * Получить фантомный чанк по указанным координатам.
     * @param x x-координата фантомного чанка.
     * @param z z-координата фантомного чанка.
     * @return фантомный чанк.
     */
    PhantomChunk getPhantomChunk(int x, int z);

    /**
     * Получить игроков, для которых в данный момент отображается данная вариация.
     * @return игроки.
     */
    Collection<Player> getPlayers();

}
