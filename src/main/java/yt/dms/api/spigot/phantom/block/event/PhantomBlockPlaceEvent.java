package yt.dms.api.spigot.phantom.block.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.spigot.phantom.block.world.PhantomWorld;
import yt.dms.api.spigot.phantom.block.world.PhantomWorldBlockData;

/**
 * Created by k.shandurenko on 15.10.2018
 */
@Getter
@RequiredArgsConstructor
public class PhantomBlockPlaceEvent extends DmsEvent {

    private final PhantomWorld world;
    private final Player player;
    private final PhantomWorldBlockData block;
    private final PhantomBlockPrePlaceEvent.PlacementData placementData;

}
