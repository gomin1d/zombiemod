package yt.dms.api.spigot.phantom.entity.specific;

import yt.dms.api.annotation.SpigotOnly;

@SpigotOnly
public final class GoldChangerExchangeRate {

    private int silver;
    private int gold;

    public GoldChangerExchangeRate(int silver, int gold) {
        this.silver = silver;
        this.gold = gold;
    }

    public int getSilver() {
        return this.silver;
    }

    public int getGold() {
        return this.gold;
    }

}
