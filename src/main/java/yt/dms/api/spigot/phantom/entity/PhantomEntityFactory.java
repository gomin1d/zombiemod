package yt.dms.api.spigot.phantom.entity;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.spigot.phantom.entity.specific.GoldChangerExchangeRate;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;

/**
 * Created by RINES on 03.07.2018.
 */
@SpigotOnly
public interface PhantomEntityFactory {

    /**
     * Получить заспавненную фантомную сущность по ее идентификатору.
     * @param id идентификатор сущности.
     * @return сущность или null.
     */
    PhantomEntity getById(int id);

    /**
     * Создать фантомного игрока.
     * Это действие не спавнит сущность, а перед спавном нужно установить ей локацию!
     * @param uuid UUID игрока.
     * @param name ник игрока.
     * @param skinName ник игрока, скин которого должен быть у данного фантомного игрока.
     * @param capeName ник игрока, плащ которого должен быть у данного фантомного игрока.
     * @return фантомного игрока.
     */
    PhantomPlayer createPlayer(UUID uuid, String name, String skinName, String capeName);

    /**
     * @see PhantomEntityFactory#createPlayer(UUID, String, String, String)
     */
    default PhantomPlayer createPlayer(UUID uuid, String name, String skinAndCapeName) {
        return createPlayer(uuid, name, skinAndCapeName, skinAndCapeName);
    }

    /**
     * @see PhantomEntityFactory#createPlayer(String, String, String)
     */
    default PhantomPlayer createPlayer(UUID uuid, String name) {
        return createPlayer(uuid, name, name);
    }

    /**
     * @see PhantomEntityFactory#createPlayer(String, String, String)
     */
    default PhantomPlayer createPlayer(String name, String skinName, String capeName) {
        return createPlayer(UUID.randomUUID(), name, skinName, capeName);
    }

    /**
     * @see PhantomEntityFactory#createPlayer(String, String, String)
     */
    default PhantomPlayer createPlayer(String name, String skinAndCapeName) {
        return createPlayer(name, skinAndCapeName, skinAndCapeName);
    }

    /**
     * @see PhantomEntityFactory#createPlayer(String, String, String)
     */
    default PhantomPlayer createPlayer(String name) {
        return createPlayer(name, name);
    }

    /**
     * Создать фантомную сущность.
     * Это действие не спавнит сущность, а перед спавном нужно установить ей локацию!
     * @param id идентификатор сущности.
     * @param type тип сущности.
     * @return фантомную сущность.
     * @throws IllegalArgumentException если type == PLAYER.
     */
    PhantomEntity createEntity(int id, EntityType type) throws IllegalArgumentException;

    /**
     * @see PhantomEntityFactory#createEntity(int, EntityType)
     */
    default PhantomEntity createEntity(EntityType type) {
        return createEntity(getRandomIdForPhantomEntity(), type);
    }

    /**
     * Создать фантомную сущность с возможностью экипирования.
     * Это действие не спавнит сущность, а перед спавном нужно установить ей локацию!
     * @param id идентификатор сущности.
     * @param type тип сущности.
     * @return фантомную сущность.
     * @throws IllegalArgumentException если указанный тип не может быть экипированным или == PLAYER.
     */
    PhantomEquippableEntity createEquippableEntity(int id, EntityType type) throws IllegalArgumentException;

    /**
     * @see PhantomEntityFactory#createEquippableEntity(int, EntityType)
     */
    default PhantomEquippableEntity createEquippableEntity(EntityType type) throws IllegalArgumentException {
        return createEquippableEntity(getRandomIdForPhantomEntity(), type);
    }

    /**
     * Создать фантомную голограмму.
     * @param id идентификатор сущности.
     * @param text текст (имя) голограммы с заменой & на цветовые коды.
     * @return фантомную голограмму.
     */
    PhantomHologram createHologram(int id, String text);

    /**
     * @see PhantomEntityFactory#createHologram(int, String)
     */
    default PhantomHologram createHologram(String text) {
        return createHologram(getRandomIdForPhantomEntity(), text);
    }

    default int getRandomIdForPhantomEntity() {
        return ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE - 400) + 300;
    }

    /**
     * Создать и заспавнить фантомного игрока-обменщика золота.
     * @deprecated Смотри {@link PhantomEntityFactory#spawnAutoGoldChanger(Location, double)}.
     * @param position позиция для спавна.
     * @param fromSilverToGold курсы обмена серебра на золото.
     * @param fromGoldToSilver курсы обмена золота на серебро.
     * @throws IllegalArgumentException если курсы обмены null или их количество не лежит в границах от 3 до 5 включительно.
     */
    @Deprecated
    void spawnGoldChanger(Location position, List<GoldChangerExchangeRate> fromSilverToGold, List<GoldChangerExchangeRate> fromGoldToSilver) throws IllegalArgumentException;

    /**
     * Создать и заспавнить фантомного игрока-обменщика золота.
     * В отличие от {@link PhantomEntityFactory#spawnGoldChanger(Location, List, List)},
     * здесь не задаются явные цены: у обменщика может будет приобрести 250, 500, 1000, 2000, 5000, 10000 и 20000 серебра,
     * а также обменять ровно такое же количество серебра на золото.
     * Курсы обмены будут автоматически подобраны исходя из экономики сервера.
     * @param position позиция для спавна.
     * @param minigameHardness модификатор сложности получения серебра на данном режиме. Для SkyWars и BedWars = 1.0.
     */
    void spawnAutoGoldChanger(Location position, double minigameHardness);

    /**
     * Создать и заспавнить фантомную голограмму для входа в очередь (и выхода из нее).
     * @param position позиция для спавна.
     * @param visibleName отображаемый текст (не нужно здесь указывать "Войти/Выйти из очереди") с заменой & на цветовые коды.
     * @param queueName название очереди (техническое).
     */
    void spawnQueueHologram(Location position, String visibleName, String queueName);

    /**
     * Заспавнить топ игроков данного минирежима по указанной статистике.
     * @param spawnLocations позиции для спавна игроков из топа. Порядок топа определяется порядком итерирования.
     * @param statName название статистики, по которой нужно строить топ.
     * @param statNamer функция для получения имени статистики в правильном падеже по количеству
     *                  (например, 1 - убийство, 2 - убийства, 5 - убийств).
     */
    void spawnTop(List<Location> spawnLocations, String statName, Function<Integer, String> statNamer);

}
