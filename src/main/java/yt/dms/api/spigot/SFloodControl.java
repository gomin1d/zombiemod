package yt.dms.api.spigot;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.both.FloodControl;

import java.util.concurrent.TimeUnit;

/**
 * Created by RINES on 28.06.2018.
 */
@SpigotOnly
public interface SFloodControl extends FloodControl {

    /**
     * @see FloodControl#checkAndAdd(String, String, int, int, long, TimeUnit)
     */
    default boolean checkAndAdd(Player player, String floodKey, int addition, int limit, long cooldown, TimeUnit cooldownTimeUnit) {
        return checkAndAdd(player.getName(), floodKey, addition, limit, cooldown, cooldownTimeUnit);
    }

    /**
     * @see FloodControl#checkAndAdd(String, String, int, long, TimeUnit)
     */
    default boolean checkAndAdd(Player player, String floodKey, int limit, long cooldown, TimeUnit cooldownTimeUnit) {
        return checkAndAdd(player.getName(), floodKey, limit, cooldown, cooldownTimeUnit);
    }

    /**
     * @see FloodControl#checkAndAdd(String, String, long, TimeUnit)
     */
    default boolean checkAndAdd(Player player, String floodKey, long cooldown, TimeUnit cooldownTimeUnit) {
        return checkAndAdd(player.getName(), floodKey, cooldown, cooldownTimeUnit);
    }

    /**
     * @see FloodControl#checkAndAddGetTimeLeft(String, String, int, int, long, TimeUnit)
     */
    default long checkAndAddGetTimeLeft(Player player, String floodKey, int addition, int limit, long cooldown, TimeUnit cooldownTimeUnit) {
        return checkAndAddGetTimeLeft(player.getName(), floodKey, addition, limit, cooldown, cooldownTimeUnit);
    }

    /**
     * @see FloodControl#checkAndAddGetTimeLeft(String, String, int, long, TimeUnit)
     */
    default long checkAndAddGetTimeLeft(Player player, String floodKey, int limit, long cooldown, TimeUnit cooldownTimeUnit) {
        return checkAndAddGetTimeLeft(player.getName(), floodKey, limit, cooldown, cooldownTimeUnit);
    }

    /**
     * @see FloodControl#checkAndAddGetTimeLeft(String, String, long, TimeUnit)
     */
    default long checkAndAddGetTimeLeft(Player player, String floodKey, long cooldown, TimeUnit cooldownTimeUnit) {
        return checkAndAddGetTimeLeft(player.getName(), floodKey, cooldown, cooldownTimeUnit);
    }

}
