package yt.dms.api.spigot;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Утилита для работы с инфляцией и дефляцией золота.
 * Created by k.shandurenko on 23/12/2018
 */
@SpigotOnly
public interface SEconomics {

    /**
     * Получить общее количество золота в экономике проекта.
     * @return общее количество золота в экономике проекта.
     */
    int getAllGold();

    /**
     * Получить цену, равную указанному проценту от общего количества золота в экономике проекта.
     * @param percentage процент от общего количества золота в экономике проекта.
     * @return цену.
     */
    default int getPrice(double percentage) {
        return (int) Math.ceil(getAllGold() * percentage);
    }

    /**
     * Получить цену в золоте для комплекта серебра.
     * @param silver количество серебра в комплекте.
     * @param gamemodeHardness модификатор сложности добычи серебра на этом минирежиме. Для SkyWars и BedWars = 1.0
     * @return цену серебра в золоте.
     */
    default int getSilverPriceInGold(int silver, double gamemodeHardness) {
        return getPrice(silver * getGlobalConstant() * gamemodeHardness);
    }

    /**
     * Получить цену в серебре для комплекта золота.
     * Цена возвращается с учетом нашей комиссии перевода серебра в золото в размере 50%.
     * @param gold количество золота в комплекте.
     * @param gamemodeHardness модификатор сложности добычи серебра на этом минирежиме. Для SkyWars и BedWars = 1.0
     * @return цену золота в серебре.
     */
    default int getGoldPriceInSilver(int gold, double gamemodeHardness) {
        return (int) Math.ceil(1.5D * gold / getPrice(getGlobalConstant() * gamemodeHardness));
    }

    /**
     * Получить количество золота, которое можно получить при трате указанного количества серебра.
     * Цена возвращается с учетом нашей комиссии перевода серебра в золото в размере 50%.
     * @param silver количество серебра, которое является ценой комплекта.
     * @param gamemodeHardness модификатор сложности добычи серебра на этом минирежиме. Для SkyWars и BedWars = 1.0
     * @return количество золота, которое можно получить за указанное количество серебра.
     */
    default int getSilverPriceInGoldRevert(int silver, double gamemodeHardness) {
        return (int) (getSilverPriceInGold(silver, gamemodeHardness) / 1.5D);
    }

    /**
     * Магическая константа для всех расчетов в нашей золотой экономике <3
     * @return магическая константа.
     */
    default double getGlobalConstant() {
        return .000000000804829D;
    }

}
