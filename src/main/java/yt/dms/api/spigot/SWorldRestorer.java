package yt.dms.api.spigot;

import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by RINES on 23.06.2018.
 */
@SpigotOnly
public interface SWorldRestorer {

    /**
     * Подготовить все загруженные в память сервера миры к их возможному восстановлению.
     * Один и тот же мир не нужно подготавливать несколько раз.
     */
    void prepareAll();

    /**
     * Подготовить миры, чье название находится в указанной коллекции, к их возможному восстановлению.
     * Если название мира имеется в коллекции, но сам мир в данный момент не загружен, он не
     * будет подготовлен к восстановлению.
     * Один и тот же мир не нужно подготавливать несколько раз.
     * @param worldNames названия миров.
     */
    void prepare(Collection<String> worldNames);

    /**
     * Подготовить мир с указанным названием к его возможному восстановлению.
     * Если он не загружен, ничего не произойдет.
     * Один и тот же мир не нужно подготавливать несколько раз.
     * @param worldName название мира.
     */
    default void prepare(String worldName) {
        prepare(Collections.singleton(worldName));
    }

    /**
     * Восстановить все миры, ожидающие восстановления.
     */
    void restore();

}
