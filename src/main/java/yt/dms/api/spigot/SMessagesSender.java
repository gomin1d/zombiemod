package yt.dms.api.spigot;

import org.bukkit.entity.Player;
import yt.dms.api.both.MessagesSender;
import yt.dms.api.minigame.shard.DmsMinigameShardBasis;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Этот интерфейс реализуется вместо обычного MessagesSender на spigot-серверах.
 * Created by RINES on 25.06.2018.
 */
public interface SMessagesSender extends MessagesSender {

    /**
     * Отправить сообщение (message) от имени sender всем игрокам сервера.
     * Если отправитель не в сети, ничего не делает.
     * @param sender отправитель.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    default void broadcastMessage(Player sender, String message) {
        broadcastMessage(sender.getName(), message);
    }

    /**
     * Отправить сообщение (message) от имени sender всем игрокам игрового шарда (shard).
     * Если отправитель не в сети, ничего не делает.
     * @param shard игровой шард.
     * @param sender сообщение (цветовые коды в нем автоматически не заменятся).
     * @param message отправитель.
     */
    default void broadcastMessage(DmsMinigameShardBasis shard, Player sender, String message) {
        sendMessage(shard.getPlayers(), sender, message);
    }

    /**
     * Отправить сообщения (messages) от имени sender всем игрокам сервера.
     * Если отправитель не в сети, ничего не делает.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void broadcastMessages(Player sender, Collection<String> messages) {
        broadcastMessages(sender.getName(), messages);
    }

    /**
     * Отправить сообщения (messages) от имени sender всем игрокам игрового шарда (shard).
     * Если отправитель не в сети, ничего не делает.
     * @param shard игровой шард.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void broadcastMessages(DmsMinigameShardBasis shard, Player sender, Collection<String> messages) {
        sendMessages(shard.getPlayers(), sender, messages);
    }

    /**
     * Отправить сообщение (message) от имени sender игроку receiver.
     * Если один из игроков не в онлайне, ничего не делает.
     * @param receiver получатель.
     * @param sender отправитель.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    default void sendMessage(Player receiver, Player sender, String message) {
        sendMessage(receiver.getName(), sender.getName(), message);
    }

    /**
     * Отправить сообщения (messages) от имени sender игроку receiver.
     * Если один из игроков не в онлайне, ничего не делает.
     * @param receiver получатель.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void sendMessages(Player receiver, Player sender, Collection<String> messages) {
        sendMessages(receiver.getName(), sender.getName(), messages);
    }

    /**
     * Отправить сообщение (message) от имени sender игрокам receivers.
     * Если отправителя нет в сети или ни один из получателей не онлайн, ничего не делает.
     * @param receivers получатели.
     * @param sender отправитель.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    default void sendMessage(Collection<Player> receivers, Player sender, String message) {
        sendMessage(receivers.stream().map(Player::getName).collect(Collectors.toList()), sender.getName(), message);
    }

    /**
     * Отправить сообщения (messages) от имени sender игрокам receivers.
     * Если отправителя нет в сети или ни один из получателей не онлайн, ничего не делает.
     * @param receivers получатели.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void sendMessages(Collection<Player> receivers, Player sender, Collection<String> messages) {
        sendMessages(receivers.stream().map(Player::getName).collect(Collectors.toList()), sender.getName(), messages);
    }

}
