package yt.dms.api.spigot;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.player.DmsPlayer;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by k.shandurenko on 21.09.2018
 */
@SpigotOnly
public interface SBar {

    float DEFAULT_WIDTH = .33F;
    int WAITING_R = 250, WAITING_G = 250, WAITING_B = 100;

    /**
     * Отправить бар игрокам.
     * @param playerNames ники игроков.
     * @param bar бар.
     */
    default void sendToPlayers(Collection<String> playerNames, Bar bar) {
        Collection<Player> spigotPlayers = new HashSet<>();
        for (String playerName : playerNames) {
            Player spigotPlayer = Bukkit.getPlayerExact(playerName);
            if (spigotPlayer != null) {
                spigotPlayers.add(spigotPlayer);
            }
        }
        if (!spigotPlayers.isEmpty()) {
            sendToSpigotPlayers(spigotPlayers, bar);
        }
    }

    /**
     * Отправить бар игрокам.
     * @param players игроки.
     * @param bar бар.
     */
    default void sendToDmsPlayers(Collection<DmsPlayer> players, Bar bar) {
        Collection<Player> spigotPlayers = new HashSet<>();
        for (DmsPlayer player : players) {
            Player spigotPlayer = player.spigot().getSpigotPlayer();
            if (spigotPlayer != null) {
                spigotPlayers.add(spigotPlayer);
            }
        }
        if (!spigotPlayers.isEmpty()) {
            sendToSpigotPlayers(spigotPlayers, bar);
        }
    }

    /**
     * Отправить бар игрокам.
     * @param players игроки.
     * @param bar бар.
     */
    void sendToSpigotPlayers(Collection<Player> players, Bar bar);

    /**
     * Отправить бар игроку.
     * @param playerName ник игрока.
     * @param bar бар.
     */
    default void sendToPlayer(String playerName, Bar bar) {
        Player spigotPlayer = Bukkit.getPlayerExact(playerName);
        if (spigotPlayer != null) {
            sendToSpigotPlayer(spigotPlayer, bar);
        }
    }

    /**
     * Отправить бар игроку.
     * @param player игрок.
     * @param bar бар.
     */
    default void sendToDmsPlayer(DmsPlayer player, Bar bar) {
        Player spigotPlayer = player.spigot().getSpigotPlayer();
        if (spigotPlayer != null) {
            sendToSpigotPlayer(spigotPlayer, bar);
        }
    }

    /**
     * Отправить бар игроку.
     * @param player игрок.
     * @param bar бар.
     */
    void sendToSpigotPlayer(Player player, Bar bar);

    /**
     * Отправить бар всем игрокам сервера.
     * @param bar бар.
     */
    void broadcast(Bar bar);

    /**
     * Убрать бар для игроков.
     * @param playerNames ники игроков.
     */
    default void clearPlayers(Collection<String> playerNames) {
        Collection<Player> spigotPlayers = new HashSet<>();
        for (String playerName : playerNames) {
            Player spigotPlayer = Bukkit.getPlayerExact(playerName);
            if (spigotPlayer != null) {
                spigotPlayers.add(spigotPlayer);
            }
        }
        if (!spigotPlayers.isEmpty()) {
            clearSpigotPlayers(spigotPlayers);
        }
    }

    /**
     * Убрать бар для игроков.
     * @param players игроки.
     */
    default void clearDmsPlayers(Collection<DmsPlayer> players) {
        Collection<Player> spigotPlayers = new HashSet<>();
        for (DmsPlayer player : players) {
            Player spigotPlayer = player.spigot().getSpigotPlayer();
            if (spigotPlayer != null) {
                spigotPlayers.add(spigotPlayer);
            }
        }
        if (!spigotPlayers.isEmpty()) {
            clearSpigotPlayers(spigotPlayers);
        }
    }

    /**
     * Убрать бар для игроков.
     * @param players игроки.
     */
    void clearSpigotPlayers(Collection<Player> players);

    /**
     * Убрать бар для игрока.
     * @param playerName ник игрока.
     */
    default void clearPlayer(String playerName) {
        Player spigotPlayer = Bukkit.getPlayerExact(playerName);
        if (spigotPlayer != null) {
            clearSpigotPlayer(spigotPlayer);
        }
    }

    /**
     * Убрать бар для игрока.
     * @param player игрок.
     */
    default void clearDmsPlayer(DmsPlayer player) {
        Player spigotPlayer = player.spigot().getSpigotPlayer();
        if (spigotPlayer != null) {
            clearSpigotPlayer(spigotPlayer);
        }
    }

    /**
     * Убрать бар для игрока.
     * @param player игрок.
     */
    void clearSpigotPlayer(Player player);

    /**
     * Создать объект Bar с цветом, который мы обычно используем в фазе ожидания игроков.
     * Этот метод стоит использовать в момент начала фазы ожидания.
     * Этот метод ничего никому не показывает и лишь создает объект Bar!
     * @param text текст бара: все амперсанды (&) в нем будут заменены на цветовые коды.
     * @param lifetimeSeconds количество секунд, сколько этот бар должен прожить (обычно, 60 для фазы ожидания).
     * @return объект Bar.
     */
    default Bar createWaitingBar(String text, int lifetimeSeconds) {
        return createWaitingBar(text, 0, lifetimeSeconds);
    }

    /**
     * Создать объект Bar с цветом, который мы обычно используем в фазе ожидания игроков.
     * Этот метод стоит использовать, когда, например, заходит новый игрок в течение фазы ожидания.
     * Этот метод ничего никому не показывает и лишь создает объект Bar!
     * @param text текст бара: все амперсанды (&) в нем будут заменены на цветовые коды.
     * @param launchedSecondsAgo количество секунд с момента начала существования бара.
     * @param lifetimeSeconds количество секунд, сколько этот бар должен прожить.
     * @return объект Bar.
     */
    default Bar createWaitingBar(String text, int launchedSecondsAgo, int lifetimeSeconds) {
        return createBar(text, WAITING_R, WAITING_G, WAITING_B, launchedSecondsAgo, lifetimeSeconds);
    }

    /**
     * Создать объект Bar, который будет автоматически равномерно терять свою заполненность за отведенное время,
     * в конце концов уничтожаясь.
     * Этот метод ничего никому не показывает и лишь создает объект Bar!
     * @param text текст бара: все амперсанды (&) в нем будут заменены на цветовые коды.
     * @param red величина красного цвета в RGB (0-255).
     * @param green величина зеленого цвета в RGB (0-255).
     * @param blue величина синего цвета в RGB (0-255).
     * @param lifetimeSeconds количество секунд, сколько этот бар должен прожить.
     * @return объект Bar.
     */
    default Bar createBar(String text, int red, int green, int blue, int lifetimeSeconds) {
        return createBar(text, red, green, blue, DEFAULT_WIDTH, 0, lifetimeSeconds);
    }

    /**
     * Создать вечный объект Bar (у которого не будет какого-либо отсчета и самоуничтожения)
     * заданной ширины (и полностью заполненный).
     * Этот метод ничего никому не показывает и лишь создает объект Bar!
     * @param text текст бара: все амперсанды (&) в нем будут заменены на цветовые коды.
     * @param red величина красного цвета в RGB (0-255).
     * @param green величина зеленого цвета в RGB (0-255).
     * @param blue величина синего цвета в RGB (0-255).
     * @param width ширина в процентах от ширины экрана (0F - 1F).
     * @return объект Bar.
     */
    default Bar createBar(String text, int red, int green, int blue, float width) {
        return createBar(text, red, green, blue, width, 0, 0);
    }

    /**
     * Создать объект Bar, который был запущен некоторое время назад.
     * Этот метод ничего никому не показывает и лишь создает объект Bar!
     * @param text текст бара: все амперсанды (&) в нем будут заменены на цветовые коды.
     * @param red величина красного цвета в RGB (0-255).
     * @param green величина зеленого цвета в RGB (0-255).
     * @param blue величина синего цвета в RGB (0-255).
     * @param launchedSecondsAgo количество секунд с момента начала существования бара.
     * @param lifetimeSeconds количество секунд, сколько этот бар должен прожить.
     * @return объект Bar.
     */
    default Bar createBar(String text, int red, int green, int blue, int launchedSecondsAgo, int lifetimeSeconds) {
        return createBar(text, red, green, blue, DEFAULT_WIDTH, launchedSecondsAgo, lifetimeSeconds);
    }

    /**
     * Создать вечный объект Bar (у которого не будет какого-либо отсчета и самоуничтожения),
     * который заполнен на определенный процент.
     * Этот метод ничего никому не показывает и лишь создает объект Bar!
     * @param text текст бара: все амперсанды (&) в нем будут заменены на цветовые коды.
     * @param red величина красного цвета в RGB (0-255).
     * @param green величина зеленого цвета в RGB (0-255).
     * @param blue величина синего цвета в RGB (0-255).
     * @param width ширина в процентах от ширины экрана (0F - 1F).
     * @param progress процент заполненности бара (0F - 1F).
     * @return объект Bar.
     */
    default Bar createBar(String text, int red, int green, int blue, float width, float progress) {
        return createBar(text, red, green, blue, width, (int) (10000 * progress), 10000);
    }

    /**
     * Создать объект Bar с заданными параметрами.
     * Этот метод ничего никому не показывает и лишь создает объект Bar!
     * @param text текст бара: все амперсанды (&) в нем будут заменены на цветовые коды.
     * @param red величина красного цвета в RGB (0-255).
     * @param green величина зеленого цвета в RGB (0-255).
     * @param blue величина синего цвета в RGB (0-255).
     * @param width ширина в процентах от ширины экрана (0F - 1F).
     * @param launchedSecondsAgo количество секунд с момента начала существования бара.
     * @param lifetimeSeconds количество секунд, сколько этот бар должен прожить.
     * @return объект Bar.
     */
    default Bar createBar(String text, int red, int green, int blue, float width, int launchedSecondsAgo, int lifetimeSeconds) {
        return new Bar(text, red, green, blue, width, launchedSecondsAgo, lifetimeSeconds);
    }

    @Getter
    class Bar {

        private final String text;
        private final int red;
        private final int green;
        private final int blue;
        private final float width;
        private final int launchedSecondsAgo;
        private final int lifetimeSeconds;

        private Bar(String text, int red, int green, int blue, float width, int launchedSecondsAgo, int lifetimeSeconds) {
            this.text = text;
            this.red = red;
            this.green = green;
            this.blue = blue;
            this.width = width;
            this.launchedSecondsAgo = launchedSecondsAgo;
            this.lifetimeSeconds = lifetimeSeconds;
        }
    }

}
