package yt.dms.api.spigot;

import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.spigot.table.Table;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by k.shandurenko on 27.07.2018
 */
@SpigotOnly
public interface STable {

    /**
     * Создать новый объект Table с указанным id.
     * @param id идентификатор таблицы.
     * @return реализацию Table.
     */
    Table constructNew(int id);

    /**
     * Создать новый объект Table со случайно сгенерированным идентификатором таблицы.
     * @return реализацию Table.
     */
    default Table constructNew() {
        return constructNew(ThreadLocalRandom.current().nextInt(1000000000));
    }

}
