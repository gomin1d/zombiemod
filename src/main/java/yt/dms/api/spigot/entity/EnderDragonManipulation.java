package yt.dms.api.spigot.entity;

import org.bukkit.Location;
import org.bukkit.entity.EnderDragon;
import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 13.10.2018
 */
@SpigotOnly
public interface EnderDragonManipulation extends EntityManipulation<EnderDragon> {

    /**
     * Установить позицию, которая должна стать целью для дракона.
     * @param location позиция.
     */
    void setTargetLocation(Location location);

    /**
     * Вернуть дракона в свободное плавание.
     */
    void unsetTargetLocation();

}
