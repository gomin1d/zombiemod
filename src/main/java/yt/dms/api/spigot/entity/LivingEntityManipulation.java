package yt.dms.api.spigot.entity;

import org.bukkit.entity.LivingEntity;
import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 13.10.2018
 */
@SpigotOnly
public interface LivingEntityManipulation<T extends LivingEntity> extends EntityManipulation<T> {

    /**
     * Установить значение аттрибута, если он присущ этой сущности.
     * @param attribute аттрибут.
     * @param value значение.
     * @return true, если аттрибут был проставлен; false в противном случае.
     */
    boolean setAttribute(Attribute attribute, double value);

    /**
     * Получить значение аттрибута этой сущности.
     * @param attribute аттрибут.
     * @return значение аттрибута.
     */
    double getAttribute(Attribute attribute);

    /**
     * Установить максимальную дальность стрельбы для данной сущности, если стрельба ей присуща.
     * @param range расстояние в блоках.
     * @return true, если дальность была проставлена; false в противном случае.
     */
    boolean setArrowAttackRange(float range);

    /**
     * Установить скорость стрельбы для данной сущности, если стрельба ей присуща.
     * @param min минимальное время в тиках между залпами.
     * @param max максимальное время в тиках между залпами.
     * @return true/false.
     */
    boolean setArrowAttackSpeed(int min, int max);

    enum Attribute {
        FOLLOW_RANGE,
        MAX_HEALTH,
        KNOCKBACK_RESISTANCE,
        MOVEMENT_SPEED,
        ATTACK_DAMAGE
    }

}
