package yt.dms.api.spigot.entity;

import org.bukkit.entity.Entity;
import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 13.10.2018
 */
@SpigotOnly
public interface EntityManipulation<T extends Entity> {

    /**
     * Получить сущность.
     * @return сущность.
     */
    T getEntity();

    /**
     * Установить защиту от огня для сущности.
     * @param value true/false.
     */
    void setFireProof(boolean value);

    /**
     * Проверить, имеется ли у сущности защита от огня.
     * @return true/false.
     */
    boolean isFireProof();

}
