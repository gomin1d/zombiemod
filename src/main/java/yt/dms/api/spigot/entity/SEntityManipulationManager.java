package yt.dms.api.spigot.entity;

import org.bukkit.entity.Creature;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.spigot.entity.logic.EntityLogicBuilder;
import yt.dms.api.spigot.entity.logic.EntityLogicHelper;

/**
 * Created by k.shandurenko on 13.10.2018
 */
@SpigotOnly
public interface SEntityManipulationManager {

    /**
     * Получить утилиту для управления драконов.
     * @param dragon дракон.
     * @return утилита для управления драконом.
     */
    EnderDragonManipulation getEnderDragonManipulation(EnderDragon dragon);

    /**
     * Получить утилиту для управления сущностью.
     * @param entity сущность.
     * @param <T> тип сущности.
     * @return утилита для управления сущностью.
     */
    <T extends Entity> EntityManipulation<T> getEntityManipulation(T entity);

    /**
     * Получить утилиту для управления живой сущностью.
     * @param livingEntity живая сущность.
     * @param <T> тип живой сущности.
     * @return утилита для управления живой сущностью.
     */
    <T extends LivingEntity> LivingEntityManipulation<T> getLivingEntityManipulation(T livingEntity);

    /**
     * Получить утилиту для управления сущностью типа Creature.
     * @param creature сущность.
     * @param <T> тип сущности.
     * @return утилита для управления сущностью типа Creature.
     */
    <T extends Creature> CreatureManipulation<T> getCreatureManipulation(T creature);

    /**
     * Получить билдер для логики/поведения сущности типа Creature.
     * @return билдер для логики/поведения сущности типа Creature.
     */
    EntityLogicBuilder logicBuilder();

    /**
     * Получить утилиту для упрощенной генерации алгоритмов поиска цели, используемых в билдере для логики/поведения
     * сущностей типа Creature.
     * @return утилита для упрощенной генерации алгоритмов поиска цели.
     */
    EntityLogicHelper getLogicHelper();

}
