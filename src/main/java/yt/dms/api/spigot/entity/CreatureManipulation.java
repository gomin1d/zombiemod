package yt.dms.api.spigot.entity;

import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 13.10.2018
 */
@SpigotOnly
public interface CreatureManipulation<T extends Creature> extends LivingEntityManipulation<T> {

    /**
     * Получить локацию, куда сейчас идет сущность.
     * @return локация.
     */
    Location getDestination();

    /**
     * Установить локацию, куда нужно идти сущности.
     * @param location локация.
     */
    void setDestination(Location location);

    /**
     * Установить сущность, к которой нужно идти данной сущности.
     * @param entity сущность-цель.
     */
    void setDestination(Entity entity);

    /**
     * Сделать эту сущность мирной.
     * @return true, если вышло; false иначе.
     */
    boolean makePeaceful();

    /**
     * Сделать эту сущность агрессивной.
     * @param aggressionRange радиус, в котором сущность будет искать себе жертву.
     * @return true, если вышло; false иначе.
     */
    boolean makeAggressive(float aggressionRange);

    /**
     * Получить значение любви (связано с размножением) сущности.
     * @return значение любви.
     */
    int getLove();

    /**
     * Установить значение любви (связано с размножением) сущности.
     * @param love значение любви.
     * @return true, если вышло; false иначе.
     */
    boolean setLove(int love);

    /**
     * Если сущность является выпавшим предметом, установить, может ли она исчезнуть через какое-то время.
     * @param value true/false.
     * @return true, если вышло; false иначе.
     */
    boolean setCantDespawnAsItem(boolean value);

    /**
     * Узнать, может ли этот выпавший предмет исчезнуть через какое-то время.
     * @return true, если может; false иначе или если сущность не является выпавшим предметом.
     */
    boolean isCantDespawnAsItem();

    /**
     * Если сущность является курицей, установить, может ли она нести яйца.
     * @param value true/false.
     */
    void setCantDropEggs(boolean value);

    /**
     * Узнать, может ли эта курица нести яйца.
     * @return true, если может; false иначе или если сущность не является курицей.
     */
    boolean isCantDropEggs();

}
