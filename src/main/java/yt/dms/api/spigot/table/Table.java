package yt.dms.api.spigot.table;

import org.bukkit.Location;
import yt.dms.api.annotation.SpigotOnly;

import java.util.Arrays;
import java.util.List;

/**
 * Created by k.shandurenko on 27.07.2018
 */
@SpigotOnly
public interface Table {

    /**
     * Получить id этой таблицы.
     * @return id этой таблицы.
     */
    int getID();

    /**
     * Получить все колонки этой таблицы.
     * @return все колонки этой таблицы.
     */
    List<TableColumn> getColumns();

    /**
     * Добавить колонку в эту таблицу.
     * @param column колонка.
     * @return эта таблица.
     */
    Table addColumn(TableColumn column);

    /**
     * Получить локацию этой таблицы.
     * @return локация этой таблицы.
     */
    Location getLocation();

    /**
     * Установить локацию этой таблицы.
     * @param location локация.
     * @return эта таблица.
     */
    Table setLocation(Location location);

    /**
     * Получить строки этой таблицы (в порядке от первой к последней).
     * @return строки этой таблицы.
     */
    List<List<String>> getRows();

    /**
     * Отчистить все строки этой таблицы.
     */
    void clearRows();

    /**
     * Добавить строку в эту таблицу.
     * Чтобы строка отобразилась в таблице, ее нужно обновить: {@link Table#update()}
     * @param row строка. Элементы переданного списка идут в порядке колонок таблицы.
     * @return эта таблица.
     * @throws IllegalArgumentException если row = null или количество элементов в переданном списке не соответствует
     *                                  количеству колонок в этой таблице.
     */
    Table addRow(List<String> row) throws IllegalArgumentException;

    /**
     * Добавить строку в эту таблицу.
     * Чтобы строка отобразилась в таблице, ее нужно обновить: {@link Table#update()}
     * @param row строка. Элементы переданного массива идут в порядке колонок таблицы.
     * @return эта таблица.
     * @throws IllegalArgumentException если row = null или количество элементов в переданном списке не соответствует
     *                                  количеству колонок в этой таблице.
     */
    default Table addRow(String[] row) throws IllegalArgumentException {
        return addRow(Arrays.asList(row));
    }

    /**
     * Получить угол поворота этой таблицы в градусах вокруг оси X.
     * Изначально равен 0.
     * @return угол поворота этой таблицы в градусах вокруг оси X.
     */
    float getRotationX();

    /**
     * Установить угол поворота этой таблицы в градусах вокруг оси X.
     * @param angle угол поворота в градусах.
     * @return эта таблица.
     */
    Table setRotationX(float angle);

    /**
     * Получить угол поворота этой таблицы в градусах вокруг оси Y.
     * При установке локации становится равным {@link Location#getYaw()}
     * @return угол поворота этой таблицы в градусах вокруг оси Y.
     */
    float getRotationY();

    /**
     * Установить угол поворота этой таблицы в градусах вокруг оси Y.
     * При установке локации угол все равно станет равным {@link Location#getYaw()}
     * @param angle угол поворота в градусах.
     * @return эта таблица.
     */
    Table setRotationY(float angle);

    /**
     * Получить угол поворота этой таблицы в градусах вокруг оси Z.
     * Изначально равен 0.
     * @return угол поворота этой таблицы в градусах вокруг оси Z.
     */
    float getRotationZ();

    /**
     * Установить угол поворота этой таблицы в градусах вокруг оси Z.
     * @param angle угол поворота в градусах.
     * @return эта таблица.
     */
    Table setRotationZ(float angle);

    /**
     * Отобразить таблицу в мире.
     * @throws IllegalStateException если у этой таблицы не задана локация или если она уже создана.
     */
    void create() throws IllegalStateException;

    /**
     * Обновить данные таблицы в мире.
     * @throws IllegalStateException если эта таблица не создана.
     */
    void update() throws IllegalStateException;

    /**
     * Удалить табилцу из мира.
     * @throws IllegalStateException если эта таблица не создана.
     */
    void delete() throws IllegalStateException;

}
