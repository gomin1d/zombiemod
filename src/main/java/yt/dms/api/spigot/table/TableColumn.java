package yt.dms.api.spigot.table;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 27.07.2018
 */
@SpigotOnly
public class TableColumn {

    private final String name;
    private final int width;

    /**
     * @param name текст заголовка колонки таблицы.
     * @param width ширина колонки таблицы в пикселях.
     *              Рекомендованная ширина для полного ника игрока (с префиксом и суффиксом) - 128 пикселей.
     *              Рекомендованная ширина для "Уровень" (в значениях которого лишь небольшие числа) - 32 пикселя.
     */
    public TableColumn(String name, int width) {
        this.name = name;
        this.width = width;
    }

    public String getName() {
        return this.name;
    }

    public int getWidth() {
        return this.width;
    }

}
