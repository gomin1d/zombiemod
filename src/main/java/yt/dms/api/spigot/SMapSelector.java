package yt.dms.api.spigot;

import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import yt.dms.api.annotation.SpigotOnly;

import java.util.function.Predicate;

/**
 * Из всевозможных карт на голосование игрокам выбирается 3 карты (или меньше, если суммарное
 * количество карт меньше 3). Игроки выбирают уже одну карту из этих трех.
 * Created by RINES on 30.06.2018.
 */
@SpigotOnly
public interface SMapSelector {

    /**
     * Начать голосование карт.
     * Всем входящим игрокам будет выдаваться предметик для голосования.
     * @param mapAcceptor функция, которая должна по конфигу мира говорить, подходит ли он.
     */
    void startVoting(Predicate<FileConfiguration> mapAcceptor);

    /**
     * @see SMapSelector#startVoting()
     */
    default void startVoting() {
        startVoting(fc -> true);
    }

    /**
     * Завершить голосование карт.
     * У всех игроков заберется предметик для голосования.
     * @return мир, который был выбран голосование, а после - скопирован в корневую
     * директорию сервера и загружен.
     */
    World endVoting();

    /**
     * Получить общее количество карт, которое может попасть на голосование.
     * @return общее количество карт, которое может попасть на голосование.
     */
    int getMapCount();

}
