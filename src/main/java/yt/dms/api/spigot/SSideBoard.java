package yt.dms.api.spigot;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.player.DmsPlayer;

import java.util.Collection;

/**
 * Created by k.shandurenko on 27.07.2018
 */
@SpigotOnly
public interface SSideBoard {

    /**
     * Отправить строку сайдборда.
     * @param player игрок, которому отправить.
     * @param priority приоритет: чем он выше, тем выше будет строка. Строка с максимальным приоритетом является
     *                 заголовком сайдборда. Если отправляется строка с приоритетом, уже установленным у одной из строк,
     *                 предыдущая строка с указанным приоритетом будет заменена данной строкой (таким образом,
     *                 в сайдборде не может быть несколько строк с одним приоритетом).
     * @param name название (левая часть) строки.
     * @param value значение (правая часть) строки.
     */
    void send(Player player, int priority, String name, String value);

    /**
     * Отправить строку сайдборда.
     * @param player игрок, которому отправить.
     * @param priority {@link SSideBoard#send(Player, int, String, String)}
     * @param name название (левая часть) строки.
     * @param value значение (правая часть) строки.
     */
    default void send(DmsPlayer player, int priority, String name, String value) {
        send(player.spigot().getSpigotPlayer(), priority, name, value);
    }

    /**
     * Отправить строку сайдборда.
     * @param players игроки, которым отправить.
     * @param priority {@link SSideBoard#send(Player, int, String, String)}
     * @param name название (левая часть) строки.
     * @param value значение (правая часть) строки.
     */
    void send(Collection<Player> players, int priority, String name, String value);

    /**
     * Отправить анимированную строку сайдборда. Имейте ввиду, что анимация работает только на заголовке сайдборда.
     * @param player игрок, которому отправить.
     * @param priority {@link SSideBoard#send(Player, int, String, String)}
     * @param name текст строки.
     * @param animationGamma цветовая гамма для анимации.
     */
    void send(Player player, int priority, String name, AnimationGamma animationGamma);

    /**
     * Отправить анимированную строку сайдборда. Имейте ввиду, что анимация работает только на заголовке сайдборда.
     * @param player игрок, которому отправить.
     * @param priority {@link SSideBoard#send(DmsPlayer, int, String, String)}
     * @param name текст строки.
     * @param animationGamma цветовая гамма для анимации.
     */
    default void send(DmsPlayer player, int priority, String name, AnimationGamma animationGamma) {
        send(player.spigot().getSpigotPlayer(), priority, name, animationGamma);
    }

    /**
     * Отправить анимированную строку сайдборда. Имейте ввиду, что анимация работает только на заголовке сайдборда.
     * @param players игроки, котороым отправить.
     * @param priority {@link SSideBoard#send(Player, int, String, String)}
     * @param name текст строки.
     * @param animationGamma цветовая гамма для анимации.
     */
    void send(Collection<Player> players, int priority, String name, AnimationGamma animationGamma);

    /**
     * Убрать из сайдборда игрока строку с заданным приоритетом.
     * @param player игрок, у которого нужно удалить строку.
     * @param priority приоритет строки.
     */
    void remove(Player player, int priority);

    /**
     * Убрать из сайдборда игрока строку с заданным приоритетом.
     * @param player игрок, у которого нужно удалить строку.
     * @param priority приоритет строки.
     */
    default void remove(DmsPlayer player, int priority) {
        remove(player.spigot().getSpigotPlayer(), priority);
    }

    /**
     * Убрать из сайдборда игрока строку с заданным приоритетом.
     * @param players игроки, у которых нужно удалить строку.
     * @param priority приоритет строки.
     */
    void remove(Collection<Player> players, int priority);

    enum AnimationGamma {
        GOLD, AQUA, PURPLE, RED, GREEN
    }

}
