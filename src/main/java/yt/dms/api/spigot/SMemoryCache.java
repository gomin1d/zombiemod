package yt.dms.api.spigot;

import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.mem.MemoryCache;

/**
 * Created by k.shandurenko on 29.07.2018
 */
@SpigotOnly
public interface SMemoryCache extends MemoryCache, SMemory {

}
