package yt.dms.api.spigot;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import lombok.Getter;
import org.bukkit.entity.Player;
import yt.dms.api.DMS;
import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Created by k.shandurenko on 02.10.2018
 */
@SpigotOnly
public interface SGuiButtons {

    /**
     * Спрятать кнопку от игрока.
     * @param player игрок.
     * @param button кнопка.
     */
    void hideButton(Player player, GuiButton button);

    /**
     * Спрятать кнопку от игроков.
     * @param players игроки.
     * @param button кнопка.
     */
    void hideButton(Collection<Player> players, GuiButton button);

    /**
     * Спрятать кнопки от игрока.
     * @param player игрок.
     * @param buttons кнопки.
     */
    default void hideButtons(Player player, Collection<GuiButton> buttons) {
        buttons.forEach(button -> hideButtons(player, buttons));
    }

    /**
     * Спрятать кнопки от игроков.
     * @param players игроки.
     * @param buttons кнопки.
     */
    default void hideButtons(Collection<Player> players, Collection<GuiButton> buttons) {
        buttons.forEach(button -> hideButton(players, button));
    }

    /**
     * Показать кнопку игроку.
     * @param player игрок.
     * @param button кнопка.
     */
    void showButton(Player player, GuiButton button);

    /**
     * Показать кнопку игрокам.
     * @param players игроки.
     * @param button кнопка.
     */
    void showButton(Collection<Player> players, GuiButton button);

    /**
     * Показать кнопки игроку.
     * @param player игрок.
     * @param buttons кнопки.
     */
    default void showButtons(Player player, Collection<GuiButton> buttons) {
        buttons.forEach(button -> showButton(player, button));
    }

    /**
     * Показать кнопки игрокам.
     * @param players игроки.
     * @param buttons кнопки.
     */
    default void showButtons(Collection<Player> players, Collection<GuiButton> buttons) {
        buttons.forEach(button -> showButton(players, button));
    }

    /**
     * Создать кнопку для работы с очередями с автоматически подобранным расположением по переданному билдеру.
     * @param builder билдер для кнопки.
     * @return кнопка.
     */
    default GuiButton createQueueButton(GuiButtonBuilder builder) {
        return createQueueButtons(Sets.newHashSet(builder)).iterator().next();
    }

    /**
     * Создать кнопки для работы с очередями с автоматически подобранным расположением по переданным билдерам.
     * @param builders билдеры для кнопок.
     * @return кнопки.
     */
    default Collection<GuiButton> createQueueButtons(Collection<GuiButtonBuilder> builders) {
        Preconditions.checkArgument(builders.size() > 0 && builders.size() < 7, "Illegal builders amount given");
        Collection<GuiButton> result = new HashSet<>();
        Iterator<GuiButtonBuilder> iterator = builders.iterator();
        GuiButtonBuilder builder = iterator.next();
        switch (builders.size()) {
            case 1:
                result.add(builder.buildForQueue(.33F, .8F, .04F, .341F));
                break;
            case 2:
                result.add(builder.buildForQueue(.33F, .83F, .04F, .341F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.33F, .77F, .04F, .341F));
                break;
            case 3:
                result.add(builder.buildForQueue(.33F, .83F, .04F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.52F, .83F, .04F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.33F, .77F, .04F, .341F));
                break;
            case 4:
                result.add(builder.buildForQueue(.33F, .83F, .04F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.52F, .83F, .04F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.33F, .77F, .04F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.52F, .77F, .04F, .15F));
                break;
            case 5:
                result.add(builder.buildForQueue(.33F, .77F, .03F, .341F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.33F, .85F, .03F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.52F, .85F, .03F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.33F, .81F, .03F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.52F, .81F, .03F, .15F));
                break;
            case 6:
                result.add(builder.buildForQueue(.33F, .77F, .03F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.52F, .77F, .03F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.33F, .85F, .03F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.52F, .85F, .03F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.33F, .81F, .03F, .15F));
                builder = iterator.next();
                result.add(builder.buildForQueue(.52F, .81F, .03F, .15F));
                break;
        }
        return result;
    }

    /**
     * Получить новый билдер для кнопок.
     * @param text текст на кнопке (& будут заменены на цветовые коды).
     * @return билдер.
     */
    default GuiButtonBuilder builder(String text) {
        return new GuiButtonBuilder(text);
    }

    @Getter
    class GuiButtonBuilder {

        private final String text;
        private String queueName;
        private int red, green, blue;
        private int redOnHover, greenOnHover, blueOnHover;
        private int alpha;

        private GuiButtonBuilder(String text) {
            this.text = DMS.getChatUtil().colorize(text);
        }

        /**
         * Установить цвет кнопки (в состоянии, когда на нее не наведена мышка).
         * @param red красный цвет (0-255).
         * @param green зеленый цвет (0-255).
         * @param blue синий цвет (0-255).
         * @param alpha прозрачность (0-255).
         * @return этот же билдер.
         */
        public GuiButtonBuilder color(int red, int green, int blue, int alpha) {
            this.red = Math.max(0, Math.min(255, red));
            this.green = Math.max(0, Math.min(255, green));
            this.blue = Math.max(0, Math.min(255, blue));
            this.alpha = Math.max(0, Math.min(255, alpha));
            return this;
        }

        /**
         * Установить цвет кнопки при наведении мышки на нее.
         * @param red красный цвет (0-255).
         * @param green зеленый цвет (0-255).
         * @param blue синий цвет (0-255).
         * @return этот же билдер.
         */
        public GuiButtonBuilder colorOnHover(int red, int green, int blue) {
            this.redOnHover = Math.max(0, Math.min(255, red));
            this.greenOnHover = Math.max(0, Math.min(255, green));
            this.blueOnHover = Math.max(0, Math.min(255, blue));
            return this;
        }

        /**
         * Установить имя очереди, с которой будет происходить работа по клику на кнопку,
         * которая будет создана с помощью этого билдера.
         * @param queueName имя очереди.
         * @return этот же билдер.
         */
        public GuiButtonBuilder queueName(String queueName) {
            this.queueName = queueName;
            return this;
        }

        /**
         * Создать кнопку из билдера.
         * @param x позиция кнопки по ширине в процентах от ширины экрана (0-1).
         * @param y позиция кнопки по высоте в процентах от высоты экрана (0-1).
         * @param height высота кнопки в процентах от высоты экрана (0-1).
         * @param width ширина кнопки в процентах от ширины экрана (0-1).
         * @param onClick действие, которое произойдет с игроком по клику на кнопку.
         * @return кнопку.
         */
        public GuiButton build(float x, float y, float height, float width, Consumer<Player> onClick) {
            return new GuiButton(this.text, x, y, height, width,
                    this.red, this.green, this.blue,
                    this.redOnHover, this.greenOnHover, this.blueOnHover,
                    alpha, onClick);
        }

        /**
         * Создать кнопку для вхождения/выхода из игровой очереди из этого билдера.
         * @param x позиция кнопки по ширине в процентах от ширины экрана (0-1).
         * @param y позиция кнопки по высоте в процентах от высоты экрана (0-1).
         * @param height высота кнопки в процентах от высоты экрана (0-1).
         * @param width ширина кнопки в процентах от ширины экрана (0-1).
         * @return кнопку.
         */
        public GuiButton buildForQueue(float x, float y, float height, float width) {
            Preconditions.checkState(this.queueName != null, "No queue name provided in GuiButtonBuilder");
            return build(x, y, height, width, player -> {
                if (DMS.spigot().getFloodControl().checkAndAdd(player, "queues", 1, 2, 1, TimeUnit.SECONDS)) {
                    DMS.spigot().getQueueUtils().togglePlayerQueues(player, this.queueName);
                }
            });
        }

    }

    @Getter
    class GuiButton {

        private final String identifier;
        private final String text;
        private final float x;
        private final float y;
        private final float height;
        private final float width;
        private final int red;
        private final int green;
        private final int blue;
        private final int redOnHover;
        private final int greenOnHover;
        private final int blueOnHover;
        private final int alpha;
        private final Consumer<Player> onClick;

        private GuiButton(String text, float x, float y, float height, float width, int red, int green, int blue,
                          int redOnHover, int greenOnHover, int blueOnHover, int alpha, Consumer<Player> onClick) {
            this.identifier = "apib" + ThreadLocalRandom.current().nextInt(1000000000);
            this.text = text;
            this.x = x;
            this.y = y;
            this.height = height;
            this.width = width;
            this.red = red;
            this.green = green;
            this.blue = blue;
            this.redOnHover = redOnHover;
            this.greenOnHover = greenOnHover;
            this.blueOnHover = blueOnHover;
            this.alpha = alpha;
            this.onClick = onClick;
        }

    }

}
