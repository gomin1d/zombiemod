package yt.dms.api.spigot;

import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.player.achievements.Achievement;

/**
 * Created by k.shandurenko on 25.07.2018
 */
@SpigotOnly
public interface SHooks {

    /**
     * Получить полный модификатор серебра указанного игрока.
     * Сюдавключаются модификатор от группы, его бустеров, гильдии, сезонные модификаторы и другие.
     * @param playerName ник игрока.
     * @return полный модификатор серебра указанного игрока.
     */
    float getPlayersFullSilverModifier(String playerName);

    /**
     * Проиграть эффект получения конкретного достижения указанным игроком.
     * @param playerName ник игрока.
     * @param achievement достижение.
     * @param achievementLevel уровень достижения.
     */
    void playAchievementReception(String playerName, Achievement achievement, int achievementLevel);

}
