package yt.dms.api.spigot;

import org.bukkit.Location;
import org.bukkit.World;
import yt.dms.api.DMS;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.mem.Memory;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Этот интерфейс реализуется вместо обычного Memory на spigot-серверах.
 * Created by RINES on 25.06.2018.
 */
@SpigotOnly
public interface SMemory extends Memory {

    /**
     * Получить spigot-compatible MemoryCache.
     * @return реализацию SMemoryCache.
     */
    @Override
    SMemoryCache getCache();

    /**
     * Установить значение локации по ключу.
     * @param key ключ.
     * @param location значение.
     */
    default void setLocation(String key, Location location) {
        setString(key, DMS.getSerializator().getLocationsSerializer().serializeLocation(location));
    }

    /**
     * Установить значение списка локаций по ключу.
     * @param key ключ.
     * @param locations значение.
     */
    default void setLocationList(String key, List<Location> locations) {
        setStringList(key, locations.stream().map(DMS.getSerializator().getLocationsSerializer()::serializeLocation).collect(Collectors.toList()));
    }

    /**
     * Установить значение сета локаций по ключу.
     * @param key ключ.
     * @param locations значение.
     */
    default void setLocationSet(String key, Set<Location> locations) {
        setStringSet(key, locations.stream().map(DMS.getSerializator().getLocationsSerializer()::serializeLocation).collect(Collectors.toSet()));
    }

    /**
     * Получить значение локации по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     */
    default Location getLocation(String key) {
        String value = getString(key);
        return value == null ? null : DMS.getSerializator().getLocationsSerializer().deserializeLocation(value, null);
    }

    /**
     * Получить значение локации по ключу и заменить мир локации на переданный.
     * @param key ключ.
     * @param world мир, который нужно использовать вместо указанного в локации.
     * @return значение или null, если отсутствует.
     */
    default Location getLocation(String key, World world) {
        String value = getString(key);
        return value == null ? null : DMS.getSerializator().getLocationsSerializer().deserializeLocation(value, world);
    }

    /**
     * Получить значение списка локаций по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     */
    default List<Location> getLocationList(String key) {
        return getLocationList(key, (World) null);
    }

    /**
     * Получить значение списка локаций по ключу и заменить мир локаций на переданный.
     * @param key ключ.
     * @param world мир, который нужно использовать вместо указанного в локациях.
     * @return значение или null, если отсутствует.
     */
    default List<Location> getLocationList(String key, World world) {
        List<String> value = getStringList(key);
        return value == null ? null : value.stream().map(v -> DMS.getSerializator().getLocationsSerializer().deserializeLocation(v, world)).collect(Collectors.toList());
    }

    /**
     * Получить значение сета локаций по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     */
    default Set<Location> getLocationSet(String key) {
        return getLocationSet(key, (World) null);
    }

    /**
     * Получить значение сета локаций по ключу и заменить мир локаций на переданный.
     * @param key ключ.
     * @param world мир, который нужно использовать вместо указанного в локациях.
     * @return значение или null, если отсутствует.
     */
    default Set<Location> getLocationSet(String key, World world) {
        Set<String> value = getStringSet(key);
        return value == null ? null : value.stream().map(v -> DMS.getSerializator().getLocationsSerializer().deserializeLocation(v, world)).collect(Collectors.toSet());
    }

    /**
     * Получить значение локации по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     */
    default Location getLocation(String key, Location defaultValue) {
        return getLocation(key, defaultValue, null);
    }

    /**
     * Получить значение локации по ключу и заменить мир локации на переданный.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param world мир, который нужно использовать вместо указанного в локации.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     */
    default Location getLocation(String key, Location defaultValue, World world) {
        if (defaultValue == null) {
            return getLocation(key);
        }
        String value = getString(key);
        return value == null ? defaultValue : DMS.getSerializator().getLocationsSerializer().deserializeLocation(value, world);
    }

    /**
     * Получить значение списка локаций по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     */
    default List<Location> getLocationList(String key, List<Location> defaultValue) {
        return getLocationList(key, defaultValue, null);
    }

    /**
     * Получить значение списка локаций по ключу и заменить мир локаций на переданный.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param world мир, который нужно использовать вместо указанного в локациях.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     */
    default List<Location> getLocationList(String key, List<Location> defaultValue, World world) {
        if (defaultValue == null) {
            return getLocationList(key);
        }
        List<String> value = getStringList(key, defaultValue.stream().map(DMS.getSerializator().getLocationsSerializer()::serializeLocation).collect(Collectors.toList()));
        return value == null ? defaultValue : value.stream().map(v -> DMS.getSerializator().getLocationsSerializer().deserializeLocation(v, world)).collect(Collectors.toList());
    }

    /**
     * Получить значение сета локаций по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     */
    default Set<Location> getLocationSet(String key, Set<Location> defaultValue) {
        return getLocationSet(key, defaultValue, null);
    }

    /**
     * Получить значение сета локаций по ключу и заменить мир локаций на переданный.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param world мир, который нужно использовать вместо указанного в локациях.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     */
    default Set<Location> getLocationSet(String key, Set<Location> defaultValue, World world) {
        if (defaultValue == null) {
            return getLocationSet(key);
        }
        Set<String> value = getStringSet(key, defaultValue.stream().map(DMS.getSerializator().getLocationsSerializer()::serializeLocation).collect(Collectors.toSet()));
        return value == null ? null : value.stream().map(v -> DMS.getSerializator().getLocationsSerializer().deserializeLocation(v, world)).collect(Collectors.toSet());
    }

}
