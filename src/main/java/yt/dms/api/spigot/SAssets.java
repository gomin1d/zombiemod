package yt.dms.api.spigot;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;

@SpigotOnly
public interface SAssets {

    /**
     * Добавить музыкальный ассет, который загрузится при заходе на этот сервер.
     * @param folder папка ассета.
     * @param name название (без расширения).
     */
    void addMusicAsset(String folder, String name);

    /**
     * Добавить текстурный ассет, который загрузится при заходе на этот сервер.
     * @param folder папка ассета.
     * @param name название (без расширения).
     */
    void addTextureAsset(String folder, String name);

    /**
     * Добавить модельный ассет, который загрузится при заходе на этот сервер.
     * @param folder папка ассета.
     * @param name название (без расширения).
     */
    void addModelAsset(String folder, String name);

    /**
     * Начать проигрывание музыки для указанного игрока.
     * @param listener слушатель.
     * @param folder папка ассета.
     * @param name название (без расширения).
     */
    void playMusic(Player listener, String folder, String name);

    /**
     * Начать проигрывание музыки для указанных игроков.
     * @param listeners слушатели.
     * @param folder папка ассета.
     * @param name название (без расширения).
     */
    void playMusic(Collection<Player> listeners, String folder, String name);

    /**
     * Начать проигрывание музыки для указанного игрока.
     * По завершению одной мелодии будет сразу же подставляться следующая, циклично.
     * @param listener слушатель.
     * @param musics музыки для воспроизведения.
     */
    void playRepeatedly(Player listener, Collection<FixedLengthMusicAsset> musics);

    /**
     * Начать проигрывание музыки для указанных игроков.
     * По завершению одной мелодии будет сразу же подставляться следующая, циклично.
     * @param listeners слушатели.
     * @param musics музыки для воспроизведения.
     */
    void playRepeatedly(Collection<Player> listeners, Collection<FixedLengthMusicAsset> musics);

    /**
     * Остановить проигрывание конкретной музыки, если она играет, для указанного игрока.
     * @param listener слушатель.
     * @param folder папка ассета.
     * @param name название (без расширения).
     */
    void stopMusic(Player listener, String folder, String name);

    /**
     * Остановить проигрывание конкретной музыки, если она играет, для указанных игроков.
     * @param listeners слушатели.
     * @param folder папка ассета.
     * @param name название (без расширения).
     */
    void stopMusic(Collection<Player> listeners, String folder, String name);

    /**
     * Остановить проигрывание всех музык для указанного игрока.
     * @param listener слушатель.
     */
    void stopMusics(Player listener);

    /**
     * Остановить проигрывание всех музык для указанных игроков.
     * @param listeners слушатели.
     */
    void stopMusics(Collection<Player> listeners);

    class FixedLengthMusicAsset {

        private final String folder;
        private final String name;
        private final int durationInSeconds;

        public FixedLengthMusicAsset(String folder, String name, int durationInSeconds) {
            this.folder = folder;
            this.name = name;
            this.durationInSeconds = durationInSeconds;
        }

        public String getFolder() {
            return this.folder;
        }

        public String getName() {
            return this.name;
        }

        public int getDurationInSeconds() {
            return this.durationInSeconds;
        }

    }

}
