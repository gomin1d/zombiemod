package yt.dms.api.spigot.item;

import org.bukkit.Color;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;

/**
 * Created by RINES on 28.06.2018.
 */
public class DmsItemStackMetaBuilder {

    private final DmsItemStackMeta meta = new DmsItemStackMeta();

    public DmsItemStackMetaBuilder amount(int amount) {
        this.meta.amount = amount;
        return this;
    }

    public DmsItemStackMetaBuilder durability(int durability) {
        this.meta.durability = (short) durability;
        return this;
    }

    public DmsItemStackMetaBuilder materialData(int materialData) {
        return durability(materialData);
    }

    public DmsItemStackMetaBuilder leatherArmorColor(Color color) {
        this.meta.leatherArmorColor = color;
        return this;
    }

    public DmsItemStackMetaBuilder enchantment(Enchantment enchantment, int level) {
        this.meta.enchantments.put(enchantment, level);
        return this;
    }

    public DmsItemStackMetaBuilder flag(ItemFlag flag) {
        this.meta.flags.add(flag);
        return this;
    }

    public DmsItemStackMeta build() {
        return this.meta;
    }

}
