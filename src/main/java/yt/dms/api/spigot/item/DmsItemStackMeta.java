package yt.dms.api.spigot.item;

import lombok.Getter;
import org.bukkit.Color;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import yt.dms.api.annotation.SpigotOnly;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by RINES on 28.06.2018.
 */
@Getter
@SpigotOnly
public class DmsItemStackMeta {

    int amount = 1;
    short durability;
    Color leatherArmorColor;
    final Map<Enchantment, Integer> enchantments = new HashMap<>();
    final Set<ItemFlag> flags = EnumSet.noneOf(ItemFlag.class);

    DmsItemStackMeta() {}

}
