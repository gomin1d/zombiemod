package yt.dms.api.player;

import yt.dms.api.party.DmsAutoParty;
import yt.dms.api.party.DmsParty;

/**
 * Created by k.shandurenko on 04.08.2018
 */
public interface DmsPlayerParty {

    /**
     * Создать новую группу.
     * Все возможные ошибки будут обработаны на координаторе.
     */
    void create();

    /**
     * Пригласить указанного игрока в группу.
     * Все возможные неуказанные ошибки будут обработаны на координаторе.
     * @param playerName ник приглашаемого игрока.
     * @throws IllegalArgumentException если указанный ник игрока = null, короче 3 или длиннее 16 символов.
     */
    void invite(String playerName) throws IllegalArgumentException;

    /**
     * Пригласить указанного игрока в группу.
     * Все возможные ошибки будут обработаны на координаторе.
     * @param player приглашаемый игрок.
     */
    default void invite(DmsPlayer player) {
        invite(player.getName());
    }

    /**
     * Принять последнее принятое приглашение в группу.
     * Все возможные ошибки будут обработаны на координаторе.
     */
    void acceptLastInvitation();

    /**
     * Отклонить последнее принятое приглашение в группу.
     * Все возможные ошибки будут обработаны на координаторе.
     */
    void declineLastInvitation();

    /**
     * Покинуть группу.
     * Все возможные ошибки будут обработаны на координаторе.
     */
    void leave();

    /**
     * Исключить указанного игрока из группы.
     * Все возможные неуказанные ошибки будут обработаны на координаторе.
     * @param playerName ник исключаемого игрока.
     * @throws IllegalArgumentException если указанный ник игрока = null, короче 3 или длиннее 16 символов.
     */
    void kick(String playerName) throws IllegalArgumentException;

    /**
     * Исключить указанного игрока из группы.
     * Все возможные ошибки будут обработаны на координаторе.
     * @param player исключаемый игрок.
     */
    default void kick(DmsPlayer player) {
        kick(player.getName());
    }

    /**
     * Распустить группу.
     * Все возможные ошибки будут обработаны на координаторе.
     */
    void disband();

    /**
     * Изменить лидера группы на указанного игрока.
     * Все возможные неуказанные ошибки будут обработаны на координаторе.
     * @param playerName ник нового лидера.
     * @throws IllegalArgumentException если указанный ник игрока = null, короче 3 или длиннее 16 символов.
     */
    void changeLeader(String playerName) throws IllegalArgumentException;

    /**
     * Изменить лидера группы на указанного игрока.
     * Все возможные ошибки будут обработаны на координаторе.
     * @param player новый лидер.
     */
    default void changeLeader(DmsPlayer player) {
        changeLeader(player.getName());
    }

    /**
     * Синхронная проверка с запросом к координатору на то, состоит ли игрок в группу.
     * Проверить это можно и асинхронно, получив группу игрока и проверив, существует ли она.
     * @return true/false.
     */
    boolean isInParty();

    /**
     * Отправить сообщение в групповой чат.
     * @param message текст сообщения.
     * @throws IllegalArgumentException если сообщение = null или длиннее 512 символов.
     */
    void sendPartyMessage(String message) throws IllegalArgumentException;

    /**
     * Показать информацию о группе данному игроку.
     */
    void showInfo();

    /**
     * Получить группу игрока (даже если ее не существует, будет возвращена реализация DmsParty).
     * @deprecated Появилось {@link DmsPlayerParty#getAutoParty()}.
     * @return реализацию DmsParty.
     */
    DmsParty getParty();

    /**
     * Получить динамически обновляемую группу игрока.
     * @return реализацию DmsAutoParty, если игрок состоит в группе; null иначе.
     */
    DmsAutoParty getAutoParty();

}
