package yt.dms.api.player;

import com.google.common.util.concurrent.ListenableFuture;
import yt.dms.api.DMS;
import yt.dms.api.annotation.BungeeOnly;
import yt.dms.api.annotation.SpigotOnly;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by RINES on 03.06.2018.
 */
public interface DmsPlayer {

    /**
     * Получить ник игрока.
     * @return ник игрока.
     */
    String getName();

    /**
     * Предзагрузить разом все секции игрока, которые когда-либо будут использованы на спиготе.
     * Сюда входят: Permissions, NetworkLeveling, Infractions, Gold, NameVisibility, MinigameStats, Preferences,
     * Guild, Achievements.
     * Также сюда входит loadNameInTrueCase().
     */
    @SpigotOnly
    void loadSectionsForSpigot();

    /**
     * Предзагрузить разом все секции игрока, которые когда-либо будут использованы на банжи.
     * Сюда входят: Permissions, NetworkLeveling, Infractions, Gold, NameVisibility, Preferences, Guild.
     * Также сюда входит loadNameInTrueCase().
     */
    @BungeeOnly
    void loadSectionsForBungee();

    /**
     * Обновить имя, чтобы оно было в верном регистре.
     * Метод блокирующий.
     */
    void loadNameInTrueCase();

    /**
     * Блокирующий метод для проверки того, заходил ли игрок хоть раз на проект.
     * @return true/false.
     */
    default boolean doesPlayerExist() {
        try {
            return doesPlayerExistFuture().get(250, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Неблокирующий метод, возвращающий ListenableFuture, для проверки того, заходил ли игрок хоть раз на проект.
     * @return фьючер с true/false.
     */
    ListenableFuture<Boolean> doesPlayerExistFuture();

    /**
     * Блокирующий метод для проверки того, находится ли игрок в сети (на проекте).
     * @return true/false.
     */
    default boolean isOnlineOnProject() {
        try {
            return isOnlineOnProjectFuture().get(250, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Неблокирующий метод, возвращающий ListenableFuture, для проверки того, находится ли игроки в сети (на проекте).
     * @return фьючер с true/false.
     */
    ListenableFuture<Boolean> isOnlineOnProjectFuture();

    /**
     * Получить секцию с данными по игроку.
     * Данные секции могут быть не загружены, но загрузятся синхронно при
     * вызове более-менее любого метода изнутри.
     * @param sectionClass класс секции.
     * @param <T> секция.
     * @return секцию.
     */
    <T> T getSection(Class<T> sectionClass);

    /**
     * Получить фьючер секции с данными по игроку.
     * Этот метод заставляет секцию прогрузиться данными с координатора проекта,
     * а фьючер будет установлен в момент, когда секция станет загруженной.
     * @param sectionClass класс секции.
     * @param <T> секция.
     * @return фьючер на секцию.
     */
    <T> ListenableFuture<T> getSectionFuture(Class<T> sectionClass);

    /**
     * Получить информацию о группах игрока.
     * Если она не загружена, загружает ее.
     * Информация автоматически обновляется.
     * (то есть не для оффлайн-игроков).
     * @return информация о группах игрока.
     */
    default DmsPlayerPermissions getPermissions() {
        return getSection(DmsPlayerPermissions.class);
    }

    /**
     * @see DmsPlayer#getPermissions()
     * @see DmsPlayer#getSectionFuture(Class)
     */
    default ListenableFuture<DmsPlayerPermissions> getPermissionsFuture() {
        return getSectionFuture(DmsPlayerPermissions.class);
    }

    /**
     * Получить информацию о межсерверном опыте и уровне игрока.
     * Если она не загружена, загружает ее.
     * Информация автоматически обновляется.
     * (то есть не для оффлайн-игроков).
     * @return информация о межсерверном опыте и уровне игрока.
     */
    default DmsPlayerNetworkLeveling getNetworkLeveling() {
        return getSection(DmsPlayerNetworkLeveling.class);
    }

    /**
     * @see DmsPlayer#getNetworkLeveling()
     * @see DmsPlayer#getSectionFuture(Class)
     */
    default ListenableFuture<DmsPlayerNetworkLeveling> getNetworkLevelingFuture() {
        return getSectionFuture(DmsPlayerNetworkLeveling.class);
    }

    /**
     * Получить информацию об активных наказаниях игрока.
     * Если она не загружена, загружает ее.
     * Информация автоматически обновляется.
     * (то есть не для оффлайн-игроков).
     * @return информация об активных наказаниях игрока.
     */
    default DmsPlayerInfractions getInfractions() {
        return getSection(DmsPlayerInfractions.class);
    }

    /**
     * @see DmsPlayer#getInfractions()
     * @see DmsPlayer#getSectionFuture(Class)
     */
    default ListenableFuture<DmsPlayerInfractions> getInfractionsFuture() {
        return getSectionFuture(DmsPlayerInfractions.class);
    }

    /**
     * Получить информацию о золоте игрока.
     * Если она не загружена, загружает ее.
     * Информация автоматически обновляется.
     * (то есть не для оффлайн-игроков).
     * @return информация о золоте игрока.
     */
    default DmsPlayerGold getGold() {
        return getSection(DmsPlayerGold.class);
    }

    /**
     * @see DmsPlayer#getGold()
     * @see DmsPlayer#getSectionFuture(Class)
     */
    default ListenableFuture<DmsPlayerGold> getGoldFuture() {
        return getSectionFuture(DmsPlayerGold.class);
    }

    /**
     * Получить информацию о статистике игрока на данном минирежиме.
     * Если она не загружена, загружает ее.
     * Информация автоматически обновляется.
     * (то есть не для оффлайн-игроков).
     * @return информация о статистике игрока на данном минирежиме.
     */
    @SpigotOnly
    default DmsPlayerMinigameStats getCurrentMinigameStats() {
        return getSection(DmsPlayerMinigameStats.class);
    }

    /**
     * @see DmsPlayer#getCurrentMinigameStats()
     * @see DmsPlayer#getSectionFuture(Class)
     */
    @SpigotOnly
    default ListenableFuture<DmsPlayerMinigameStats> getCurrentMinigameStatsFuture() {
        return getSectionFuture(DmsPlayerMinigameStats.class);
    }

    /**
     * Получить методы для работы с друзьями.
     * Все методы запрашивают данные с координатора и синхронны.
     * @return реализацию DmsPlayerFriends.
     */
    default DmsPlayerFriends getFriends() {
        return getSection(DmsPlayerFriends.class);
    }

    /**
     * Получить информацию об онлайне игрока.
     * Все методы запрашивают данные с координатора и синхронны.
     * @return реализацию DmsPlayerOnlineInfo.
     */
    default DmsPlayerOnlineInfo getOnlineInfo() {
        return getSection(DmsPlayerOnlineInfo.class);
    }

    /**
     * Получить информацию об отображаемом имени игрока и его аттрибутах (префиксе, суффиксе).
     * Если она не загружена, загружает ее.
     * Информация автоматически обновляется.
     * @return информация об отображаемом имени игрока и его аттрибутах.
     */
    default DmsPlayerNameVisibility getNameVisibility() {
        return getSection(DmsPlayerNameVisibility.class);
    }

    /**
     * @see DmsPlayer#getNameVisibility()
     * @see DmsPlayer#getSectionFuture(Class)
     */
    default ListenableFuture<DmsPlayerNameVisibility> getNameVisibilityFuture() {
        return getSectionFuture(DmsPlayerNameVisibility.class);
    }

    /**
     * Получить информацию о предпочтениях этого игрока.
     * Если она не загружена, загружает ее.
     * Информация автоматически обновляется.
     * @return информация о предпочтениях этого игрока.
     */
    default DmsPlayerPreferences getPreferences() {
        return getSection(DmsPlayerPreferences.class);
    }

    /**
     * @see DmsPlayer#getPreferences()
     * @see DmsPlayer#getSectionFuture(Class)
     */
    default ListenableFuture<DmsPlayerPreferences> getPreferencesFuture() {
        return getSectionFuture(DmsPlayerPreferences.class);
    }

    /**
     * Получить информацию о гильдии игрока и методы для работы с ней.
     * Если она не загружена, загружает ее.
     * Информация автоматически обновляется.
     * @return информация о гильдии игрока и методы для работы с ней.
     */
    default DmsPlayerGuild getGuild() {
        return getSection(DmsPlayerGuild.class);
    }

    /**
     * @see DmsPlayer#getGuild()
     * @see DmsPlayer#getSectionFuture(Class)
     */
    default ListenableFuture<DmsPlayerGuild> getGuildFuture() {
        return getSectionFuture(DmsPlayerGuild.class);
    }

    /**
     * Получить информацию о достижениях игрока.
     * Если она не загружена, загружает ее.
     * Информация автоматически обновляется.
     * @return информация о достижениях игрока.
     */
    @SpigotOnly
    default DmsPlayerAchievements getAchievements() {
        return getSection(DmsPlayerAchievements.class);
    }

    /**
     * @see DmsPlayer#getAchievements()
     * @see DmsPlayer#getSectionFuture(Class)
     */
    @SpigotOnly
    default ListenableFuture<DmsPlayerAchievements> getAchievementsFuture() {
        return getSectionFuture(DmsPlayerAchievements.class);
    }

    /**
     * Получить информацию о группе игрока и методы для работы с ней.
     * @return информация о группе игрока и методы для работы с ней.
     */
    default DmsPlayerParty getParty() {
        return getSection(DmsPlayerParty.class);
    }

    /**
     * Получить информацию о косметических предметах игрока и методы для работы с ними.
     * @return информация о косметических предметах игрока и методы для работы с ними.
     */
    @SpigotOnly
    default DmsPlayerCosmetics getCosmetics() {
        return getSection(DmsPlayerCosmetics.class);
    }

    /**
     * Отправить игрока на другой сервер.
     * @param serverName имя сервера.
     */
    @SpigotOnly
    default void sendToServer(String serverName) {
        DMS.spigot().getBungeeUtils().sendPlayer(this, serverName);
    }

    /**
     * Отправить нотификацию игроку.
     * К заголовку и телу применяется замена цветовых кодов с &.
     * @param header заголовок объявления.
     * @param body тело объявления.
     */
    @SpigotOnly
    default void sendNotification(String header, List<String> body) {
        DMS.spigot().getNotificationUtils().send(this, header, body);
    }

    /**
     * Получить полное отображаемое имя игрока (без цветных кодов).
     * @return полное отображаемое имя игрока (без цветных кодов).
     */
    default String getVisibleFullNameUncolored() {
        return getNameVisibility().getVisibleFullNameUncolored();
    }

    /**
     * Закастить этот объект к DmsSpigotPlayer.
     * @return этот же объект, но закасченный к DmsSpigotPlayer.
     */
    @SpigotOnly
    default DmsSpigotPlayer spigot() {
        return (DmsSpigotPlayer) this;
    }

}
