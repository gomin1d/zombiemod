package yt.dms.api.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;

/**
 * Этот интерфейс реализуется вместо обычного DmsPlayer на spigot-серверах.
 * Created by RINES on 25.06.2018.
 */
@SpigotOnly
public interface DmsSpigotPlayer extends DmsPlayer {

    /**
     * Возвращает spigot-игрока, который является данным DmsPlayer.
     * @return null, если игрок не в сети на данном сервере, иначе spigot-игрока.
     */
    default Player getSpigotPlayer() {
        return Bukkit.getPlayerExact(getName());
    }

    /**
     * Проверка на то, онлайн ли игрок на данном spigot-сервере.
     * @return true/false.
     */
    default boolean isOnlineThere() {
        return getSpigotPlayer() != null;
    }

}
