package yt.dms.api.player;

import com.google.common.util.concurrent.ListenableFuture;
import yt.dms.api.annotation.BungeeOnly;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.common.DmsRegistry;

import java.util.List;

/**
 * Created by k.shandurenko on 26.07.2018
 */
public interface DmsPlayerRegistry<P extends DmsPlayer> extends DmsRegistry<String, P> {

    /**
     * Получить и предзагрузить для указанных игроков все секции, необходимые для спигота.
     * @param playerNames ники игроков.
     * @return игроки.
     */
    @SpigotOnly
    List<P> loadPlayersForSpigot(List<String> playerNames);

    /**
     * Получить и предзагрузить для указанных игроков все секции, необходимые для банжи.
     * @param playerNames ники игроков.
     * @return игроки.
     */
    @BungeeOnly
    List<P> loadPlayersForBungee(List<String> playerNames);

    /**
     * Получить и предзагрузить для указанных игроков все секции, необходимые для их успешного отображения в топе.
     * Сюда входят секции, которые как-либо влияют на отображаемые имена игроков.
     * @param playerNames ники игроков.
     * @return игроки.
     */
    @SpigotOnly
    List<P> loadPlayersForMinigameTop(List<String> playerNames);

    /**
     * @see DmsPlayerRegistry#loadPlayersForMinigameTop(List)
     * @param playerNames ники игроков.
     * @return фьючер на игроков.
     */
    @SpigotOnly
    ListenableFuture<List<P>> loadPlayersForMinigameTopAsync(List<String> playerNames);

    /**
     * Закастить этот объект к DmsBungeePlayerRegistry.
     * @return этот же объект, но закасченный к DmsBungeePlayerRegistry.
     */
    default DmsBungeePlayerRegistry<P> bungee() {
        return (DmsBungeePlayerRegistry<P>) this;
    }

}
