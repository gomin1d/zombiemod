package yt.dms.api.player;

/**
 * Created by k.shandurenko on 15.07.2018
 */
public interface DmsPlayerPreferences {

    /**
     * Включен ли прием личных сообщений от всех для этого игрока?
     * @return true/false.
     */
    boolean isPersonalMessagesEnabled();

    /**
     * Включен ли чат персонала для этого игрока?
     * @return true/false.
     */
    boolean isStaffChatEnabled();

    /**
     * Включен ли вип-чат для этого игрока?
     * @return true/false.
     */
    boolean isVipChatEnabled();

    /**
     * Включен ли прием заявок в друзья от других игроков для этого игрока?
     * @return true/false.
     */
    boolean isFriendRequestsEnabled();

    /**
     * Включил ли прием приглашений в группы от других игроков для этого игрока?
     * @return true/false.
     */
    boolean isPartyInvitationsEnabled();

    /**
     * Получить формат сообщения-приветствия при входе игрока на лобби-сервер.
     * @return формат сообщения-приветствия или null.
     */
    String getLobbyJoinMessageFormat();

    /**
     * Установить разрешение на написание личных сообщений этому игроку.
     * @param value true/false.
     */
    void setPersonalMessagesEnabled(boolean value);

    /**
     * Включить/отключить чат персонала для этого игрока.
     * @param value true/false.
     */
    void setStaffChatEnabled(boolean value);

    /**
     * Включить/отключить вип-чат для этого игрока.
     * @param value true/false.
     */
    void setVipChatEnabled(boolean value);

    /**
     * Включить/отключить прием заявок в друзья от других игроков для этого игрока.
     * @param value true/false.
     */
    void setFriendRequestsEnabled(boolean value);

    /**
     * Включить/отключить прием приглашений в группы от других игроков для этого игрока.
     * @param value true/false.
     */
    void setPartyInvitationsEnabled(boolean value);

    /**
     * Установить формат сообщения-приветствия при входе игрока на лобби-сервер.
     * @param messageFormat формат сообщения-приветствия.
     */
    void setLobbyJoinMessageFormat(String messageFormat);

}
