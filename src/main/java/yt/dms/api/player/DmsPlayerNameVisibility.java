package yt.dms.api.player;

/**
 * Created by RINES on 07.06.2018.
 */
public interface DmsPlayerNameVisibility {

    /**
     * Получить ник игрока.
     * @return ник игрока.
     */
    String getOriginalName();

    /**
     * Получить основную группу игрока.
     * @return основная группа игрока.
     */
    PermissionGroup getOriginalPermissionGroup();

    /**
     * Узнать, скрывается ли игрок под другим именем/другой группой.
     * @return true/false.
     */
    boolean isHidden();

    /**
     * Получить отображаемый ник игрока.
     * Если игрок указал не-свой ник через /hide, здесь будет это имя.
     * Если игрок не пользовался /hide, здесь будет его же имя.
     * @return отображаемый ник игрока.
     */
    String getVisibleName();

    /**
     * Получить отображаемую группу игрока.
     * Если игрок указал не-свою группу через /hide, здесь будет эта группа.
     * Если игрок не пользовался /hide, здесь будет его же группа.
     * @return отображаемая группа игрока.
     */
    PermissionGroup getVisiblePermissionGroup();

    /**
     * Получить суффикс игрока (с цветами, то есть заменой & на цветовые коды) и дополнительным форматированием.
     * Если у игрока нет суффикса, будет возвращена пустая строка.
     * @return суффикс игрока (с цветами, то есть заменой & на цветовые коды) или пустая строка, если его нет.
     */
    String getSuffix();

    /**
     * Получить отображаемый префикс игрока (без замены & на цветовые коды).
     * Если игрок указал не-свою группу через /hide, здесь будет префикс этой группы.
     * Если игрок не пользовался /hide, здесь будет префикс его же группы.
     * Более того, префикс будет подчеркнутым, если межсерверный уровень игрока не меньше 29 и у него есть донат-группа.
     * @return отображаемый префикс игрока (без замены & на цветовые коды).
     */
    String getVisiblePrefixUncolored();

    /**
     * Получить реальный префикс игрока (без замены & на цветовые коды).
     * Префикс будет подчеркнутым, если межсерверный уровень игрока не меньше 29 и у него есть донат-группа.
     * @return реальный префикс игрока (без замены & на цветовые коды).
     */
    String getOriginalPrefixUncolored();

    /**
     * Установить суффикс игрока. Должен быть без цветовых кодов и не длиннее 11 символов.
     * @param suffix новый суффикс игрока (или нулл, если его нужно убрать).
     * @throws IllegalArgumentException если суффикс длиннее 11 символов.
     */
    void setSuffix(String suffix) throws IllegalArgumentException;

    /**
     * "Спрятать" игрока под другим именем и группой.
     * Нулы не допускаются.
     * @param hiddenName выбранное имя (должно быть не длиннее 16 символов).
     * @param hiddenGroup выбранная группа (не может быть группой разработчика, администратора или владельца).
     * @throws IllegalArgumentException если нарушено условие на аргументы
     */
    void hide(String hiddenName, PermissionGroup hiddenGroup) throws IllegalArgumentException;

    /**
     * Вернуть игроку обычные имя и группу.
     * @throws IllegalStateException если игрок и так не скрыт.
     */
    void unhide() throws IllegalStateException;

    /**
     * Получить реальное полное имя игрока (без цветных кодов).
     * @return реальное полное имя игрока (без цветных кодов).
     */
    default String getOriginalFullNameUncolored() {
        return getOriginalPrefixUncolored() + getOriginalName() + getSuffix();
    }

    /**
     * Получить полное отображаемое имя игрока (без цветных кодов).
     * @return полное отображаемое имя игрока (без цветных кодов).
     */
    default String getVisibleFullNameUncolored() {
        return getVisiblePrefixUncolored() + getVisibleName() + getSuffix();
    }

}
