package yt.dms.api.player;

import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import yt.dms.api.annotation.BungeeOnly;

/**
 * Created by k.shandurenko on 03.10.2018
 */
@BungeeOnly
public interface DmsBungeePlayerRegistry<P extends DmsPlayer> extends DmsPlayerRegistry<P> {

    /**
     * Получить игрока из регистра, если он там есть, или предзагрузить его.
     * @param player игрок.
     * @return DmsPlayer.
     */
    default P get(ProxiedPlayer player) {
        return get(player.getName());
    }

    /**
     * Получить игрока из регистра, если он там есть, или предзагрузить его.
     * @param connection подключение игрока.
     * @return DmsPlayer.
     */
    default P get(PendingConnection connection) {
        return get(connection.getName());
    }

}
