package yt.dms.api.player;

import yt.dms.api.guild.DmsGuild;
import yt.dms.api.guild.GuildPerk;

/**
 * В этом интерфейсе представлены методы оказания влияния на гильдию со стороны конкретного игрока.
 * Created by k.shandurenko on 15.07.2018
 */
public interface DmsPlayerGuild {

    /**
     * Проверить, находится ли этот игрок в гильдии.
     * @return true/false.
     */
    boolean isInGuild();

    /**
     * Если этот игрок находится в гильдии, получить ID этой гильдии.
     * @return ID гильдии, в которой состоит этот игрок; или 0, если он не состоит в гильдии.
     */
    int getGuildID();

    /**
     * Если этот игрок находится в гильдии, получить эту гильдию.
     * @return гильдию, в которой состоит этот игрок; или null, если он в ней не состоит.
     */
    DmsGuild getGuild();

    /**
     * Создать новую гильдию.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @param guildName название гильдии.
     * @throws IllegalStateException если данный игрок уже состоит в гильдии.
     * @throws IllegalArgumentException если название гильдии = null, короче 3 или длиннее 16 символов.
     */
    void createNew(String guildName) throws IllegalStateException, IllegalArgumentException;

    /**
     * Распустить гильдию.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     */
    void disband() throws IllegalStateException;

    /**
     * Изменить лидера гильдии на другого игрока.
     * @param newLeaderName ник нового лидера.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     * @throws IllegalArgumentException если ник нового лидера = null, короче 3 или длиннее 16 символов.
     */
    void changeLeader(String newLeaderName) throws IllegalStateException, IllegalArgumentException;

    /**
     * Повысить в звании участника гильдии до офицера.
     * @param guildMemberName ник участника гильдии.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     * @throws IllegalArgumentException если ник участника гильдии = null, короче 3 или длиннее 16 символов.
     */
    void promoteToOfficer(String guildMemberName) throws IllegalStateException, IllegalArgumentException;

    /**
     * Понизить в звании офицера гильдии до участника.
     * @param guildMemberName ник офицера гильдии.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     * @throws IllegalArgumentException если ник офицера гильдии = null, короче 3 или длиннее 16 символов.
     */
    void demoteToMember(String guildMemberName) throws IllegalStateException, IllegalArgumentException;

    /**
     * Пригласить нового игрока в гильдию.
     * @param playerName ник игрока.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     * @throws IllegalArgumentException если ник игрока = null, короче 3 или длиннее 16 символов.
     */
    void invite(String playerName) throws IllegalStateException, IllegalArgumentException;

    /**
     * Принять приглашение в указанную гильдию.
     * @param guildName название гильдии.
     * @throws IllegalStateException если данный игрок уже состоит в гильдии.
     * @throws IllegalArgumentException если название гильдии = null, короче 3 или длиннее 16 символов.
     */
    void acceptInvitation(String guildName) throws IllegalStateException, IllegalArgumentException;

    /**
     * Отклонить приглашение в указанную гильдию.
     * @param guildName название гильдии.
     * @throws IllegalStateException если данный игрок уже состоит в гильдии.
     * @throws IllegalArgumentException если название гильдии = null, короче 3 или длиннее 16 символов.
     */
    void declineInvitation(String guildName) throws IllegalStateException, IllegalArgumentException;

    /**
     * Покинуть гильдию.
     * @throws IllegalStateException если данный игрок и так не состоит в гильдии.
     */
    void leave() throws IllegalStateException;

    /**
     * Исключить игрока из гильдии.
     * @param guildMemberName ник игрока.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     * @throws IllegalArgumentException если указанный ник игрока = null, короче 3 или длиннее 16 символов.
     */
    void kick(String guildMemberName) throws IllegalStateException, IllegalArgumentException;

    /**
     * Перечислить в гильдию некоторое количество своего золота.
     * @param amount количество золота.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     * @throws IllegalArgumentException если указанное количество золота меньше ста.
     */
    void donateGold(int amount) throws IllegalStateException, IllegalArgumentException;

    /**
     * Отправить сообщение в гильдийский чат.
     * @param message сообщение.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     */
    void sendGuildChatMessage(String message) throws IllegalStateException;

    /**
     * Увеличить уровень перка гильдии.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @param perk перк.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     */
    void upgradePerk(GuildPerk perk) throws IllegalStateException;

    /**
     * Изменить приветственное сообщение гильдии.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @param messageOfTheDay приветственное сообщение гильдии.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     */
    void changeMessageOfTheDay(String messageOfTheDay) throws IllegalStateException;

    /**
     * Изменить логотип гильдии.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @param logoURL ссылка на новый логотип гильдии.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     */
    void changeLogoURL(String logoURL) throws IllegalStateException;

    /**
     * Повысить уровень гильдии.
     * Проверка гильдийских прав для исполнения метода происходит на стороне координатора проекта.
     * @throws IllegalStateException если данный игрок не состоит в гильдии.
     */
    void levelUp() throws IllegalStateException;

    /**
     * Вывести информацию о конкретной гильдии.
     * @param guildName название гильдии.
     * @throws IllegalArgumentException если название гильдии = null, короче 3 или длиннее 16 символов.
     */
    void printInfo(String guildName) throws IllegalArgumentException;

    /**
     * Вывести информацию о своей гильдии.
     * @throws IllegalStateException если игрок не состоит в гильдии.
     */
    void printInfo() throws IllegalStateException;

    /**
     * Вывести топ сильнейших гильдий проекта.
     */
    void showTop();

}
