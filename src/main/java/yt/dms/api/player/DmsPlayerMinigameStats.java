package yt.dms.api.player;

import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;
import java.util.Map;

/**
 * Created by RINES on 03.06.2018.
 */
@SpigotOnly
public interface DmsPlayerMinigameStats {

    /**
     * Получить серебро игрока на этом режиме.
     * @return серебро игрока на этом режиме.
     */
    int getSilver();

    /**
     * Изменить количество серебра игрока на этом режиме.
     * Суммарное количество не должно быть меньше 0.
     * На этот метод не распространяется модификатор серебра игрока.
     * @param delta количество серебра, которое нужно прибавить/убавить.
     * @throws IllegalArgumentException если после изменения серебро игрока станет меньше 0.
     */
    default void changeSilver(int delta) throws IllegalArgumentException {
        changeSilver(delta, false);
    }

    /**
     * Изменить количество серебра игрока на этом режиме.
     * Суммарное количество не должно быть меньше 0.
     * @param delta количество серебра, которое нужно прибавить/убавить.
     * @param useSilverMultiplier если изменяемая сумма положительна, нужно ли ее умножать на модификатор серебра игрока?
     * @throws IllegalArgumentException если после изменения серебро игрока станет меньше 0.
     */
    void changeSilver(int delta, boolean useSilverMultiplier) throws IllegalArgumentException;

    /**
     * Получить значение указанной статистики на этом режиме для данного игрока.
     * @param statName имя статистики.
     * @return значение статистики.
     */
    int getStat(String statName);

    /**
     * Установить значение статистики на этом режиме для данного игрока.
     * @param statName имя статистики.
     * @param value значение статистики.
     */
    void setStat(String statName, int value);

    /**
     * Изменить значение статистики на этом режиме для данного игрока.
     * @param statName имя статистики.
     * @param delta величина, на которую значение статистики нужно изменить.
     */
    default void changeStat(String statName, int delta) {
        if (delta == 0) {
            return;
        }
        setStat(statName, getStat(statName) + delta);
    }

    /**
     * Увеличить значение статистики на этом режиме для данного игрока на 1.
     * @param statName имя статистики.
     */
    default void incrementStat(String statName) {
        changeStat(statName, 1);
    }

    /**
     * Изменить значение серебра на этом режиме для данного игрока на указанную
     * величину, но сделать это не сейчас (отправляя пакет на координатор проекта
     * и заставляя того исполнить запрос в базу данных), а отложенно.
     * @param delta величина, на которую значение серебра нужно изменить.
     * @param useSilverMultiplier если изменяемая сумма положительна, нужно ли ее умножать на модификатор серебра игрока?
     */
    void modifySilverChange(int delta, boolean useSilverMultiplier);

    /**
     * Изменить значение серебра на этом режиме для данного игрока на указанную
     * величину, но сделать это не сейчас (отправляя пакет на координатор проекта
     * и заставляя того исполнить запрос в базу данных), а отложенно.
     * Должно использоваться в случаях, когда серебро изменяется очень часто.
     * @param delta величина, на которую значение серебра нужно изменить.
     */
    default void modifySilverChange(int delta) {
        modifySilverChange(delta, false);
    }

    /**
     * Изменить значение статистики на этом режиме для данного игрока на указанную
     * величину, но сделать это не сейчас (отправляя пакет на координатор проекта и
     * заставляя того исполнить запрос в базу данных), а отложенно.
     * Должно использоваться для очень часто изменяемых статистик.
     * @param statName имя статистики.
     * @param delta величина, на которую значение статистики нужно изменить.
     */
    void modifyStatChange(String statName, int delta);

    /**
     * @see DmsPlayerMinigameStats#modifyStatChange(String, int)
     * @see DmsPlayerMinigameStats#incrementStat(String)
     * @param statName имя статистики.
     */
    default void incrementStatChange(String statName) {
        modifyStatChange(statName, 1);
    }

    /**
     * Получить значение, на которое должна быть отложенно изменена указанная
     * статистика этого режима для данного игрока.
     * @param statName имя статистики.
     * @return величина, на которую значение статистики должно быть изменено.
     */
    int getStatChange(String statName);

    /**
     * То самое отложенное изменение измененной статистики.
     * Использование этого метода сбрасывает кеш изменения для указанной статистики.
     * @param statName имя статистики.
     */
    void applyStatChange(String statName);

    /**
     * То самое отложенное изменение всех отложенно-измененных статистик и серебра
     * для данного игрока на этом режиме.
     * Использование этого метода сбрасывает кеш изменения для всех статистик и серебра.
     */
    void applyStatChanges();

    /**
     * Получить все статистики данного игрока на этом режиме.
     * @return все статистики данного игрока на этом режиме.
     */
    Map<String, Integer> getAllStats();

    /**
     * Получить уровень фичи на этом режиме для данного игрока.
     * @param featureId идентификатор фичи.
     * @return уровень фичи.
     */
    int getFeatureLevel(String featureId);

    /**
     * Проверка на то, есть ли фича у данного игрока на этом режиме.
     * Если есть, проверка на то, достаточный ли у нее уровень.
     * @param featureId идентификатор фичи.
     * @param featureLevel требуемый уровень фичи.
     * @return true/false.
     */
    default boolean hasFeature(String featureId, int featureLevel) {
        return getFeatureLevel(featureId) >= featureLevel;
    }

    /**
     * Проверка на то, есть ли фича у данного игрока на этом режиме.
     * @param featureId идентификатор фичи.
     * @return true/false.
     */
    default boolean hasFeature(String featureId) {
        return hasFeature(featureId, 1);
    }

    /**
     * Добавить фичу определенного уровня данному игроку на этом режиме.
     * @param featureId идентификатор фичи.
     * @param level уровень фичи.
     * @throws IllegalArgumentException если указанный уровень фичи меньше 1.
     * @throws IllegalStateException если у игрока уже есть данная фича указанного уровня или выше.
     */
    void addFeature(String featureId, int level) throws IllegalArgumentException, IllegalStateException;

    /**
     * Добавить фичу данному игроку на этом режиме.
     * @param featureId идентификатор фичи.
     * @throws IllegalStateException если у игрока уже есть данная фича.
     */
    default void addFeature(String featureId) throws IllegalStateException {
        addFeature(featureId, 1);
    }

    /**
     * Забрать фичу (и все ее уровни) у данного игрока на этом режиме.
     * @param featureId идентификатор фичи.
     * @throws IllegalStateException если у игрока нет данной фичи.
     */
    void takeFeature(String featureId) throws IllegalStateException;

    /**
     * Получить идентификаторы всех фич, которые есть у данного игрока на этом режиме.
     * @return коллекция идентификаторов всех фич.
     */
    Collection<String> getAllFeatureIds();

    /**
     * Получить идентификаторы всех фич и их уровни, которые есть у данного игрока на этом режиме.
     * @return мап, где ключи - идентификаторы фич, а значения - их уровни.
     */
    Map<String, Integer> getAllFeaturesWithLevels();

    /**
     * Купить фичу конкретного уровня для данного игрока на этом режиме.
     * Автоматически повышает уровень фичи и списывает серебро (работает оптимальнее, чем вызов двух методов сверху).
     * @param featureId идентификатор фичи.
     * @param featureLevel уровень фичи.
     * @param priceInSilver цена указанного уровня фичи в серебре.
     * @throws IllegalArgumentException если указанный уровень фичи меньше 1 или количество серебра у игрока после
     *                                  изменения станет меньше 0.
     * @throws IllegalStateException если у игрока уже есть данная фича указанного уровня или выше.
     */
    void buyFeature(String featureId, int featureLevel, int priceInSilver) throws IllegalArgumentException, IllegalStateException;

    /**
     * Купить фичу для данного игрока на этом режиме.
     * Автоматически добавляет фичу игроку и списывает серебро (работает оптимальнее, чем вызов двух методов сверху).
     * @param featureId идентификатор фичи.
     * @param priceInSilver цена указанной фичи в серебре.
     * @throws IllegalArgumentException если количество серебра у игрока после изменения станет меньше 0.
     * @throws IllegalStateException если у игрока уже есть данная фича.
     */
    default void buyFeature(String featureId, int priceInSilver) throws IllegalArgumentException, IllegalStateException {
        buyFeature(featureId, 1, priceInSilver);
    }

    /**
     * Сохранить инвентарь игрока.
     * @throws IllegalStateException если игрок не в сети.
     */
    void savePlayerInventory() throws IllegalStateException;

    /**
     * Заполнить инвентарь игрока сохраненным контентом.
     * Метод асинхронный и заполнит инвентарь в ближайшем будущем.
     * @throws IllegalStateException если игрок не в сети.
     */
    void fulfillPlayerInventory() throws IllegalStateException;

    /**
     * Заполнить инвентарь игрока сохраненным контентом прямо сейчас.
     * Метод синхронный.
     * @throws IllegalStateException если игрок не в сети.
     */
    void fulfillPlayerInventoryNow() throws IllegalStateException;

    /**
     * Получить реализацию DmsPlayerMinigameStorage для хранения дополнительных данных по игроку на этом минирежиме.
     * @return реализацию DmsPlayerinigameStorage.
     */
    DmsPlayerMinigameStorage getStorage();

}
