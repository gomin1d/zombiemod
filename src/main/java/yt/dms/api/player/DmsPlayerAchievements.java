package yt.dms.api.player;

import yt.dms.api.player.achievements.Achievement;

/**
 * Created by k.shandurenko on 23.07.2018
 */
public interface DmsPlayerAchievements {

    /**
     * Проверить, имеется ли у игрока указанное достижение определенного уровня.
     * @param achievement достижение.
     * @param level уровень достижения.
     * @return true, если уровень достижения игрока больше или равен указанному; false в ином случае.
     */
    boolean hasAchievement(Achievement achievement, int level);

    /**
     * Проверить, имеется ли у игрока указанное достижение.
     * @param achievement достижение.
     * @return true/false.
     */
    default boolean hasAchievement(Achievement achievement) {
        return hasAchievement(achievement, 1);
    }

    /**
     * Добавить игроку указанное достижение определенного уровня.
     * @param achievement достижение.
     * @param level уровень достижения.
     * @return true, если достижение было добавлено; false в ином случае (если оно у него уже имелось).
     */
    boolean addAchievement(Achievement achievement, int level);

    /**
     * Добавить игроку указанное достижение.
     * @param achievement достижение.
     * @return true/false.
     */
    default boolean addAchievement(Achievement achievement) {
        return addAchievement(achievement, 1);
    }

    /**
     * Забрать достижение у игрока (все его уровни).
     * @param achievement достижение.
     * @return true, если достижение было забрано; false, если у игрока не было этого достижения.
     */
    boolean removeAchievement(Achievement achievement);

    /**
     * Получить уровень достижений игрока (сумма уровней всех полученных им достижений).
     * @return уровень достижений игрока.
     */
    int getPlayersAchievementLevel();

}
