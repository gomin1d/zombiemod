package yt.dms.api.player.achievements;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

/**
 * Created by k.shandurenko on 23.07.2018
 */
public enum Achievement {
    FIRST_JOIN(AchievementSection.GLOBAL, "Первый вход", Lists.newArrayList(
            "Впервые зайдите на проект."
    ), 250, 0),
    FIRST_MESSAGE(AchievementSection.GLOBAL, "Оно говорит!!", Lists.newArrayList(
            "Отправьте свое первое сообщение."
    ), 250, 0),
    VIP(AchievementSection.GLOBAL, "Важная персона", Lists.newArrayList(
            "Купите группу VIP на проекте."
    ), 0, 1000),
    VIP_PLUS(AchievementSection.GLOBAL, "Очень важная персона", Lists.newArrayList(
            "Купите группу VIP+ на проекте."
    ), 0, 2500),
    RICH(AchievementSection.GLOBAL, "Рич-бич", Lists.newArrayList(
            "Купите группу RICH на проекте."
    ), 0, 5000),
    SPONSOR(AchievementSection.HIDDEN, "Местный миллиардер", Lists.newArrayList(
            "Получите группу Спонсора на проекте."
    ), 0, 10000),
    YOUTUBE(AchievementSection.HIDDEN, "Камера, мотор!", Lists.newArrayList(
            "Получите группу YouTube на проекте."
    ), 5000, 0),
    YOUTUBE_PLUS(AchievementSection.HIDDEN, "Популяризатор", Lists.newArrayList(
            "Получите группу YouTube+ на проекте."
    ), 20000, 0),
    YOUTUBE_PLUS_PLUS(AchievementSection.HIDDEN, "All inclusive", Lists.newArrayList(
            "Получите группу YouTube++ на проекте."
    ), 75000, 0),
    STAFF(AchievementSection.HIDDEN, "Часть команды - часть корабля", Lists.newArrayList(
            "Станьте частью команды нашего проекта"
    ), 0, 0),
    INTRO(AchievementSection.GLOBAL, "Прилежный ученик", Lists.newArrayList(
            "Пройдите обучающий сценарий."
    ), 250, 2500),
    HUB_NPCS(AchievementSection.GLOBAL, "Искатель приключений", Lists.newArrayList(
            "Найдите всех NPC'ов в хабе."
    ), 250, 5000),
    MORE_GOLD(AchievementSection.GLOBAL, "Хранитель злата", new List[]{
            Lists.newArrayList(
                    "Добейтесь, чтобы на вашем аккаунте",
                    "лежало не менее 10000 золота."
            ),
            Lists.newArrayList(
                    "Добейтесь, чтобы на вашем аккаунте",
                    "лежало не менее 50000 золота."
            ),
            Lists.newArrayList(
                    "Добейтесь, чтобы на вашем аккаунте",
                    "лежало не менее 100000 золота."
            ),
            Lists.newArrayList(
                    "Добейтесь, чтобы на вашем аккаунте",
                    "лежало не менее 250000 золота."
            ),
            Lists.newArrayList(
                    "Добейтесь, чтобы на вашем аккаунте",
                    "лежало не менее 500000 золота."
            ),
            Lists.newArrayList(
                    "Добейтесь, чтобы на вашем аккаунте",
                    "лежало не менее 1000000 золота."
            )
    }, new int[]{100, 500, 1000, 2500, 5000, 10000}, new int[]{1000, 2500, 5000, 10000, 20000, 30000}),
    MORE_LEVEL(AchievementSection.GLOBAL, "Хранитель знаний", new List[]{
            Lists.newArrayList("Достигните 5го уровня DMS."),
            Lists.newArrayList("Достигните 10го уровня DMS."),
            Lists.newArrayList("Достигните 15го уровня DMS."),
            Lists.newArrayList("Достигните 20го уровня DMS."),
            Lists.newArrayList("Достигните 25го уровня DMS."),
            Lists.newArrayList("Достигните 30го уровня DMS."),
            Lists.newArrayList("Достигните 40го уровня DMS."),
            Lists.newArrayList("Достигните 50го уровня DMS."),
            Lists.newArrayList("Достигните 60го уровня DMS."),
            Lists.newArrayList("Достигните 70го уровня DMS."),
            Lists.newArrayList("Достигните 80го уровня DMS."),
            Lists.newArrayList("Достигните 90го уровня DMS."),
            Lists.newArrayList("Достигните 100го уровня DMS."),
    }, new int[]{500, 1000, 1500, 2000, 2500, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000}, new int[]{1000, 2000, 3000, 4000, 5000, 10000, 20000, 40000, 60000, 80000, 100000, 125000, 150000}),
    ACHIEVEMENTS_MASTER(AchievementSection.GLOBAL, "Хранитель истории", new List[]{
            Lists.newArrayList("Добейтесь 5го уровня достижений."),
            Lists.newArrayList("Добейтесь 10го уровня достижений."),
            Lists.newArrayList("Добейтесь 15го уровня достижений."),
            Lists.newArrayList("Добейтесь 20го уровня достижений."),
            Lists.newArrayList("Добейтесь 25го уровня достижений."),
            Lists.newArrayList("Добейтесь 30го уровня достижений."),
            Lists.newArrayList("Добейтесь 35го уровня достижений."),
            Lists.newArrayList("Добейтесь 40го уровня достижений."),
            Lists.newArrayList("Добейтесь 45го уровня достижений."),
            Lists.newArrayList("Добейтесь 50го уровня достижений."),
            Lists.newArrayList("Добейтесь 60го уровня достижений."),
            Lists.newArrayList("Добейтесь 70го уровня достижений."),
            Lists.newArrayList("Добейтесь 80го уровня достижений."),
            Lists.newArrayList("Добейтесь 90го уровня достижений."),
            Lists.newArrayList("Добейтесь 100го уровня достижений."),
    }, new int[]{100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500}, new int[]{1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000}),
    JUMP_OUT_OF_HUB(AchievementSection.HIDDEN, "Прыжок веры", Lists.newArrayList("Спрыгните в хабе в бездну."), 250, 2500),
    DMS_LOVER(AchievementSection.HIDDEN, "Самый приверженный фанат", Lists.newArrayList("Напишите в чате \"я люблю дмс\"."), 500, 5000),
    ENDER_PORTAL(AchievementSection.HIDDEN, "Побег в другой мир", Lists.newArrayList("Войдите в эндер-портал в хабе", "или лобби проекта."), 250, 2500),
    DROPPER(AchievementSection.GLOBAL, "Грациозный попрыгун", Lists.newArrayList("Пройдите сквозь все кольца дроппера", "в лобби или хабе проекта."), 250, 1000),
    ANCIENT_CHEST(AchievementSection.GLOBAL, "Собиратель древностей", Lists.newArrayList("Откройте свой первый древний сундук."), 100, 1000),
    OWNER_CLICK(AchievementSection.HIDDEN, "Я его видел!", Lists.newArrayList("Кликните правой кнопкой мыши", "по владельцу проекта на хабе."), 0, 2500),
    JOIN_GUILD(AchievementSection.GLOBAL, "Нас - легион", Lists.newArrayList("Создайте или вступите в гильдию."), 0, 1000),
    PRISON_EVO_LEVEL(AchievementSection.PRISON_EVO, "Знатный копатель", new List[]{
            Lists.newArrayList("Достигните 30го уровня на режиме."),
            Lists.newArrayList("Достигните 40го уровня на режиме."),
            Lists.newArrayList("Достигните 50го уровня на режиме."),
            Lists.newArrayList("Достигните 60го уровня на режиме."),
            Lists.newArrayList("Достигните 65го уровня на режиме."),
            Lists.newArrayList("Достигните 69го уровня на режиме.")
    }, new int[]{200, 350, 500, 750, 1000, 1500}, new int[]{500, 750, 1000, 1250, 1500, 2000}),
    PRISON_EVO_BOSS_FOREST_KEEPER(AchievementSection.PRISON_EVO, "Новый хранитель леса", new List[]{
            Lists.newArrayList("Одолейте хранителя леса."),
            Lists.newArrayList("Одолейте хранителя леса в героическом режиме.")
    }, new int[]{100, 500}, new int[]{250, 1000}),
    PRISON_EVO_BOSS_BLAZE_KING(AchievementSection.PRISON_EVO, "Мастер над огнем", new List[]{
            Lists.newArrayList("Одолейте повелителя огня."),
            Lists.newArrayList("Одолейте повелителя огня в героическом режиме.")
    }, new int[]{250, 750}, new int[]{500, 1500}),
    PRISON_EVO_BOSS_AQUA_LORD(AchievementSection.PRISON_EVO, "Исследователь подводных глубин", new List[]{
            Lists.newArrayList("Одолейте Шайна, подводного владыку."),
            Lists.newArrayList("Одолейте Шайна, подводного владыку в героическом режиме.")
    }, new int[]{400, 1000}, new int[]{1000, 2000}),
    PRISON_EVO_BOSS_GLADIATOR(AchievementSection.PRISON_EVO, "Чемпион арены", new List[]{
            Lists.newArrayList("Одолейте Диаваля, последнего гладиатора."),
            Lists.newArrayList("Одолейте Диаваля, последнего гладиатора в героическом режиме.")
    }, new int[]{500, 1250}, new int[]{1500, 2500}),
    PRISON_EVO_MONEY(AchievementSection.PRISON_EVO, "Копатель-богатей", new List[]{
            Lists.newArrayList("Имейте на счету 100.000.000$."),
            Lists.newArrayList("Имейте на счету 1.000.000.000$."),
            Lists.newArrayList("Имейте на счету 10.000.000.000$."),
            Lists.newArrayList("Имейте на счету 100.000.000.000$."),
            Lists.newArrayList("Имейте на счету 1.000.000.000.000$."),
    }, new int[]{100, 250, 500, 750, 1000}, new int[]{500, 750, 1000, 1500, 2000}),
    PRISON_EVO_SHARDS(AchievementSection.PRISON_EVO, "Коллекционер шардов", new List[]{
            Lists.newArrayList("Имейте на счету 1000 шардов."),
            Lists.newArrayList("Имейте на счету 5000 шардов."),
            Lists.newArrayList("Имейте на счету 10000 шардов."),
    }, new int[]{250, 500, 1000}, new int[]{500, 1000, 2000}),
    FISH_KING_FISHES(AchievementSection.FISH_KING, "Рыбалка - моё всё", new List[]{
            Lists.newArrayList("Поймайте свою первую рыбку."),
            Lists.newArrayList("Поймайте 10 рыб."),
            Lists.newArrayList("Поймайте 100 рыб."),
            Lists.newArrayList("Поймайте 500 рыб."),
            Lists.newArrayList("Поймайте 1000 рыб."),
            Lists.newArrayList("Поймайте 3000 рыб."),
    }, new int[]{0, 0, 250, 500, 750, 1000}, new int[]{0, 100, 500, 1000, 2500, 5000}),
    FISH_KING_WEIGHT(AchievementSection.FISH_KING, "Тяжеловес на крючке!", new List[]{
            Lists.newArrayList("Поймайте рыбу весом не менее 3х килограмм."),
            Lists.newArrayList("Поймайте рыбу весом не менее 5ти килограмм."),
            Lists.newArrayList("Поймайте рыбу весом не менее 8ми килограмм."),
            Lists.newArrayList("Поймайте рыбу весом не менее 10ти килограмм."),
            Lists.newArrayList("Поймайте рыбу весом не менее 12ти килограмм."),
            Lists.newArrayList("Поймайте рыбу весом не менее 15ти килограмм."),
    }, new int[]{0, 100, 250, 400, 700, 1000}, new int[]{0, 250, 500, 1000, 1500, 2000}),
    FISH_KING_LEVEL(AchievementSection.FISH_KING, "Истинный рыболов", new List[]{
            Lists.newArrayList("Достигните 5го уровня на режиме."),
            Lists.newArrayList("Достигните 10го уровня на режиме."),
            Lists.newArrayList("Достигните 15го уровня на режиме."),
            Lists.newArrayList("Достигните 20го уровня на режиме."),
            Lists.newArrayList("Достигните 25го уровня на режиме."),
    }, new int[]{0, 100, 250, 500, 1000}, new int[]{0, 250, 500, 1000, 2500}),
    FISH_KING_ROD_LEVEL(AchievementSection.FISH_KING, "Мастер снастей", new List[]{
            Lists.newArrayList("Улучшите удочку до 3го уровня."),
            Lists.newArrayList("Улучшите удочку до 5го уровня."),
            Lists.newArrayList("Улучшите удочку до 8го уровня."),
            Lists.newArrayList("Улучшите удочку до 10го уровня."),
            Lists.newArrayList("Улучшите удочку до 12го уровня."),
    }, new int[]{0, 100, 250, 500, 1000}, new int[]{0, 250, 500, 1000, 2500}),
    SW_KILLS(AchievementSection.SKY_WARS, "Небесный убийца", new List[]{
            Lists.newArrayList("Совершите 10 убийств."),
            Lists.newArrayList("Совершите 100 убийств."),
            Lists.newArrayList("Совершите 200 убийств."),
            Lists.newArrayList("Совершите 400 убийств."),
            Lists.newArrayList("Совершите 800 убийств."),
            Lists.newArrayList("Совершите 1200 убийств."),
            Lists.newArrayList("Совершите 2500 убийств."),
            Lists.newArrayList("Совершите 5000 убийств."),
    }, new int[]{0, 100, 200, 300, 400, 500, 1000, 2000}, new int[]{50, 200, 400, 600, 800, 1000, 2000, 4000}),
    SW_WINS(AchievementSection.SKY_WARS, "Небесный чемпион", new List[]{
            Lists.newArrayList("Победите в игре."),
            Lists.newArrayList("Победите в 5 играх."),
            Lists.newArrayList("Победите в 20 играх."),
            Lists.newArrayList("Победите в 50 играх."),
            Lists.newArrayList("Победите в 100 играх."),
            Lists.newArrayList("Победите в 200 играх."),
            Lists.newArrayList("Победите в 500 играх."),
            Lists.newArrayList("Победите в 1000 игр."),
    }, new int[]{0, 100, 200, 300, 400, 500, 1000, 2000}, new int[]{50, 200, 400, 600, 800, 1000, 2000, 4000}),
    SW_SOLO_GAME_KILLS(AchievementSection.SKY_WARS, "Чудовище на небесах", new List[]{
            Lists.newArrayList("Совершите 3 убийства за одну игру."),
            Lists.newArrayList("Совершите 4 убийства за одну игру."),
            Lists.newArrayList("Совершите 5 убийств за одну игру."),
            Lists.newArrayList("Совершите 6 убийств за одну игру."),
            Lists.newArrayList("Совершите 7 убийств за одну игру.")
    }, new int[]{100, 200, 300, 400, 500}, new int[]{200, 400, 600, 800, 1000}),
    SW_MASTER(AchievementSection.SKY_WARS, "Мастер СкайВарса", new List[]{
            Lists.newArrayList("Победите каждым классом один раз."),
            Lists.newArrayList("Победите каждым классом 5 раз."),
            Lists.newArrayList("Победите каждым классом 10 раз."),
            Lists.newArrayList("Победите каждым классом 25 раз."),
            Lists.newArrayList("Победите каждым классом 50 раз.")
    }, new int[]{250, 500, 750, 1000, 2000}, new int[]{500, 1000, 1500, 2000, 4000}),
    SW_GAME_LEVEL(AchievementSection.SKY_WARS, "Уровень игры: Превосходно", new List[]{
            Lists.newArrayList("Покажите очень высокий уровень игры, победив."),
            Lists.newArrayList("Покажите очень высокий уровень игры, победив в",
                    "игре со средним высоким уровнем."),
            Lists.newArrayList("Покажите очень высокий уровень игры, победив в",
                    "игре со средним очень высоким уровнем."),
    }, new int[]{100, 250, 500}, new int[]{200, 500, 1000}),
    SW_RATING(AchievementSection.SKY_WARS, "Просто лучший", new List[]{
            Lists.newArrayList("Достигните 1250 рейтинга."),
            Lists.newArrayList("Достигните 1500 рейтинга."),
            Lists.newArrayList("Достигните 1750 рейтинга."),
            Lists.newArrayList("Достигните 2000 рейтинга."),
            Lists.newArrayList("Достигните 2200 рейтинга."),
            Lists.newArrayList("Достигните 2400 рейтинга."),
            Lists.newArrayList("Достигните 2600 рейтинга."),
            Lists.newArrayList("Достигните 2700 рейтинга."),
            Lists.newArrayList("Достигните 2800 рейтинга."),
            Lists.newArrayList("Достигните 2900 рейтинга."),
            Lists.newArrayList("Достигните 3000 рейтинга.")
    }, new int[]{100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100}, new int[]{200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000, 2200}),
    ZERUS_LEVEL(AchievementSection.ZERUS, "Искатель приключений", new List[]{
            Lists.newArrayList("Достигните 9 уровня."),
            Lists.newArrayList("Достигните 19 уровня."),
            Lists.newArrayList("Достигните 29 уровня."),
            Lists.newArrayList("Достигните 39 уровня."),
            Lists.newArrayList("Достигните 49 уровня."),
            Lists.newArrayList("Достигните 59 уровня."),
            Lists.newArrayList("Достигните 69 уровня.")
    }, new int[]{100, 200, 400, 800, 1200, 1600, 2000}, new int[]{200, 400, 800, 1600, 2400, 3200, 4000}),
    ZERUS_BOSS(AchievementSection.ZERUS, "Гроза боссов", new List[]{
            Lists.newArrayList("Одолейте босса 18 уровня или выше."),
            Lists.newArrayList("Одолейте босса 30 уровня или выше."),
            Lists.newArrayList("Одолейте босса 39 уровня или выше."),
            Lists.newArrayList("Одолейте босса 49 уровня или выше."),
            Lists.newArrayList("Одолейте босса 59 уровня или выше."),
            Lists.newArrayList("Одолейте босса 69 или 70 уровня.")
    }, new int[]{200, 400, 800, 1600, 2400, 3200}, new int[]{400, 800, 1600, 3200, 4800, 6400}),
    ZERUS_MASTERY(AchievementSection.ZERUS, "Знатный ремесленник", new List[]{
            Lists.newArrayList(
                    "Повысьте уровень владения любым",
                    "ремеслом до 100."
            ),
            Lists.newArrayList(
                    "Повысьте уровень владения любым",
                    "ремеслом до 200."
            ),
            Lists.newArrayList(
                    "Повысьте уровень владения любым",
                    "ремеслом до 300."
            ),
            Lists.newArrayList(
                    "Повысьте уровень владения любым",
                    "ремеслом до 400."
            )
    }, new int[]{250, 500, 1000, 1500}, new int[]{500, 1000, 2000, 3000}),
    ZERUS_PROFESSION(AchievementSection.ZERUS, "Не просто кто-то", new List[]{
            Lists.newArrayList("Получите первую профессию."),
            Lists.newArrayList("Получите вторую профессию.")
    }, new int[]{250, 750}, new int[]{500, 1500}),
    ZERUS_ENCHANTING(AchievementSection.ZERUS, "Мастер заточки", new List[]{
            Lists.newArrayList("Заточите предмет на +5."),
            Lists.newArrayList("Заточите предмет на +6."),
            Lists.newArrayList("Заточите предмет на +7.")
    }, new int[]{250, 500, 1000}, new int[]{500, 1000, 2000}),
    ZERUS_QUESTS(AchievementSection.ZERUS, "Исследователь мира", new List[]{
            Lists.newArrayList("Выполните 10 заданий."),
            Lists.newArrayList("Выполните 50 заданий."),
            Lists.newArrayList("Выполните 100 заданий."),
    }, new int[]{0, 250, 500}, new int[]{100, 500, 1000});

    private final AchievementSection section;
    private final String name;
    private final List<String>[] descriptions;
    private final int[] goldRewards;
    private final int[] networkLevelingExperienceRewards;

    Achievement(AchievementSection section, String name, List<String>[] descriptions, int[] goldRewards, int[] networkLevelingExperienceRewards) {
        this.section = section;
        this.name = name;
        this.descriptions = descriptions;
        this.goldRewards = goldRewards;
        this.networkLevelingExperienceRewards = networkLevelingExperienceRewards;
    }

    Achievement(AchievementSection section, String name, List<String>[] descriptions) {
        this(section, name, descriptions, null, null);
    }

    Achievement(AchievementSection section, String name, List<String> description, Integer goldReward, Integer networkLevelingExperienceReward) {
        this(section, name, new List[]{description}, goldReward == null ? null : new int[]{goldReward}, networkLevelingExperienceReward == null ? null : new int[]{networkLevelingExperienceReward});
    }

    Achievement(AchievementSection section, String name, List<String> description) {
        this(section, name, description, null, null);
    }

    /**
     * Получить секцию достижения.
     * @return секция достижения.
     */
    public AchievementSection getSection() {
        return this.section;
    }

    /**
     * Получить название достижения.
     * @return название достижения.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Получить описание достижения определенного уровня.
     * @param level уровень достижения.
     * @return описание достижения определенного уровня.
     * @throws IllegalArgumentException если переданный уровень < 1 или больше максимального уровня достижения.
     */
    public List<String> getDescription(int level) {
        Preconditions.checkArgument(level > 0 && level <= this.descriptions.length);
        return Collections.unmodifiableList(this.descriptions[level - 1]);
    }

    /**
     * Получить количество золота, которое игрок получит при выполнении указанного уровня достижения.
     * @return количество золота, которое игрок получит при выполнении указанного уровня достижения.
     * @throws IllegalArgumentException если переданный уровень < 1 или больше максимального уровня достижения.
     */
    public int getGoldReward(int level) {
        Preconditions.checkArgument(level > 0 && level <= this.descriptions.length);
        return this.goldRewards == null || level > this.goldRewards.length ? 0 : this.goldRewards[level - 1];
    }

    /**
     * Получить количество межсерверного опыта, которое игрок получит при выполнении указанного уровня достижения.
     * @return количество межсерверного опыта, которое игрок получит при выполнении указанного уровня достижения.
     * @throws IllegalArgumentException если переданный уровень < 1 или больше максимального уровня достижения.
     */
    public int getNetworkLevelingExperienceReward(int level) {
        Preconditions.checkArgument(level > 0 && level <= this.descriptions.length);
        return this.networkLevelingExperienceRewards == null || level > this.networkLevelingExperienceRewards.length ? 0 : this.networkLevelingExperienceRewards[level - 1];
    }

    /**
     * Получить максимальный уровень достижения.
     * @return максимальный уровень достижения.
     */
    public int getLevels() {
        return this.descriptions.length;
    }

}
