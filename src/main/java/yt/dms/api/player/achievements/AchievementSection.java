package yt.dms.api.player.achievements;

/**
 * Created by k.shandurenko on 23.07.2018
 */
public enum AchievementSection {
    GLOBAL,
    SKY_WARS,
    BED_WARS,
    AZERUS,
    COLONY_WARS,
    MY_LITTLE_FARM,
    ANNIHILATION,
    HIDDEN,
    PRISON_EVO,
    FISH_KING,
    ZERUS
}
