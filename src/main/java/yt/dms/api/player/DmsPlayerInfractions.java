package yt.dms.api.player;

/**
 * Created by RINES on 03.06.2018.
 */
public interface DmsPlayerInfractions {

    /**
     * Получить информацию о муте.
     * @return информация о муте.
     */
    InfractionsSection getMuteInfo();

    /**
     * Получить информацию о бане.
     * @return информация о бане.
     */
    InfractionsSection getBanInfo();

    /**
     * Проверка на то, замучен ли игрок.
     * @return замучен ли игрок.
     */
    default boolean isMuted() {
        return getMuteInfo().isActive();
    }

    /**
     * Проверка на то, забанен ли игрок.
     * @return забанен ли игрок.
     */
    default boolean isBanned() {
        return getBanInfo().isActive();
    }

    interface InfractionsSection {

        /**
         * Активна ли данная секция (замучен/забанен ли игрок).
         * @return true/false.
         */
        boolean isActive();

        /**
         * Если секция активна, получение имени того, кто выдал наказание.
         * @return имя наказавшего.
         */
        String getEnforcerName();

        /**
         * Если секция активна, проверка того, является ли тот, кто выдал наказание, игроков (он может являться консолью).
         * @return true/false.
         */
        boolean isEnforcerPlayer();

        /**
         * Если секция активна, получение причины наказания.
         * @return причина наказания.
         */
        String getReason();

        /**
         * Если секция активна, получение временной метки, когда оно спадет.
         * @return
         */
        long getEndTime();

        /**
         * Установить секцию в активное состояние, если она неактивна.
         * @param enforcerName имя наказавшего.
         * @param reason причина наказания.
         * @param durationInMillis длительность наказания в миллисекундах.
         */
        void setActive(String enforcerName, String reason, long durationInMillis);

        /**
         * Установить секцию в неактивное состояние, если она активна.
         */
        void setInactive(String enforcerName);

    }

}
