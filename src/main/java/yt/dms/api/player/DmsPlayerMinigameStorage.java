package yt.dms.api.player;

import com.google.common.base.Preconditions;
import org.bukkit.Location;
import yt.dms.api.DMS;
import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 20.09.2018
 */
@SpigotOnly
public interface DmsPlayerMinigameStorage {

    /**
     * @see DmsPlayerMinigameStorage#getThing(Class, String)
     * @param name название локации
     * @return локацию или null
     * @throws IllegalArgumentException если название локации null
     */
    default Location getLocation(String name) throws IllegalArgumentException {
        Preconditions.checkArgument(name != null, "Given name is null");
        String value = getString("loc_" + name);
        return value == null ? null : DMS.getSerializator().getLocationsSerializer().deserializeLocation(value, null);
    }

    /**
     * @see DmsPlayerMinigameStorage#addThing(Class, String, Object)
     * @param name название локации
     * @param location локация
     * @throws IllegalArgumentException если название локации или локация null
     */
    default void addLocation(String name, Location location) throws IllegalArgumentException {
        Preconditions.checkArgument(name != null, "Given name is null");
        Preconditions.checkArgument(location != null, "Given location is null");
        addString("loc_" + name, DMS.getSerializator().getLocationsSerializer().serializeLocation(location));
    }

    /**
     * @see DmsPlayerMinigameStorage#addLocation(String, Location)
     */
    default void setLocation(String name, Location location) throws IllegalArgumentException {
        addLocation(name, location);
    }

    /**
     * @see DmsPlayerMinigameStorage#removeThing(Class, String)
     * @param name название локации
     * @throws IllegalArgumentException если название локации null
     */
    default void removeLocation(String name) throws IllegalArgumentException {
        Preconditions.checkArgument(name != null, "Given name is null");
        removeString("loc_" + name);
    }

    /**
     * @see DmsPlayerMinigameStorage#getThing(Class, String)
     * @param name название лонга
     * @return лонг или null
     * @throws IllegalArgumentException если название лонга null
     */
    default Long getLong(String name) throws IllegalArgumentException {
        return getThing(Long.class, name);
    }

    /**
     * @see DmsPlayerMinigameStorage#addThing(Class, String, Object)
     * @param name название лонга
     * @param value значение
     * @throws IllegalArgumentException если название лонга null
     */
    default void addLong(String name, long value) throws IllegalArgumentException {
        addThing(Long.class, name, value);
    }

    /**
     * @see DmsPlayerMinigameStorage#addLong(String, long)
     */
    default void setLong(String name, long value) throws IllegalArgumentException {
        addLong(name, value);
    }

    /**
     * @see DmsPlayerMinigameStorage#removeThing(Class, String)
     * @param name название лонга
     * @throws IllegalArgumentException если название лонга null
     */
    default void removeLong(String name) throws IllegalArgumentException {
        removeThing(Long.class, name);
    }

    /**
     * @see DmsPlayerMinigameStorage#getThing(Class, String)
     * @param name название инта
     * @return инт или null
     * @throws IllegalArgumentException если название инта null
     */
    default Integer getInt(String name) throws IllegalArgumentException {
        return getThing(Integer.class, name);
    }

    /**
     * @see DmsPlayerMinigameStorage#addThing(Class, String, Object)
     * @param name название инта
     * @param value значение
     * @throws IllegalArgumentException если название инта null
     */
    default void addInt(String name, int value) throws IllegalArgumentException {
        addThing(Integer.class, name, value);
    }

    /**
     * @see DmsPlayerMinigameStorage#addInt(String, int)
     */
    default void setInt(String name, int value) throws IllegalArgumentException {
        addInt(name, value);
    }

    /**
     * @see DmsPlayerMinigameStorage#removeThing(Class, String)
     * @param name название инта
     * @throws IllegalArgumentException если название инта null
     */
    default void removeInt(String name) throws IllegalArgumentException {
        removeThing(Integer.class, name);
    }

    /**
     * @see DmsPlayerMinigameStorage#getThing(Class, String)
     * @param name название строки
     * @return строка или null
     * @throws IllegalArgumentException если название строки null
     */
    default String getString(String name) throws IllegalArgumentException {
        return getThing(String.class, name);
    }

    /**
     * @see DmsPlayerMinigameStorage#addThing(Class, String, Object)
     * @param name название строки
     * @param value значение
     * @throws IllegalArgumentException если название строки или значение null
     */
    default void addString(String name, String value) throws IllegalArgumentException {
        addThing(String.class, name, value);
    }

    /**
     * @see DmsPlayerMinigameStorage#addString(String, String)
     */
    default void setString(String name, String value) throws IllegalArgumentException {
        addString(name, value);
    }

    /**
     * @see DmsPlayerMinigameStorage#removeThing(Class, String)
     * @param name название строки
     * @throws IllegalArgumentException если название строки null
     */
    default void removeString(String name) throws IllegalArgumentException {
        removeThing(String.class, name);
    }

    /**
     * Получить сохранненое поле для данного игрока на этом минирежиме.
     * @param thingClass класс сущности.
     * @param thingName название сущности.
     * @param <T> тип сущности.
     * @return сущность или null, если она не сохранена.
     * @throws IllegalArgumentException если передан неподдерживаемый класс сущности или название сущности null.
     */
    <T> T getThing(Class<T> thingClass, String thingName) throws IllegalArgumentException;

    /**
     * Добавить/сохранить поле для данного игрока на этом минирежиме.
     * @param thingClass класс сущности.
     * @param thingName название сущности.
     * @param thing сущность.
     * @param <T> тип сущности.
     * @throws IllegalArgumentException если передан неподдерживаемый класс сущности или название сущности null
     *                                  или сущность null.
     */
    <T> void addThing(Class<T> thingClass, String thingName, T thing) throws IllegalArgumentException;

    /**
     * Удалить поле для данного игрока на этом минирежиме.
     * @param thingClass класс сущности.
     * @param thingName название сущности.
     * @param <T> тип сущности.
     * @throws IllegalArgumentException если передан неподдерживаемый класс сущности или название сущности null.
     */
    <T> void removeThing(Class<T> thingClass, String thingName) throws IllegalArgumentException;

}
