package yt.dms.api.player;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 09.12.2018
 */
@SpigotOnly
public interface DmsPlayerCosmetics {

    /**
     * Добавить этому игроку случайный древний сундук (который может быть добавлен случайно).
     */
    void addRandomChest();

    /**
     * Проверить, имеется ли у этого игрока косметический предмет с указанным идентификатором.
     * @param itemID идентификатор предмета.
     * @return true/false.
     */
    boolean hasItem(int itemID);

    /**
     * Проверить, имеется ли у данного игрока доступ к косметическому предмету с указанным идентификатором.
     * При этом не факт, что у игрока имеется данный эффект, т.к. он может быть доступен ему
     * исходя из его статуса (донат/ютубер/админ и тд).
     * @param itemID идентификатор предмета.
     * @return true/false.
     */
    boolean hasAccessToItem(int itemID);

}
