package yt.dms.api.player;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import yt.dms.api.annotation.BungeeOnly;

/**
 * Этот интерфейс реализуется вместо обычного DmsPlayer на прокси-серверах.
 * Created by k.shandurenko on 03.10.2018
 */
@BungeeOnly
public interface DmsBungeePlayer extends DmsPlayer {

    /**
     * Возвращает bungee-игрока, который является данным DmsPlayer.
     * @return null, если игрок не в сети на данном прокси-сервере, иначе bungee-игрока.
     */
    default ProxiedPlayer getBungeePlayer() {
        return ProxyServer.getInstance().getPlayer(getName());
    }

    /**
     * Проверка на то, онлайн ли игрок на данном прокси-сервере.
     * @return true/false.
     */
    default boolean isOnlineThere() {
        return getBungeePlayer() != null;
    }

}
