package yt.dms.api.player;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by RINES on 03.06.2018.
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum PermissionGroup {

    PLAYER(0, "&7", "&7", "&7Игрок", "&7Игроки", 1f),
    VIP(100, "&6&lVIP &6", "&6", "&6&lVIP", "&6&lVIP", 2f),
    BETA(101, "&6&lβeta &6", "&6", "&f&lБета-Тестер", "&f&lβeta", 2f),
    VIP_PLUS(200, "&3&lVIP+ &3", "&3", "&3&lVIP+", "&3&lVIP+", 3f),
    RICH(300, "&b&lRICH &b", "&b", "&b&lRICH", "&b&lRICH", 5f),
    RICH_PLUS(301, "&c&lСпонсор &c", "&c", "&c&lСпонсор", "&c&lСпонсоры", 10f),
    YOUTUBE(350, "&6&lYOUTUBE &6", "&6", "&6&lYoutube", "&6&lYoutubers", 5f),
    YOUTUBE_PLUS(351, "&6&lYOUTUBE+ &6", "&6", "&6&lYoutube+", "&6&lYoutubers+", 6f),
    YOUTUBE_PLUS_PLUS(352, "&6&lYOUTUBE++ &6", "&6", "&6&lYoutube++", "&6&lYoutubers++", 7f),
    SPECIAL(399, "&d&lUNIQUE &d", "&d", "&d&lВажная персона", "&d&lUniques", 7f),
    JRBUILDER(400, "&e&lМл. Строитель &e", "&e", "&e&lМл. Строитель", "&e&lМл. Строители", 1.5f),
    BUILDER(425, "&e&lСтроитель &e", "&e", "&e&lСтроитель", "&e&lСтроители", 2f),
    SRBUILDER(450, "&e&lСт. Строитель &e", "&e", "&e&lСт. Строитель", "&e&lСт. Строители", 3f),
    QA(475, "&7&lМл. Тестер &7", "&7", "&7&lМл. Тестер", "&7&lМл. Тестеры", 1.5f),
    MIDQA(476, "&7&lТестер &7", "&7", "&7&lТестер", "&7&lТестеры", 2f),
    SRQA(477, "&7&lСт. Тестер &7", "&7", "&7&lСт. Тестер", "&7&lСт. Тестеры", 3f),
    HELPER(500, "&a&lМл. Модератор &a", "&a", "&a&lМл. Модератор", "&a&lМл. Модераторы", 2f),
    MODERATOR(600, "&a&lМодератор &a", "&a", "&a&lМодератор", "&a&lМодераторы", 3f),
    SRMODERATOR(650, "&a&lСт. Модератор &a", "&a", "&a&lСт. Модератор", "&a&lСт. Модераторы", 5f),
    DEVELOPER(700, "&9&lМл. Разработчик &9", "&9", "&9&lМл. Разработчик", "&9&lМл. Разработчики", 10f),
    MIDDEVELOPER(701, "&9&lРазработчик &9", "&9", "&9&lРазработчик", "&9&lРазработчики", 10f),
    SRDEVELOPER(702, "&9&lСт. Разработчик &9", "&9", "&9&lСт. Разработчик", "&9&lСт. Разработчики", 10f),
    CURATOR(750, "&c&lКуратор &c", "&c", "&c&lКуратор", "&c&lКураторы", 10f),
    ADMINISTRATOR(800, "&c&lАдминистратор &c", "&c", "&c&lАдминистратор", "&c&lАдминистраторы", 10f),
    OWNER(900, "&4&lВладелец ", "&4&l", "&4&lВладелец", "&4&lВладельцы", 10f),
    STAR(-1, "&6&lSTAR &6", "&6", "&6&lSTAR", "&6&lSTARs", 1F);

    private final static Map<Integer, PermissionGroup> byIds;

    static {
        byIds = new HashMap<>(values().length);
        for (PermissionGroup pg : values())
            byIds.put(pg.getId(), pg);
    }

    @Getter
    private final int id;

    @Getter
    private final String uncoloredPrefix;

    @Getter
    private final String uncoloredNameColor;

    @Getter
    private final String normalName;

    @Getter
    private final String pluralName;

    @Getter
    private final float coinsMultiplier;

    public final boolean isStaff() {
        return getId() >= JRBUILDER.getId();
    }

    public String getName() {
        return name();
    }

    public static PermissionGroup getById(int id) {
        return byIds.get(id);
    }

}
