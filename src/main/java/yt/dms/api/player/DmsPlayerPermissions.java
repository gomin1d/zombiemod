package yt.dms.api.player;

import java.util.Collection;

/**
 * Created by RINES on 03.06.2018.
 */
public interface DmsPlayerPermissions {

    /**
     * Получить основную группу игрока.
     * @return группа.
     */
    PermissionGroup getMainPermissionGroup();

    /**
     * Получить все группы игрока.
     * @return все группы игрока.
     */
    Collection<PermissionGroup> getAllPermissionGroups();

    /**
     * Проверка на то, есть ли у игрока группа.
     * Заметьте: у игрока, например, может не быть группы VIP, но могут быть ее права.
     * @param group группа.
     * @return true/false.
     */
    boolean hasPermissionGroup(PermissionGroup group);

    /**
     * Добавить группу игроку.
     * @param enforcerName ник выдавшего группу.
     * @param group группа.
     */
    void addPermissionGroup(String enforcerName, PermissionGroup group);

    /**
     * Забрать группу у игрока.
     * @param enforcerName ник забравшего группу.
     * @param group группа.
     */
    void removePermissionGroup(String enforcerName, PermissionGroup group);

    /**
     * Получить максимальную группу игрока, не являющуюся группой сотрудника проекта.
     */
    PermissionGroup getMaximalNonStaffGroup();

    /**
     * Получить модификатор серебра для игрока от его правовых групп.
     * Для обычных игроков это значение равно 1.
     * @return модификатор серебра для игрока.
     */
    float getSilverModifier();

    /**
     * Получить силу голоса этого игрока при выборе карты.
     * Для обычных игроков это значение равно 1.
     * @return сила голоса этого игрока при выборе карты.
     */
    int getMapVotePower();

    default boolean isOwner() {
        return hasPermissionGroup(PermissionGroup.OWNER);
    }

    default boolean isAdministrator() {
        return isOwner() || hasPermissionGroup(PermissionGroup.ADMINISTRATOR);
    }

    default boolean isCurator() {
        return isAdministrator() || hasPermissionGroup(PermissionGroup.CURATOR);
    }

    default boolean isHeadModerator() {
        return isCurator() && isSrModerator();
    }

    default boolean isHeadBuilder() {
        return isCurator() && isSrBuilder();
    }

    default boolean isHeadQA() {
        return isCurator() && isSrQA();
    }

    default boolean isSrDeveloper() {
        return isAdministrator() || hasPermissionGroup(PermissionGroup.SRDEVELOPER);
    }

    default boolean isSeniorDeveloper() {
        return isSrDeveloper();
    }

    default boolean isMidDeveloper() {
        return isSrDeveloper() || hasPermissionGroup(PermissionGroup.MIDDEVELOPER);
    }

    default boolean isMiddleDeveloper() {
        return isMidDeveloper();
    }

    default boolean isJrDeveloper() {
        return isMidDeveloper() || hasPermissionGroup(PermissionGroup.DEVELOPER);
    }

    default boolean isJuniorDeveloper() {
        return isJrDeveloper();
    }

    default boolean isDeveloper() {
        return isJrDeveloper();
    }

    default boolean isSeniorModerator() {
        return isAdministrator() || hasPermissionGroup(PermissionGroup.SRMODERATOR);
    }

    default boolean isSrModerator() {
        return isSeniorModerator();
    }

    default boolean isModerator() {
        return isSeniorModerator() || hasPermissionGroup(PermissionGroup.MODERATOR);
    }

    default boolean isJuniorModerator() {
        return isModerator() || hasPermissionGroup(PermissionGroup.HELPER);
    }

    default boolean isJrModerator() {
        return isJuniorModerator();
    }

    default boolean isHelper() {
        return isJuniorModerator();
    }

    default boolean isQA() {
        return isMiddleQA() || hasPermissionGroup(PermissionGroup.QA);
    }

    default boolean isMiddleQA() {
        return isSrQA() || hasPermissionGroup(PermissionGroup.MIDQA);
    }

    default boolean isSrQA() {
        return isAdministrator() || hasPermissionGroup(PermissionGroup.SRQA);
    }

    default boolean isSeniorQA() {
        return isSrQA();
    }

    default boolean isTester() {
        return isQA();
    }

    default boolean isSeniorBuilder() {
        return isAdministrator() || hasPermissionGroup(PermissionGroup.SRBUILDER);
    }

    default boolean isSrBuilder() {
        return isSeniorBuilder();
    }

    default boolean isBuilder() {
        return isSeniorBuilder() || hasPermissionGroup(PermissionGroup.BUILDER);
    }

    default boolean isJuniorBuilder() {
        return isBuilder() || hasPermissionGroup(PermissionGroup.JRBUILDER);
    }

    default boolean isJrBuilder() {
        return isJuniorBuilder();
    }

    default boolean isStaff() {
        return getMainPermissionGroup().isStaff();
    }

    default boolean isYoutuberPlusPlus() {
        return isAdministrator() || hasPermissionGroup(PermissionGroup.YOUTUBE_PLUS_PLUS);
    }

    default boolean isYoutuberPlus() {
        return isYoutuberPlusPlus() || hasPermissionGroup(PermissionGroup.YOUTUBE_PLUS);
    }

    default boolean isYoutuber() {
        return isYoutuberPlus() || hasPermissionGroup(PermissionGroup.YOUTUBE);
    }

    default boolean isUnique() {
        return isAdministrator() || hasPermissionGroup(PermissionGroup.SPECIAL);
    }

    default boolean isSpecial() {
        return isUnique();
    }

    default boolean isStar() {
        return hasPermissionGroup(PermissionGroup.STAR);
    }

    default boolean isSponsor() {
        return isUnique() || isCurator() || isYoutuberPlusPlus() || hasPermissionGroup(PermissionGroup.RICH_PLUS);
    }

    default boolean isRichPlus() {
        return isSponsor();
    }

    default boolean isRich() {
        return isSponsor() || isDeveloper() || isSrQA() || isSeniorModerator() || isSeniorBuilder() || isYoutuber() || hasPermissionGroup(PermissionGroup.RICH);
    }

    default boolean isVipPlus() {
        return isRich() || isMiddleQA() || isModerator() || isBuilder() || hasPermissionGroup(PermissionGroup.VIP_PLUS);
    }

    default boolean isVip() {
        return isVipPlus() || isTester() || isJrModerator() || isJrBuilder() || hasPermissionGroup(PermissionGroup.VIP) || hasPermissionGroup(PermissionGroup.BETA);
    }

}
