package yt.dms.api.player;

/**
 * Created by k.shandurenko on 15.07.2018
 */
public interface DmsPlayerOnlineInfo {

    /**
     * Проверить, находится ли игрок в онлайне на проекте.
     * @return true/false.
     */
    boolean isOnline();

    /**
     * Получить имя сервера, на котором в данный момент находится игрок.
     * Если он не в сети, вернется null.
     * @return имя сервера, на котором в данный момент находится игрок, или null.
     */
    String getCurrentServerName();

    /**
     * Получить имя последнего сервера, на котором игрок находился (или находится).
     * @return имя последнего сервера, на котором игрок находился (или находится).
     */
    String getLastServerName();

    /**
     * Получить таймштамп последнего онлайна игрока.
     * Если он в сети, вернется 0.
     * @return таймштамп последнего онлайна игрока или 0.
     */
    long getLastOnlineTimestamp();

    /**
     * Получить ip-адрес игрока (со времени его последнего захода).
     * @return ip-адрес игрока.
     */
    String getLastAddress();

}
