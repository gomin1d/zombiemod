package yt.dms.api.player;

/**
 * Created by k.shandurenko on 15.07.2018
 */
public interface DmsPlayerFriends {

    /**
     * Получить максимальное количество друзей, которое может завести этот игрок.
     * @return максимальное количество друзей, которое может завести этот игрок.
     */
    int getFriendsLimit();

    /**
     * Отправить запрос на дружбу указанному игроку.
     * @param playerName ник игрока, которому отправляем запрос.
     * @throws IllegalArgumentException если переданный ник = null, короче 3 или длиннее 16 символов.
     */
    void sendFriendRequestTo(String playerName) throws IllegalArgumentException;

    /**
     * Принять запрос на дружбу от указанного игрока.
     * @param playerName ник игрока, запрос которого одобряем.
     * @throws IllegalArgumentException если переданный ник = null, короче 3 или длиннее 16 символов.
     */
    void acceptRequest(String playerName) throws IllegalArgumentException;

    /**
     * Прекратить дружбу с указанным игроком.
     * @param playerName ник игрока, с которым прекращаем дружить.
     * @throws IllegalArgumentException если переданный ник = null, короче 3 или длиннее 16 символов.
     */
    void removeFriend(String playerName) throws IllegalArgumentException;

    /**
     * Отклонить запрос на дружбу от указанного игрока.
     * @param playerName ник игрока, запрос которого отклоняем.
     * @throws IllegalArgumentException если переданный ник = null, короче 3 или длиннее 16 символов.
     */
    void declineRequest(String playerName) throws IllegalArgumentException;

    /**
     * Проверить, является ли указанный игрок другом этого игрока.
     * @param playerName ник игрока, которого проверяем.
     * @throws IllegalArgumentException если переданный ник = null, короче 3 или длиннее 16 символов.
     */
    boolean isFriend(String playerName) throws IllegalArgumentException;

    /**
     * Проверить, является ли указанный игрок другом этого игрока.
     * @param player игрок, которого проверяем.
     * @throws IllegalArgumentException если переданный ник = null, короче 3 или длиннее 16 символов.
     */
    default boolean isFriend(DmsPlayer player) {
        return isFriend(player.getName());
    }

    /**
     * Проверить, есть ли у этого игрока активный запрос на дружбу от указанного игрока.
     * @param playerName ник игрока, которого проверяем.
     * @throws IllegalArgumentException если переданный ник = null, короче 3 или длиннее 16 символов.
     */
    boolean hasFriendRequestFrom(String playerName) throws IllegalArgumentException;

    /**
     * Проверить, есть ли у этого игрока активный запрос на дружбу от указанного игрока.
     * @param player игрок, которого проверяем.
     * @throws IllegalArgumentException если переданный ник = null, короче 3 или длиннее 16 символов.
     */
    default boolean hasFriendRequestFrom(DmsPlayer player) {
        return hasFriendRequestFrom(player.getName());
    }

    /**
     * Вывести конкретную страницу списка друзей этого игрока для него же.
     * @param page номер страницы.
     * @throws IllegalArgumentException если номер страницы меньше 1.
     */
    void printFriends(int page) throws IllegalArgumentException;

    /**
     * Изменить настройку приема заявок в друзья для этого игрока.
     * @return true, если после изменения игрок может принимать заявки в друзья, и false в другом случае.
     */
    boolean toggleRequestsReception();

}
