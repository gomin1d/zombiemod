package yt.dms.api.player;

/**
 * Created by RINES on 03.06.2018.
 */
public interface DmsPlayerGold {

    /**
     * Получить количество золота у игрока.
     * @return количество золота у игрока.
     */
    int getAmount();

    /**
     * Изменить золото у игрока. Суммарное кол-во золота не должно быть меньше 0.
     * @param goldDelta количество золота, которое нужно добавить/убавить.
     * @throws IllegalArgumentException если после изменения золото игрока станет меньше 0.
     */
    void change(int goldDelta) throws IllegalArgumentException;

}
