package yt.dms.api;

import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.minigame.DmsMinigame;
import yt.dms.api.minigame.DmsMinigameTeamSorter;
import yt.dms.api.minigame.plugin.DmsMinigameSharder;
import yt.dms.api.party.DmsPartyRegistry;
import yt.dms.api.spigot.*;
import yt.dms.api.spigot.cosmetics.CollectiblesRegistry;
import yt.dms.api.spigot.entity.SEntityManipulationManager;
import yt.dms.api.spigot.phantom.block.PhantomBlockFactory;
import yt.dms.api.spigot.phantom.block.world.PhantomWorldFactory;
import yt.dms.api.spigot.phantom.entity.PhantomEntityFactory;

/**
 * Created by RINES on 05.06.2018.
 */
@SpigotOnly
public interface DmsSpigot {

    /**
     * Получить ссылку сервера (например, @hub для хабов или @swlobby для лобби скайварсов).
     * @return имя ссылки с @ или null, если ее нет.
     */
    String getServerLink();

    /**
     * Получить тип сервера (например, hub, swlobby, swd).
     * @return тип сервера.
     */
    String getServerType();

    /**
     * Получить внутренний тип сервера (например, GAME, LOBBY, HUB, AFK).
     * @return внутренний тип сервера.
     */
    String getInternalServerType();

    /**
     * Получить внутренний тип сервера в виде константы.
     * @see DmsSpigot#getInternalServerType()
     * @return внутренний тип сервера в виде константы.
     */
    default ServerType getInternalServerTypeAsEnum() {
        String type = getInternalServerType();
        try {
            return ServerType.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException ignored) {
            return ServerType.ANOTHER;
        }
    }

    /**
     * Получить имя сервера, если оно уже получено (например, hub, hub2 или azerus1-2).
     * @return имя сервера или null, если оно еще не получено.
     */
    String getServerName();

    /**
     * Получить имя лобби-сервера или ссылку на него (например, @swlobby или hub1), если для данного сервера
     * задан лобби-сервер. В противном случае вернет null.
     * @return имя/ссылка на лобби-сервер или null, если лобби-сервер не указан для данного сервера.
     */
    String getLobbyName();

    /**
     * Утилиты спигота, связанные с банжикордом (с отправкой игроков на другие сервера).
     * @return реализацию SBungee.
     */
    SBungee getBungeeUtils();

    /**
     * Утилиты спигота, связанные с уведомлениями (например, уведомление о доступных новых заданиях),
     * которые появляются сверху в правой части экрана.
     * @return реализацию SNotification.
     */
    SNotification getNotificationUtils();

    /**
     * Утилиты спигота, связанные с таб-листом.
     * @return реализацию STab.
     */
    STab getTabUtils();

    /**
     * Утилиты спигота, связанные с восстановлением миров.
     * @return реализацию SWorldRestorer.
     */
    SWorldRestorer getWorldRestorer();

    /**
     * Утилиты спигота, связанные с ожиданием игроков из DMS-очередей на игру.
     * @return реализацию SQueues.
     */
    SQueues getQueueUtils();

    /**
     * Утилиты спигота, связанные с созданием собственных менюшек-инвентарей.
     * @return реализацию SMenu.
     */
    SMenu getMenuUtils();

    /**
     * Утилита спигота для работы с сайдбордами.
     * @return реализацию SSideBoard.
     */
    SSideBoard getSideBoardUtils();

    /**
     * Утилита спигота для работы с таблицами.
     * Работа с таблицами не потокобезопасна (но они не зависят от серверного потока)!
     * @return реализацию STable.
     */
    STable getTableUtils();

    /**
     * Утилита спигота для работы с барами.
     * @return реализацию SBar.
     */
    SBar getBarUtils();

    /**
     * Утилита спигота для работы с гуи-кнопками.
     * @return реализацию SGuiButtons.
     */
    SGuiButtons getGuiButtonsUtils();

    /**
     * Утилита спигота для отправки сообщений в чат от имени игроков.
     * Используется для внутренней модерации (чтобы сообщения были кликабельны для модераторов, чтобы последние
     * могли выдать мут).
     * @return реализацию SMessagesSender.
     */
    SMessagesSender getMessagesSender();

    /**
     * Утилита спигота для контроля действий по флуду.
     * @return реализацию SFloodControl.
     */
    SFloodControl getFloodControl();

    /**
     * Утилита спигота для выбора карт на минирежимах.
     * @return реализацию SMapSelector.
     */
    SMapSelector getMapSelector();

    /**
     * Утилита спигота для управления серверными правами игроков (для обратной совместимости с другими плагинами).
     * @return реализацию SPermissions.
     */
    SPermissions getPermissions();

    /**
     * Утилита спигота для менеджмента дополнительных клиентских файлов, необходимых для игры на этом сервере,
     * а также их дальнейшего использования.
     * @return реализацию SAssets.
     */
    SAssets getAssets();

    /**
     * Утилита спигота для управления сущностями без прибегания к NMS.
     * @return реализацию SEntityManipulationManager.
     */
    SEntityManipulationManager getEntityManipulationManager();

    /**
     * Утилита спигота с некоторыми внутренними функциями, необходимых для обратной совместимости с этим апи.
     * @return реализацию SHooks.
     */
    SHooks getHooks();

    /**
     * Утилита спигота для работы с курсом золота/серебра, чтобы мы не дошли до дефолта экономики.
     * @return реализацию SEconomics.
     */
    SEconomics getEconomics();

    /**
     * Получить реализацию DmsMinigame минирежима данного сервера.
     * @return реализация DmsMinigame, если на этом сервере есть активный минирежим. Иначе - null.
     */
    DmsMinigame getMinigame();

    /**
     * Получить реализацию DmsCommandManager.
     * @return реализацию DmsCommandManager.
     */
    DmsSpigotCommandManager getCommandManager();

    /**
     * Получить реализацию CollectiblesRegistry, необходимую для регистрации новых
     * предметов в коллекцию.
     * @return реализацию CollectiblesRegistry.
     */
    CollectiblesRegistry getCollectiblesRegistry();

    /**
     * Получить реализацию PhantomEntityFactory для работы с сущностями на пакетах.
     * @return реализацию PhantomEntityFactory.
     */
    PhantomEntityFactory getPhantomEntityFactory();

    /**
     * Получить реализацию PhantomBlockFactory для работы с блоками на пакетах.
     * @return реализацию PhantomBlockFactory.
     */
    PhantomBlockFactory getPhantomBlockFactory();

    /**
     * Получить реализацию PhantomWorldFactory для работы с мирами на пакетах, которые проецируют
     * на себя блоки из других существующих bukkit-миров, но, при этом, могут иметь внутри себя
     * и другие, установленные в ручном порядке, блоки на пакетах, которые будут подгружены, в
     * отличие от использования методов {@link PhantomBlockFactory}, не единожды, но и при
     * отгрузке/подгрузке чанков их содержащих.
     * @return реализацию PhantomWorldFactory.
     */
    PhantomWorldFactory getPhantomWorldFactory();

    /**
     * Получить реализацию DmsMinigameTeamSorter для сортировки игроков по командам на минирежимах.
     * @return реализацию DmsMinigameTeamSorter.
     */
    DmsMinigameTeamSorter getTeamSorter();

    /**
     * Получить реализацию DmsPartyRegistry для динамической работы с группами игроков.
     * @return реализацию DmsPartyRegistry.
     */
    DmsPartyRegistry getPartyRegistry();

    /**
     * Получить реализацию DmsMinigameSharder для разбивания игры на несколько шардов в пределах одного сервера.
     * @return реализацию DmsMinigameSharder.
     */
    DmsMinigameSharder getSharder();

    /**
     * Запустить событие в главном серверном потоке.
     * @param run событие.
     */
    void runSync(Runnable run);

    enum ServerType {
        HUB,
        LOBBY,
        GAME,
        ANOTHER
    }

}
