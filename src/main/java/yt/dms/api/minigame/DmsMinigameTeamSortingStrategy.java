package yt.dms.api.minigame;

import com.google.common.util.concurrent.ListenableFuture;
import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;
import java.util.List;

/**
 * Created by k.shandurenko on 04.08.2018
 */
@SpigotOnly
public interface DmsMinigameTeamSortingStrategy {

    /**
     * Рассортировать игроков по командам так, чтобы в каждой из команд было примерно равное количество игроков.
     * Количество команд уже задано.
     * @param players игроки.
     * @param teams команды. Если некоторые игроки выбрали какие-то команды (и точно должны в них быть),
     *              то в этих списках должны иметься те самые игроки (при этом они могут быть и в переменной
     *              players, ничего плохого не произойдет).
     * @return список команд - в каждой команде уже был вызван метод setPlayers (вполне вероятно, что асинхронно(!)).
     */
    ListenableFuture<List<DmsMinigameSortingTeam>> sortBalanced(Collection<Player> players, List<DmsMinigameSortingTeam> teams);

    /**
     * Рассортировать игроков по командам с учетом того, что в каждой команде должно быть ровно playersPerTeam игроков.
     * Если игроков недостаточно, в последней команде может быть меньше требуемого количества игроков.
     * Количество команд не задано и сгенерируется автоматически.
     * @see DmsMinigameTeamSortingStrategy#sortBalanced(Collection, List)
     * @param players игроки.
     * @param playersPerTeam количество игроков в одной команде.
     * @return список команд.
     */
    ListenableFuture<List<DmsMinigameSortingTeam>> sortWithFixedMembersSize(Collection<Player> players, int playersPerTeam);

}
