package yt.dms.api.minigame;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;

import java.util.List;

/**
 * Created by k.shandurenko on 04.08.2018
 */
@SpigotOnly
public interface DmsMinigameSortingTeam {

    /**
     * Получить игроков, которые уже выбрали эту команду.
     * @return список игроков, которые уже выбрали эту команду.
     */
    List<Player> getPlayers();

    /**
     * Установить игроков, которые будут играть в этой команде.
     * @param players список игроков, которые будут играть в этой команде.
     */
    void setPlayers(List<Player> players);

}
