package yt.dms.api.minigame;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by RINES on 03.06.2018.
 */
@SpigotOnly
public enum DmsMinigameType {
    UNDEFINED,
    COLONY_WARS,
    ARCADES,
    FISH_KING,
    EPIC_BOSS_FIGHT,
    PRISON_EVO,
    ZABELOV_VANILLA,
    VACUUM,
    MOB_ARENA,
    EVIL_DRAGONS,
    AZERUS,
    BUILD_BATTLE,
    SPACE,
    HEROES,
    SKY_WARS,
    ZOMBIEMOD,
    CLAN_HALL
}
