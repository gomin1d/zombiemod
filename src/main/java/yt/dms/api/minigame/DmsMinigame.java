package yt.dms.api.minigame;

import com.google.common.util.concurrent.ListenableFuture;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.player.DmsPlayer;

import java.util.LinkedHashMap;

/**
 * Created by RINES on 11.06.2018.
 */
@SpigotOnly
public interface DmsMinigame {

    /**
     * Получить топ игроков данного минирежима по указанной статистике.
     * Значения метода локально кешируются на небольшое время, поэтому самому об этом заморачиваться не нужно.
     * @param statName имя статистики.
     * @return отсортированная мапа, где ключами являются сами игроки, а значениями - величина указанной
     * статистики для данного игрока. Первый в порядке итерирования игрок будет первым в топе, далее
     * порядок определяет позицию в топе.
     */
    ListenableFuture<LinkedHashMap<DmsPlayer, Integer>> getTopByStat(String statName);

    /**
     * Проверить, изменился ли резуьтат последнего вызова getTopByStat() относительно предыдущего вызова
     * (для той же статистики, разумеется).
     * @param statName имя статистики.
     * @return true, если результат вызова последнего getTopByStat() изменился относительно предыдущего вызова.
     */
    boolean isTopChanged(String statName);

}
