package yt.dms.api.minigame;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 08.08.2018
 */
@SpigotOnly
public enum DmsMinigamePhase {
    TECHNICAL, //настройка, перезагрузка, да и всё что угодно, когда мы не ждем игроков и не играем
    WAITING_FOR_PLAYERS, //когда ожидаем игроков из очереди
    IN_GAME //когда играем
}
