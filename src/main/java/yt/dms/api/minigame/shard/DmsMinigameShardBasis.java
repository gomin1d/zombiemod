package yt.dms.api.minigame.shard;

import org.bukkit.entity.Player;
import yt.dms.api.DMS;
import yt.dms.api.annotation.SpigotOnly;

import java.util.Collection;

/**
 * Created by k.shandurenko on 09.08.2018
 */
@SpigotOnly
public interface DmsMinigameShardBasis {

    /**
     * Получить идентификатор этого игрового шарда.
     * @return идентификатор этого игрового шарда.
     */
    int getID();

    /**
     * Получить коллекцию игроков на данном игровом шарде.
     * @return коллекция игроков с данного игрового шарда.
     */
    Collection<Player> getPlayers();

    /**
     * Поставить шард в очередь ожидания игроков.
     * @see yt.dms.api.spigot.SQueues#awaitPlayers(DmsMinigameShardBasis, int, int, Collection, Runnable)
     * @param minPlayers
     * @param maxPlayers
     * @param queueNames
     * @param gameStarter
     */
    default void awaitPlayers(int minPlayers, int maxPlayers, Collection<String> queueNames, Runnable gameStarter) {
        DMS.spigot().getQueueUtils().awaitPlayers(
                this,
                minPlayers,
                maxPlayers,
                queueNames,
                gameStarter
        );
    }

    /**
     * Убрать шард из очереди ожидания игроков.
     * @see yt.dms.api.spigot.SQueues#stopAwaitingPlayers(DmsMinigameShardBasis)
     */
    default void stopAwaitingPlayers() {
        DMS.spigot().getQueueUtils().stopAwaitingPlayers(this);
    }

}
