package yt.dms.api.minigame.shard;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 24/12/2018
 */
@SpigotOnly
public interface DmsMinigameShard extends DmsMinigameShardBasis {

    /**
     * Пометить шард готовым к восстановлению.
     * Этот метод необходимо использовать, когда на шарде уже не осталось игроков.
     * Если после использования этого метода все шарды готовы к восстановлению, они будут восстановлены.
     * В противном случае данный шард перейдет в спящий режим (или, другими словами, режим ожидания других шардов).
     */
    void markForRestoration();

}
