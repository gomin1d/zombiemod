package yt.dms.api.minigame.plugin;

import com.google.common.base.Preconditions;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import yt.dms.api.DMS;
import yt.dms.api.minigame.DmsMinigamePhase;
import yt.dms.api.minigame.DmsMinigameType;
import yt.dms.api.spigot.SFloodControl;
import yt.dms.api.spigot.SMemoryCache;

/**
 * Created by k.shandurenko on 08.08.2018
 */
public abstract class DmsMinigamePlugin extends JavaPlugin {

    private final DmsMinigameType minigameType;

    @Setter
    @Getter
    private DmsMinigamePhase phase = DmsMinigamePhase.TECHNICAL;

    private DmsMinigameConfig minigameConfig;

    private boolean enabling;

    protected DmsMinigamePlugin(DmsMinigameType minigameType) {
        this.minigameType = minigameType;
        DMS.setMinigameType(minigameType);
    }

    @Override
    public void onEnable() {
        super.onEnable();
        this.minigameConfig = new DmsMinigameConfig(this.minigameType);
        this.enabling = true;
        try {
            enabling();
        } finally {
            this.enabling = false;
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();
        disabling();
        try {
            Thread.sleep(1000L); //чтобы успеть отправить все нужные пакеты
        } catch (InterruptedException ignoted) {}
    }

    /**
     * То, что будет выполняться в onEnable(), чтобы не городить super.onEnable().
     */
    protected abstract void enabling();

    /**
     * То, что будет выполняться в onDisable(), чтобы не городить super.onDisable().
     */
    protected abstract void disabling();

    public SMemoryCache getMemory() {
        return DMS.getMemory().spigot().getCache();
    }

    public SFloodControl getFloodControl() {
        return DMS.spigot().getFloodControl();
    }

    /**
     * Получить тип миниигры, реализуемый на данном сервере.
     * @return тип миниигры, реализуемый на данном сервере.
     */
    public DmsMinigameType getMinigameType() {
        return this.minigameType;
    }

    /**
     * Получить межсерверную конфигурацию этого минирежима, сохраненную в {@link yt.dms.api.mem.Memory}.
     * Автоматически подставляет тип минирежима префиксом к названию каждого ключа, так что не нужно
     * заботиться об их (ключей) уникальности.
     * @return межсерверную конфигурацию этого минирежима.
     */
    public DmsMinigameConfig getMinigameConfig() {
        return this.minigameConfig;
    }

    @Override
    @Deprecated
    public FileConfiguration getConfig() {
        throw new IllegalStateException("This method is obsolete. Use DmsMinigamePlugin#getMinigameConfig() instead");
    }

    /**
     * @see DmsMinigamePlugin#getConfig()
     * @return обычный файл конфигурации.
     */
    @Deprecated
    public FileConfiguration getFileConfig() {
        return super.getConfig();
    }

    /**
     * Проверить, включен ли игровой модуль.
     * @param module игровой модуль.
     * @return true/false.
     */
    public synchronized boolean isMinigameModuleEnabled(DmsMinigameModule module) {
        return module.isEnabled();
    }

    /**
     * Включить игровой модуль.
     * @param module игровой модуль.
     */
    public synchronized void enableMinigameModule(DmsMinigameModule module) {
        Preconditions.checkState(this.enabling, "Modules can be enabled only during plugin enabling");
        Preconditions.checkState(!module.isEnabled(), "Module %s is already enabled", module.name());
        Preconditions.checkState(module.getRequiredModules().stream().allMatch(DmsMinigameModule::isEnabled),
                "Module %s can't be enabled: not all required modules are enabled", module.name());
        Preconditions.checkState(module.getIncompatibleModules().stream().noneMatch(DmsMinigameModule::isEnabled),
                "Module %s can't be enabled: at least one incompatible module is enabled", module.name());
        module.getOnEnable().accept(this);
        module.setEnabled(true);
    }

    /**
     * Отключить игровой модуль.
     * @param module игровой модуль.
     */
    public synchronized void disableMinigameModule(DmsMinigameModule module) {
        Preconditions.checkState(module.isEnabled(), "Module %s is not enabled", module.name());
        Preconditions.checkState(module.isCanBeDisabled(), "Module %s can't be disabled", module.name());
        for (DmsMinigameModule mod : DmsMinigameModule.values()) {
            if (mod != module && mod.isEnabled() && mod.getRequiredModules().contains(module)) {
                throw new IllegalStateException("Module " + module.name() + " can't be disabled: there is at least one another module for which this one is required");
            }
        }
        module.getOnDisable().accept(this);
        module.setEnabled(false);
    }

    public DmsMinigameSharder getSharder() {
        return DMS.spigot().getSharder();
    }

}
