package yt.dms.api.minigame.plugin;

import yt.dms.api.DMS;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.minigame.DmsMinigameType;
import yt.dms.api.spigot.SMemoryCache;

import java.util.List;
import java.util.Set;

/**
 * @see DmsMinigamePlugin#getMinigameConfig()
 * Created by k.shandurenko on 02.09.2018
 */
@SpigotOnly
public class DmsMinigameConfig implements SMemoryCache {

    private final SMemoryCache memory;
    private final String prefix;

    DmsMinigameConfig(DmsMinigameType minigameType) {
        this.memory = DMS.getMemory().spigot().getCache();
        this.prefix = minigameType.name().toLowerCase() + ".";
    }

    @Override
    public SMemoryCache getCache() {
        return this.memory;
    }

    @Override
    public void setString(String key, String value) throws IllegalArgumentException {
        this.memory.setString(this.prefix + key, value);
    }

    @Override
    public void setInt(String key, int value) throws IllegalArgumentException {
        this.memory.setInt(this.prefix + key, value);
    }

    @Override
    public void setLong(String key, long value) throws IllegalArgumentException {
        this.memory.setLong(this.prefix + key, value);
    }

    @Override
    public void setBoolean(String key, boolean value) throws IllegalArgumentException {
        this.memory.setBoolean(this.prefix + key, value);
    }

    @Override
    public void setFloat(String key, float value) throws IllegalArgumentException {
        this.memory.setFloat(this.prefix + key, value);
    }

    @Override
    public void setStringList(String key, List<String> value) throws IllegalArgumentException {
        this.memory.setStringList(this.prefix + key, value);
    }

    @Override
    public void setIntList(String key, List<Integer> value) throws IllegalArgumentException {
        this.memory.setIntList(this.prefix + key, value);
    }

    @Override
    public void setStringSet(String key, Set<String> value) throws IllegalArgumentException {
        this.memory.setStringSet(this.prefix + key, value);
    }

    @Override
    public void setIntSet(String key, Set<Integer> value) throws IllegalArgumentException {
        this.memory.setIntSet(this.prefix + key, value);
    }

    @Override
    public String getString(String key) throws IllegalArgumentException {
        return this.memory.getString(this.prefix + key);
    }

    @Override
    public Integer getInt(String key) throws IllegalArgumentException {
        return this.memory.getInt(this.prefix + key);
    }

    @Override
    public Long getLong(String key) throws IllegalArgumentException {
        return this.memory.getLong(this.prefix + key);
    }

    @Override
    public Boolean getBoolean(String key) throws IllegalArgumentException {
        return this.memory.getBoolean(this.prefix + key);
    }

    @Override
    public Float getFloat(String key) throws IllegalArgumentException {
        return this.memory.getFloat(this.prefix + key);
    }

    @Override
    public List<String> getStringList(String key) throws IllegalArgumentException {
        return this.memory.getStringList(this.prefix + key);
    }

    @Override
    public List<Integer> getIntList(String key) throws IllegalArgumentException {
        return this.memory.getIntList(this.prefix + key);
    }

    @Override
    public Set<String> getStringSet(String key) throws IllegalArgumentException {
        return this.memory.getStringSet(this.prefix + key);
    }

    @Override
    public Set<Integer> getIntSet(String key) throws IllegalArgumentException {
        return this.memory.getIntSet(this.prefix + key);
    }

    @Override
    public String getString(String key, String defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        return this.memory.getString(this.prefix + key, defaultValue, writeIfNotPresent);
    }

    @Override
    public int getInt(String key, int defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        return this.memory.getInt(this.prefix + key, defaultValue, writeIfNotPresent);
    }

    @Override
    public long getLong(String key, long defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        return this.memory.getLong(this.prefix + key, defaultValue, writeIfNotPresent);
    }

    @Override
    public boolean getBoolean(String key, boolean defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        return this.memory.getBoolean(this.prefix + key, defaultValue, writeIfNotPresent);
    }

    @Override
    public float getFloat(String key, float defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        return this.memory.getFloat(this.prefix + key, defaultValue, writeIfNotPresent);
    }

    @Override
    public List<String> getStringList(String key, List<String> defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        return this.memory.getStringList(this.prefix + key, defaultValue, writeIfNotPresent);
    }

    @Override
    public List<Integer> getIntList(String key, List<Integer> defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        return this.memory.getIntList(this.prefix + key, defaultValue, writeIfNotPresent);
    }

    @Override
    public Set<String> getStringSet(String key, Set<String> defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        return this.memory.getStringSet(this.prefix + key, defaultValue, writeIfNotPresent);
    }

    @Override
    public Set<Integer> getIntSet(String key, Set<Integer> defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        return this.memory.getIntSet(this.prefix + key, defaultValue, writeIfNotPresent);
    }

    @Override
    public void invalidate(String key) {
        this.memory.invalidate(this.prefix + key);
    }

}
