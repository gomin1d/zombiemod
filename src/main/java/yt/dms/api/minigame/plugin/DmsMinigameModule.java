package yt.dms.api.minigame.plugin;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.function.Consumer;

/**
 * Created by k.shandurenko on 02.09.2018
 */
@Getter(AccessLevel.PACKAGE)
@RequiredArgsConstructor
public enum DmsMinigameModule {
    /**
     * Добавляет подкоманду /mga spawn, через которую можно изменить существующий
     * спавн сервера, а также телепортироваться на него.
     * Спавн хранится в {@link DmsMinigamePlugin#getMinigameConfig()} под ключом "spawn_location".
     */
    SPAWN_POSITION(false),

    /**
     * Добавляет команду /spawn, доступную всем, при использовании которой игроки
     * будут мгновенно телепортированы на точку спавна вне зависимости от того,
     * находились ли они в бою и падали ли в пропасть.
     *
     * Требует включенного {@link DmsMinigameModule#SPAWN_POSITION}
     * Не может использоваться вместе с {@link DmsMinigameModule#SAFE_SPAWN_COMMAND}.
     */
    UNSAFE_SPAWN_COMMAND(false),

    /**
     * Добавляет команду /spawn, доступную всем, при использовании которой игроки
     * будут телепортированы на точку спавна в течение 10 секунд. В этом время они
     * не должны двигаться или получать какой-либо урон.
     *
     * Требует включенного {@link DmsMinigameModule#SPAWN_POSITION}.
     * Не может использоваться вместе с {@link DmsMinigameModule#UNSAFE_SPAWN_COMMAND}.
     */
    SAFE_SPAWN_COMMAND(false);

    static {
        UNSAFE_SPAWN_COMMAND.addRequiredModules(SPAWN_POSITION);
        UNSAFE_SPAWN_COMMAND.addIncompatibleModules(SAFE_SPAWN_COMMAND);
        SAFE_SPAWN_COMMAND.addRequiredModules(SPAWN_POSITION);
        SAFE_SPAWN_COMMAND.addIncompatibleModules(UNSAFE_SPAWN_COMMAND);
    }

    private final boolean canBeDisabled;
    private Collection<DmsMinigameModule> requiredModules = Collections.emptySet();
    private Collection<DmsMinigameModule> incompatibleModules = Collections.emptySet();

    @Setter(AccessLevel.PACKAGE)
    private boolean enabled;

    @Setter
    private Consumer<DmsMinigamePlugin> onEnable;

    @Setter
    private Consumer<DmsMinigamePlugin> onDisable;

    private void addRequiredModules(DmsMinigameModule... modules) {
        this.requiredModules = addModules(this.requiredModules, modules);
    }

    private void addIncompatibleModules(DmsMinigameModule... modules) {
        this.incompatibleModules = addModules(this.incompatibleModules, modules);
    }

    private Collection<DmsMinigameModule> addModules(Collection<DmsMinigameModule> collection,
                                                     DmsMinigameModule... modules) {
        if (collection.isEmpty()) {
            collection = EnumSet.noneOf(DmsMinigameModule.class);
        }
        Collections.addAll(collection, modules);
        return collection;
    }

}
