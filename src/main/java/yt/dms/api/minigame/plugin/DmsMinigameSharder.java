package yt.dms.api.minigame.plugin;

import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.minigame.shard.DmsMinigameShard;
import yt.dms.api.minigame.shard.DmsMinigameShardBasis;

import java.util.function.Consumer;

/**
 * Created by k.shandurenko on 24/12/2018
 */
@SpigotOnly
public interface DmsMinigameSharder {

    /**
     * Создать новый игровой шард.
     * @param starter метод, который будет вызван сразу после инициализации шарда, а далее -
     *                каждый раз после восстановления шарда.
     * @param restorer метод, который будет вызываться каждый раз, когда для данного шарда будет
     *                 необходимо сбросить все данные относительно последней прошедшей на нем игры.
     * @return новый игровой шард.
     */
    DmsMinigameShard createNewShard(Consumer<DmsMinigameShard> starter, Consumer<DmsMinigameShard> restorer);

    /**
     * Получить шард игрока.
     * @param playerName ник игрока, чей шард нужно получить.
     * @return шард, на котором находится игрок (или должен находиться, то есть где он ожидается) или null.
     */
    DmsMinigameShardBasis getPlayerShard(String playerName);

    /**
     * Получить шард игрока.
     * @param player игрок.
     * @return шард, на котором находится игрок или null.
     */
    default DmsMinigameShardBasis getPlayerShard(Player player) {
        return getPlayerShard(player.getName());
    }

}
