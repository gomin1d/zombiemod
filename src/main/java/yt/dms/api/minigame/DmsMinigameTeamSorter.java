package yt.dms.api.minigame;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 04.08.2018
 */
@SpigotOnly
public interface DmsMinigameTeamSorter {

    /**
     * Стратегия сортировки, которая раскидывает игроков по командам абсолютно случайно.
     * Эта стратегия абсолютно синхронна.
     * @return описанная стратегия сортировки.
     */
    DmsMinigameTeamSortingStrategy getRandomStrategy();

    /**
     * Стратегия сортировки, которая старается раскидать игроков из одной гильдии в одну команду.
     * Эта стратегия абсолютно синхронна.
     * @return описанная стратегия сортировки.
     */
    DmsMinigameTeamSortingStrategy getGuildBasedStrategy();

    /**
     * Стратегия сортировки, которая старается раскидать игроков из одной группы в одну команду.
     * @return описанная стратегия сортировки.
     */
    DmsMinigameTeamSortingStrategy getPartyBasedStrategy();

    /**
     * Стратегия сортировки, которая старается раскидать игроков из одной группы, а затем -
     * из одной гильдии - в одну команду.
     *
     * @return описанная стратегия сортировки.
     */
    DmsMinigameTeamSortingStrategy getPartyGuildBasedStrategy();

}
