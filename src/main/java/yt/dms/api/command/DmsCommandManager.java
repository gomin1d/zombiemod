package yt.dms.api.command;

import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.command.exception.DmsCommandException;
import yt.dms.api.spigot.DmsSpigotCommandManager;

/**
 * Created by RINES on 22.06.2018.
 */
public interface DmsCommandManager {

    /**
     * Зарегистрировать команду в этом менеджере.
     * @param command команда.
     */
    void registerCommand(DmsCommand command);

    /**
     * Получить команду, зарегистрированную ранее в этом менеджере, по ее имени.
     * @param commandName имя команды.
     * @return команда или null.
     */
    DmsCommand getCommandByName(String commandName);

    @SpigotOnly
    default DmsSpigotCommandManager spigot() {
        return (DmsSpigotCommandManager) this;
    }

    /**
     * Обработать для указанного исполнителя задачу выполнения команды.
     * Если в ходе ее исполнения будет выкинуто исключение, оно будет адекватно обработано и
     * исполнителю будет выведено сообщение, соответствующее выкинутому исключению.
     * @param task задача выполнения команды.
     * @param executor исполнитель команды.
     */
    void processExecution(DmsCommandExecutionTask task, DmsCommandExecutor executor);

    interface DmsCommandExecutionTask {
        void process() throws DmsCommandException;
    }

}
