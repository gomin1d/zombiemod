package yt.dms.api.command;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import yt.dms.api.command.exception.*;
import yt.dms.api.player.DmsPlayer;
import yt.dms.api.util.DmsFutures;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by RINES on 22.06.2018.
 */
public abstract class DmsCommand {

    private final Map<String, DmsCommand> subcommands = new HashMap<>();

    private DmsCommandManager commandManager;
    private DmsCommand ancestor;

    private final String name;
    private final String description;
    private final String[] aliases;

    /**
     * @param name имя команды.
     * @param description описание команды для help'a.
     * @param aliases алиасы команды.
     */
    protected DmsCommand(String name, String description, String... aliases) {
        this.name = name;
        this.description = description;
        this.aliases = aliases;
        if (!name.equals("help")) {
            subcommand(new DmsCommand("help", "справка помощи по команде") {
                @Override
                protected void showHelp(DmsCommandExecutor executor) {
                    //Nothing
                }

                @Override
                public DmsCommandAccessCheckResult hasAccess(DmsPlayer player) {
                    return DmsCommand.this.hasAccess(player);
                }

                @Override
                public void execute(DmsCommandExecutor executor, String[] args) {
                    DmsCommand ancestor = DmsCommand.this;
                    List<String> commandNames = new LinkedList<>();
                    while (ancestor != null) {
                        commandNames.add(0, ancestor.getName());
                        ancestor = ancestor.getAncestor();
                    }
                    String commandName = commandNames.stream().collect(Collectors.joining(" "));
                    executor.sendMessagePrefixed("Команды", "&6Справка помощи по команде &b/%s&6:", commandName);
                    printHelp("&b/" + commandName, DmsCommand.this, executor);
                }

                private void printHelp(String prefix, DmsCommand current, DmsCommandExecutor executor) {
                    if (current.getName().equals("help")) {
                        return;
                    }
                    if (current != DmsCommand.this) {
                        prefix += " " + current.getName();
                    }
                    Set<DmsCommand> subcommands = new HashSet<>(current.getSubcommands().values());
                    if (subcommands.size() == 1) {
                        if (executor.isConsole() || current.hasAccess(executor.getPlayer()) == null) {
                            executor.sendMessagePrefixed("Команды", "%s &8- &7%s", prefix, current.getDescription());
                        }
                        return;
                    }
                    for (DmsCommand subcommand : subcommands) {
                        printHelp(prefix, subcommand, executor);
                    }
                }

            });
        }
    }

    /**
     * Внутренняя команда для понимания того, в каком менеджере команд
     * была зарегистрирована эта команда.
     * @param commandManager менеджер команд.
     */
    public void setCommandManager(DmsCommandManager commandManager) {
        Preconditions.checkState(this.commandManager == null, "This command is already linked to command manager");
        this.commandManager = commandManager;
    }

    /**
     * Получить менеджер команд, в котором была зарегистрирована эта команда.
     * @return менеджер команд.
     */
    public DmsCommandManager getCommandManager() {
        if (this.commandManager != null) {
            return this.commandManager;
        }
        DmsCommand ancestor = this;
        while (ancestor.ancestor != null) {
            ancestor = ancestor.ancestor;
        }
        return ancestor.commandManager;
    }

    /**
     * Получить родительскую команду (если эта является подкомандой).
     * @return null или родительскую команду, если имеется.
     */
    public DmsCommand getAncestor() {
        return this.ancestor;
    }

    /**
     * Получить имя команды.
     * @return имя команды.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Получить описание команды.
     * @return описание команды.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Получить алиасы команды.
     * @return алиасы команды.
     */
    public String[] getAliases() {
        return this.aliases;
    }

    /**
     * Получить все подкоманды этой команды.
     * @return все подкоманды этой команды.
     */
    public Map<String, DmsCommand> getSubcommands() {
        return Collections.unmodifiableMap(this.subcommands);
    }

    /**
     * Добавить подкоманду для этой команды.
     * @param subcommand реализация подкоманды.
     * @return эту же самую команду.
     * @throws IllegalArgumentException если подкоманда с указанным именем уже имеется.
     */
    public DmsCommand subcommand(DmsCommand subcommand) throws IllegalArgumentException {
        List<String> subcommandNames = new ArrayList<>();
        subcommandNames.add(subcommand.getName());
        subcommandNames.addAll(Arrays.asList(subcommand.getAliases()));
        subcommandNames = subcommandNames.stream().map(String::toLowerCase).collect(Collectors.toList());
        Preconditions.checkArgument(subcommandNames.stream().noneMatch(this.subcommands::containsKey), "Command %s already has subcommand of name %s (or one of it's aliases)", this.name, subcommand.getName());
        Preconditions.checkArgument(subcommand.ancestor == null, "Subcommand %s is already used by another command (it's ancestor is %s)", subcommand.name, subcommand.ancestor == null ? "unknown" : subcommand.ancestor.name);
        subcommandNames.forEach(sname -> this.subcommands.put(sname, subcommand));
        subcommand.ancestor = this;
        return this;
    }

    /**
     * Показать справку помощи по этой команде указанному исполнителю.
     * @param executor исполнитель команды.
     */
    protected abstract void showHelp(DmsCommandExecutor executor);

    /**
     * Проверка на то, имеется ли у указанного игрока доступ к этой команде.
     * Если доступ подкоманды позволяет ее использование, а ее родителя-команды - нет,
     * то подкоманду все равно можно использовать.
     * @param player игрок.
     * @return null, если доступ есть, иначе информацию о том, для кого команда доступна.
     */
    public abstract DmsCommandAccessCheckResult hasAccess(DmsPlayer player);

    /**
     * Исполнить команду с указанными аргументами.
     * Если сработала одна из подкоманд данной команды, этот метод не будет вызван.
     * @param executor исполнитель команды.
     * @param args аргументы.
     * @throws DmsCommandNotEnoughArgumentsException если аргументов оказалось недостаточно.
     * @throws DmsCommandNoAccessException если у игрока нет прав на исполнение этой команды.
     * @throws DmsCommandPlayerIsNotOnlineException если в команде нужно указывать игроков и требуется их онлайн,
     *                                              а один из них оказался не в сети.
     * @throws DmsCommandUnavailableFromConsoleException если была выполнена из консоли, но это не поддерживается.
     */
    public abstract void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException;

    /**
     * Если во время исполнения команды нужно зависеть от результата асинхронной операции,
     * которую можно представить в виде фьючера, то можно внутри {@link DmsCommand#execute(DmsCommandExecutor, String[])}
     * метода использовать этот метод: внутри него так же, как и внутри обычного блока исполнения,
     * можно выкидывать исключения, которые будут адекватно обработаны.
     * @param executor исполнитель команды.
     * @param future фьючер, результата выполнения которого мы ожидаем.
     * @param handler обработчик, получающий результат выполнения фьючера.
     * @param <T> параметризация - тип ожидаемого от фьючера значения.
     */
    protected <T> void followFuture(DmsCommandExecutor executor, ListenableFuture<T> future, DmsCommandFutureHandler<T> handler) {
        DmsFutures.addCallback(future, value -> getCommandManager().processExecution(() -> handler.handle(value), executor), exception -> {
            getCommandManager().processExecution(() -> {
                throw new DmsCommandException("Во время исполнения команды что-то пошло не так: " + exception.getMessage() + ".");
            }, executor);
        });
    }

    /**
     * Внутренний метод для исполнения команды.
     * @param executor исполнитель команды.
     * @param args аргументы.
     * @throws DmsCommandNotEnoughArgumentsException если аргументов оказалось недостаточно.
     * @throws DmsCommandNoAccessException если у игрока нет прав на исполнение этой команды или исполненной подкоманды.
     * @throws DmsCommandPlayerIsNotOnlineException если в команде нужно указывать игроков и требуется их онлайн,
     *                                              а один из них оказался не в сети.
     * @throws DmsCommandUnavailableFromConsoleException если была выполнена из консоли, но это не поддерживается.
     */
    public void execute0(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
        if (args.length > 0) {
            String subcommandName = args[0].toLowerCase();
            DmsCommand subcommand = this.subcommands.get(subcommandName);
            if (subcommand != null) {
                String[] newArgs = new String[args.length - 1];
                System.arraycopy(args, 1, newArgs, 0, newArgs.length);
                subcommand.execute0(executor, newArgs);
                return;
            }
        }
        if (!executor.isConsole()) {
            DmsCommandAccessCheckResult checkResult = hasAccess(executor.getPlayer());
            if (checkResult != null) {
                throw new DmsCommandNoAccessException(checkResult);
            }
        }
        execute(executor, args);
    }

    public interface DmsCommandFutureHandler<T> {

        void handle(T value) throws DmsCommandException;

    }

}
