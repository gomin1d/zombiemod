package yt.dms.api.command.exception;

import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

/**
 * Исключение, кидаемое, когда для исполнения команды недостаточно аргументов.
 * Created by RINES on 22.06.2018.
 */
public class DmsCommandNotEnoughArgumentsException extends DmsCommandException {

    private final List<String> missingArguments;

    /**
     * Без указания на то, каких аргументов не хватает.
     */
    public DmsCommandNotEnoughArgumentsException() {
        this.missingArguments = null;
    }

    /**
     * С указанием того, каких аргументов не хватает.
     * @param missingArguments аргументы, которых не хватает.
     */
    public DmsCommandNotEnoughArgumentsException(String... missingArguments) {
        this.missingArguments = Lists.newArrayList(missingArguments);
    }

    /**
     * Получить список недостающих аргументов.
     * @return список недостающих аргументов.
     */
    public List<String> getMissingArguments() {
        return this.missingArguments == null ? null : Collections.unmodifiableList(this.missingArguments);
    }

}
