package yt.dms.api.command.exception;

/**
 * Все исключения, которые могут быть выкинуты легально изнутри блока исполнения наших команд,
 * наследуются от данного. Если по ходу исполнения команды выкинуть исключение именно этого класса,
 * исполнителю будет выведено уведомление об ошибке с переданным в конструктор исключения сообщением.
 * Created by RINES on 23.06.2018.
 */
public class DmsCommandException extends Exception {

    public DmsCommandException() {}

    public DmsCommandException(String message) {
        super(message);
    }

}
