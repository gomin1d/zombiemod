package yt.dms.api.command.exception;

/**
 * Исключение, кидаемое, когда команда, которая не может быть исполнена удаленным исполнителем, все-таки
 * исполняется удаленно.
 * Created by k.shandurenko on 15.08.2018
 */
public class DmsCommandUnavailableFromRemoteException extends DmsCommandException {
}
