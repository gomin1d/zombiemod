package yt.dms.api.command.exception;

/**
 * Исключение, кидаемое, когда команда, которая не может быть исполнена из консоли, исполняется из нее.
 * Created by RINES on 23.06.2018.
 */
public class DmsCommandUnavailableFromConsoleException extends DmsCommandException {
}
