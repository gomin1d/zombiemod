package yt.dms.api.command.exception;

/**
 * Исключение кидается, когда используемый в команде игрок (не исполнитель) (скорее всего, передаваемый аргументов)
 * никогда прежде не играл на нашем проекте.
 * Created by k.shandurenko on 27.07.2018
 */
public class DmsCommandPlayerNeverPlayedException extends DmsCommandException {
}
