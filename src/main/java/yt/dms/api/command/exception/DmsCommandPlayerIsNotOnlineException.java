package yt.dms.api.command.exception;

/**
 * Исключение, кидаемое, когда используемый в команде игрок (не исполнитель) (скорее всего, передаваемый аргументом)
 * находится вне сети.
 * Created by RINES on 23.06.2018.
 */
public class DmsCommandPlayerIsNotOnlineException extends DmsCommandException {

}
