package yt.dms.api.command.exception;

import yt.dms.api.command.DmsCommandAccessCheckResult;

/**
 * Исключение, которое, в основном, кидается автоматически, но которое можно выкинуть и руками.
 * Говорит о том, что исполнение невозможно ввиду того, что у исполнителя не хватает на него прав.
 * Created by RINES on 22.06.2018.
 */
public class DmsCommandNoAccessException extends DmsCommandException {

    private final DmsCommandAccessCheckResult checkResult;

    public DmsCommandNoAccessException(DmsCommandAccessCheckResult checkResult) {
        this.checkResult = checkResult;
    }

    public DmsCommandAccessCheckResult getCheckResult() {
        return this.checkResult;
    }

}
