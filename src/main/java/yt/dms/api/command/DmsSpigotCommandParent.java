package yt.dms.api.command;

import yt.dms.api.DMS;

/**
 * Полный аналог {@link DmsSpigotCommand}, но авторегистрируемый в менеджере команд.
 * Created by k.shandurenko on 16.08.2018
 */
public abstract class DmsSpigotCommandParent extends DmsSpigotCommand {

    /**
     * @see DmsSpigotCommand
     */
    protected DmsSpigotCommandParent(String name, String description, String... aliases) {
        super(name, description, aliases);
        DMS.spigot().getCommandManager().registerCommand(this);
    }

}
