package yt.dms.api.command;

import yt.dms.api.player.PermissionGroup;

/**
 * Created by RINES on 23.06.2018.
 */
public class DmsCommandAccessCheckResult {

    private final String pureReason;
    private final PermissionGroup requiredGroup;

    /**
     * Конкретная причина, почему эта команда недоступна (например, "доступно только Ринесу").
     * @param pureReason конкретная причина.
     */
    public DmsCommandAccessCheckResult(String pureReason) {
        this.pureReason = pureReason;
        this.requiredGroup = null;
    }

    /**
     * Минимальная группа, с которой эта команда доступна.
     * @param requiredGroup минимальная группа, с которой эта команда доступна.
     */
    public DmsCommandAccessCheckResult(PermissionGroup requiredGroup) {
        this.pureReason = null;
        this.requiredGroup = requiredGroup;
    }

    public String getPureReason() {
        return this.pureReason;
    }

    public PermissionGroup getRequiredGroup() {
        return this.requiredGroup;
    }

}
