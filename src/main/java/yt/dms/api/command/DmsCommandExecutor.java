package yt.dms.api.command;

import yt.dms.api.player.DmsPlayer;

/**
 * Created by RINES on 23.06.2018.
 */
public interface DmsCommandExecutor {

    /**
     * Проверка на то, является ли отправитель игроком.
     * @return true/false.
     */
    boolean isPlayer();

    /**
     * Проверка на то, является ли отправитель игроком, который отправил команду через чат ВК.
     * @return true/false.
     */
    boolean isRemotePlayer();

    /**
     * Проверка на то, является ли отправитель консолью.
     * @return true/false.
     */
    boolean isConsole();

    /**
     * Если отправитель является игроком, получить объект игрока.
     * @return если отправитель не является игроком, вернет null.
     */
    DmsPlayer getPlayer();

    /**
     * Отправить сообщение без замены цветовых кодов.
     * @param message сообщение.
     */
    void sendMessageUncolored(String message);

    /**
     * Отправить сообщение с заменой & на цветовые коды.
     * @param message сообщение.
     */
    void sendMessage(String message);

    /**
     * Отправить форматированное сообщение с заменой & на цветовые коды.
     * @param message сообщение.
     * @param args аргументы форматирования.
     */
    default void sendMessage(String message, Object... args) {
        sendMessage(String.format(message, args));
    }

    /**
     * Отправить сообщение с указанным префиксом.
     * @param prefix префикс.
     * @param message сообщение.
     */
    default void sendMessagePrefixed(String prefix, String message) {
        sendMessage("&7[&6%s&7] &e%s", prefix, message);
    }

    /**
     * Отправить отформатированное сообщение с указанным префиксом.
     * @param prefix префикс.
     * @param message сообщение.
     * @param args аргументы форматирования.
     */
    default void sendMessagePrefixed(String prefix, String message, Object... args) {
        sendMessagePrefixed(prefix, String.format(message, args));
    }

    /**
     * Отправить сообщение с префиксом команд.
     * @param message сообщение.
     */
    default void sendCommandMessage(String message) {
        sendMessagePrefixed("Команды", message);
    }

    /**
     * Отправить отформатированное сообщение с префиксом команд.
     * @param message сообщение.
     * @param args аргументы форматирования.
     */
    default void sendCommandMessage(String message, Object... args) {
        sendCommandMessage(String.format(message, args));
    }

    /**
     * Закастовать к исполнителю из ВК.
     * Очевидно, выкинет ошибку, если исполнитель таковым не является.
     * @return этот же объект, но закастованный к исполнителю из ВК.
     */
    default DmsRemotePlayerCommandExecutor remotePlayer() {
        return (DmsRemotePlayerCommandExecutor) this;
    }

}
