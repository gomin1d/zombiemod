package yt.dms.api.command;

import com.google.common.util.concurrent.ListenableFuture;
import yt.dms.api.DMS;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.command.exception.DmsCommandException;
import yt.dms.api.command.exception.DmsCommandUnavailableFromConsoleException;
import yt.dms.api.command.exception.DmsCommandUnavailableFromRemoteException;
import yt.dms.api.util.DmsFutures;

/**
 * Created by RINES on 25.06.2018.
 */
@SpigotOnly
public abstract class DmsSpigotCommand extends DmsCommand {

    /**
     * @param name        имя команды.
     * @param description описание команды для help'a.
     * @param aliases     алиасы команды.
     */
    protected DmsSpigotCommand(String name, String description, String... aliases) {
        super(name, description, aliases);
    }

    @Override
    protected void showHelp(DmsCommandExecutor executor) {
        try {
            getSubcommands().get("help").execute(executor, new String[0]);
        } catch (DmsCommandException e) {
            e.printStackTrace();
        }
    }

    /**
     * Получить красивое имя для консоли данного сервера.
     * @return красивое имя для консоли данного сервера.
     */
    protected String getConsoleSenderNameUncolored() {
        return String.format("&b&l{Spigot &a&l%s&b&l}&r", DMS.spigot().getServerName());
    }

    /**
     * Проверить, что команда была исполнена игроком с сервера, а не удаленным исполнителем или консолью.
     * @param executor исполнитель.
     * @throws DmsCommandException если команда была исполнена не игроком с сервера.
     */
    protected void checkPlayer(DmsCommandExecutor executor) throws DmsCommandException {
        if (executor.isConsole()) {
            throw new DmsCommandUnavailableFromConsoleException();
        }
        if (executor.isRemotePlayer()) {
            throw new DmsCommandUnavailableFromRemoteException();
        }
    }

    /**
     * Делает то же, что и указанный в документации ниже метод, однако выполняет фьючер синхронного в потоке спигота.
     * @see DmsCommand#followFuture(DmsCommandExecutor, ListenableFuture, DmsCommandFutureHandler)
     * @param executor исполнитель команды.
     * @param future фьючер, результата выполнения которого мы ожидаем.
     * @param handler обработчик, получающий результат выполнения фьючера.
     * @param <T> параметризация - тип ожидаемого от фьючера значения.
     */
    protected <T> void followFutureSync(DmsCommandExecutor executor, ListenableFuture<T> future, DmsCommandFutureHandler<T> handler) {
        DmsFutures.addSpigotSyncCallback(future, value -> getCommandManager().processExecution(() -> handler.handle(value), executor), exception -> {
            getCommandManager().processExecution(() -> {
                throw new DmsCommandException("Во время исполнения команды что-то пошло не так: " + exception.getMessage() + ".");
            }, executor);
        });
    }

}
