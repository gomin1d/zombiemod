package yt.dms.api.command;

/**
 * Created by k.shandurenko on 16.08.2018
 */
public interface DmsRemotePlayerCommandExecutor extends DmsCommandExecutor {

    @Override
    default void sendMessagePrefixed(String prefix, String message) {
        sendMessage(message);
    }

    /**
     * Отправлять ли сообщения в личку пользователю или в общий чат с командами?
     * @param value true/false.
     */
    void setSendingMessagesPrivately(boolean value);

    /**
     * Так как сообщения в вк отправляются не со спигота, а с проксированием через кору,
     * а пакеты доходят до коры без сохранения порядка, то сообщения перепутаются, если
     * на каждый вызов sendMessage() слать запрос к коре (да и пакетов так много будет).
     * Вместо этого, за время выполнения команды, все сообщения складируются, а после
     * скопом отправляются (вызывая этот метод).
     * Все сообщения, который будут отправлены после завершения исполнения команды (например,
     * из потока, который был запущен в ходе исполнения), будут отправляться отдельными пакетами.
     */
    void releaseMessages();

    /**
     * Если вы не хотите, чтобы после завершения команды сообщения отправлялись в разнобой,
     * то можете использовать этот метод, после чего вновь поотправлять игроку сообщения,
     * а затем не забудьте использовать {@link DmsRemotePlayerCommandExecutor#releaseMessages()},
     * и тогда все сообщения дойдут в нужном порядке, да еще и одним пакетом.
     */
    void unreleaseMessages();

}
