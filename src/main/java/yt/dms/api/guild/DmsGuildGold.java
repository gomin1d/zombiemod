package yt.dms.api.guild;

/**
 * Created by k.shandurenko on 15.07.2018
 */
public interface DmsGuildGold {

    /**
     * Получить количество золота у гильдии.
     * Вызов этого метода запрашивает количество золота гильдии напрямую у координатора проекта, метод блокирующий.
     * @return количество золота у гильдии.
     */
    int getAmount();

    /**
     * Изменить золото у гильдии. Суммарное кол-во золота не должно быть меньше 0.
     * @param goldDelta количество золота, которое нужно добавить/убавить.
     * @throws IllegalArgumentException если после изменения золото гильдии станет меньше 0.
     */
    void change(int goldDelta) throws IllegalArgumentException;

}
