package yt.dms.api.guild;

import lombok.Getter;

/**
 * Created by k.shandurenko on 15.07.2018
 */
public enum GuildPerk {
    MOTD("Приветственное сообщение", 7500),
    PARTY("Гильдийская группа", 20000),
    LOGO("Эмблема", 35000),
    SILVER_MULTIPLIER("Модификатор серебра", 15000, 15000, 15000, 15000, 15000, 15000, 15000, 15000, 15000, 15000, 30000, 30000, 50000, 50000),
    ADDITIONAL_GUILD_MEMBERS("Расширение", 3000, 6000, 9000, 12000, 15000, 18000, 21000, 24000, 27000, 30000, 33000, 36000, 39000, 42000);

    @Getter
    private final String name;
    private final int[] prices;

    GuildPerk(String name, int... prices) {
        this.name = name;
        this.prices = prices;
    }

    public int getMaxLevel() {
        return prices.length;
    }

    public int getPrice(int level) {
        return prices[level - 1];
    }
}
