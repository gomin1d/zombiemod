package yt.dms.api.guild;

import yt.dms.api.guild.clanhall.ClanHallBuilding;
import yt.dms.api.guild.clanhall.ClanHallType;
import yt.dms.api.guild.clanhall.ClanHallWarBonus;

/**
 * Created by k.shandurenko on 05/01/2019
 */
public interface DmsGuildClanHall {

    /**
     * Получить информацию по своему кланхоллу.
     * @return информация по своему кланхоллу.
     */
    DmsGuildClanHallOwn getOwnData();

    /**
     * Получить информацию по своей войне за кланхолл (эта гильдия - нападающий).
     * @return информация по своей войне за кланхолл.
     */
    DmsGuildClanHallWar getWarData();

    interface DmsGuildClanHallOwn {

        /**
         * Имеется ли у данной гильдии кланхолл?
         * @return имеется ли у данной гильдии кланхолл.
         */
        boolean hasClanHall();

        /**
         * Получить тип кланхолла, который есть у данной гильдии.
         * @return тип кланхолла, которым владеет эта гильдия, если он есть, иначе null.
         */
        ClanHallType getOwningClanHall();

        /**
         * Узнать, построено ли у этой гильдии указанное здание в кланхолле.
         * @param building здание.
         * @return построено ли у этой гильдии указанное здание в кланхолле.
         */
        boolean hasBuilding(ClanHallBuilding building);

        /**
         * Получить уровень университета в кланхолле.
         * @return уровень университета в кланхолле.
         */
        int getUniversityLevel();

        /**
         * Получить время начала постройки указанного здания, которое в данный момент
         * строится в кланхолле данной гильдии.
         * @param building здание.
         * @return время начала постройки указанного здания.
         */
        long getBuildingConstructionStartingTime(ClanHallBuilding building);

        /**
         * Получить время окончания постройки указанного здания, которое в данный
         * момент строится в кланхолле данной гильдии.
         * @param building здание.
         * @return время окончания постройки указанного здания.
         */
        long getBuildingConstructionEndingTime(ClanHallBuilding building);

        /**
         * Получить гильдию, которая объявила войну за кланхолл данной гильдии.
         * @return гильдия-нападающий.
         */
        DmsGuild getWarDeclaredGuild();

        /**
         * Установить кланхолл, которым теперь владеет данная гильдия.
         * @param clanHallType тип кланхолла.
         */
        void setOwningClanHall(ClanHallType clanHallType);

        /**
         * Забрать у данной гильдии кланхолл.
         */
        default void removeOwningClanHall() {
            setOwningClanHall(null);
        }

        /**
         * Установить гильдию, которая объявила войну за кланхолл данной гильдии.
         * @param guild гильдия-нападающий.
         */
        void setWarDeclaredGuild(DmsGuild guild);

        /**
         * Начать постройку здания.
         * @param building здание.
         */
        void startBuildingConstruction(ClanHallBuilding building);

        /**
         * Начать постройку здания указанногоу уровня.
         * @param building здание.
         * @param level уровень здания.
         * @throws IllegalArgumentException если у указанного здания нет уровней постройки.
         */
        void startLeveledBuildingConstruction(ClanHallBuilding building, int level) throws IllegalArgumentException;

        /**
         * Пометить здание как разрушенное (на войне).
         * @param building здание.
         */
        void markBuildingAsDestroyed(ClanHallBuilding building);

    }

    interface DmsGuildClanHallWar {

        /**
         * Получить тип кланхолла, которому данная гильдия объявила войну.
         * @return тип кланхолла.
         */
        ClanHallType getWarDeclaredClanHall();

        /**
         * Проверить, есть ли указанный бонус для войны у этой гильдии на предстоящую войну.
         * @param bonus бонус.
         * @return true/false.
         */
        boolean hasBonus(ClanHallWarBonus bonus);

        /**
         * Установить тип кланхолла, с которым у данной гильдии будет предстоящая война.
         * @param clanHallType тип кланхолла.
         */
        void setWarDeclaredClanHall(ClanHallType clanHallType);

        /**
         * Добавить указанный бонус для войны у этой гильдии на предстоящую войну.
         * @param bonus бонус.
         */
        void addBonus(ClanHallWarBonus bonus);

    }

}
