package yt.dms.api.guild.clanhall;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;

import java.util.List;

/**
 * Created by k.shandurenko on 05/01/2019
 */
@RequiredArgsConstructor
public enum ClanHallWarBonus {
    ARMOR("Улучшенная броня", Material.LEATHER_CHESTPLATE, Lists.newArrayList(
            "&7Все нападающие на кланхолл",
            "&7будут получать кожаную броню,",
            "&7а цена на железную будет снижена.",
            "&7Броня будет выдаваться и после смерти."
    ), .075D),
    WEAPON("Улучшенное оружие", Material.STONE_SWORD, Lists.newArrayList(
            "&7Все нападающие на кланхолл",
            "&7будут получать каменные мечи,",
            "&7а цена на железный будет снижена.",
            "&7Оружие будет выдаваться и после смерти."
    ), .075D),
    BOW("Луки", Material.BOW, Lists.newArrayList(
            "&7Все нападающие на кланхолл",
            "&7получат возможность приобретать",
            "&7луки и стрелы."
    ), .1D),
    POTIONS("Зелья", Material.POTION, Lists.newArrayList(
            "&7Все нападающие на кланхолл",
            "&7получат возможность приобретать",
            "&7различные зелья."
    ), .1D),
    CONTROL_BLOCKS_DECEASE("Ослабление контрольных блоков", Material.OBSIDIAN, Lists.newArrayList(
            "&7Для ломания контрольных блоков",
            "&7нападающим на кланхолл будет",
            "&7необходимо 15 ломаний вместо 25."
    ), .05D),
    WALL_BLOCKS_DECEASE("Ослабление стен", Material.SMOOTH_BRICK, Lists.newArrayList(
            "&7Для ломания блоков стены",
            "&7нападающим на кланхолл будет",
            "&7необходимо 5 ломаний вместо 10."
    ), .05D);


    @Getter
    private final String name;

    @Getter
    private final Material iconMaterial;

    @Getter
    private final List<String> description;

    private final double priceModifier;

    /**
     * Получить стоимость покупки этого бонуса на предстоящую войну.
     * @return стоимость покупки этого бонуса не предстоящую войну.
     */
    public int getPrice() {
        return ClanHalls.getBuildingInitialPrice(this.priceModifier);
    }

}
