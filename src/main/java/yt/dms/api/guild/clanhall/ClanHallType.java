package yt.dms.api.guild.clanhall;

import yt.dms.api.DMS;

/**
 * Created by k.shandurenko on 05/01/2019
 */
public enum ClanHallType {
    CRIDEN;

    public static ClanHallType getCurrentServerClanHallType() {
        String serverName = DMS.spigot().getServerName().toUpperCase();
        if (!serverName.startsWith("clanhall_")) {
            return null;
        }
        serverName = serverName.substring("clanhall_".length());
        try {
            return valueOf(serverName.split("-")[0]);
        } catch (Exception ex) {
            return null;
        }
    }

}
