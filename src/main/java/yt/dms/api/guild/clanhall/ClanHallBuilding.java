package yt.dms.api.guild.clanhall;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;
import yt.dms.api.spigot.DmsMaterials;

import java.util.List;

/**
 * Created by k.shandurenko on 05/01/2019
 */
@RequiredArgsConstructor
public enum ClanHallBuilding {
    LIBRARY("Библиотека", DmsMaterials.PURPLE_BOOK, Lists.newArrayList(
            "&7Увеличивает получаемый игроками",
            "&7гильдии опыт DMS на 25%."
    ), 36, .2D),
    JEWELRY("Ювелирная Лавка", DmsMaterials.GEM_SAPPHIRE, Lists.newArrayList(
            "&7Увеличивает модификатор получаемого",
            "&7игроками гильдии серебра на +2."
    ), 36, .2D),
    UNIVERSITY("Университет", Material.BOOK_AND_QUILL, Lists.newArrayList(
            "&7Единственное улучшаемое здание.",
            "&7Первый уровень добавит в кланхолл",
            "&7персонажа, который ежедневно",
            "&7будет выдавать любому кликнувшему",
            "&7по нему игроку некоторое количество",
            "&7опыта DMS.",
            "",
            "&7Каждое последующее улучшение здания",
            "&7будет увеличивать опыт, даваемый",
            "&7этим персонажем, на 50% от предыдущего",
            "&7значения."
    ), 24, .1D),
    BANK("Банк", DmsMaterials.COIN_GOLD, Lists.newArrayList(
            "&7Уменьшает комиссионный процент",
            "&7при передаче золота в гильдию",
            "&7с 10% до 5%."
    ), 48, .1D),
    ARMORER_MASTERY("Мастерская Бронника", Material.LEATHER_CHESTPLATE, Lists.newArrayList(
            "&7Во время войны за кланхолл всем",
            "&7защитникам кланхолла изначально",
            "&7будет выдаваться кожаная броня,",
            "&7а цена на железную будет снижена.",
            "&7Броня будет выдаваться и после смерти."
    ), 24, .15D),
    FORGE("Кузня", Material.STONE_SWORD, Lists.newArrayList(
            "&7Во время войны за кланхолл всем",
            "&7защитникам кланхолла изначально",
            "&7будет выдаваться каменный меч,",
            "&7а цена на железный будет снижена.",
            "&7Оружие будет выдаваться и после смерти."
    ), 24, .15D),
    RIFLE_RANGE("Стрельбище", Material.BOW, Lists.newArrayList(
            "&7Во время войны за кланхолл все",
            "&7защитники кланхолла получат",
            "&7возможность приобретать",
            "&7луки и стрелы."
    ), 24, .2D),
    ALCHEMY_LAB("Алхимическая Лаборатория", Material.POTION, Lists.newArrayList(
            "&7Во время войны за кланхолл все",
            "&7защитники кланхолла получат",
            "&7возможность приобретать",
            "&7различные зелья."
    ), 36, .2D),
    FOUNTAIN("Фонтан", Material.WATER_BUCKET, Lists.newArrayList(
            "&7Во время войны за кланхолл все",
            "&7защитники кланхолла будут",
            "&7воскрешаться на 10 секунд быстрее."
    ), 24, .2D);

    @Getter
    private final String name;

    @Getter
    private final Material iconMaterial;

    @Getter
    private final List<String> description;

    @Getter
    private final int buildingTimeInHours;

    private final double priceModifier;

    /**
     * Получить цену постройки здания.
     * @return цена постройки здания.
     */
    public int getPrice() {
        return ClanHalls.getBuildingInitialPrice(this.priceModifier);
    }

}
