package yt.dms.api.guild.clanhall;

import yt.dms.api.DMS;

/**
 * Created by k.shandurenko on 05/01/2019
 */
public class ClanHalls {

    /**
     * Получить первоначальную стоимость свободного кланхолла на аукционе в золоте.
     * @return первоначальную стоимость свободного кланхолла на аукционе в золоте.
     */
    public static int getAuctionInitialPrice() {
        return DMS.spigot().getEconomics().getPrice(getGuildsEconomicsConstant());
    }

    /**
     * Получить стоимость минимального шага в аукционе на покупку свободного кланхолла в золоте.
     * @return стоимость минимального шага в аукционе на покупку свободного кланхолла в золоте.
     */
    public static int getAuctionStepPrice() {
        return getAuctionInitialPrice();
    }

    /**
     * Получить стоимость объявления войны занятому кланхоллу в золоте.
     * @return стоимость объявления войны занятому кланхоллу в золоте.
     */
    public static int getWarDeclarationPrice() {
        return getAuctionInitialPrice() >> 1;
    }

    /**
     * Получить стоимость здания кланхолла в золоте.
     * Используется для внутренних расчетов в API.
     * @param modifier модификатор стоимость здания от базового.
     * @return стоимость здания кланхолла в золоте.
     */
    public static int getBuildingInitialPrice(double modifier) {
        return DMS.spigot().getEconomics().getPrice(getGuildsEconomicsConstant() * modifier);
    }

    private static double getGuildsEconomicsConstant() {
        return .00027578273331D;
    }

}
