package yt.dms.api.guild;

/**
 * Created by k.shandurenko on 15.07.2018
 */
public interface DmsGuildMeta {

    /**
     * Получить приветственное сообщение гильдии.
     * @return приветственное сообщение гильдии, если оно имеется; иначе null.
     */
    String getMessageOfTheDay();

    /**
     * Получить ссылку на логотип гильдии.
     * @return ссылка на логотип гильдии, если он имеется; иначе null.
     */
    String getLogoURL();

}
