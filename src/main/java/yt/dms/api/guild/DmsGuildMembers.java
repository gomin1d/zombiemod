package yt.dms.api.guild;

import yt.dms.api.player.DmsPlayer;

import java.util.Collection;
import java.util.List;

/**
 * Информация, получаемая через методы этого интерфейса, не является актуальной и может отличаться от реальности
 * на небольшое время (до нескольких минут).
 * Тем не менее, после первого вызова любого из методов данные будут блокирующе получены и закешированы, что
 * обеспечит дальнейшую неблокирующую работу с интерфейсом.
 * Created by k.shandurenko on 15.07.2018
 */
public interface DmsGuildMembers {

    /**
     * Получить ник лидера гильдии.
     * @return ник лидера гильдии.
     */
    String getLeaderName();

    /**
     * Получить ники офицеров гильдии.
     * @return ники офицеров гильдии.
     */
    Collection<String> getOfficerNames();

    /**
     * Получить ники участников гильдии.
     * @return ники участников гильдии.
     */
    Collection<String> getMemberNames();

    /**
     * Получить ники лидера, офицеров и участников гильдии.
     * Ники в листе будут идти именно в таком порядке.
     * @return ники лидера, офицеров и участников гильдии.
     */
    List<String> getAllNames();

    /**
     * Получить лидера гильдии.
     * @return лидер гильдии.
     */
    DmsPlayer getLeader();

    /**
     * Получить офицеров гильдии.
     * @return офицеров гильдии.
     */
    Collection<DmsPlayer> getOfficers();

    /**
     * Получить участников гильдии.
     * @return участников гильдии.
     */
    Collection<DmsPlayer> getMembers();

    /**
     * Получить лидера, офицеров и участников гильдии.
     * Они в листе будут идти именно в таком порядке.
     * @return список из лидера, офицеров и участников гильдии.
     */
    List<DmsPlayer> getAll();

    /**
     * Проверить, является ли игрок с указанным ником лидером этой гильдии.
     * @param playerName ник игрока.
     * @return true/false.
     */
    boolean isLeader(String playerName);

    /**
     * Проверить, является ли игрок с указанным ником офицером этой гильдии.
     * @param playerName ник игрока.
     * @return true/false.
     */
    boolean isOfficer(String playerName);

    /**
     * Проверить, является ли игрок с указанным ником лидером или офицером этой гильдии.
     * @param playerName ник игрока.
     * @return true/false.
     */
    default boolean isOfficerOrLeader(String playerName) {
        return isLeader(playerName) || isOfficer(playerName);
    }

    /**
     * Проверить, является ли игрок с указанным ником участником этой гильдии.
     * @param playerName ник игрока.
     * @return true/false.
     */
    boolean isMember(String playerName);

    /**
     * Проверить, состоит ли игрок с указанным ником в этой гильдии.
     * @param playerName ник игрока.
     * @return true/false.
     */
    boolean isInGuild(String playerName);

    /**
     * Проверить, является ли указанный игрок лидером этой гильдии.
     * @param player игрок.
     * @return true/false.
     */
    default boolean isLeader(DmsPlayer player) {
        return isLeader(player.getName());
    }

    /**
     * Проверить, является ли указанный игрок офицером этой гильдии.
     * @param player игрок.
     * @return true/false.
     */
    default boolean isOfficer(DmsPlayer player) {
        return isOfficer(player.getName());
    }

    /**
     * Проверить, является ли указанный игрок лидером или офицером этой гильдии.
     * @param player игрок.
     * @return true/false.
     */
    default boolean isOfficerOrLeader(DmsPlayer player) {
        return isOfficerOrLeader(player.getName());
    }

    /**
     * Проверить, является ли указанный игрок участником этой гильдии.
     * @param player игрок.
     * @return true/false.
     */
    default boolean isMember(DmsPlayer player) {
        return isMember(player.getName());
    }

    /**
     * Проверить, состоит ли указанный игрок в этой гильдии.
     * @param player игрок.
     * @return true/false.
     */
    default boolean isInGuild(DmsPlayer player) {
        return isInGuild(player.getName());
    }

}
