package yt.dms.api.guild;

import java.util.Map;

/**
 * Created by k.shandurenko on 15.07.2018
 */
public interface DmsGuildPerks {

    /**
     * Получить перки этой гильдии вместе с их уровнями.
     * @return перки этой гильдии вместе с их уровнями.
     */
    Map<GuildPerk, Integer> getPerks();

    /**
     * Получить уровень перка данной гильдии.
     * @param perk перк.
     * @return уровень перка (или 0, если перк у гильдии полностью отсутствует).
     */
    int getPerkLevel(GuildPerk perk);

    /**
     * Проверить, имеется ли у этой гильдии указанный перк минимум данного уровня.
     * @param perk перк.
     * @param perkLevel уровень перка.
     * @return true/false.
     */
    default boolean hasPerk(GuildPerk perk, int perkLevel) {
        return perkLevel <= getPerkLevel(perk);
    }

}
