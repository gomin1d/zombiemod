package yt.dms.api;

import yt.dms.api.annotation.BungeeOnly;
import yt.dms.api.bungee.BMessagesSender;

/**
 * Created by k.shandurenko on 31.07.2018
 */
@BungeeOnly
public interface DmsBungee {

    /**
     * Утилита банжи для отправки сообщений в чат от имени игроков.
     * Используется для внутренней модерации (чтобы сообщения были кликабельны для модераторов, чтобы последние
     * могли выдать мут).
     * @return реализацию BMessagesSender.
     */
    BMessagesSender getMessagesSender();
    
}
