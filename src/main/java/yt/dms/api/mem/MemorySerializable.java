package yt.dms.api.mem;

/**
 * Created by RINES on 04.06.2018.
 */
public interface MemorySerializable {

    String serializeToString();

}
