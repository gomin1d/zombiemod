package yt.dms.api.mem;

import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.spigot.SMemory;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Местный persistent memcached.
 * Локального кеширования нет, все запросы запрашивают данные с координатора проекта синхронно,
 * так что нужно использовать с умом. Хранить слишком много данных здесь тоже не нужно.
 * Created by RINES on 04.06.2018.
 */
public interface Memory {

    /**
     * Получить MemoryCache.
     * @return реализацию MemoryCache.
     */
    MemoryCache getCache();

    /**
     * Установить значение строки по ключу.
     * @param key ключ.
     * @param value значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    void setString(String key, String value) throws IllegalArgumentException;

    /**
     * Установить значение инта по ключу.
     * @param key ключ.
     * @param value значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    void setInt(String key, int value) throws IllegalArgumentException;

    /**
     * Установить значение лонга по ключу.
     * @param key ключ.
     * @param value значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    void setLong(String key, long value) throws IllegalArgumentException;

    /**
     * Установить значение булеана по ключу.
     * @param key ключ.
     * @param value значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    void setBoolean(String key, boolean value) throws IllegalArgumentException;

    /**
     * Установить значение флота по ключу.
     * @param key ключ.
     * @param value значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    void setFloat(String key, float value) throws IllegalArgumentException;

    /**
     * Установить значение списка строк по ключу.
     * @param key ключ.
     * @param value значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    void setStringList(String key, List<String> value) throws IllegalArgumentException;

    /**
     * Установить значение списка интов по ключу.
     * @param key ключ.
     * @param value значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    void setIntList(String key, List<Integer> value) throws IllegalArgumentException;

    /**
     * Установить значение сета строк по ключу.
     * @param key ключ.
     * @param value значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    void setStringSet(String key, Set<String> value) throws IllegalArgumentException;

    /**
     * Установить значение сета интов по ключу.
     * @param key ключ.
     * @param value значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    void setIntSet(String key, Set<Integer> value) throws IllegalArgumentException;

    /**
     * Установить значение сериализуемого объекта по ключу.
     * @param key ключ.
     * @param serializable значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default void setSerializable(String key, MemorySerializable serializable) throws IllegalArgumentException {
        Preconditions.checkArgument(serializable != null, "Given value is null");
        setString(key, serializable.serializeToString());
    }

    /**
     * Установить значение списка сериализуемых объектов по ключу.
     * @param key ключ.
     * @param serializables значения.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default void setSerializableList(String key, List<MemorySerializable> serializables) throws IllegalArgumentException {
        Preconditions.checkArgument(serializables != null, "Given value is null");
        setStringList(key, serializables.stream().map(MemorySerializable::serializeToString).collect(Collectors.toList()));
    }

    /**
     * Установить значение сета сериализуемых объектов по ключу.
     * @param key ключ.
     * @param serializables значение.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default void setSerializableSet(String key, Set<MemorySerializable> serializables) throws IllegalArgumentException {
        Preconditions.checkArgument(serializables != null, "Given value is null");
        setStringSet(key, serializables.stream().map(MemorySerializable::serializeToString).collect(Collectors.toSet()));
    }

    default void setJson(String key, JsonElement json) throws IllegalArgumentException {
        Preconditions.checkArgument(json != null, "Given value is null");
        setString(key, json.toString());
    }

    /**
     * Получить значение строки по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    String getString(String key) throws IllegalArgumentException;

    /**
     * Получить значение инта по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    Integer getInt(String key) throws IllegalArgumentException;

    /**
     * Получить значение лонга по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    Long getLong(String key) throws IllegalArgumentException;

    /**
     * Получить значение булеана по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    Boolean getBoolean(String key) throws IllegalArgumentException;

    /**
     * Получить значение флота по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    Float getFloat(String key) throws IllegalArgumentException;

    /**
     * Получить значение списка строк по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    List<String> getStringList(String key) throws IllegalArgumentException;

    /**
     * Получить значение списка интов по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    List<Integer> getIntList(String key) throws IllegalArgumentException;

    /**
     * Получить значение сета строк по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    Set<String> getStringSet(String key) throws IllegalArgumentException;

    /**
     * Получить значение сета интов по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    Set<Integer> getIntSet(String key) throws IllegalArgumentException;

    /**
     * Получить значение сериализуемого объекта по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default <T extends MemorySerializable> T getSerializable(String key, Function<String, T> deserializator) throws IllegalArgumentException {
        String value = getString(key);
        return value == null ? null : deserializator.apply(value);
    }

    /**
     * Получить значение списка сериализуемых объектов по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default <T extends MemorySerializable> List<T> getSerializableList(String key, Function<String, T> deserializator) throws IllegalArgumentException {
        List<String> value = getStringList(key);
        return value == null ? null : value.stream().map(deserializator).collect(Collectors.toList());
    }

    /**
     * Получить значение сета сериализуемых объектов по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default <T extends MemorySerializable> Set<T> getSerializableSet(String key, Function<String, T> deserializator) throws IllegalArgumentException {
        Set<String> value = getStringSet(key);
        return value == null ? null : value.stream().map(deserializator).collect(Collectors.toSet());
    }

    /**
     * Получить json-значение по ключу.
     * @param key ключ.
     * @return значение или null, если отсутствует.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default JsonElement getJson(String key) throws IllegalArgumentException {
        String value = getString(key);
        return value == null ? null : new JsonParser().parse(value);
    }

    /**
     * Получить значение строки по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default String getString(String key, String defaultValue) throws IllegalArgumentException {
        return getString(key, defaultValue, false);
    }

    /**
     * Получить значение инта по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default int getInt(String key, int defaultValue) throws IllegalArgumentException {
        return getInt(key, defaultValue, false);
    }

    /**
     * Получить значение лонга по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default long getLong(String key, long defaultValue) throws IllegalArgumentException {
        return getLong(key, defaultValue, false);
    }

    /**
     * Получить значение булеана по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default boolean getBoolean(String key, boolean defaultValue) throws IllegalArgumentException {
        return getBoolean(key, defaultValue, false);
    }

    /**
     * Получить значение флота по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default float getFloat(String key, float defaultValue) throws IllegalArgumentException {
        return getFloat(key, defaultValue, false);
    }

    /**
     * Получить значение списка строк по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default List<String> getStringList(String key, List<String> defaultValue) throws IllegalArgumentException {
        return getStringList(key, defaultValue, false);
    }

    /**
     * Получить значение списка интов по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default List<Integer> getIntList(String key, List<Integer> defaultValue) throws IllegalArgumentException {
        return getIntList(key, defaultValue, false);
    }

    /**
     * Получить значение сета строк по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default Set<String> getStringSet(String key, Set<String> defaultValue) throws IllegalArgumentException {
        return getStringSet(key, defaultValue, false);
    }

    /**
     * Получить значение сета интов по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default Set<Integer> getIntSet(String key, Set<Integer> defaultValue) throws IllegalArgumentException {
        return getIntSet(key, defaultValue, false);
    }

    /**
     * Получить значение сериализуемого объекта по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default <T extends MemorySerializable> T getSerializable(String key, T defaultValue, Function<String, T> deserializator) throws IllegalArgumentException {
        return getSerializable(key, defaultValue, false, deserializator);
    }

    /**
     * Получить значение списка сериализуемых объектов по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default <T extends MemorySerializable> List<T> getSerializableList(String key, List<T> defaultValue, Function<String, T> deserializator) throws IllegalArgumentException {
        return getSerializableList(key, defaultValue, false, deserializator);
    }

    /**
     * Получить значение сета сериализуемых объектов по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default <T extends MemorySerializable> Set<T> getSerializableSet(String key, Set<T> defaultValue, Function<String, T> deserializator) throws IllegalArgumentException {
        return getSerializableSet(key, defaultValue, false, deserializator);
    }

    /**
     * Получить json-значение по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * В случае отсутствия записи по ключу не пишет указанное базовое значение в базу.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default JsonElement getJson(String key, JsonElement defaultValue) throws IllegalArgumentException {
        String value = getString(key, defaultValue == null ? null : defaultValue.toString());
        return value == null ? null : new JsonParser().parse(value);
    }

    /**
     * Получить значение строки по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    String getString(String key, String defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException;

    /**
     * Получить значение инта по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    int getInt(String key, int defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException;

    /**
     * Получить значение лонга по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    long getLong(String key, long defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException;

    /**
     * Получить значение булеана по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    boolean getBoolean(String key, boolean defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException;

    /**
     * Получить значение флота по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    float getFloat(String key, float defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException;

    /**
     * Получить значение списка строк по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    List<String> getStringList(String key, List<String> defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException;

    /**
     * Получить значение списка интов по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    List<Integer> getIntList(String key, List<Integer> defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException;

    /**
     * Получить значение сета строк по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    Set<String> getStringSet(String key, Set<String> defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException;

    /**
     * Получить значение сета интов по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    Set<Integer> getIntSet(String key, Set<Integer> defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException;

    /**
     * Получить значение сериализуемого объекта по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default <T extends MemorySerializable> T getSerializable(String key, T defaultValue, boolean writeIfNotPresent, Function<String, T> deserializator) throws IllegalArgumentException {
        if (writeIfNotPresent) {
            return deserializator.apply(getString(key, defaultValue.serializeToString(), true));
        }
        String value = getString(key);
        return value == null ? defaultValue : deserializator.apply(value);
    }

    /**
     * Получить значение списка сериализуемых объектов по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default <T extends MemorySerializable> List<T> getSerializableList(String key, List<T> defaultValue, boolean writeIfNotPresent, Function<String, T> deserializator) throws IllegalArgumentException {
        if (writeIfNotPresent) {
            List<String> value = getStringList(key, defaultValue.stream().map(MemorySerializable::serializeToString).collect(Collectors.toList()), true);
            return value.stream().map(deserializator).collect(Collectors.toList());
        }
        List<String> value = getStringList(key);
        return value == null ? defaultValue : value.stream().map(deserializator).collect(Collectors.toList());
    }

    /**
     * Получить значение сета сериализуемых объектов по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default <T extends MemorySerializable> Set<T> getSerializableSet(String key, Set<T> defaultValue, boolean writeIfNotPresent, Function<String, T> deserializator) throws IllegalArgumentException {
        if (writeIfNotPresent) {
            Set<String> value = getStringSet(key, defaultValue.stream().map(MemorySerializable::serializeToString).collect(Collectors.toSet()), true);
            return value.stream().map(deserializator).collect(Collectors.toSet());
        }
        Set<String> value = getStringSet(key);
        return value == null ? defaultValue : value.stream().map(deserializator).collect(Collectors.toSet());
    }

    /**
     * Получить json-значение по ключу.
     * Если оно отсутствует, будет возвращено указанное базовое значение.
     * @param key ключ.
     * @param defaultValue базовое значение.
     * @param writeIfNotPresent нужно ли писать базовое значение в базу, если значение по ключу отсутствует.
     * @return значение или указанное базовое значение, если оно отсутствует по ключу.
     * @throws IllegalArgumentException если ключ = null, он пустой или его длина превосходит 64 символа.
     */
    default JsonElement getJson(String key, JsonElement defaultValue, boolean writeIfNotPresent) throws IllegalArgumentException {
        String value = getString(key, defaultValue == null ? null : defaultValue.toString(), writeIfNotPresent);
        return value == null ? null : new JsonParser().parse(value);
    }

    /**
     * Закастить этот объект к SMemory.
     * @return этот же объект, но закасченный к SMemory.
     */
    @SpigotOnly
    default SMemory spigot() {
        return (SMemory) this;
    }

}
