package yt.dms.api;

import yt.dms.api.annotation.BungeeOnly;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.both.FloodControl;
import yt.dms.api.common.DmsRegistry;
import yt.dms.api.db.DmsDatabase;
import yt.dms.api.guild.DmsGuild;
import yt.dms.api.mem.Memory;
import yt.dms.api.minigame.DmsMinigameType;
import yt.dms.api.player.DmsPlayer;
import yt.dms.api.player.DmsPlayerRegistry;
import yt.dms.api.util.ChatUtil;
import yt.dms.api.util.algo.Algo;
import yt.dms.api.util.serialization.Serializator;

/**
 * Created by RINES on 03.06.2018.
 */
public class DMS {

    private static DmsPlayerRegistry<DmsPlayer> PLAYER_REGISTRY;
    private static DmsRegistry<Integer, DmsGuild> GUILD_REGISTRY;
    private static Memory MEMORY;
    private static FloodControl FLOOD_CONTROL;
    private static ChatUtil CHAT_UTIL;
    private static Serializator SERIALIZATOR;
    private static Algo ALGO;
    private static DmsDatabase DATABASE;
    private static DmsSpigot SPIGOT;
    private static DmsBungee BUNGEE;

    private static DmsMinigameType minigameType = DmsMinigameType.UNDEFINED;

    /**
     * Получить регистр игроков.
     * @return регистр игроков.
     */
    public static DmsPlayerRegistry<DmsPlayer> getPlayerRegistry() {
        return PLAYER_REGISTRY;
    }

    /**
     * Получить регистр гильдий.
     * @return регистр гильдий.
     */
    public static DmsRegistry<Integer, DmsGuild> getGuildRegistry() {
        return GUILD_REGISTRY;
    }

    /**
     * Получить реализацию Memory для работы с различными конфигурационными данными.
     * @return реализация Memory.
     */
    public static Memory getMemory() {
        return MEMORY;
    }

    /**
     * Получить реализацию FloodControl для ограничения действий по времени.
     * @return реализация FloodControl.
     */
    public static FloodControl getFloodControl() {
        return FLOOD_CONTROL;
    }

    /**
     * Получить реализацию ChatUtil для более простой работы с сообщениями.
     * @return реализация ChatUtil.
     */
    public static ChatUtil getChatUtil() {
        return CHAT_UTIL;
    }

    /**
     * Получить реализацию Serializator для работы с сериализацией.
     * @return реализация Serializator.
     */
    public static Serializator getSerializator() {
        return SERIALIZATOR;
    }

    /**
     * Получить реализацию Algo для использования различных алгоритмов.
     * @return реализацию Algo.
     */
    public static Algo getAlgo() {
        return ALGO;
    }

    /**
     * Получить реализацию DmsDatabase для локальной работы с базой данных.
     * @return реализацию DmsDatabase.
     */
    public static DmsDatabase getDatabase() {
        return DATABASE;
    }

    /**
     * Установить тип миниигры на данном сервере.
     * @param minigameType тип миниигры.
     */
    public static void setMinigameType(DmsMinigameType minigameType) {
        DMS.minigameType = minigameType;
    }

    /**
     * Получить тип миниигры на данном сервере.
     * @return тип миниигры данного сервера.
     */
    public static DmsMinigameType getMinigameType() {
        return minigameType;
    }

    /**
     * Получить реализацию DmsSpigot для использования различных наших спиготовских вещей.
     * @return реализацию DmsSpigot.
     */
    @SpigotOnly
    public static DmsSpigot spigot() {
        return SPIGOT;
    }

    @BungeeOnly
    public static DmsBungee bungee() {
        return BUNGEE;
    }

}
