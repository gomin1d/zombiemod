package yt.dms.api.db.example;

import org.bukkit.Location;
import yt.dms.api.DMS;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.db.QueryResult;
import yt.dms.api.db.query.DmsDatabaseCreateQuery;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by k.shandurenko on 19.12.2018
 */
@SuppressWarnings("unused")
@SpigotOnly
public class DatabaseLocationStoringExample {

    private final static String TABLE = "my_gamemode_locations";

    public void createLocationsTable() {
        DMS.getDatabase().sync().table(TABLE)
                .create()
                .column("location_name", DmsDatabaseCreateQuery.ColumnType.VARCHAR_32, true)
                .column("location", DmsDatabaseCreateQuery.ColumnType.VARCHAR_128, false)
                .executeUnchecked();
    }

    public void addLocation(String locationName, Location location) {
        DMS.getDatabase().async().table(TABLE)
                .insert()
                .field("location_name", locationName)
                .field("location", DMS.getSerializator().getLocationsSerializer().serializeLocation(location))
                .onDuplicateKeyUpdate("location")
                .executeUnchecked();
    }

    public void removeLocation(String locationName) {
        DMS.getDatabase().async().table(TABLE)
                .delete()
                .where()
                .fieldEquals("location_name", locationName)
                .doneClause()
                .executeUnchecked();
    }

    public Location getLocation(String locationName) {
        QueryResult result = DMS.getDatabase().sync().table(TABLE)
                .select()
                .field("location")
                .where()
                .fieldEquals("location_name", locationName)
                .doneClause()
                .executeUnchecked();
        return result.isEmpty() ? null : DMS.getSerializator().getLocationsSerializer().deserializeLocation(result.getFirst().getString(1), null);
    }

    public Map<String, Location> getAllLocations() {
        Map<String, Location> result = new HashMap<>();
        DMS.getDatabase().sync().table(TABLE)
                .select()
                .allFields()
                .executeUnchecked()
                .getRows().forEach(row -> {
                    result.put(
                            row.getString(1),
                            DMS.getSerializator().getLocationsSerializer().deserializeLocation(row.getString(2), null)
                    );
        });
        return result;
    }

}
