package yt.dms.api.db.example;

import yt.dms.api.DMS;
import yt.dms.api.db.QueryResult;
import yt.dms.api.db.query.DmsDatabaseCreateQuery;
import yt.dms.api.db.query.clause.DmsDatabaseOrderClause;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public class DatabasePlayersRatingStoringExample {

    private final static String TABLE = "my_gamemode_players_rating";

    public void createTable() {
        DMS.getDatabase().sync().table(TABLE)
                .create()
                .column("player_name", DmsDatabaseCreateQuery.ColumnType.VARCHAR_16, true)
                .column("season", DmsDatabaseCreateQuery.ColumnType.TINY_INT, true)
                .column("rating", DmsDatabaseCreateQuery.ColumnType.INT, false)
                .executeUnchecked();
    }

    public int initializePlayerAndGetRating(String playerName, int season, int defaultRatingValue) {
        QueryResult result = DMS.getDatabase().sync().table(TABLE)
                .select()
                .field("rating")
                .where()
                .fieldEquals("player_name", playerName)
                .fieldEquals("season", season)
                .doneClause()
                .executeUnchecked();
        if (!result.isEmpty()) {
            return result.getFirst().getInt(1);
        }
        DMS.getDatabase().sync().table(TABLE)
                .insert()
                .field("player_name", playerName)
                .field("season", season)
                .field("rating", defaultRatingValue)
                .onDuplicateKeyUpdate("rating")
                .executeUnchecked();
        return defaultRatingValue;
    }

    public void setPlayerRating(String playerName, int season, int rating) {
        DMS.getDatabase().async().table(TABLE)
                .update()
                .fieldValue("rating", rating)
                .where()
                .fieldEquals("player_name", playerName)
                .fieldEquals("season", season)
                .doneClause()
                .executeUnchecked();
    }

    public void changePlayerRating(String playerName, int season, int delta) {
        DMS.getDatabase().async().table(TABLE)
                .update()
                .fieldAddition("rating", delta)
                .where()
                .fieldEquals("player_name", playerName)
                .fieldEquals("season", season)
                .doneClause()
                .executeUnchecked();
    }

    public void clearPlayersRatingForAllSeasons(String playerName) {
        DMS.getDatabase().async().table(TABLE)
                .delete()
                .where()
                .fieldEquals("player_name", playerName)
                .doneClause()
                .executeUnchecked();
    }

    public void clearPlayersRatingForGivenSeason(String playerName, int season) {
        DMS.getDatabase().async().table(TABLE)
                .delete()
                .where()
                .fieldEquals("player_name", playerName)
                .fieldEquals("season", season)
                .doneClause()
                .executeUnchecked();
    }

    public LinkedHashMap<String, Integer> getTopRatedPlayers(int season, int topSize) {
        LinkedHashMap<String, Integer> result = new LinkedHashMap<>();
        DMS.getDatabase().sync().table(TABLE)
                .select()
                .field("player_name")
                .field("rating")
                .where()
                .fieldEquals("season", season)
                .doneClause()
                .order()
                .orderBy("rating", DmsDatabaseOrderClause.OrderDirection.DESCENDING)
                .doneClause()
                .limit(topSize)
                .executeUnchecked()
                .getRows().forEach(row -> result.put(row.getString(1), row.getInt(2))
        );
        return result;
    }

    public List<String> getPlayersWithRatingAboveSorted(int season, int minimalRating) {
        return DMS.getDatabase().sync().table(TABLE)
                .select()
                .field("player_name")
                .where()
                .fieldEquals("season", season)
                .fieldGreaterOrEquals("rating", minimalRating)
                .doneClause()
                .order()
                .orderBy("rating", DmsDatabaseOrderClause.OrderDirection.DESCENDING)
                .doneClause()
                .executeUnchecked()
                .getRows().stream()
                .map(row -> row.getString(1))
                .collect(Collectors.toList());
    }

    public List<String> getAllPlayersExceptRines() {
        return DMS.getDatabase().sync().table(TABLE)
                .select()
                .distinct()
                .field("player_name")
                .where()
                .fieldNotEqual("player_name", "RINES")
                .doneClause()
                .executeUnchecked()
                .getRows().stream()
                .map(row -> row.getString(1))
                .collect(Collectors.toList());
    }

}
