package yt.dms.api.db;

import java.util.List;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public class Row {

    private final QueryResult parent;
    private final List<Object> values;

    public Row(QueryResult parent, List<Object> values) {
        this.parent = parent;
        this.values = values;
    }

    public int getInt(int index) {
        return getInt(index, 0);
    }

    public int getInt(int index, int fallback) {
        Object val = values.get(index - 1);
        if (val == null) {
            return fallback;
        }
        if (val instanceof Number) {
            return ((Number) val).intValue();
        }
        return (int) val;
    }

    public int getInt(String column) {
        return getInt(column, 0);
    }

    public int getInt(String column, int fallback) {
        return getInt(parent.getColumnIndex(column), fallback);
    }

    public long getLong(int index) {
        Object val = values.get(index - 1);
        if (val instanceof Number) {
            return ((Number) val).longValue();
        }
        return (long) val;
    }

    public long getLong(String column) {
        return getLong(parent.getColumnIndex(column));
    }

    public float getFloat(int index) {
        Object val = this.values.get(index - 1);
        if (val instanceof Number) {
            return ((Number) val).floatValue();
        }
        return (float) val;
    }

    public float getFloat(String column) {
        return getFloat(parent.getColumnIndex(column));
    }

    public boolean getBoolean(int index) {
        Object val = values.get(index - 1);
        if (val instanceof Number) {
            return ((Number) val).intValue() != 0;
        }
        return (boolean) val;
    }

    public boolean getBoolean(String column) {
        return getBoolean(parent.getColumnIndex(column));
    }

    public String getString(int index) {
        Object val = this.values.get(index - 1);
        if (val instanceof String) {
            return (String) val;
        }
        return val == null ? null : val.toString();
    }

    public String getString(String column) {
        return getString(parent.getColumnIndex(column));
    }

    public byte[] getBytes(int index) {
        return (byte[]) values.get(index - 1);
    }

    public byte[] getBytes(String column) {
        return getBytes(parent.getColumnIndex(column));
    }

    public Object getObject(int index) {
        return values.get(index - 1);
    }

    public Object getObject(String column) {
        return getObject(parent.getColumnIndex(column));
    }

}
