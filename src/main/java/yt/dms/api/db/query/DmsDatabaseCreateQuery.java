package yt.dms.api.db.query;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseCreateQuery extends DmsDatabaseMethodicalQuery {

    /**
     * Добавить столбец в новосоздаваемую таблицу.
     * @param columnName имя столбца.
     * @param columnType тип столбца.
     * @param isPrimary является ли primary/combined-ключом.
     * @return этот же билдер.
     */
    DmsDatabaseCreateQuery column(String columnName, ColumnType columnType, boolean isPrimary);

    enum ColumnType {
        VARCHAR_16,
        VARCHAR_32,
        VARCHAR_64,
        VARCHAR_128,
        TEXT,
        TINY_INT,
        INT,
        BIG_INT,
        BOOLEAN
    }

}
