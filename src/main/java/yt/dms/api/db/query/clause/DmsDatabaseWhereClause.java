package yt.dms.api.db.query.clause;

import yt.dms.api.db.query.DmsDatabaseQuery;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseWhereClause<T extends DmsDatabaseQuery> extends DmsDatabaseClause<T> {

    /**
     * Добавить требование: значение поля = указанного значения.
     * @param fieldName имя столбца.
     * @param fieldValue значение.
     * @return этот же билдер WHERE части запроса.
     */
    DmsDatabaseWhereClause<T> fieldEquals(String fieldName, Object fieldValue);

    /**
     * Добавить требование: значение поля > указанного значения.
     * @param fieldName имя столбца.
     * @param value значение.
     * @return этот же билдер WHERE части запроса.
     */
    DmsDatabaseWhereClause<T> fieldGreater(String fieldName, Object value);

    /**
     * Добавить требование: значение поля >= указанного значения.
     * @param fieldName имя столбца.
     * @param value значение.
     * @return этот же билдер WHERE части запроса.
     */
    DmsDatabaseWhereClause<T> fieldGreaterOrEquals(String fieldName, Object value);

    /**
     * Добавить требование: значение поля < указанного значения.
     * @param fieldName имя столбца.
     * @param value значение.
     * @return этот же билдер WHERE части запроса.
     */
    DmsDatabaseWhereClause<T> fieldLower(String fieldName, Object value);

    /**
     * Добавить требование: значение поля <= указанного значения.
     * @param fieldName имя столбца.
     * @param value значение.
     * @return этот же билдер WHERE части запроса.
     */
    DmsDatabaseWhereClause<T> fieldLowerOrEquals(String fieldName, Object value);

    /**
     * Добавить требование: значение поля != указанного значения.
     * @param fieldName имя столбца.
     * @param value значение.
     * @return этот же билдер WHERE части запроса.
     */
    DmsDatabaseWhereClause<T> fieldNotEqual(String fieldName, Object value);
}
