package yt.dms.api.db.query.clause;

import yt.dms.api.db.query.DmsDatabaseQuery;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseClause<T extends DmsDatabaseQuery> {

    /**
     * Вернуться к билдеру полного запроса.
     * @return билдер полного запроса.
     */
    T doneClause();

}
