package yt.dms.api.db.query;

import yt.dms.api.db.query.clause.DmsDatabaseOrderClause;
import yt.dms.api.db.query.clause.DmsDatabaseWhereClause;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseSelectQuery extends DmsDatabaseFunctionalQuery {

    /**
     * Добавить поле, которое нужно получить.
     * @param fieldName имя столбца.
     * @return этот же билдер.
     */
    DmsDatabaseSelectQuery field(String fieldName);

    /**
     * Указать, что мы хотим получить все поля из таблицы.
     * @return этот же билдер.
     */
    default DmsDatabaseSelectQuery allFields() {
        return field("*");
    }

    /**
     * Указать, что мы хотим подсчитать количество всех строк в таблице.
     * @return этот же билдер.
     */
    default DmsDatabaseSelectQuery countAll() {
        return field("COUNT(*)");
    }

    /**
     * Указать, что мы хотим получить указанные поля под флагом DISTINCT.
     * @return этот же билдер.
     */
    DmsDatabaseSelectQuery distinct();

    /**
     * Перейти в WHERE часть запроса.
     * @return билдер WHERE части данного запроса.
     */
    DmsDatabaseWhereClause<? extends DmsDatabaseSelectQuery> where();

    /**
     * Перейти в ORDER_BY часть запроса.
     * @return билдер ORDER_BY части данного запроса.
     */
    DmsDatabaseOrderClause<? extends DmsDatabaseSelectQuery> order();

    /**
     * Установить LIMIT на количество возвращаемых строк.
     * @param limit лимит.
     * @return этот же билдер.
     */
    DmsDatabaseSelectQuery limit(int limit);

}
