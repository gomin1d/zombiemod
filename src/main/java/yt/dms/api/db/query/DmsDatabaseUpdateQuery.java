package yt.dms.api.db.query;

import yt.dms.api.db.query.clause.DmsDatabaseWhereClause;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseUpdateQuery extends DmsDatabaseMethodicalQuery {

    /**
     * Обновить значение поля в строке.
     * @param fieldName имя столбца.
     * @param fieldValue значение столбца.
     * @return этот же билдер.
     */
    DmsDatabaseUpdateQuery fieldValue(String fieldName, Object fieldValue);

    /**
     * Обновить значение поля в строке, но не на значение, а на определенную величину относительно его нынешнего значения.
     * @param fieldName имя столбца.
     * @param delta величина изменения значения столбца относительно нынешнего.
     * @return этот же билдер.
     */
    DmsDatabaseUpdateQuery fieldAddition(String fieldName, int delta);

    /**
     * Перейти в WHERE часть запроса.
     * @return билдер WHERE части данного запроса.
     */
    DmsDatabaseWhereClause<? extends DmsDatabaseUpdateQuery> where();

}
