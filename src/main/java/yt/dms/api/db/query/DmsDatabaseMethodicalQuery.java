package yt.dms.api.db.query;

import java.sql.SQLException;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseMethodicalQuery extends DmsDatabaseQuery {

    /**
     * Выполнить данный запрос.
     * @throws SQLException
     */
    void execute() throws SQLException;

    /**
     * Выполнить данный запрос с автоматической обработкой возможной SQL-ошибки.
     */
    default void executeUnchecked() {
        try {
            execute();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

}
