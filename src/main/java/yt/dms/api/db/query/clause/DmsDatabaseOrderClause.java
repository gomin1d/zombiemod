package yt.dms.api.db.query.clause;

import yt.dms.api.db.query.DmsDatabaseSelectQuery;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseOrderClause<T extends DmsDatabaseSelectQuery> extends DmsDatabaseClause<T> {

    /**
     * Добавить сортировку по указанному полю.
     * @param fieldName имя столбца.
     * @param direction тип сортировки.
     * @return этот же билдер ORDER_BY части запроса.
     */
    DmsDatabaseOrderClause<T> orderBy(String fieldName, OrderDirection direction);

    enum OrderDirection {
        ASCENDING,
        DESCENDING
    }

}
