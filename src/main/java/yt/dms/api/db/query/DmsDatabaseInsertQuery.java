package yt.dms.api.db.query;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseInsertQuery extends DmsDatabaseMethodicalQuery {

    /**
     * Установить значение поля в строке.
     * @param fieldName имя столбца.
     * @param fieldValue значение столбца.
     * @return этот же билдер.
     */
    DmsDatabaseInsertQuery field(String fieldName, Object fieldValue);

    /**
     * В случае, если запись под указанными ключами уже существует в таблице, обновить значения указанных полей.
     * @param fieldNames имени столбцов таблицы, которые нужно обновить для данной строки.
     * @return этот же билдер.
     */
    DmsDatabaseInsertQuery onDuplicateKeyUpdate(String... fieldNames);

}
