package yt.dms.api.db.query;

import yt.dms.api.db.query.clause.DmsDatabaseWhereClause;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseDeleteQuery extends DmsDatabaseMethodicalQuery {

    /**
     * Перейти в WHERE часть запроса.
     * @return билдер WHERE части данного запроса.
     */
    DmsDatabaseWhereClause<? extends DmsDatabaseDeleteQuery> where();

}
