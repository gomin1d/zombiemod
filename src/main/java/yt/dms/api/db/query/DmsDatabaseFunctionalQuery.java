package yt.dms.api.db.query;

import yt.dms.api.db.QueryResult;

import java.sql.SQLException;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseFunctionalQuery extends DmsDatabaseQuery {

    /**
     * Выполнить данный запрос и получить результат его выполнения.
     * @return результат выполнения запроса.
     * @throws SQLException
     */
    QueryResult execute() throws SQLException;

    /**
     * Выполнить данный запрос и получить результат его выполнения, обрабатывая возможные
     * SQL-ошибки в автоматическом режиме.
     * @return результат выполнения запроса.
     */
    default QueryResult executeUnchecked() {
        try {
            return execute();
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

}
