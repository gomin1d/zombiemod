package yt.dms.api.db;

import yt.dms.api.db.query.*;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabaseTableWorker {

    /**
     * Получить билдер INSERT запроса.
     * @return билдер INSERT запроса.
     */
    DmsDatabaseInsertQuery insert();

    /**
     * Получить билдер UPDATE запроса.
     * @return билдер UPDATE запроса.
     */
    DmsDatabaseUpdateQuery update();

    /**
     * Получить билдер DELETE запроса.
     * @return билдер DELETE запроса.
     */
    DmsDatabaseDeleteQuery delete();

    /**
     * Интерфейс работы с конкретной таблицей базы данных в синхронном режиме.
     */
    interface SyncDmsDatabaseTableWorker extends DmsDatabaseTableWorker {

        /**
         * Получить билдер запроса на создание таблицы.
         * @return билдер запроса на создание таблицы.
         */
        DmsDatabaseCreateQuery create();

        /**
         * Получить билдер SELECT запроса.
         * @return билдер SELECT запроса.
         */
        DmsDatabaseSelectQuery select();

    }

    /**
     * Интерфейс работы с конкретной таблицей базы данных в асинхронном режиме.
     */
    interface AsyncDmsDatabaseTableWorker extends DmsDatabaseTableWorker {

    }

}
