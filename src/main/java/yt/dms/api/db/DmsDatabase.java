package yt.dms.api.db;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public interface DmsDatabase {

    /**
     * Перейти в синхронный режим работы с базой данных.
     * @return интерфейс для работы с базой данных в синхронном режиме.
     */
    DmsDatabaseWorker<DmsDatabaseTableWorker.SyncDmsDatabaseTableWorker> sync();

    /**
     * Перейти в асинхронный режим работы с базой данных.
     * @return интерфейс для работы с базой данных в асинхронном режиме.
     */
    DmsDatabaseWorker<DmsDatabaseTableWorker.AsyncDmsDatabaseTableWorker> async();

    interface DmsDatabaseWorker<T extends DmsDatabaseTableWorker> {

        /**
         * Перейти к работе с таблицей указанного имени.
         * @param tableName имя таблицы.
         * @return интерфейс для работы с таблицей указанного имени.
         * @throws IllegalArgumentException если у данного сервера нет доступа к таблице указанного имени.
         */
        T table(String tableName) throws IllegalArgumentException;

    }

}
