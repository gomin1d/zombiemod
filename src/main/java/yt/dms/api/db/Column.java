package yt.dms.api.db;

import lombok.Getter;

/**
 * Created by k.shandurenko on 19.12.2018
 */
public class Column {

    @Getter
    private final String name;

    public Column(String name) {
        this.name = name;
    }

}
