package yt.dms.api.event;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Created by RINES on 03.06.2018.
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DmsEvent {

    public void call() {
        DmsEventManager.call(this);
    }

}
