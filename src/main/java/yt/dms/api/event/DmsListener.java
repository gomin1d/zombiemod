package yt.dms.api.event;

/**
 * Класс-обертка, имплементирующий IDmsListener и автоматически регистрирующий себя в DmsEventManager.
 * Created by RINES on 03.06.2018.
 */
public class DmsListener implements IDmsListener {

    protected DmsListener() {
        DmsEventManager.register(this);
    }

}
