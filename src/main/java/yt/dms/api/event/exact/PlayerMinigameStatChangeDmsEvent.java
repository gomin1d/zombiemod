package yt.dms.api.event.exact;

import lombok.Data;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.player.DmsPlayer;

/**
 * Событие бросается, когда координатор проекта подтвердил изменение статистики игрока на минирежиме от запроса
 * с данного сервера или когда с другого места пришла команда на изменение оного.
 * Created by RINES on 03.06.2018.
 */
@Data
public class PlayerMinigameStatChangeDmsEvent extends DmsEvent {

    private final DmsPlayer player;
    private final String statName;
    private final int oldStatValue;
    private final int newStatValue;

}
