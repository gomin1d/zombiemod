package yt.dms.api.event.exact;

import lombok.Data;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.player.DmsPlayer;

/**
 * Событие бросается, когда координатор проекта подтвердил изменение опыта игрока от запроса с данного сервера
 * или когда с другого места пришла команда на изменение опыта игрока.
 * Событие также бросается, если произошел переход игрока на новый уровень.
 * Created by RINES on 03.06.2018.
 */
@Data
public class PlayerExperienceChangeDmsEvent extends DmsEvent {

    private final DmsPlayer player;
    private final int oldExperienceValue;
    private final int experienceChangeValue;
    private final int newExperienceValue; //значение, которое может быть меньше старого, т.к. произошел переход на новый уровень.

}
