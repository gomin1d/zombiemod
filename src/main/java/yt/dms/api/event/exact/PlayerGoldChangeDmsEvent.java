package yt.dms.api.event.exact;

import lombok.Data;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.player.DmsPlayer;

/**
 * Событие бросается, когда координатор проекта подтвердил изменение золота игрока от запроса с данного сервера
 * или когда с другого места пришла команда на изменение золота игрока.
 * Created by RINES on 03.06.2018.
 */
@Data
public class PlayerGoldChangeDmsEvent extends DmsEvent {

    private final DmsPlayer player;
    private final int oldGoldValue;
    private final int goldChangeValue;

}
