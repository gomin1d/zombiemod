package yt.dms.api.event.exact;

import lombok.Data;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.player.DmsPlayer;

/**
 * Событие бросается, когда координатор проекта подтвердил изменение суффикса игрока от запроса с данного сервера
 * или когда с другого места пришла команда на изменение суффикса игрока.
 * Суффикс в событии не содержит цветов, их кодов или полного оформления (то есть даже квадратных скобок).
 * Для полного оформления нового суффикса нужно воспользоваться методом апи.
 * Created by RINES on 07.06.2018.
 */
@Data
public class PlayerSuffixChangeDmsEvent extends DmsEvent {

    private final DmsPlayer player;
    private final String suffix;
    private final String suffixBefore;

}
