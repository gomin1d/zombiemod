package yt.dms.api.event.exact;

import lombok.Data;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.player.DmsPlayer;

/**
 * Событие бросается, когда координатор проекта подтвердил изменение серебра игрока от запроса с данного сервера
 * или когда с другого места пришла команда на изменение серебра игрока.
 * Created by RINES on 03.06.2018.
 */
@Data
public class PlayerSilverChangeDmsEvent extends DmsEvent {

    private final DmsPlayer player;
    private final int oldSilverValue;
    private final int silverChangeValue;

}
