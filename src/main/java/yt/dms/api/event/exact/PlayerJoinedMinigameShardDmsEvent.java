package yt.dms.api.event.exact;

import lombok.Data;
import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.minigame.shard.DmsMinigameShardBasis;

/**
 * Событие бросается после того, как игрок присоединился к игровому шарду.
 * Created by k.shandurenko on 09.08.2018
 */
@Data
@SpigotOnly
public class PlayerJoinedMinigameShardDmsEvent extends DmsEvent {

    private final Player player;
    private final DmsMinigameShardBasis shard;

}
