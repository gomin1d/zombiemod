package yt.dms.api.event.exact;

import lombok.Data;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.player.DmsPlayer;
import yt.dms.api.player.PermissionGroup;

/**
 * Событие бросается, когда координатор проекта подтвердил изменение группы игрока от запроса с данного сервера
 * или когда с другого места пришла команда на изменение группы игрока.
 * Created by RINES on 01.08.2018.
 */
@Data
public class PlayerGroupChangeDmsEvent extends DmsEvent {

    private final DmsPlayer player;
    private final PermissionGroup group;
    private final boolean addedOrRemoved;

}
