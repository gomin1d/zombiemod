package yt.dms.api.event.exact;

import lombok.Data;
import org.bukkit.entity.Player;
import yt.dms.api.annotation.SpigotOnly;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.minigame.shard.DmsMinigameShardBasis;

/**
 * Событие бросается перед тем, как игрок будет исключен из игрового шарда.
 * Created by k.shandurenko on 09.08.2018
 */
@Data
@SpigotOnly
public class PlayerQuitMinigameShardDmsEvent extends DmsEvent {

    private final Player player;
    private final DmsMinigameShardBasis shard;

}
