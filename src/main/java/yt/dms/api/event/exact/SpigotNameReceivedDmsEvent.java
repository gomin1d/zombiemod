package yt.dms.api.event.exact;

import lombok.Data;
import yt.dms.api.event.DmsEvent;

/**
 * Событие бросается, когда сервер получает свое название от координатора проекта.
 * Важно понимать, что оно может бросаться не только после старта сервера и вашего плагина,
 * но и до запуска вашего плагина, а также в случаях, когда произошло переподключение к координатору проекта.
 * Created by k.shandurenko on 10.12.2018
 */
@Data
public class SpigotNameReceivedDmsEvent extends DmsEvent {

    private final String serverName;

}
