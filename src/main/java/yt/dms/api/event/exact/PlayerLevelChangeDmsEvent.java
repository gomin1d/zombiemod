package yt.dms.api.event.exact;

import lombok.Data;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.player.DmsPlayer;

/**
 * Событие бросается, когда координатор проекта подтвердил изменение уровня игрока от запроса с данного сервера
 * или когда с другого места пришла команда на изменение уровня игрока.
 * Если новый уровень больше старого, он всегда будет больше на 1 (на переход сразу через несколько уровней бросается
 * несколько событий). Если новый уровень меньше старого, он может быть меньше на любую величину.
 * Created by RINES on 03.06.2018.
 */
@Data
public class PlayerLevelChangeDmsEvent extends DmsEvent {

    private final DmsPlayer player;
    private final int oldLevel;
    private final int newLevel;

}
