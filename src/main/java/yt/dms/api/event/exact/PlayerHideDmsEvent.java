package yt.dms.api.event.exact;

import lombok.Data;
import yt.dms.api.event.DmsEvent;
import yt.dms.api.player.DmsPlayer;
import yt.dms.api.player.PermissionGroup;

/**
 * Событие бросается, когда координатор проекта подтвердил скрытие игрока от запроса с данного сервера
 * или когда с другого места пришла команда на скрытие игрока.
 * Created by RINES on 07.06.2018.
 */
@Data
public class PlayerHideDmsEvent extends DmsEvent {

    private final DmsPlayer player;
    private final String hiddenName;
    private final PermissionGroup hiddenGroup;
    private final String hiddenNameBefore;
    private final PermissionGroup hiddenGroupBefore;

}
