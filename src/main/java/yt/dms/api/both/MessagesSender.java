package yt.dms.api.both;

import yt.dms.api.player.DmsPlayer;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by RINES on 07.06.2018.
 */
public interface MessagesSender {

    /**
     * Отправить сообщение (message) от имени sender всем игрокам сервера.
     * Если отправитель не в сети, ничего не делает.
     * @param sender отправитель.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    default void broadcastMessage(DmsPlayer sender, String message) {
        broadcastMessage(sender.getName(), message);
    }

    /**
     * Отправить сообщения (messages) от имени sender всем игрокам сервера.
     * Если отправитель не в сети, ничего не делает.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void broadcastMessages(DmsPlayer sender, Collection<String> messages) {
        broadcastMessages(sender.getName(), messages);
    }

    /**
     * Отправить сообщение (message) от имени sender игроку receiver.
     * Если один из игроков не в онлайне, ничего не делает.
     * @param receiver получатель.
     * @param sender отправитель.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    default void sendMessage(DmsPlayer receiver, DmsPlayer sender, String message) {
        sendMessage(receiver.getName(), sender.getName(), message);
    }

    /**
     * Отправить сообщения (messages) от имени sender игроку receiver.
     * Если один из игроков не в онлайне, ничего не делает.
     * @param receiver получатель.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void sendMessages(DmsPlayer receiver, DmsPlayer sender, Collection<String> messages) {
        sendMessages(receiver.getName(), sender.getName(), messages);
    }

    /**
     * Отправить сообщение (message) от имени sender игрокам receivers.
     * Если отправителя нет в сети или ни один из получателей не онлайн, ничего не делает.
     * @param receivers получатели.
     * @param sender отправитель.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    default void sendMessage(Collection<DmsPlayer> receivers, DmsPlayer sender, String message) {
        sendMessage(receivers.stream().map(DmsPlayer::getName).collect(Collectors.toList()), sender.getName(), message);
    }

    /**
     * Отправить сообщения (messages) от имени sender игрокам receivers.
     * Если отправителя нет в сети или ни один из получателей не онлайн, ничего не делает.
     * @param receivers получатели.
     * @param sender отправитель.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    default void sendMessages(Collection<DmsPlayer> receivers, DmsPlayer sender, Collection<String> messages) {
        sendMessages(receivers.stream().map(DmsPlayer::getName).collect(Collectors.toList()), sender.getName(), messages);
    }

    /**
     * Отправить сообщение (message) от имени sender всем игрокам сервера.
     * Если отправитель не в сети, ничего не делает.
     * @param sender ник отправителя.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    void broadcastMessage(String sender, String message);

    /**
     * Отправить сообщения (messages) от имени sender всем игрокам сервера.
     * Если отправитель не в сети, ничего не делает.
     * @param sender ник отправителя.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    void broadcastMessages(String sender, Collection<String> messages);

    /**
     * Отправить сообщение (message) от имени sender игроку receiver.
     * Если один из игроков не в онлайне, ничего не делает.
     * @param receiver ник получателя.
     * @param sender ник отправителя.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    void sendMessage(String receiver, String sender, String message);

    /**
     * Отправить сообщения (messages) от имени sender игроку receiver.
     * Если один из игроков не в онлайне, ничего не делает.
     * @param receiver ник получателя.
     * @param sender ник отправителя.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    void sendMessages(String receiver, String sender, Collection<String> messages);

    /**
     * Отправить сообщение (message) от имени sender игрокам receivers.
     * Если отправителя нет в сети или ни один из получателей не онлайн, ничего не делает.
     * @param receivers ник получателей.
     * @param sender ник отправителя.
     * @param message сообщение (цветовые коды в нем автоматически не заменятся).
     */
    void sendMessage(Collection<String> receivers, String sender, String message);

    /**
     * Отправить сообщения (messages) от имени sender игрокам receivers.
     * Если отправителя нет в сети или ни один из получателей не онлайн, ничего не делает.
     * @param receivers ник получателей.
     * @param sender ник отправителя.
     * @param messages сообщения (цветовые коды в них автоматически не заменятся).
     */
    void sendMessages(Collection<String> receivers, String sender, Collection<String> messages);

}
