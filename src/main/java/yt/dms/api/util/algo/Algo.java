package yt.dms.api.util.algo;

/**
 * Created by k.shandurenko on 24.09.2018
 */
public interface Algo {

    /**
     * Получить реализацию алгоритма Дейкстры.
     * @return реализацию алгоритма Дейкстры.
     */
    Dijkstra getDijkstra();

}
