package yt.dms.api.util.algo;

import com.google.common.collect.Multimap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Map;

/**
 * Created by k.shandurenko on 24.09.2018
 */
public interface Dijkstra {

    DijkstraTwoDimensionalArrayResult onTwoDimensionalArray(int startingPositionX, int startingPositionY, boolean[][] field);

    <T> DijkstraGraphResult<T> onGraph(T startingPosition, Multimap<T, T> field);

    @Getter
    @RequiredArgsConstructor
    class DijkstraGraphResult<T> {

        private final Map<T, T> paths;
        private final Map<T, Integer> weights;

    }

    @Getter
    @RequiredArgsConstructor
    class DijkstraTwoDimensionalArrayResult {

        private final TwoDimensionalCoordinate[][] paths;
        private final Integer[][] weights;

        @Getter
        @RequiredArgsConstructor
        public static class TwoDimensionalCoordinate {

            private final int x;
            private final int y;

        }

    }

}
