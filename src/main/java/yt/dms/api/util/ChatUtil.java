package yt.dms.api.util;

/**
 * Created by k.shandurenko on 26.07.2018
 */
public interface ChatUtil {

    /**
     * Получить правильное слагательное наклонение в зависимости от количества.
     * Например, 2 -> убийства; 12 -> убийств; 21 -> убийство.
     * @param amount количество.
     * @param uno наклонение для одного (убийство).
     * @param duo наклонение для нескольких (убийства).
     * @param many наклонение для многих (убийств).
     * @return правильное слагательное наклонение в зависимости от количества.
     */
    default String transformByCount(int amount, String uno, String duo, String many) {
        int mod10 = amount % 10, mod100 = amount % 100;
        if (mod10 == 1 && mod100 != 11) {
            return uno;
        }
        if (mod10 >= 2 && mod10 <= 4 && (mod100 < 10 || mod100 > 20)) {
            return duo;
        }
        return many;
    }

    /**
     * Преобразовать & в цветовые коды.
     * @param message сообщение.
     * @return преобразованное сообщение.
     */
    String colorize(String message);

    /**
     * Отформатировать сообщение и преобразовать & в цветовые коды.
     * @param message сообщение.
     * @param args аргументы форматирования.
     * @return отформатированное преобразованное сообщение.
     */
    default String colorize(String message, Object... args) {
        return colorize(String.format(message, args));
    }

    /**
     * Добавить к сообщению префикс в нашем стиле.
     * Все & будут заменены на цветовые коды.
     * @param prefix префикс.
     * @param message сообщение.
     * @return сообщение с префиксом.
     */
    default String prefixed(String prefix, String message) {
        return colorize("&7[&6%s&7] &e%s", prefix, message);
    }

    /**
     * Отформатировать сообщение и добавить к нему префикс в нашем стиле.
     * Все & будут заменены на цветовые коды.
     * @param prefix префикс.
     * @param message сообщение.
     * @param messageArgs аргументы формативарония сообщения.
     * @return отформатированное сообщение с префиксом.
     */
    default String prefixed(String prefix, String message, Object... messageArgs) {
        return prefixed(prefix, String.format(message, messageArgs));
    }

}
