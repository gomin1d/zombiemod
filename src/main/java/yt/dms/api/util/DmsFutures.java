package yt.dms.api.util;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import yt.dms.api.DMS;
import yt.dms.api.annotation.SpigotOnly;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * Created by k.shandurenko on 04.08.2018
 */
public class DmsFutures {

    private final static Executor EXECUTOR = Executors.newFixedThreadPool(4, new ThreadFactory() {

        private final AtomicInteger id = new AtomicInteger();

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setDaemon(true);
            t.setName("DmsFuturesThread-" + this.id.incrementAndGet());
            return t;
        }

    });

    /**
     * Добавить асинхронный коллбек на фьючер.
     * @param future фьючер.
     * @param runner то, что нужно сделать с получаемым с фьючера значением.
     * @param <T> параметризация типа получаемого с фьючера значения.
     */
    public static <T> void addCallback(ListenableFuture<T> future, Consumer<T> runner) {
        addCallback(future, runner, null);
    }

    /**
     * Добавить асинхронный коллбек на фьючер.
     * @param future фьючер.
     * @param runner то, что нужно сделать с получаемым с фьючера значением.
     * @param onFailure то, что нужно сделать в случае ловли фьючером исключения.
     * @param <T> параметризация типа получаемого с фьючера значения.
     */
    public static <T> void addCallback(ListenableFuture<T> future, Consumer<T> runner, Consumer<Throwable> onFailure) {
        Futures.addCallback(future, new FutureCallback<T>() {
            @Override
            public void onSuccess(T result) {
                runner.accept(result);
            }

            @Override
            public void onFailure(Throwable throwable) {
                if (onFailure != null) {
                    onFailure.accept(throwable);
                } else {
                    throwable.printStackTrace();
                }
            }
        }, EXECUTOR);
    }

    /**
     * Добавить синхронный с главным потоком спигота коллбек на фьючер.
     * @param future фьючер.
     * @param syncRunner то, что нужно сделать с получаемым с фьючера значением.
     * @param <T> параметризация типа получаемого с фьючера значения.
     */
    @SpigotOnly
    public static <T> void addSpigotSyncCallback(ListenableFuture<T> future, Consumer<T> syncRunner) {
        addSpigotSyncCallback(future, syncRunner, null);
    }

    /**
     * Добавить синхронный с главным потоком спигота коллбек на фьючер.
     * @param future фьючер.
     * @param syncRunner то, что нужно сделать с получаемым с фьючера значением.
     * @param onFailureSync то, что нужно сделать в случае ловли фьючером исключения.
     * @param <T> параметризация типа получаемого с фьючера значения.
     */
    @SpigotOnly
    public static <T> void addSpigotSyncCallback(ListenableFuture<T> future, Consumer<T> syncRunner, Consumer<Throwable> onFailureSync) {
        addCallback(future, result -> DMS.spigot().runSync(() -> syncRunner.accept(result)), exception -> DMS.spigot().runSync(() -> onFailureSync.accept(exception)));
    }

}
