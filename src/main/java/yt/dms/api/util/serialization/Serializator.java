package yt.dms.api.util.serialization;

import yt.dms.api.annotation.SpigotOnly;

/**
 * Created by k.shandurenko on 07.09.2018
 */
public interface Serializator {

    /**
     * Получить реализацию DataSerializer без каких-либо данных внутри.
     * @return пустую реализацию DataSerializer.
     */
    DataSerializer createEmptyDataSerializer();

    /**
     * Получить реализацию DataSerializer с внутренними данными в виде переданного массива байт.
     * @param data данные, поверх которых нужно построить DataSerializer.
     * @return реализацию DataSerializer поверх переданного массива байт.
     */
    DataSerializer createDataSerializer(byte[] data);

    /**
     * Получить реализацию ItemStackSerializer для сериализации и десериализации ItemStack'ов.
     * @return реализацию ItemStackSerializer.
     */
    @SpigotOnly
    ItemStackSerializer getItemStackSerializer();

    /**
     * Получить реализацию LocationsSerializer для сериализации и десериализации Location.
     * @return реализацию LocationsSerializer.
     */
    @SpigotOnly
    LocationsSerializer getLocationsSerializer();

}
