package yt.dms.zombiemod;

public enum GameStage {

    DISABLED, COUNTDOWN, INGAME, RESTARTING

}
