package yt.dms.zombiemod.lib.mgapi.session;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;
import yt.dms.zombiemod.lib.mgapi.MObject;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.lib.mgapi.arena.GameArena;
import yt.dms.zombiemod.lib.mgapi.events.*;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.player.GameTeam;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Game session class
 *
 * @author serega6531
 */

public class GameSession extends MObject {

    private int GAME_TIME;
    private int WAIT_TIME;
    // (in seconds)

    private final ArrayList<GameTeam> teams = new ArrayList<>();
    private final GameArena arena;

    private final SessionManager manager;

    private ZMRunnable runnable;
    private boolean isstarted = false;
    private int minplayers, maxplayers;

    /**
     * Creating new session
     *
     * @param arena   Arena to use
     * @param manager {@link SessionManager Manager} for callback
     */

    GameSession(GameArena arena, SessionManager manager) {
        this.manager = manager;
        this.arena = arena;
        GAME_TIME = 60 * 15;
        WAIT_TIME = 60;
        minplayers = 2;
        maxplayers = 16;
        arena.use();
    }

    /**
     * Copy constructor
     */
    GameSession(GameSession sess, GameArena arena) {
        if (sess.isStarted()) {
            throw new IllegalStateException("Cannot copy started session");
        }

        if (sess.getArena().isInUse()) {
            sess.getArena().stopUse();
        }

        manager = sess.manager;
        GAME_TIME = sess.GAME_TIME;
        WAIT_TIME = sess.WAIT_TIME;
        minplayers = sess.minplayers;
        maxplayers = sess.maxplayers;
        runnable = sess.runnable;

        runnable.updateSession(this);

        isstarted = sess.isstarted;
        teams.addAll(sess.teams);
        data.putAll(sess.data);

        this.arena = arena;
    }

    /**
     * Restarts game countdown
     *
     * @throws IllegalStateException If game is already started
     */

    public void restart() throws IllegalStateException {
        if (isStarted())
            stopGame();
        arena.use();
        startCountDown();
    }

    /**
     * Set countdown
     *
     * @param wait Lobby wait time
     * @param game Game time
     */

    public void setTime(int wait, int game) {
        WAIT_TIME = wait;
        GAME_TIME = game;

        if(runnable != null)
            runnable.cancel();

        if (isstarted) {
            runnable = new GameRunnable(this);
            runnable.runTaskTimer(MinigamesAPI.getInstance().getPlugin(), 0L, 20L);
        } else
            startCountDown();
    }

    /**
     * Set players limit
     *
     * @param min Min
     * @param max Max
     */

    public void setPlayersLimit(int min, int max) {
        minplayers = min;
        maxplayers = max;
    }

    /**
     * Returns players limit
     * Result[0] = min;
     * Result[1] = max;
     *
     * @return Players limit
     */

    public int[] getPlayersLimit() {
        return new int[]{minplayers, maxplayers};
    }

    /**
     * @return Arena
     */

    public GameArena getArena() {
        return arena;
    }

    /**
     * @return Is game started
     */

    public boolean isStarted() {
        return isstarted;
    }

    /**
     * @return Player's in session count
     */

    public int getPlayersCount() {
        int count = 0;
        for (GameTeam team : getTeams()) count += team.getSize();
        return count;
    }

    /**
     * @return Array of teams
     */

    public GameTeam[] getTeams() {
        return teams.toArray(new GameTeam[teams.size()]);
    }

    public GameTeam getTeamByColor(ChatColor color) {
        for (GameTeam team : teams)
            if (team.getColor() == color) return team;
        return null;
    }

    public GamePlayer[] getAllPlayers() {
        GamePlayer[] res = new GamePlayer[getPlayersCount()];
        int c = 0;

        for (GameTeam team : teams) {
            List<GamePlayer> t = team.getPlayers();
            for (int i = 0; i < t.size(); i++, c++) {
                res[c] = t.get(i);
            }
        }

        return res;
    }

    public boolean joinPlayer(String name) throws NullPointerException, IllegalStateException {
        return joinPlayer(MinigamesAPI.getInstance().getGamePlayer(name));
    }

    /**
     * Adding new player to session.
     * Will not add if it was cancelled by {@link PlayerJoinGameSessionEvent event} or session already contains 16 players.
     * Check using {@link GameSession#getPlayersCount() getPlayersCount()}.
     * Will add to smaller team.
     *
     * @param player Game player
     * @return Was player added
     */

    public boolean joinPlayer(GamePlayer player) throws NullPointerException, IllegalStateException {
        if (player == null) throw new NullPointerException("Player cannot be null");
        if (!player.isOnline()) throw new IllegalStateException("Player " + player.getName() + " not online");
        GameTeam tojoin;
        if (teams.size() == 0) {
            GameTeam team = new GameTeam();
            addTeam(team);
            tojoin = team;
        } else {
            tojoin = teams.get(0);
            int min = tojoin.getSize();
            for (GameTeam team : getTeams())
                if (team.getSize() < min) tojoin = team;
        }
        return joinPlayer(player, tojoin);
    }

    /**
     * Adding new player to session.
     * Will not add if it was cancelled by {@link PlayerJoinGameSessionEvent event} or session already contains 16 players.
     * Check using {@link GameSession#getPlayersCount() getPlayersCount()}.
     *
     * @param player Game player
     * @param team   Team to join
     * @return Was player added
     */

    @SuppressWarnings("deprecation")
    public boolean joinPlayer(GamePlayer player, GameTeam team) throws NullPointerException {
        if (player == null) throw new NullPointerException("Player cannot be null");
        if (team == null) throw new NullPointerException("Team cannot be null");
        if (getPlayersCount() == maxplayers) {
            MinigamesAPI.debug(player.getName() + " can't join: too much players");
            return false;
        }
        PlayerJoinGameSessionEvent event = new PlayerJoinGameSessionEvent(player, this);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            MinigamesAPI.debug(player.getName() + " can't join: cancelled by event");
            return false;
        }

        GameSession last = MinigamesAPI.getInstance().getPlayerSession(player.getName());
        if (last != null) {
            last.removePlayer(player);
        }
        team.addPlayer(player);
        MinigamesAPI.debug(player.getName() + " joined to session");
        return true;
    }

    /**
     * Removing player from session.
     * Raising {@link PlayerLeaveGameSessionEvent event}.
     *
     * @param player Player
     */

    @SuppressWarnings("deprecation")
    public void removePlayer(GamePlayer player) throws NullPointerException {
        if (player == null) throw new NullPointerException("Player cannot be null");

        for (GameTeam team : getTeams()) team.removePlayer(player);
        removePlayer0(player);
    }

    private void removePlayer0(GamePlayer player) {
        MinigamesAPI.debug("Removing player " + player.getName());
        Bukkit.getPluginManager().callEvent(new PlayerLeaveGameSessionEvent(player, this));
        /*if(player.isOnline()){
			try {
				Class.forName("org.mcsg.double0negative.tabapi.TabAPI");
			} catch(NullPointerException | ClassNotFoundException e){}
		}*/
    }

    /**
     * @param name Player name
     * @return Is player in this session
     */

    public boolean isPlayerInSession(String name) {
        if (name == null) return false;
        for (GameTeam team : getTeams())
            if (team.containsPlayer(name)) return true;
        return false;
    }

    /**
     * @param name Player name
     * @return Player's team
     */

    public GameTeam getPlayerTeam(String name) {
        if (name == null) return null;
        for (GameTeam team : getTeams())
            if (team.containsPlayer(name)) return team;
        return null;
    }

    /**
     * Sending message to all session members
     *
     * @param message Message
     */

    public void sendBroadcastMessage(String message) {
        for (GameTeam team : getTeams())
            for (GamePlayer player : team.getPlayers())
                player.getEntity().sendMessage(message);
    }

    /**
     * Starting countdown to game start
     */

    private void startCountDown() {
        MinigamesAPI.debug("Starting countdown");
        runnable = new CountDownRunnable(this);
        runnable.runTaskTimer(MinigamesAPI.getInstance().getPlugin(), 0L, 20L);
    }

    /**
     * Add player to session
     *
     * @param team Команда
     */

    public void addTeam(GameTeam team) throws NullPointerException {
        if (team == null) throw new NullPointerException("Team cannot be null");
        teams.add(team);
    }

    /**
     * Add player to session
     */

    public void addTeam() {
        addTeam(new GameTeam());
    }

    /**
     * Starting game.
     * Stopping countdown.
     * Teleport teams to spawn.
     * Raising {@link GameSessionStartEvent event}.
     *
     * @throws IllegalStateException when players count not in [min, max]
     */

    public void startGame() {
        isstarted = true;
        Bukkit.getPluginManager().callEvent(new GameSessionStartEvent(this));
        MinigamesAPI.debug("Game started");

        runnable.cancel();
        runnable = new GameRunnable(this);
        runnable.runTaskTimer(MinigamesAPI.getInstance().getPlugin(), 0L, 20L);
    }

    /**
     * Stopping game. Raising {@link GameSessionStopEvent event}.
     */

    public void stopGame() {
        MinigamesAPI.debug("Stopping game");
        runnable.cancel();
        arena.stopUse();

        if (isStarted()) {
            isstarted = false;
            Bukkit.getPluginManager().callEvent(new GameSessionStopEvent(this));

            ListIterator<GamePlayer> iterator;
            for (GameTeam team : getTeams()) {
                iterator = team.getPlayers().listIterator();
                while (iterator.hasNext()) {
                    GamePlayer p = iterator.next();
                    iterator.remove();
                    removePlayer0(p);
                }
            }
        }
    }

    public void remove() {
        manager.removeSession(this);
    }

    private void removeOfflinePlayers() {
        for (GameTeam team : getTeams()) {
            for (GamePlayer player : team.getPlayers()) {
                if (!player.isOnline()) {
                    removePlayer(player);
                    return;
                }
            }
        }
    }

    public String toString() {
        return "GameSession[teams=" + teams.toString() + ",arena=" + arena.toString() + "]";
    }

    abstract class ZMRunnable extends BukkitRunnable {

        public abstract void updateSession(GameSession sess);

    }

    /**
     * Countdown to game start class
     *
     * @author serega6531
     */

    class CountDownRunnable extends ZMRunnable {

        private GameSession session;

        public CountDownRunnable(GameSession session) {
            this.session = session;
            countdown = session.WAIT_TIME;
        }

        int countdown;

        public void run() {
            if (countdown == 0) {
                GameSessionCountDownTickEvent e = new GameSessionCountDownTickEvent(session, session.WAIT_TIME);
                Bukkit.getPluginManager().callEvent(e);
                countdown = session.WAIT_TIME - e.getTime();
                if (session.getPlayersCount() >= minplayers) {
                    session.startGame();
                } else {
                    countdown = session.WAIT_TIME;
                    Bukkit.getPluginManager().callEvent(new GameSessionNotEnoughtPlayersEvent(session));
                }
                return;
            }
            session.removeOfflinePlayers();
            GameSessionCountDownTickEvent e = new GameSessionCountDownTickEvent(session, session.WAIT_TIME - countdown);
            Bukkit.getPluginManager().callEvent(e);
            countdown = session.WAIT_TIME - e.getTime();
            countdown--;
        }

        @Override
        public void updateSession(GameSession sess) {
            this.session = sess;
        }

    }

    /**
     * Countdown to game end class
     *
     * @author serega6531
     */

    class GameRunnable extends ZMRunnable {

        private GameSession session;

        public GameRunnable(GameSession session) {
            this.session = session;
            countdown = session.GAME_TIME;
        }

        int countdown;

        public void run() {
            if (countdown == 0) {
                GameSessionTickEvent e = new GameSessionTickEvent(session, session.GAME_TIME);
                Bukkit.getPluginManager().callEvent(e);
                countdown = session.GAME_TIME - e.getTime();
                if (countdown == 0) {
                    Bukkit.getPluginManager().callEvent(new GameSessionTimeoutEvent(session));
                    session.stopGame();
                }
                return;
            }
            countdown--;
            session.removeOfflinePlayers();
            GameSessionTickEvent e = new GameSessionTickEvent(session, session.GAME_TIME - countdown);
            Bukkit.getPluginManager().callEvent(e);
            countdown = session.GAME_TIME - e.getTime();
        }

        @Override
        public void updateSession(GameSession sess) {
            this.session = sess;
        }

    }

}