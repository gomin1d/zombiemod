package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

/**
 * Raise when player trying to cross arena border.
 *
 * @author serega6531
 */

public class GamePlayerCrossArenaBorderEvent extends Event {

    private final GamePlayer player;
    private final GameSession sess;

    public GamePlayerCrossArenaBorderEvent(GamePlayer player, GameSession sess) {
        this.player = player;
        this.sess = sess;
    }

    /**
     * @return Game player
     */

    public GamePlayer getPlayer() {
        return player;
    }

    /**
     * @return Session
     */

    public GameSession getSession() {
        return sess;
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
