package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

public class MAPINeedUpdateChatColorEvent extends Event {

    private final GameSession session;
    private CustomFormatter customFormatter;


    public MAPINeedUpdateChatColorEvent(final GameSession session, String format) {
        this.session = session;
        customFormatter = p -> format;
    }

    /**
     * @return Game session
     */

    public GameSession getSession() {
        return session;
    }

    public void setCustomFormatter(CustomFormatter f) throws NullPointerException {
        if (f == null) throw new NullPointerException("Formatter cannot be null");
        customFormatter = f;
    }

    public String formatChat(String p) {
        return customFormatter.format(p);
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
