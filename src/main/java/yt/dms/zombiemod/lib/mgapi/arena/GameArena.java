package yt.dms.zombiemod.lib.mgapi.arena;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Game arena
 *
 * @author serega6531
 */

public class GameArena {

    private final Location fc;
    private final Location sc;
    private final Location[] spawns;
    private final String name;
    private final String displayName;
    private boolean inuse = false;

    private final HashMap<String, Object> data = new HashMap<>();

    /**
     * DO NOT USE DIRECTLY!
     */

    GameArena(String name, Location fc, Location sc, Location[] spawns) {
        this.name = name;
        this.displayName = name;

        double tmp;

        if (fc.getX() < sc.getX()) {
            tmp = fc.getX();
            fc.setX(sc.getX());
            sc.setX(tmp);
        }

        if (fc.getY() < sc.getY()) {
            tmp = fc.getY();
            fc.setY(sc.getY());
            sc.setY(tmp);
        }

        if (fc.getZ() < sc.getZ()) {
            tmp = fc.getZ();
            fc.setZ(sc.getZ());
            sc.setZ(tmp);
        }

        this.fc = fc;
        this.sc = sc;
        this.spawns = spawns;
    }

    public GameArena(String name, String displayName, Location spawn) {
        this.fc = null;
        this.sc = null;
        this.spawns = new Location[] {spawn};
        this.name = name;
        this.displayName = displayName;
    }

    /**
     * Loading arena from config
     *
     * @param section {@link org.bukkit.configuration.ConfigurationSection Section} with arena's data
     * @return Arena
     */

    static GameArena loadFromConfig(ConfigurationSection section) {
        if (section == null) throw new NullPointerException("Section cannot be null");
        MinigamesAPI.debug("Loading arena " + section.getString("name"));
        ArrayList<Location> spawns = new ArrayList<>();
        for (String num : section.getValues(false).keySet())
            if (num.startsWith("spawn")) spawns.add(deserializeLocation(section.getConfigurationSection(num)));

        GameArena ret = new GameArena(
                ChatColor.translateAlternateColorCodes('&', section.getString("name")),
                deserializeLocation(section.getConfigurationSection("fc")),
                deserializeLocation(section.getConfigurationSection("sc")),
                spawns.toArray(new Location[0]));

        ConfigurationSection data = section.getConfigurationSection("data");

        if (data != null) {
            for (String num : data.getValues(false).keySet()) {
                if (data.isConfigurationSection(num)) {
                    ConfigurationSection ds = data.getConfigurationSection(num);
                    if (ds.contains("x") && ds.contains("y") && ds.contains("z")) {
                        ret.addData(num, deserializeLocation(ds));
                    }
                } else {
                    ret.addData(num, data.get(num));
                }
            }
        }

        return ret;
    }

    public void addData(String key, Object value) throws NullPointerException {
        if (key == null) throw new NullPointerException("Key cannot be null");
        if (value != null)
            data.put(key, value);
        else
            data.remove(key);
    }

    public Object getData(String key) {
        return data.get(key);
    }

    /**
     * @return Arena name
     */

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return this.displayName == null
                ? this.name
                : this.displayName;
    }

    /**
     * @return Arenas corners (bigger first)
     */

    public Location[] getCorners() {
        return new Location[]{fc, sc};
    }

    /**
     * @return Array of arena's spawns
     */

    public Location[] getSpawns() {
        return spawns;
    }

    /**
     * Saving arena to config
     *
     * @param fileConfiguration {@link org.bukkit.configuration.file.FileConfiguration Config}
     */

    public void saveArena(FileConfiguration fileConfiguration) {
        if (fileConfiguration == null) throw new NullPointerException("Config cannot be null");
        MinigamesAPI.debug("Saving arena " + name);
        ConfigurationSection sec = fileConfiguration.createSection("arenas." + name);
        sec.createSection("fc", serializeLocation(fc));
        sec.createSection("sc", serializeLocation(sc));
        for (int i = 0; i < spawns.length; i++)
            sec.createSection("spawn" + i, serializeLocation(spawns[i]));
        sec.set("name", name);
        for (Entry<String, Object> ent : data.entrySet()) {
            if (ent.getValue() instanceof Location)
                sec.createSection("data." + ent.getKey(), serializeLocation((Location) ent.getValue()));
            else
                sec.set("data." + ent.getKey(), ent.getValue());
        }
    }

    /**
     * Set arena using status
     */

    public void use() throws IllegalStateException {
        if (inuse) {
            MinigamesAPI.debug("Arena " + name + " is already using");
            throw new IllegalStateException("Arena " + name + " is already using");
        }
        MinigamesAPI.debug("Start using arena " + name);
        inuse = true;
    }

    /**
     * Set arena using status
     */

    public void stopUse() {
        MinigamesAPI.debug("Stop using arena " + name);
        inuse = false;
    }

    /**
     * @return Arena using status
     */

    public boolean isInUse() {
        return inuse;
    }

    private Map<String, Object> serializeLocation(Location loc) {
        Map<String, Object> map = new HashMap<>();

        map.put("x", loc.getX());
        map.put("y", loc.getY());
        map.put("z", loc.getZ());
        map.put("w", loc.getWorld().getName());
        return map;
    }

    private static Location deserializeLocation(ConfigurationSection section) {
        return new Location(
                Bukkit.getWorld(section.getString("w")),
                section.getDouble("x"),
                section.getDouble("y"),
                section.getDouble("z"));
    }

//    public boolean isLocInArena(Location loc) {
//        return loc.toVector().isInAABB(sc.toVector(), fc.toVector());
//    }

    public String toString() {
        return "GameArena[name=" + name + ",using=" + inuse + "]";
    }

}