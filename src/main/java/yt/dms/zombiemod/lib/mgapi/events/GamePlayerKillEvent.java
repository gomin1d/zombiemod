package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

/**
 * Raise when player kills another player in game session
 *
 * @author serega6531
 */

public class GamePlayerKillEvent extends Event {

    private final GamePlayer killer;
    private final GamePlayer victim;
    private final GameSession session;

    private final PlayerDeathEvent event;

    public GamePlayerKillEvent(GamePlayer killer, GamePlayer victim, GameSession session, PlayerDeathEvent bukkitEvent) {
        this.killer = killer;
        this.victim = victim;
        this.session = session;
        this.event = bukkitEvent;
    }

    /**
     * @return Killer
     */

    public GamePlayer getKiller() {
        return killer;
    }

    /**
     * @return Killer's victim
     */

    public GamePlayer getVictim() {
        return victim;
    }

    /**
     * @return Session where kill is happened
     */

    public GameSession getSession() {
        return session;
    }

    /**
     * @return Bukkit source event
     */

    public PlayerDeathEvent getBukkitEvent() {
        return event;
    }

    private final static HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
