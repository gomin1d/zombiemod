package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

/**
 * Raise every second in game session
 *
 * @author serega6531
 */

public class GameSessionTickEvent extends Event {

    private final GameSession session;
    private int time;

    public GameSessionTickEvent(GameSession session, int time) {
        this.session = session;
        this.time = time;
    }

    /**
     * @return Game session
     */

    public GameSession getSession() {
        return session;
    }

    /**
     * @return Time until game start
     */

    public int getTime() {
        return time;
    }

    /**
     * Set time until game start
     */

    public void setTime(int time) {
        if (time < 0) time = 0;
        this.time = time;
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
