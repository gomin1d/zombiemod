package yt.dms.zombiemod.lib.mgapi;

import org.bukkit.Bukkit;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import yt.dms.zombiemod.lib.mgapi.events.*;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;
import yt.dms.zombiemod.lib.mgapi.signs.GameSign;
import yt.dms.zombiemod.lib.mgapi.signs.SignManager;

@SuppressWarnings("unused")
class EventListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        GameSession s = MinigamesAPI.getInstance().getPlayerSession(e.getPlayer().getName());
        if (s != null) {
            s.removePlayer(MinigamesAPI.getInstance().getGamePlayer(e.getPlayer().getName()));
        }
        e.setQuitMessage(null);
    }

//    @EventHandler(ignoreCancelled = true)
//    public void onMove(PlayerMoveEvent e) {
//        GameSession session = MinigamesAPI.getInstance().getPlayerSession(e.getPlayer().getName());
//        if (session != null) {
//            GameArena arena = session.getArena();
//            if (!arena.isLocInArena(e.getTo())) {
//                Bukkit.getPluginManager().callEvent(new GamePlayerCrossArenaBorderEvent(
//                        MinigamesAPI.getInstance().getGamePlayer(e.getPlayer().getName()), session)
//                );
//            }
//        }
//    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        BlockState state = e.getClickedBlock().getState();
        if (state instanceof Sign) {
            Sign sign = (Sign) state;
            GameSign gs = SignManager.getSign(sign.getLocation());
            if (gs != null)
                gs.getHandler().onClick(e.getPlayer(), gs.getCommand(), gs.getArgument());
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onDeath(PlayerDeathEvent e) {
        Player victim = e.getEntity();
        Player killer = victim.getKiller();
        GameSession s1 = MinigamesAPI.getInstance().getPlayerSession(victim.getName());
        if (killer == null && s1 != null) {
            GamePlayerDeathEvent event = new GamePlayerDeathEvent(
                    MinigamesAPI.getInstance().getGamePlayer(victim.getName()),
                    s1,
                    e);
            Bukkit.getPluginManager().callEvent(event);
            e.setDeathMessage("");
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onKill(PlayerDeathEvent e) {
        Player victim = e.getEntity();
        Player killer = victim.getKiller();
        if (killer == null) return;
        GameSession s1, s2;
        s1 = MinigamesAPI.getInstance().getPlayerSession(killer.getName());
        s2 = MinigamesAPI.getInstance().getPlayerSession(victim.getName());
        if (s1 != null && s1.equals(s2)) {
            GamePlayerKillEvent event = new GamePlayerKillEvent(
                    MinigamesAPI.getInstance().getGamePlayer(killer.getName()),
                    MinigamesAPI.getInstance().getGamePlayer(victim.getName()),
                    s1,
                    e);
            Bukkit.getPluginManager().callEvent(event);
            e.setDeathMessage("");
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onHit(EntityDamageByEntityEvent e) {
        if (e.getDamager().getType() == EntityType.PLAYER && e.getEntityType() == EntityType.PLAYER) {
            GamePlayer damager = MinigamesAPI.getInstance().getGamePlayer(e.getDamager().getName());
            GamePlayer victim = MinigamesAPI.getInstance().getGamePlayer(e.getEntity().getName());
            GameSession s1 = MinigamesAPI.getInstance().getPlayerSession(e.getDamager().getName());
            GameSession s2 = MinigamesAPI.getInstance().getPlayerSession(e.getEntity().getName());
            if (s1 != null && s1.equals(s2)) {
                GamePlayerHitEvent event = new GamePlayerHitEvent(damager, victim, s1, e);
                Bukkit.getPluginManager().callEvent(event);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onBreak(BlockBreakEvent e) {
        if (SignManager.containsSign(e.getBlock().getLocation())) {
            GameSign sign = SignManager.getSign(e.getBlock().getLocation());
            GameSignDestroyEvent event = new GameSignDestroyEvent(sign);
            Bukkit.getPluginManager().callEvent(event);
            SignManager.unregisterSign(e.getBlock().getLocation());
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        GameSession sess = MinigamesAPI.getInstance().getPlayerSession(e.getPlayer().getName());
        if (sess != null) {
            MAPINeedUpdateChatColorEvent e1 = new MAPINeedUpdateChatColorEvent(sess, e.getFormat());
            Bukkit.getPluginManager().callEvent(e1);
            e.setFormat(e1.formatChat(e.getPlayer().getName()));
        }
    }

}
