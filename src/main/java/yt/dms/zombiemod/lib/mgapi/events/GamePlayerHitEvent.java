package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

/**
 * Raising when player hits another player in game session
 *
 * @author serega6531
 */

public class GamePlayerHitEvent extends Event {

    private final GamePlayer damager;
    private final GamePlayer victim;
    private final GameSession session;

    private final EntityDamageByEntityEvent event;

    public GamePlayerHitEvent(GamePlayer damager, GamePlayer victim, GameSession session, EntityDamageByEntityEvent bukkitEvent) {
        this.damager = damager;
        this.victim = victim;
        this.session = session;
        this.event = bukkitEvent;
    }

    /**
     * @return Damager
     */

    public GamePlayer getDamager() {
        return damager;
    }

    /**
     * @return Damager's victim
     */

    public GamePlayer getVictim() {
        return victim;
    }

    /**
     * @return Session where player hits another player
     */

    public GameSession getSession() {
        return session;
    }

    /**
     * @return Bukkit source event
     */

    public EntityDamageByEntityEvent getBukkitEvent() {
        return event;
    }

    private final static HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
