package yt.dms.zombiemod.lib.mgapi;

import yt.dms.zombiemod.data.DataKey;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

@SuppressWarnings("unchecked")
public class MObject {

    protected final Map<String, Object> data = new HashMap<>();

    public void addData(DataKey dataKey, Object value) throws NullPointerException {
        this.data.put(dataKey.getKey(), value);
    }

    public <T> T getData(DataKey dataKey) {
        return (T) data.get(dataKey.getKey());
    }

    public boolean hasData(DataKey key) {
        return this.data.containsKey(key.getKey());
    }

    public void clearData() {
        this.data.clear();
    }

    public <T> T computeData(DataKey dataKey, BiFunction<String, T, T> remappingFunction) {
        return (T) this.data.compute(dataKey.getKey(), (k, v) -> remappingFunction.apply(k, (T) v));
    }

}
