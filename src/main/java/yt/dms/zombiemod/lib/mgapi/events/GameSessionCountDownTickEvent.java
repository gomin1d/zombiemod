package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

/**
 * Raise every second in countdown
 *
 * @author serega6531
 */

public class GameSessionCountDownTickEvent extends Event {

    private final GameSession session;
    private int time;

    public GameSessionCountDownTickEvent(GameSession session, int time) {
        this.session = session;
        this.time = time;
    }

    /**
     * @return Game session
     */

    public GameSession getSession() {
        return session;
    }

    /**
     * @return Time until countdown start
     */

    public int getTime() {
        return time;
    }

    /**
     * Set time until start
     */

    public void setTime(int time) {
        if (time < 0) time = 0;
        this.time = time;
    }

    /**
     * Locks countdown. Use every time.
     */

    public void lockTime() {
        time = 0;
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
