package yt.dms.zombiemod.lib.mgapi.events;

public interface CustomFormatter {

    String format(String p);

}
