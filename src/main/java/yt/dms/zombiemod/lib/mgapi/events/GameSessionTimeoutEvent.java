package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

/**
 * Raise when game session time is out
 *
 * @author serega6531
 */

public class GameSessionTimeoutEvent extends Event {

    private final GameSession session;

    public GameSessionTimeoutEvent(GameSession session) {
        this.session = session;
    }

    /**
     * @return Game session
     */

    public GameSession getSession() {
        return session;
    }

    private final static HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}