package yt.dms.zombiemod.lib.mgapi;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import yt.dms.zombiemod.lib.mgapi.arena.ArenaManager;
import yt.dms.zombiemod.lib.mgapi.session.SessionManager;
import yt.dms.zombiemod.lib.mgapi.signs.SignManager;

/**
 * Minigame controller.
 * Contains all managers.
 *
 * @author serega6531
 */

public class MinigamePluginController {

    private final Plugin plugin;
    private final FileConfiguration config;
    private final SessionManager sm;
    private final ArenaManager am;
    private final SignManager sim;

    /**
     * Do not use directly.
     */

    MinigamePluginController(Plugin plugin) {
        this.plugin = plugin;
        this.config = plugin.getConfig();
        sm = new SessionManager();
        am = new ArenaManager();
        sim = new SignManager();

        am.loadArenas(config);
    }

    /**
     * @return Main minigames class
     */

    public Plugin getPlugin() {
        return plugin;
    }

    /**
     * @return Sessions manager
     */

    public SessionManager getSessionManager() {
        return sm;
    }

    /**
     * @return Arenas manager
     */

    public ArenaManager getArenaManager() {
        return am;
    }

    /**
     * @return Signs manager
     */

    public SignManager getSignManager() {
        return sim;
    }

    /**
     * @return Plugin config
     */

    public FileConfiguration getConfig() {
        return config;
    }

    /**
     * @return Main MinigamesAPI class
     * @see MinigamesAPI#getInstance()
     */

    public MinigamesAPI getAPI() {
        return MinigamesAPI.getInstance();
    }

}
