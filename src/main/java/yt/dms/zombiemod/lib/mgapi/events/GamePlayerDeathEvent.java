package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

/**
 * Raise when player dies in game session
 *
 * @author kargond
 */

public class GamePlayerDeathEvent extends Event {

    private final GamePlayer player;
    private final GameSession session;

    private final PlayerDeathEvent event;

    public GamePlayerDeathEvent(GamePlayer player, GameSession session, PlayerDeathEvent bukkitEvent) {
        this.player = player;
        this.session = session;
        this.event = bukkitEvent;
    }

    /**
     * @return Player
     */

    public GamePlayer getPlayer() {
        return player;
    }

    /**
     * @return Session where kill is happened
     */

    public GameSession getSession() {
        return session;
    }

    /**
     * @return Bukkit source event
     */

    public PlayerDeathEvent getBukkitEvent() {
        return event;
    }

    private final static HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
