package yt.dms.zombiemod.lib.mgapi;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;
import yt.dms.zombiemod.lib.mgapi.signs.SignManager;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Main MinigamesAPI class
 *
 * @author serega6531
 */

public class MinigamesAPI {

    private final HashMap<String, GamePlayer> players = new HashMap<>();
    private MinigamePluginController api;

    public Connection db;

    private static MinigamesAPI singletone;
    private Plugin pl;
    private static boolean debug = true;

    public MinigamesAPI() {
        singletone = this;
    }

    public MinigamePluginController run(Plugin pl) {
        this.pl = pl;
//        debug = pl.getConfig().getBoolean("debug-mode", false);
        try {
            Class.forName("org.sqlite.JDBC");
            pl.getDataFolder().mkdir();
            db = DriverManager.getConnection("jdbc:sqlite:" + pl.getDataFolder().getAbsolutePath() + File.separator + "database.db");
            db.createStatement().execute("CREATE TABLE IF NOT EXISTS money (user TEXT PRIMARY KEY UNIQUE NOT NULL, money INTEGER NOT NULL)");
        } catch (ClassNotFoundException | SQLException e) {
            pl.getLogger().warning("Cant open database: " + e.getMessage());
            throw new IllegalStateException("[MinigamesAPI]: Cant open database: " + e.getMessage());
        }
        Bukkit.getPluginManager().registerEvents(new EventListener(), pl);
        SignManager.runSignUpdater(pl);
        MinigamePluginController cont = new MinigamePluginController(pl);
        this.api = cont;
        return cont;
    }

    public void stop() {
        api.getArenaManager().saveArenas(api.getConfig());
        api.getPlugin().saveConfig();
        for (GameSession session : api.getSessionManager().getAllSessions()) session.stopGame();
        for (GamePlayer player : players.values())
            player.pushMoney();
        Bukkit.getScheduler().cancelTasks(pl);
    }

    /**
     * Return player session
     *
     * @param player Player name
     * @return Session, if it exists, or null
     */

    public GameSession getPlayerSession(String player) {
        GameSession session = api.getSessionManager().getPlayerSesion(player);
        if (session != null) {
            return session;
        }
        return null;
    }

    /**
     * Return player status
     *
     * @param player Player name
     */

    public boolean isPlayerInSession(String player) {
        return getPlayerSession(player) != null;
    }

    /**
     * Return game player class
     *
     * @param player Player name
     */

    public GamePlayer getGamePlayer(String player) {
        if (players.containsKey(player)) {
            return players.get(player);
        }
        GamePlayer gplayer = new GamePlayer(player);
        players.put(player, gplayer);
        return gplayer;
    }

    /**
     * Return MinigamesAPI {@link MinigamesAPI#singletone class}
     */

    public static MinigamesAPI getInstance() {
        return singletone;
    }

    /**
     * Show debug message
     */

    public static void debug(String message) {
        if (debug)
            singletone.getPlugin().getLogger().info("[MinigamesAPI DEBUG] " + message);
    }

    public static boolean isDebug() {
        return debug;
    }

    public Plugin getPlugin() {
        return pl;
    }

    public MinigamePluginController getMinigamePluginController() {
        return this.api;
    }

}
