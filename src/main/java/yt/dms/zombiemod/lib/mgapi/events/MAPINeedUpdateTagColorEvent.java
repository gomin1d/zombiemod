package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

public class MAPINeedUpdateTagColorEvent extends Event {

    private final GameSession session;
    private CustomFormatter customFormatter;


    public MAPINeedUpdateTagColorEvent(final GameSession session) {
        this.session = session;
        customFormatter = p -> p;
    }

    /**
     * @return Game session
     */

    public GameSession getSession() {
        return session;
    }

    public void setCustomFormatter(CustomFormatter f) throws NullPointerException {
        if (f == null) throw new NullPointerException("Formatter cannot be null");
        customFormatter = f;
    }

    public String formatTag(String p) {
        return customFormatter.format(p);
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
