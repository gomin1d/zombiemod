package yt.dms.zombiemod.lib.mgapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

/**
 * Raise when player leaving game session.
 *
 * @author serega6531
 */

public class PlayerLeaveGameSessionEvent extends Event {

    private final GamePlayer player;
    private final GameSession session;

    public PlayerLeaveGameSessionEvent(GamePlayer player, GameSession session) {
        this.player = player;
        this.session = session;
    }

    /**
     * @return Game player
     */

    public GamePlayer getPlayer() {
        return player;
    }

    /**
     * @return Game session
     */

    public GameSession getSession() {
        return session;
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
