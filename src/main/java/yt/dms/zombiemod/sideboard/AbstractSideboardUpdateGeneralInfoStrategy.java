package yt.dms.zombiemod.sideboard;

import org.bukkit.entity.Player;
import yt.dms.api.DMS;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.gamer.Gamer;

public abstract class AbstractSideboardUpdateGeneralInfoStrategy
        extends AbstractSideboardUpdateStrategy
        implements SideboardUpdateGeneralInfoStrategy {

    @Override
    public void updateRating(Player player, Gamer gamer) {
        this.update(player, gamer, 4, "Рейтинг", "&a", Gamer::getPoints);
    }

    @Override
    public void updateXp(Player player, Gamer gamer) {
        this.update(player, gamer, 3, "Серебро", "&a", Gamer::getXp);
    }

    @Override
    public void updateServer(Player player) {
        SIDE_BOARD_UTILS.send(player, 2, "Сервер", "&a" + DMS.spigot().getServerName());
    }

    @Override
    public void updateSite(Player player) {
        SIDE_BOARD_UTILS.send(player, 1, "§6Наш сайт", "&bwww.dms.yt");
    }

    @Override
    public void sendGeneralInformation(Player player, Gamer gamer) {
        if (gamer == null) {
            gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);
        }

        this.updateRating(player, gamer);
        this.updateXp(player, gamer);
        this.updateServer(player);
        this.updateSite(player);
    }

}
