package yt.dms.zombiemod.sideboard.game;

import org.bukkit.entity.Player;
import yt.dms.api.spigot.SSideBoard;
import yt.dms.zombiemod.ZombieModGameLaunchPoint;
import yt.dms.zombiemod.sideboard.AbstractSideboardUpdateGeneralInfoStrategy;

public class GameSideboardUpdateStrategyImpl
        extends AbstractSideboardUpdateGeneralInfoStrategy
        implements GameSideboardUpdateStrategy {

    @Override
    public void updateMap(Player player) {
        SIDE_BOARD_UTILS.send(player, 5, "Карта", "&c" + ZombieModGameLaunchPoint.getPlugin().getSession().getArena().getDisplayName());
    }

    @Override
    public void updateSurvivors(Player player, int survivors) {
        SIDE_BOARD_UTILS.send(player, 7, "Выживших", "&9&l" + survivors);
    }

    @Override
    public void updateZombies(Player player, int zombies) {
        SIDE_BOARD_UTILS.send(player, 6, "Зараженных", "&c&l" + zombies);
    }

    @Override
    public void send(Player player) {
        SIDE_BOARD_UTILS.send(player, 9, "ZombieMod", SSideBoard.AnimationGamma.AQUA);
        this.update(player);
    }

    @Override
    public void update(Player player) {
        this.updateMap(player);
        this.updateSurvivors(player);
        this.updateZombies(player);
        this.sendGeneralInformation(player);
    }

}
