package yt.dms.zombiemod.sideboard.game;

import org.bukkit.entity.Player;
import yt.dms.zombiemod.ZTeam;
import yt.dms.zombiemod.sideboard.SideboardUpdateGeneralInfoStrategy;
import yt.dms.zombiemod.utils.Utils;

public interface GameSideboardUpdateStrategy extends SideboardUpdateGeneralInfoStrategy {

    void updateMap(Player player);

    default void updateSurvivors(Player player) {
        this.updateSurvivors(player, Utils.getPlayersInTeam(ZTeam.SURVIVOR).size());
    }

    void updateSurvivors(Player player, int survivors);

    default void updateZombies(Player player) {
        this.updateZombies(player, Utils.getPlayersInTeam(ZTeam.ZOMBIE).size());
    }

    void updateZombies(Player player, int zombies);

}
