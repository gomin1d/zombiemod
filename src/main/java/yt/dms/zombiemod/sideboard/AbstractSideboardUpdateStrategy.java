package yt.dms.zombiemod.sideboard;

import org.bukkit.entity.Player;
import yt.dms.api.DMS;
import yt.dms.api.spigot.SSideBoard;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.gamer.Gamer;

import java.util.function.Function;

public abstract class AbstractSideboardUpdateStrategy implements SideboardUpdateStrategy {

    public static final SSideBoard SIDE_BOARD_UTILS = DMS.spigot().getSideBoardUtils();

    protected void update(Player player, Gamer gamer, int priority, String name, String color, Function<Gamer, Object> getObject) {
        if (gamer == null) {
            gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);
        }

        SIDE_BOARD_UTILS.send(player, priority, name, color + getObject.apply(gamer));
    }

}
