package yt.dms.zombiemod.sideboard;

import org.bukkit.entity.Player;
import yt.dms.zombiemod.gamer.Gamer;

public interface SideboardUpdateGeneralInfoStrategy extends SideboardUpdateStrategy {

    default void updateXp(Player player) {
        this.updateXp(player, null);
    }

    void updateXp(Player player, Gamer gamer);

    default void updateRating(Player player) {
        this.updateRating(player, null);
    }

    void updateRating(Player player, Gamer gamer);

    void updateServer(Player player);

    void updateSite(Player player);

    default void sendGeneralInformation(Player player) {
        this.sendGeneralInformation(player, null);
    }

    void sendGeneralInformation(Player player, Gamer gamer);

}
