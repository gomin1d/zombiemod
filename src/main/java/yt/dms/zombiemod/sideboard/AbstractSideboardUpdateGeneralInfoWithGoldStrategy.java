package yt.dms.zombiemod.sideboard;

import org.bukkit.entity.Player;
import yt.dms.api.DMS;
import yt.dms.zombiemod.gamer.Gamer;

public abstract class AbstractSideboardUpdateGeneralInfoWithGoldStrategy
        extends AbstractSideboardUpdateGeneralInfoStrategy
        implements SideboardUpdateGeneralInfoWithGoldStrategy {

    @Override
    public void updateRating(Player player, Gamer gamer) {
        this.update(player, gamer, 5, "Рейтинг", "&a", Gamer::getPoints);
    }

    @Override
    public void updateGold(Player player) {
        SIDE_BOARD_UTILS.send(player, 4, "Золото ", "&a" + DMS.getPlayerRegistry().get(player.getName()).getGold().getAmount());
    }

    @Override
    public void sendGeneralInformationWithGold(Player player, Gamer gamer) {
        this.sendGeneralInformation(player, gamer);
        this.updateGold(player);
    }

}
