package yt.dms.zombiemod.sideboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public interface SideboardUpdateStrategy {

    /**
     * Отправить сайдборд игроку
     *
     * @param player игрок
     */
    void send(Player player);

    /**
     * Обновить сайдборд
     *
     * @param player игрок
     */
    void update(Player player);

    /**
     * Обновить сайдборд для всех игроков
     */
    default void update() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            this.update(player);
        }
    }

}
