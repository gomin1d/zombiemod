package yt.dms.zombiemod.sideboard.lobby;

import org.bukkit.entity.Player;
import yt.dms.api.spigot.SSideBoard;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.gamer.Gamer;
import yt.dms.zombiemod.sideboard.AbstractSideboardUpdateGeneralInfoWithGoldStrategy;

public class LobbySideboardUpdateStrategyImpl
        extends AbstractSideboardUpdateGeneralInfoWithGoldStrategy
        implements LobbySideboardUpdateStrategy {

    @Override
    public void send(Player player) {
        SIDE_BOARD_UTILS.send(player, 9, "ZombieMod", SSideBoard.AnimationGamma.AQUA);

        this.update(player);
    }

    @Override
    public void updateZombieKilled(Player player, Gamer gamer) {
        this.update(player, gamer, 8, "Убито зомби", "&a", Gamer::getZombieKilled);
    }

    @Override
    public void updateSurvivorsKilled(Player player, Gamer gamer) {
        this.update(player, gamer, 7, "Инфицировано людей", "&c", Gamer::getSurvivorsKilled);
    }

    @Override
    public void updateGamesPlayed(Player player, Gamer gamer) {
        this.update(player, gamer, 6, "Сыграно матчей", "&a", Gamer::getGamesPlayed);
    }

    @Override
    public void update(Player player) {
        Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);

        this.updateSurvivorsKilled(player, gamer);
        this.updateZombieKilled(player, gamer);
        this.updateGamesPlayed(player, gamer);
        this.sendGeneralInformationWithGold(player, gamer);
    }

}
