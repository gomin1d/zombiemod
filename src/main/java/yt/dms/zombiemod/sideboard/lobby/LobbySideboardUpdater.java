package yt.dms.zombiemod.sideboard.lobby;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class LobbySideboardUpdater implements Listener {

    private final LobbySideboardUpdateStrategy sideboardUpdateStrategy;

    public LobbySideboardUpdater(LobbySideboardUpdateStrategy sideboardUpdateStrategy) {
        this.sideboardUpdateStrategy = sideboardUpdateStrategy;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        this.sideboardUpdateStrategy.send(event.getPlayer());
    }

}
