package yt.dms.zombiemod.sideboard.lobby;

import org.bukkit.entity.Player;
import yt.dms.zombiemod.gamer.Gamer;
import yt.dms.zombiemod.sideboard.SideboardUpdateGeneralInfoWithGoldStrategy;

public interface LobbySideboardUpdateStrategy extends SideboardUpdateGeneralInfoWithGoldStrategy {

    default void updateZombieKilled(Player player) {
        this.updateZombieKilled(player, null);
    }

    void updateZombieKilled(Player player, Gamer gamer);

    default void updateSurvivorsKilled(Player player) {
        this.updateSurvivorsKilled(player, null);
    }

    void updateSurvivorsKilled(Player player, Gamer gamer);

    default void updateGamesPlayed(Player player) {
        this.updateGamesPlayed(player, null);
    }

    void updateGamesPlayed(Player player, Gamer gamer);

}
