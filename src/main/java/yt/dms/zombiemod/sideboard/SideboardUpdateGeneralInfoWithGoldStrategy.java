package yt.dms.zombiemod.sideboard;

import org.bukkit.entity.Player;
import yt.dms.zombiemod.gamer.Gamer;

public interface SideboardUpdateGeneralInfoWithGoldStrategy extends SideboardUpdateGeneralInfoStrategy {

    void updateGold(Player player);

    default void sendGeneralInformationWithGold(Player player) {
        this.sendGeneralInformationWithGold(player, null);
    }

    void sendGeneralInformationWithGold(Player player, Gamer gamer);

}
