package yt.dms.zombiemod.sideboard.wait;

import org.bukkit.entity.Player;
import yt.dms.api.spigot.SSideBoard;
import yt.dms.zombiemod.ZombieModGameLaunchPoint;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.sideboard.AbstractSideboardUpdateGeneralInfoWithGoldStrategy;

public class WaitGameSideboardUpdateStrategyImpl
        extends AbstractSideboardUpdateGeneralInfoWithGoldStrategy
        implements WaitGameSideboardUpdateStrategy {

    @Override
    public void updatePlayers(Player player) {
        SIDE_BOARD_UTILS.send(player, 7, "Игроков",
                "&a" + ZombieModGameLaunchPoint.getPlugin().getSession().getPlayersCount() +
                        "§8/§4" + ZombieModBootstrap.getSettings().getMaxPlayers()
        );
    }

    @Override
    public void updatePlayersNeeded(Player player) {
        SIDE_BOARD_UTILS.send(player, 6, "Для старта нужно", "&a" + ZombieModBootstrap.getSettings().getMinPlayers());
    }

    @Override
    public void send(Player player) {
        SIDE_BOARD_UTILS.send(player, 9, "ZombieMod", SSideBoard.AnimationGamma.AQUA);
        this.update(player);
    }

    @Override
    public void update(Player player) {
        this.updatePlayers(player);
        this.updatePlayersNeeded(player);
        this.sendGeneralInformationWithGold(player);
    }

}
