package yt.dms.zombiemod.sideboard.wait;

import org.bukkit.entity.Player;
import yt.dms.zombiemod.sideboard.SideboardUpdateGeneralInfoWithGoldStrategy;

public interface WaitGameSideboardUpdateStrategy extends SideboardUpdateGeneralInfoWithGoldStrategy {

    void updatePlayers(Player player);

    void updatePlayersNeeded(Player player);

}
