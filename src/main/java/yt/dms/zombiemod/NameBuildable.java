package yt.dms.zombiemod;

import yt.dms.zombiemod.gamer.Gamer;

public interface NameBuildable {

    /**
     * Получить отображаемое название возможности
     *
     * @return название возможности
     */
    String getDisplayName();

    /**
     * Есть ли данная возможность у геймера
     * @param gamer геймер
     * @return {@code true} если имеет, {@code false} в противном случае
     */
    boolean has(Gamer gamer);

    /**
     * Сгенерировать отображаемое название для определенного геймера
     * Если у геймера есть данная возможность, то в конце названия добавляется
     * 'Доступно', в противном случае 'Недоступно'
     * @param gamer геймер
     * @return название возможности
     */
    default String buildDisplayNameForGamer(Gamer gamer) {
        return this.getDisplayName() + " " + (this.has(gamer) ? Message.AVAILABLE : Message.NOTAVAILABLE);
    }

}
