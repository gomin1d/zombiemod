package yt.dms.zombiemod;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import yt.dms.zombiemod.cases.ItemData;

public class ZMItemData {

    private ShopElement.ShopElementType type;
    private String displayName;
    private ItemData item;
    private int weight;
    private int content;

    public ZMItemData(ShopElement.ShopElementType type, String displayName, ItemData item, int weight, int content) {
        this.type = type;
        this.displayName = displayName;
        this.item = item;
        this.weight = weight;
        this.content = content;
    }

    public ShopElement.ShopElementType getType() {
        return type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public ItemData getItem() {
        return item;
    }

    public int getWeight() {
        return weight;
    }

    public int getContent() {
        return content;
    }

    public ItemStack create() {
        ItemStack i = item.create();
        net.minecraft.server.v1_8_R3.ItemStack cis = CraftItemStack.asNMSCopy(i);
        NBTTagCompound tag;

        if (!cis.hasTag())
            cis.setTag(new NBTTagCompound());
        tag = cis.getTag();

        tag.setString("ztype", type.toString());
        tag.setInt("zcid", content);

        return CraftItemStack.asCraftMirror(cis);
    }

}
