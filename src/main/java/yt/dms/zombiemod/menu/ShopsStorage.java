package yt.dms.zombiemod.menu;

import lombok.Getter;
import yt.dms.zombiemod.menu.shop.ShopOpenStrategy;
import yt.dms.zombiemod.menu.shop.ShopOpenStrategyImpl;
import yt.dms.zombiemod.menu.shop.choose.ChooseElementShopOpenStrategy;
import yt.dms.zombiemod.menu.shop.choose.ChooseElementShopOpenStrategyImpl;
import yt.dms.zombiemod.menu.shop.element.ShopElement;
import yt.dms.zombiemod.menu.shop.weapon.WeaponShopOpenStrategy;
import yt.dms.zombiemod.menu.shop.weapon.WeaponShopOpenStrategyImpl;

import java.util.HashMap;
import java.util.Map;

import static yt.dms.zombiemod.menu.shop.element.ShopElement.create;
import static yt.dms.zombiemod.menu.shop.element.ShopElementType.ZOMBIE;

@Getter
public class ShopsStorage {

    private final WeaponShopOpenStrategy weaponShopOpenStrategy;

    private final ChooseElementShopOpenStrategy chooseZombieShopOpenStrategy;

    private final ShopOpenStrategy shopOpenStrategy;

    private static final Map<Integer, ShopElement> ITEMS_FOR_ZOMBIE_SHOP = new HashMap<Integer, ShopElement>() {{
        put(0, create(1, 266, "§c§lКласический", ZOMBIE,
                "§7Жизни:§2 100"
        ));
        put(1, create(2, 318, "§c§lОхотник", ZOMBIE,
                "§7Жизни:§2 70",
                "§7На зомби есть эффект скорости 4"
        ));
        put(2, create(7, 280, "§c§lТоксик", ZOMBIE,
                "§7Жизни:§2 80",
                "§7Может создавать токсичное облако,",
                "§7которое отравляет игроков",
                "§7в радиусе 7 блоков"
        ));
        put(3, create(4, 370, "§c§lПрыгун", ZOMBIE,
                "§7Жизни:§2 60",
                "§7На зомби есть эффект большого прыжка 2"
        ));
        put(4, create(5, 331, "§c§lПудж", ZOMBIE,
                "§7Жизни:§2 140",
                "§7На последней минуте может",
                "§7активировать способность §4Крик",
                "§7Каждую минуту получает бомбу"
        ));
        put(5, create(6, 264, "§c§lАлиен", ZOMBIE,
                "§7Жизни:§2 80",
                "§7При получении урона шанс 25% наложить",
                "§7эффект слепоты на врага на 5 сек.",
                "§7Не может получить эффект слепоты",
                "§7При ударе ослепляет врага на 10 сек."
        ));
        put(6, create(3, 265, "§c§lСкелет", ZOMBIE,
                "§7Жизни:§2 10",
                "§7На зомби есть эффект невидимости,",
                "§7если он ударил кого-либо,",
                "§7то эффект пропадает на 3 секунды",
                "§7Если в него попал снаряд,",
                "§7то эффект пропадает на 1 секунду"
        ));
        put(7, create(8, 281, "§c§lПаук", ZOMBIE,
                "§7Жизни:§2 40",
                "§7Умеет стрелять паутиной и",
                "§7прыгать на врагов."
        ));
    }};

    public ShopsStorage() {
        this.weaponShopOpenStrategy = new WeaponShopOpenStrategyImpl(this);
        this.chooseZombieShopOpenStrategy = new ChooseElementShopOpenStrategyImpl(
                "§f§lЗомби",
                ITEMS_FOR_ZOMBIE_SHOP,
                this::getShopOpenStrategy
        );
        this.shopOpenStrategy = new ShopOpenStrategyImpl(
                this.weaponShopOpenStrategy,
                this.chooseZombieShopOpenStrategy
        );
    }

}
