package yt.dms.zombiemod.menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import yt.dms.api.spigot.menu.MenuItem;

public class MenuItemWrapper extends MenuItem {

    private ClickHandler clickHandler = ((player, clickType, slot) -> {});

    /**
     * Создать кликабельный предмет для меню.
     * @param icon отображаемая иконка.
     */
    public MenuItemWrapper(ItemStack icon) {
        super(icon);
    }

    /**
     * Создать кликабельный предмет для меню без отображаемой иконки
     * (а на самом деле с иконкой в виде админиума без названия и описания).
     */
    public MenuItemWrapper() {
        this(new ItemStack(Material.BEDROCK));
    }

    public MenuItemWrapper withClickHandler(ClickHandler clickHandler) {
        this.clickHandler = clickHandler;

        return this;
    }

    @Override
    public void onClick(Player player, ClickType clickType, int slot) {
        this.clickHandler.onClick(player, clickType, slot);
    }

    @FunctionalInterface
    public interface ClickHandler {

        void onClick(Player player, ClickType clickType, int slot);

    }

}
