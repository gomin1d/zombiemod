package yt.dms.zombiemod.menu.shop.element;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import yt.dms.api.DMS;
import yt.dms.zombiemod.Message;
import yt.dms.zombiemod.ZombieType;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.gamer.Gamer;
import yt.dms.zombiemod.gun.Gun;

import static yt.dms.zombiemod.ZombieModGameLaunchPoint.NOCSAPI;

@SuppressWarnings("Duplicates")
public enum ShopElementType {

    ZOMBIE {
        @Override
        public void apply0(Player player, int contentId, Gamer gamer) {
            if (gamer.hasZombie(contentId) || (!NOCSAPI && DMS.getPlayerRegistry().get(player.getName()).getPermissions().isYoutuber())) {
                Message.CHOOSEZOMBIE.send(player);
                gamer.buyZombie(contentId);
                gamer.setZombieType(ZombieType.byId(contentId));
            } else {
                Message.NOZOMBIE.send(player);
            }
        }
    }, GUN {
        @Override
        public void apply0(Player player, int contentId, Gamer gamer) {
            Gun gun = ZombieModBootstrap.getInstance().getGunRegistry().get(contentId);

            if (gun == null) {
                return;
            }

            if (gamer.hasGun(contentId) || (!NOCSAPI && DMS.getPlayerRegistry().get(player.getName()).getPermissions().isYoutuber())) {
                Message.CHOOSEGUN.send(player);
                gamer.buyGun(contentId);

                if (gun.getGroup() == 4) {
                    if (gamer.getGun(4) != 0) {
                        gamer.setGun(5, gamer.getGun(4));
                    }

                    gamer.setGun(4, contentId);
                } else {
                    gamer.setGun(gun.getGroup(), contentId);
                }
            } else {
                Message.NOGUN.send(player);
            }
        }
    }, ARMOR {
        @Override
        public void apply0(Player player, int contentId, Gamer gamer) {
            if (gamer.haveArmor()) {
                Message.HASARMOR.send(player);
            } else {
                if (!NOCSAPI && DMS.getPlayerRegistry().get(player.getName()).getPermissions().isYoutuber()) {
                    Message.HASARMOR.send(player);
                    gamer.addArmor();
                    player.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
                } else {
                    Message.NOARMOR.send(player);
                }
            }
        }
    }, DECORATE {
        @Override
        void apply0(Player player, int contentId, Gamer gamer) {
            // nothing
        }

        @Override
        public void apply(Player player, int contentId) {
            // nothing
        }
    };

    public void apply(Player player, int contentId) {
        Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);

        this.apply0(player, contentId, gamer);
    }

    abstract void apply0(Player player, int contentId, Gamer gamer);

}
