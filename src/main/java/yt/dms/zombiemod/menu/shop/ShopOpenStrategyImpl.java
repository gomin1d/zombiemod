package yt.dms.zombiemod.menu.shop;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import yt.dms.api.DMS;
import yt.dms.api.spigot.item.DmsItemStack;
import yt.dms.api.spigot.item.DmsItemStackMetaBuilder;
import yt.dms.api.spigot.menu.Menu;
import yt.dms.zombiemod.menu.MenuItemWrapper;
import yt.dms.zombiemod.menu.shop.choose.ChooseElementShopOpenStrategy;
import yt.dms.zombiemod.menu.shop.weapon.WeaponShopOpenStrategy;

public class ShopOpenStrategyImpl implements ShopOpenStrategy {

    private Menu shopMenu;

    public ShopOpenStrategyImpl(WeaponShopOpenStrategy weaponShopOpenStrategy, ChooseElementShopOpenStrategy chooseZombieShopOpenStrategy) {
        DmsItemStack weaponShop = new DmsItemStack(Material.SKULL_ITEM, "§f§lМагазин оружия", new DmsItemStackMetaBuilder()
                .durability(3)
                .build()
        );
        DmsItemStack zombieShop = new DmsItemStack(Material.SKULL_ITEM, "§f§lМагазин зомби", new DmsItemStackMetaBuilder()
                .durability(2)
                .build()
        );

        this.shopMenu = DMS.spigot().getMenuUtils().create("§f§lМагазин", 1);
        this.shopMenu.addItem(new MenuItemWrapper(weaponShop).withClickHandler((clicker, clickType, slot) -> {
            weaponShopOpenStrategy.open(clicker, true);
        }), 2);
        this.shopMenu.addItem(new MenuItemWrapper(zombieShop).withClickHandler((clicker, clickType, slot) -> {
            chooseZombieShopOpenStrategy.open(clicker, true);
        }), 6);
    }

    @Override
    public void open(Player player, boolean addBackItem) {
        this.shopMenu.open(player);
    }

}
