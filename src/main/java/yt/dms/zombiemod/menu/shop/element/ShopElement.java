package yt.dms.zombiemod.menu.shop.element;

import lombok.Getter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import yt.dms.api.DMS;
import yt.dms.zombiemod.NameBuildable;
import yt.dms.zombiemod.ZombieType;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.gamer.Gamer;

import java.util.*;

public class ShopElement {

    private int contentId, itemId;
    private String name;
    private List<String> lore;

    @Getter
    private ShopElementType type;

    public ShopElement(int contentId, int itemId, String name, List<String> lore, ShopElementType type) {
        this.contentId = contentId;
        this.itemId = itemId;
        this.name = name;
        this.lore = lore;
        this.type = type;

        this.lore.replaceAll(DMS.getChatUtil()::colorize);
    }

    public static ShopElement loadFromSection(ConfigurationSection section) {
        return new ShopElement(
                section.getInt("cid", 0),
                section.getInt("iid", 1),
                section.getString("name", "unknown"),
                section.getStringList("lore"),
                ShopElementType.valueOf(section.getString("type", "DECORATE").toUpperCase())
        );
    }

    public static Map<Integer, ShopElement> loadMapFromSection(ConfigurationSection section) {
        return section.getValues(false).entrySet().stream()
                .map(entry -> new AbstractMap.SimpleEntry<>(
                        Integer.parseInt(entry.getKey()),
                        loadFromSection((ConfigurationSection) entry.getValue())
                )).collect(
                        HashMap::new,
                        (map, entry) -> map.put(entry.getKey() - 1, entry.getValue()),
                        Map::putAll
                );
    }

    public static ShopElement create(int cid, int iid, String name, ShopElementType type, String... lore) {
        return new ShopElement(cid, iid, name, Arrays.asList(lore), type);
    }

    public void handleClick(Player player) {
        this.type.apply(player, this.contentId);
    }

    public ItemStack build(Player player) {
        ItemStack itemStack = new ItemStack(this.itemId);
        ItemMeta itemMeta = itemStack.getItemMeta();

        itemMeta.setDisplayName(this.buildName(player));
        itemMeta.setLore(this.lore);

        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    private String buildName(Player player) {
        String result = this.name;

        if (player != null && (this.type == ShopElementType.GUN || this.type == ShopElementType.ZOMBIE)) {
            Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);
            NameBuildable nameBuildable = this.type == ShopElementType.GUN
                    ? ZombieModBootstrap.getInstance().getGunRegistry().get(this.contentId)
                    : ZombieType.byId(this.contentId);

            return nameBuildable.buildDisplayNameForGamer(gamer);
        }

        return DMS.getChatUtil().colorize(result);
    }

}
