package yt.dms.zombiemod.menu.shop;

import org.bukkit.entity.Player;
import yt.dms.zombiemod.menu.OpenMenuStrategy;

public interface ShopOpenStrategy extends OpenMenuStrategy {

    /**
     * Открыть инвентарь магазина игроку
     * В родительском методе open(Player, boolean) игнорируется второй аргумент,
     * так как возвращаться некуда
     *
     * @param player игрок
     */
    @Override
    default void open(Player player) {
        this.open(player, false);
    }

}
