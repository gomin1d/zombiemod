package yt.dms.zombiemod.menu.shop.weapon;

import org.bukkit.entity.Player;
import yt.dms.zombiemod.menu.OpenMenuStrategy;

public interface WeaponShopOpenStrategy extends OpenMenuStrategy {

    /**
     * Открыть инвентарь магазина оружия игроку
     *
     * @param player игрок
     * @param addBackItem надо ли добавлять кнопку возвращения назад
     */
    @Override
    void open(Player player, boolean addBackItem);

    /**
     * Открыть инвентарь магазина оружия игроку без добавления кнопки возвращения назад
     *
     * @param player игрок
     */
    @Override
    default void open(Player player) {
        this.open(player, false);
    }

}
