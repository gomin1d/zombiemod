package yt.dms.zombiemod.menu.shop.choose;

import org.bukkit.entity.Player;
import yt.dms.api.DMS;
import yt.dms.api.spigot.menu.Menu;
import yt.dms.api.spigot.menu.MenuItem;
import yt.dms.zombiemod.menu.MenuItemWrapper;
import yt.dms.zombiemod.menu.OpenMenuStrategy;
import yt.dms.zombiemod.menu.shop.element.ShopElement;
import yt.dms.zombiemod.utils.MenuUtils;

import java.util.Map;
import java.util.function.Supplier;

public class ChooseElementShopOpenStrategyImpl implements ChooseElementShopOpenStrategy {

    private final String title;

    private final Map<Integer, ShopElement> items;

    private final MenuItem backItem;

    public ChooseElementShopOpenStrategyImpl(String title, Map<Integer, ShopElement> items, Supplier<OpenMenuStrategy> backTo) {
        this.title = title;
        this.items = items;

        this.backItem = new MenuItemWrapper(MenuUtils.BACK_ITEM_ICON).withClickHandler((clicker, clickType, slot) -> {
            backTo.get().open(clicker, true);
        });
    }

    @Override
    public void open(Player player, boolean addBackItem) {
        // Округляем в большую и добавляем еще 9 слотов, если надо добавить кнопку возврата
        int lines = ((this.items.size() + 8) / 9) + (addBackItem ? 1 : 0);
        Menu menu = DMS.spigot().getMenuUtils().create(this.title, lines);

        for (Map.Entry<Integer, ShopElement> entry : this.items.entrySet()) {
            ShopElement shopElement = entry.getValue();

            menu.addItem(
                    new MenuItemWrapper(shopElement.build(player))
                            .withClickHandler((clicker, clickType, slot) -> shopElement.handleClick(clicker)),
                    entry.getKey()
            );
        }

        if (addBackItem) {
            menu.addItem(this.backItem, lines * 9 - 5);
        }

        menu.open(player);
    }

}
