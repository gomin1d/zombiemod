package yt.dms.zombiemod.menu.shop.weapon;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import yt.dms.api.DMS;
import yt.dms.api.spigot.item.DmsItemStack;
import yt.dms.api.spigot.menu.Menu;
import yt.dms.api.spigot.menu.MenuItem;
import yt.dms.zombiemod.menu.MenuItemWrapper;
import yt.dms.zombiemod.menu.ShopsStorage;
import yt.dms.zombiemod.menu.shop.choose.ChooseElementShopOpenStrategy;
import yt.dms.zombiemod.menu.shop.choose.ChooseElementShopOpenStrategyImpl;
import yt.dms.zombiemod.menu.shop.element.ShopElement;
import yt.dms.zombiemod.menu.shop.element.ShopElementType;
import yt.dms.zombiemod.utils.MenuUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static yt.dms.zombiemod.menu.shop.element.ShopElement.create;
import static yt.dms.zombiemod.menu.shop.element.ShopElementType.GUN;

public class WeaponShopOpenStrategyImpl implements WeaponShopOpenStrategy {

    private final Map<Integer, MenuItem> preparedItems;

    private final MenuItem backItem;

    private static final String TITLE = "Оружия";

    private static final Map<Integer, ShopElement> ITEMS_FOR_FIRST_PAGE = new HashMap<Integer, ShopElement>() {{
        put(0, create(1, 268, "§7§lClub", GUN,
                "§7Урон: §214",
                "§7Отдача: §210"
        ));
        put(1, create(12, 272, "§7§lToxic", GUN,
                "§7Урон: §28",
                "§7Отдача: §210",
                "§7Накладывает эффект отравления на 8 секунд"
        ));
        put(2, create(18, 285, "§7§lЛёдяной нож", GUN,
                "§7Урон: §210",
                "§7Отдача: §210",
                "§7Накладывает эффект медлительности 2го уровня на 8 секунд"
        ));
        put(3, create(21, 276, "§7§lНож мясника", GUN,
                "§7Урон: §28",
                "§7Отдача: §210",
                "§7Шанс 40% нанести 18 урона"
        ));
        put(4, create(27, 256, "§7§lБензопила", GUN,
                "§7Урон: §25",
                "§7Отдача: §210",
                "§7Шанс 5% убить зомби с 1 удара"
        ));
    }};

    private static final Map<Integer, ShopElement> ITEMS_FOR_SECOND_PAGE = new HashMap<Integer, ShopElement>() {{
        put(0, create(2, 290, "§3§lGlock", GUN,
                "§7Урон: §21",
                "§7Урон в голову: §24",
                "§7Патронов в обойме: §220",
                "§7Перезарядка: §24 сек.",
                "§7Скорострельность: §cНизкая",
                "§7Точность: §280% §8(§e90%§8)",
                "§7Вес: §22.5 §7кг. (5% замедление)"
        ));
        put(1, create(3, 294, "§3§lDeagle", GUN,
                "§7Урон: §25",
                "§7Урон в голову: §29",
                "§7Патронов в обойме: §27",
                "§7Перезарядка: §23.5 сек.",
                "§7Скорострельность: §4Очень низкая",
                "§7Точность: §260% §8(§e80%§8)",
                "§7Вес: §22.5 §7кг. (5% замедление)"
        ));
        put(2, create(20, 258, "§3Револьвер", GUN,
                "§7Урон: §25",
                "§7Урон в голову: §210",
                "§7Патронов в обойме: §26",
                "§7Перезарядка: §23.5 сек.",
                "§7Скорострельность: §4Очень низкая",
                "§7Точность: §280% §8(§e95%§8)",
                "§7Вес: §22 §7кг. (4% замедление)"
        ));
        put(3, create(17, 270, "§3Double Berettas", GUN,
                "§7Урон: §22",
                "§7Урон в голову: §25",
                "§7Патронов в обойме: §215+15",
                "§7Перезарядка: §25 сек.",
                "§7Скорострельность: §3Высокая",
                "§7Точность: §235% §8(§e45%§8)", "§7Вес: §23.5 §7кг. (7% замедление)"
        ));
    }};

    private static final Map<Integer, ShopElement> ITEMS_FOR_THIRD_PAGE = new HashMap<Integer, ShopElement>() {{
        put(0, create(4, 293, "§a§lMP5", GUN,
                "§7Урон: §21",
                "§7Урон в голову: §25",
                "§7Патронов в обойме: §250",
                "§7Перезарядка: §24.5 сек.",
                "§7Скорострельность: §6Средняя",
                "§7Точность: §265% §8(§e85%§8)",
                "§7Вес: §24.5 §7кг. (9% замедление)"
        ));
        put(1, create(5, 271, "§a§lM4", GUN,
                "§7Урон: §22",
                "§7Урон в голову: §28",
                "§7Патронов в обойме: §230",
                "§7Перезарядка: §23.5 сек.",
                "§7Скорострельность: §3Высокая",
                "§7Точность: §285% §8(§e95%§8)",
                "§7Вес: §25 §7кг. (10% замедление)"
        ));
        put(2, create(6, 275, "§a§lAK-47", GUN,
                "§7Урон: §23",
                "§7Урон в голову: §27",
                "§7Патронов в обойме: §230",
                "§7Перезарядка: §24.5 сек",
                "§7Скорострельность: §3Высокая",
                "§7Точность: §275% §8(§e80%§8)",
                "§7Вес: §25 §7кг. (10% замедление)"
        ));
        put(3, create(7, 269, "§a§lSuper", GUN,
                "§7Урон: §27",
                "§7Урон в голову: §212",
                "§7Патронов в обойме: §27",
                "§7Перезарядка: §27x1 сек.",
                "§7Скорострельность: §4Очень низкая",
                "§7Точность: §20% §8(§e40%§8)",
                "§2Отдача: §32",
                "§7Вес: §26 §7кг. (12% замедление)"
        ));
        put(4, create(8, 267, "§a§lSSG552", GUN,
                "§7Урон: §23",
                "§7Урон в голову: §28",
                "§7Патронов в обойме: §230",
                "§7Перезарядка: §24 сек.",
                "§7Скорострельность: §6Средняя",
                "§7Точность: §215% §8(§c100%§8)",
                "§7Вес: §25 §7кг. (10% замедление)"
        ));
        put(5, create(19, 279, "§a§lГолечник", GUN,
                "§7Урон: §22",
                "§7Урон в голову: §24",
                "§7Патронов в обойме: §211",
                "§7Перезарядка: §25 сек.",
                "§7Скорострельность: §6Средняя",
                "§7Точность: §220% §8(§e60%§8)",
                "§7Накладывает эффект слепоты на 3 сек.",
                "§7Вес: §26 §7кг. (12% замедление)"
        ));
        put(6, create(11, 273, "§a§lАрбалет", GUN,
                "§7Урон: §28",
                "§7Урон в голову: §214",
                "§7Патронов в обойме: §21",
                "§7Перезарядка: §22 сек.",
                "§7Скорострельность: §8-",
                "§7Точность: §235% §8(§c100%§8)",
                "§7Отдача: §23",
                "§7Возможно приблизить!",
                "§7Шанс 5% нанести 30 доп. урона",
                "§7Вес: §23 §7кг. (6% замедление)"
        ));
        put(7, create(9, 277, "§a§lMG", GUN,
                "§7Урон: §23",
                "§7Урон в голову: §26",
                "§7Патронов в обойме: §2100",
                "§7Перезарядка: §28 сек.",
                "§7Скорострельность: §6Средняя",
                "§7Точность: §235% §8(§e60%§8)",
                "§7Вес: §29 §7кг. (18% замедление)"
        ));
        put(8, create(10, 286, "§a§lСнайперка", GUN,
                "§7Урон: §25",
                "§7Урон в голову: §211",
                "§7Патронов в обойме: §25",
                "§7Перезарядка: §25x1.2 сек.",
                "§7Скорострельность: §4Очень низкая",
                "§7Точность: §c100%",
                "§2Отдача: §34",
                "§7Возможно приблизить!",
                "§7При попадании в голову на зомби",
                "§7накладывает эффект тошноты на 8 секунд",
                "§7Вес: §28 §7кг. (16% замедление)"
        ));
        put(9, create(22, 284, "§a§lDart", GUN,
                "§7Урон: §23",
                "§7Урон в голову: §26",
                "§7Патронов в обойме: §27",
                "§7Перезарядка: §24.5 сек.",
                "§7Скорострельность: §cНизкая",
                "§7Точность: §250% §8(§e75%§8)",
                "§7Отравляет цель на 5 сек", "§7Вес: §24 §7кг. (8% замедление)"
        ));
        put(10, create(13, 278, "§a§lОгнемет", GUN,
                "§7Урон: §23",
                "§7Урон в голову: §22",
                "§7Патронов в обойме: §250",
                "§7Перезарядка: §212 сек.",
                "§7Скорострельность: §6Средняя",
                "§7Точность: §20%",
                "§7Поджигает цель",
                "§7Вес: §212 §7кг. (24% замедление)"
        ));
        put(11, create(24, 291, "§a§lSpiderGun", GUN,
                "§7Урон: §28",
                "§7Урон в голову: §28",
                "§7Патронов в обойме: §29",
                "§7Перезарядка: §24 сек.",
                "§7Скорострельность: §6Средняя",
                "§7Точность: §275 §8(§e85%§8%)",
                "§7Замедляет цель",
                "§7Вес: §27.5 §7кг. (15% замедление)"
        ));
    }};

    private static final Map<Integer, ShopElement> ITEMS_FOR_FOURTH_PAGE = new HashMap<Integer, ShopElement>() {{
        put(0, create(14, 368, "§e§lОгненная граната", GUN,
                "§7Поджигает цель на 11 сек."
        ));
        put(1, create(15, 336, "§e§lЛедяная граната", GUN,
                "§7Замораживает цель на 10 сек."
        ));
        put(2, create(16, 287, "§e§lТоксичная граната", GUN,
                "§7Накладывает на цель эффект отравления 2го уровня на 8 сек."
        ));
        put(3, create(25, 334, "§e§lСлеповая граната", GUN,
                "§7Накладывает на цель эффект слепоты на 9 сек."
        ));
        put(4, create(26, 296, "§e§lТермитная граната", GUN,
                "§7Наносит 25 урона врагу."
        ));
    }};

    public WeaponShopOpenStrategyImpl(ShopsStorage shopsStorage) {
        Map<Integer, ChooseElementShopOpenStrategy> specificWeapons = new HashMap<>();
        this.preparedItems = new HashMap<>();

        for (int i = 0; i < 4; i++) {
            final int indexAsFinal = i;
            String shopTitle;
            ItemStack icon;
            Map<Integer, ShopElement> items;

            // Разрешили захардкодить))0
            switch (i) {
                case 0: {
                    shopTitle = "§a§lОружие ближнего боя";
                    icon = new DmsItemStack(Material.STONE_SWORD, shopTitle);
                    items = ITEMS_FOR_FIRST_PAGE;

                    break;
                }
                case 1: {
                    shopTitle = "§a§lПистолеты";
                    icon = new DmsItemStack(Material.GOLD_HOE, shopTitle);
                    items = ITEMS_FOR_SECOND_PAGE;

                    break;
                }
                case 2: {
                    shopTitle = "§a§lАвтоматы";
                    icon = new DmsItemStack(Material.STONE_AXE, shopTitle);
                    items = ITEMS_FOR_THIRD_PAGE;

                    break;
                }
                case 3: {
                    shopTitle = "§a§lГранаты";
                    icon = new DmsItemStack(Material.LEATHER, shopTitle, Collections.singletonList("§fМожно выбрать 2 штуки"));
                    items = ITEMS_FOR_FOURTH_PAGE;

                    break;
                }
                default: {
                    continue;
                }
            }

            specificWeapons.put(i, new ChooseElementShopOpenStrategyImpl(shopTitle, items, () -> this));
            this.preparedItems.put(i, new MenuItemWrapper(icon).withClickHandler(((clicker, clickType, slot) -> {
                specificWeapons.get(indexAsFinal).open(clicker, true);
            })));
        }

        ItemStack icon = new DmsItemStack(Material.IRON_CHESTPLATE, "&a&lБронежилет", Collections.singletonList("&7Защита: &26"));
        this.preparedItems.put(8, new MenuItemWrapper(icon).withClickHandler((clicker, clickType, slot) -> {
            ShopElementType.ARMOR.apply(clicker, 0);
        }));

        this.backItem = new MenuItemWrapper(MenuUtils.BACK_ITEM_ICON).withClickHandler((clicker, clickType, slot) -> {
            shopsStorage.getShopOpenStrategy().open(clicker, true);
        });
    }

    @Override
    public void open(Player player, boolean addBackItem) {
        Menu menu = DMS.spigot().getMenuUtils().create(TITLE, addBackItem ? 2 : 1);

        for (Map.Entry<Integer, MenuItem> entry : this.preparedItems.entrySet()) {
            menu.addItem(entry.getValue(), entry.getKey());
        }

        if (addBackItem) {
            menu.addItem(this.backItem, 13);
        }

        menu.open(player);
    }

}
