package yt.dms.zombiemod.menu.shop.choose;

import org.bukkit.entity.Player;
import yt.dms.zombiemod.menu.OpenMenuStrategy;

public interface ChooseElementShopOpenStrategy extends OpenMenuStrategy {

    /**
     * Открыть инвентарь магазина выбора элемента игроку
     *
     * @param player игрок
     * @param addBackItem надо ли добавлять кнопку возвращения назад
     */
    @Override
    void open(Player player, boolean addBackItem);

}
