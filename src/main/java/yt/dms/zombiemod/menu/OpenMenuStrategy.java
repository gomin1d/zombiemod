package yt.dms.zombiemod.menu;

import org.bukkit.entity.Player;

public interface OpenMenuStrategy {

    /**
     * Открыть инвентарь меню игроку
     *
     * @param player игрок
     * @param addBackItem надо ли добавлять кнопку возвращения назад
     */
    void open(Player player, boolean addBackItem);

    /**
     * Открыть инвентарь меню игроку без добавления кнопки возвращения назад
     *
     * @param player игрок
     */
    default void open(Player player) {
        this.open(player, false);
    }

}
