package yt.dms.zombiemod.gun;

import java.util.HashMap;
import java.util.Map;

public class HardcodedGunRegistry implements GunRegistry {

    private final Map<Integer, Gun> guns = new HashMap<>();

    @Override
    public Gun get(int id) {
        return this.guns.get(id);
    }

    @Override
    public int load() {
        this.create(  1   ,  1  ,  "Club"           ,  "§7§lClub"               );
        this.create(  12  ,  1  ,  "Toxic"          ,  "§7§lToxic"              );
        this.create(  18  ,  1  ,  "Ice"            ,  "§7§lЛёдяной нож"        );
        this.create(  21  ,  1  ,  "Cleaver"        ,  "§7§lНож мясника"        );
        this.create(  27  ,  1  ,  "Chainsaw"       ,  "§7§lБензопила"          );
        this.create(  2   ,  2  ,  "Glock"          ,  "§3§lGlock"              );
        this.create(  3   ,  2  ,  "Deagle"         ,  "§3§lDeagle"             );
        this.create(  17  ,  2  ,  "Double"         ,  "§3Double Berettas"      );
        this.create(  20  ,  2  ,  "Revolver"       ,  "§3Револьвер"            );
        this.create(  4   ,  3  ,  "MP5"            ,  "§a§lMP5"                );
        this.create(  5   ,  3  ,  "M4"             ,  "§a§lM4"                 );
        this.create(  6   ,  3  ,  "AK-47"          ,  "§a§lAK-47"              );
        this.create(  7   ,  3  ,  "Super"          ,  "§a§lSuper"              );
        this.create(  8   ,  3  ,  "SSG552"         ,  "§a§lSSG552"             );
        this.create(  9   ,  3  ,  "MG"             ,  "§a§lMG"                 );
        this.create(  10  ,  3  ,  "Hunting"        ,  "§a§lСнайперка"          );
        this.create(  11  ,  3  ,  "Crossbow"       ,  "§a§lАрбалет"            );
        this.create(  13  ,  3  ,  "Flamethrower"   ,  "§a§lОгнемет"            );
        this.create(  19  ,  3  ,  "Needler"        ,  "§a§lГолечник"           );
        this.create(  22  ,  3  ,  "Dart"           ,  "§a§lDart"               );
        this.create(  24  ,  3  ,  "SpiderGun"      ,  "§a§lSpiderGun"          );
        this.create(  14  ,  4  ,  "FireGranade"    ,  "§e§lОгненная граната"   );
        this.create(  15  ,  4  ,  "FreezGranade"   ,  "§e§lЛедяная граната"    );
        this.create(  16  ,  4  ,  "ToxicGranade"   ,  "§e§lТоксичная граната"  );
        this.create(  25  ,  4  ,  "Flashbang"      ,  "§e§lСлеповая граната"   );
        this.create(  26  ,  4  ,  "DamageGrenade"  ,  "§e§lТермитная граната"  );

        return this.guns.size();
    }

    private void create(int id, int group, String name, String displayName) {
        this.guns.put(id, new SimpleGun(id, group, name, displayName));
    }

}
