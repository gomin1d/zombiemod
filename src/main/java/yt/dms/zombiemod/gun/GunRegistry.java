package yt.dms.zombiemod.gun;

public interface GunRegistry {

    /**
     * Получить оружие по его айди
     * 
     * @param id айди
     * @return оружие или null, если оружия с таким айди не найдено
     */
    Gun get(int id);

    /**
     * Загрузить все оружие
     *
     * @return количество загруженного оружия
     */
    int load();

}
