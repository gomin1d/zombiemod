package yt.dms.zombiemod.gun;

import com.shampaggon.crackshot.CSUtility;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import yt.dms.zombiemod.NameBuildable;
import yt.dms.zombiemod.gamer.Gamer;

@Getter
@AllArgsConstructor
public class SimpleGun implements Gun, NameBuildable {

    private final int id, group;

    private final String name, displayName;

    @Override
    public boolean has(Gamer gamer) {
        return gamer.hasGun(this.id);
    }

    @Override
    public ItemStack buildItem() {
        ItemStack item = new CSUtility().generateWeapon(this.name);
        ItemMeta meta = item.getItemMeta();

        meta.spigot().setUnbreakable(true);

        item.setItemMeta(meta);
        return item;
    }

}
