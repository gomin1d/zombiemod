package yt.dms.zombiemod.gun;

import org.bukkit.inventory.ItemStack;
import yt.dms.zombiemod.NameBuildable;

public interface Gun extends NameBuildable {

    /**
     * Получить айди оружия
     *
     * @return айди оружия
     */
    int getId();

    /**
     * Получить группу оружия
     * @return группу оружия
     */
    int getGroup();

    /**
     * Создать предмет с этим оружием
     *
     * @return предмет
     */
    ItemStack buildItem();

}
