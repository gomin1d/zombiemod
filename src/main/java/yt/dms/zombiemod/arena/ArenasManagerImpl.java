package yt.dms.zombiemod.arena;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.utils.Utils;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArenasManagerImpl implements ArenasManager {

    private final List<String> arenas;

    private static final String MAPS_FOLDER = "maps";

    public ArenasManagerImpl() {
        File mapsFolder = new File(ZombieModBootstrap.getServerFolder(), MAPS_FOLDER);

        if (mapsFolder.exists()) {
            File[] files = mapsFolder.listFiles();

            if (files != null) {
                this.arenas = Collections.unmodifiableList(Stream.of(files)
                        .filter(file -> file.isDirectory() || file.getName().endsWith(".zip"))
                        .map(File::getName)
                        .collect(Collectors.toList())
                );

                return;
            }
        }

        this.arenas = Collections.emptyList();
    }

    @Override
    public void load(String name) throws IllegalStateException {
        if (this.isLoaded(name)) {
            throw new IllegalStateException("arena '" + name + "' already loaded");
        }

        Utils.copyWorldAndLoad(MAPS_FOLDER, name);
    }

    @Override
    public boolean isLoaded(String name) {
        return Bukkit.getWorld(name) != null;
    }

    @Override
    public List<String> getAllArenas() {
        return this.arenas;
    }

    @Override
    public boolean hasArena(String name) {
        return this.arenas.contains(name);
    }

    @Override
    public void setSpawn(String name, Location location) {
        ZombieModBootstrap.getInstance().getMinigameConfig().spigot().setLocation("arena." + name + ".spawn", location);
    }

    @Override
    public Location getSpawn(String name) {
        return ZombieModBootstrap.getInstance().getMinigameConfig().spigot().getLocation("arena." + name + ".spawn");
    }

    @Override
    public String getDisplayName(String name) {
        return ZombieModBootstrap.getInstance().getMinigameConfig().getString("arena." + name + ".display_name");
    }

    @Override
    public void setDisplayName(String name, String displayName) {
        ZombieModBootstrap.getInstance().getMinigameConfig().setString("arena." + name + ".display_name", displayName);
    }

}
