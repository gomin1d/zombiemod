package yt.dms.zombiemod.arena;

import org.bukkit.Location;

import java.util.List;

public interface ArenasManager {

    /**
     * Загрузить арену по её названию
     * @param name название арены
     * @throws IllegalStateException если арена уже загружена
     */
    void load(String name) throws IllegalStateException;

    /**
     * Узнать, загружена ли арена
     * @param name название арены
     * @return {@code true} если загружена, {@code false} в противном случае
     */
    boolean isLoaded(String name);

    /**
     * Получить список всех арен
     * @return список всех арен
     */
    List<String> getAllArenas();

    /**
     * Проверить, есть ли арена с таким названием
     * @param name название арены
     * @return {@code true} если такая арена есть, {@code false} в противном случае
     */
    boolean hasArena(String name);

    /**
     * Установить точку спавна для арены
     * @param name название арены
     * @param location точка спавна
     */
    void setSpawn(String name, Location location);

    /**
     * Получить точку спавна для арены
     * @param name название арены
     * @return точка спавна арены или null, если не установлена
     */
    Location getSpawn(String name);

    /**
     * Получить отображаемое название у арены
     * @param name название арены
     * @return отображаемое название у арены
     */
    String getDisplayName(String name);

    /**
     * Установить отображаемое название для арены
     * @param name название арены
     * @param displayName отображаемое название арены
     */
    void setDisplayName(String name, String displayName);

}
