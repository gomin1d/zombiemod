package yt.dms.zombiemod.utils;

import com.shampaggon.crackshot.CSUtility;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import yt.dms.api.DMS;
import yt.dms.api.spigot.item.DmsItemStack;
import yt.dms.zombiemod.Message;
import yt.dms.zombiemod.ZTeam;
import yt.dms.zombiemod.ZombieModGameLaunchPoint;
import yt.dms.zombiemod.ZombieType;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.data.GamePlayerData;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Utils {

    /**
     * Загрузить все миры из папки
     *
     * @param folder папка с мирами
     */
    public static List<World> copyWorldsAndLoad(String folder) {
        File serverFolder = ZombieModBootstrap.getServerFolder();
        File mapsFolder = new File(serverFolder, folder);

        if (!mapsFolder.exists()) {
            throw new RuntimeException("Could not found folder '" + folder + "' in server folder");
        }

        if (!mapsFolder.isDirectory()) {
            throw new RuntimeException("'" + folder + "' is not folder");
        }

        File[] files = mapsFolder.listFiles();

        if (files == null) {
            return Collections.emptyList();
        }

        return Stream.of(files)
                .filter(file -> file.isDirectory() || file.getName().endsWith(".zip"))
                .map(file -> copyWorldAndLoad(folder, file.getName()))
                .collect(Collectors.toList());
    }

    /**
     * Скопировать мир и загрузить его
     * Мир может быть как папкой, так и архивом, в котором содержится папка с миром
     *
     * @param folder папка откуда надо взять мир
     * @param name   название мира (== названию папки || == названию архива)
     */
    public static World copyWorldAndLoad(String folder, String name) {
        File serverFolder = ZombieModBootstrap.getServerFolder();
        boolean isZip = name.endsWith(".zip");
        String mapFolderName = isZip
                ? name.substring(0, name.lastIndexOf(".zip"))
                : name;

        File mapFolder = new File(serverFolder, mapFolderName);

        if (mapFolder.exists()) {
            mapFolder.delete();
        }

        mapFolder.mkdirs();

        try {
            mapFolder.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException("Could not create a folder '" + mapFolderName + "' in server folder", e);
        }

        File directory = new File(serverFolder, folder);

        if (!directory.exists()) {
            throw new RuntimeException("Could not found folder '" + folder + "'");
        }

        if (!directory.isDirectory()) {
            throw new RuntimeException("'" + folder + "' is not folder");
        }

        File target = new File(directory, name);

        if (isZip) {
            try {
                unzip(target.getAbsolutePath(), serverFolder.getAbsolutePath());
            } catch (IOException e) {
                throw new RuntimeException("Could not unzip a archive '/" + folder + "/" + mapFolderName + "'", e);
            }
        } else {
            try {
                copy(target, mapFolder);
            } catch (IOException e) {
                throw new RuntimeException("Could not copy a folder '/" + folder + "/" + name + "' to '/" + name + "'", e);
            }
        }

        return Bukkit.createWorld(new WorldCreator(mapFolderName));
    }

    public static void copy(File sourceLocation, File targetLocation) throws IOException {
        if (sourceLocation.isDirectory()) {
            copyDirectory(sourceLocation, targetLocation);
        } else {
            copyFile(sourceLocation, targetLocation);
        }
    }

    private static void copyDirectory(File source, File target) throws IOException {
        if (!target.exists()) {
            target.mkdir();
        }

        String[] list = source.list();

        if (list != null) {
            for (String f : list) {
                copy(new File(source, f), new File(target, f));
            }
        }
    }

    private static void copyFile(File source, File target) throws IOException {
        try (
                InputStream in = new FileInputStream(source);
                OutputStream out = new FileOutputStream(target)
        ) {
            byte[] buf = new byte[1024];
            int length;

            while ((length = in.read(buf)) > 0) {
                out.write(buf, 0, length);
            }
        }
    }

    public static void unzip(final String zipFilePath, final String unzipLocation) throws IOException {
        if (!(Files.exists(Paths.get(unzipLocation)))) {
            Files.createDirectories(Paths.get(unzipLocation));
        }

        try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFilePath))) {
            ZipEntry entry = zipInputStream.getNextEntry();

            while (entry != null) {
                Path filePath = Paths.get(unzipLocation, entry.getName());

                if (!entry.isDirectory()) {
                    unzipFiles(zipInputStream, filePath);
                } else {
                    Files.createDirectories(filePath);
                }

                zipInputStream.closeEntry();
                entry = zipInputStream.getNextEntry();
            }
        }
    }

    public static void unzipFiles(final ZipInputStream zipInputStream, final Path unzipFilePath) throws IOException {
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(unzipFilePath.toAbsolutePath().toString()))) {
            byte[] bytesIn = new byte[1024];
            int read;

            while ((read = zipInputStream.read(bytesIn)) != -1) {
                bos.write(bytesIn, 0, read);
            }
        }
    }

    public static List<GamePlayer> getPlayersInTeam(ZTeam team) {
        return Stream.of(ZombieModGameLaunchPoint.getPlugin().getSession().getAllPlayers())
                .filter(gamePlayer -> gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM) == team)
                .collect(Collectors.toList());
    }

    public static void makeZombie(GamePlayer gamePlayer, boolean first) {
        MinigamesAPI.debug("Turning " + gamePlayer.getName() + " into zombie. StackTrace:");

        if (MinigamesAPI.isDebug()) {
            new Exception().printStackTrace();
        }

        final Player player = gamePlayer.getEntity();
        gamePlayer.addData(GamePlayerData.ZOMBIE_TEAM, ZTeam.ZOMBIE);
        ZombieType zombieType = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player).getZombieType();
        gamePlayer.addData(GamePlayerData.ZOMBIE_TYPE, zombieType);
        player.getInventory().clear(); // Очищаем инвентарь будущему зомби (предыдущий кодер это делал вне этого метода, урод)
        player.setMaxHealth(zombieType.getHealth());
        player.setHealth(zombieType.getHealth());
        player.setPlayerListName(ChatColor.RED + player.getName());
        player.setDisplayName(ChatColor.RED + player.getName() + ChatColor.RESET);
        player.getInventory().setChestplate(new ItemStack(Material.AIR));

        zombieType.disguise(player);

        player.getActivePotionEffects().forEach(e -> player.removePotionEffect(e.getType()));
        player.setFireTicks(0); // Убираем горение игрока

        final PotionEffect[] effects = zombieType.getEffects();
        replaceEffects(player, effects);
        Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> replaceEffects(player, effects), 40);
        Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> replaceEffects(player, effects), 200);
        // жуткие костыли от кракшота

        Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
            new CSUtility().giveWeapon(player, "Bomb", 1);
            player.getInventory().setItem(8, new ItemStack(Material.COMPASS));
        }, 5);

        if (first && zombieType == ZombieType.SHADOW) {
            Bukkit.broadcastMessage(String.format("§eОСТОРОЖНО! Игрок §4%s §eиграет за класс §4§lShadow", player.getName()));
        }

        switch (zombieType) {
            case TOXIC: {
                Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
                    ItemStack helmet = new ItemStack(Material.CARPET);
                    helmet.setDurability((short) 15);
                    player.getInventory().setHelmet(helmet);
                    player.getInventory().setItem(1, new DmsItemStack(Material.RECORD_10, "§с§lТоксичное облако"));
                }, 1);
            }

            case SHADOW: {
                for (GamePlayer gamePlayer2 : ZombieModGameLaunchPoint.getPlugin().getSession().getAllPlayers()) {
                    if (gamePlayer2.getData(GamePlayerData.ZOMBIE_TEAM) == ZTeam.SURVIVOR) {
                        gamePlayer2.getEntity().hidePlayer(player);
                    }
                }

                break;
            }

//            case BLOODSEEKER: {
//                Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
//                    ItemStack skull = new ItemStack(Material.SKULL_ITEM);
//                    skull.setDurability((short) 3);
//                    player.getInventory().setHelmet(skull);
//                }, 1);
//
//                break;
//            }

            case SPIDER: {
                Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
                    player.getInventory().setItem(1, new DmsItemStack(Material.CLAY_BALL, "§aПаутина"));
                }, 1);

                break;
            }
        }
    }

    public static void replaceEffects(Player p, PotionEffect[] effects) {
        p.getActivePotionEffects().forEach(e -> p.removePotionEffect(e.getType()));

        for (PotionEffect eff : effects) {
            p.addPotionEffect(eff, true);
        }
    }

    public static void broadcastTitleMsg(Message title, Message subtitle, Object... replacements) {
        String titleAsString = title == null ? "" : title.toString(replacements);
        String subtitleAsString = subtitle == null ? "" : subtitle.toString(replacements);

        if (!ZombieModGameLaunchPoint.NOCSAPI) {
            Bukkit.getOnlinePlayers().forEach(p -> {
                sendTitle(p, titleAsString, subtitleAsString, 0, 40, 0);
            });
        } else {
            Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage("[title]" + titleAsString + "/" + subtitleAsString));
        }
    }

    public static void sendTitle(Player player, String title, String subtitle, int i, int i1, int i2) {
        IChatBaseComponent chatCancelTitle = IChatBaseComponent.ChatSerializer.a(
                "{\"text\": \"" + DMS.getChatUtil().colorize(title) + "\"}");
        IChatBaseComponent subCancelTitle = IChatBaseComponent.ChatSerializer.a(
                "{\"text\": \"" + DMS.getChatUtil().colorize(subtitle) + "\"}");
        PacketPlayOutTitle cancelTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, chatCancelTitle);
        PacketPlayOutTitle cancelSubTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, subCancelTitle);
        PacketPlayOutTitle cancelLength = new PacketPlayOutTitle(i, i1, i2);
        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().playerConnection.sendPacket(cancelTitle);
        craftPlayer.getHandle().playerConnection.sendPacket(cancelSubTitle);
        craftPlayer.getHandle().playerConnection.sendPacket(cancelLength);
    }

    public static String onCenter(String text, String s, int textLength) {
        StringBuilder result = new StringBuilder();
        int lengthWithoutColor = ChatColor.stripColor(text).length();
        int startIndex = (textLength - lengthWithoutColor) / 2;
        int first = startIndex - 1;
        int second = textLength - first - lengthWithoutColor - 2;

        result.append(repeat(s, first));
        result.append(text);
        result.append(repeat(s, second));

        return result.toString();
    }

    public static String repeat(String s, int i) {
        StringBuilder sb = new StringBuilder();

        for (; i >= 0; i--) {
            sb.append(s);
        }

        return sb.toString();
    }

}
