package yt.dms.zombiemod;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class VoteController {

    private Map<String, Integer> arenaVotes = new HashMap<>();
    private Map<String, String> playerVotes = new HashMap<>();

    private List<Inventory> invs = new ArrayList<>();

    public VoteController(String[] arenas) {
        for (String arena : arenas)
            arenaVotes.put(arena, 0);
    }

    public void vote(Player p, String arenaName, int addVotes) {
        int votesCount = arenaVotes.get(arenaName);
        arenaVotes.put(arenaName, votesCount + addVotes);
        playerVotes.put(p.getName(), arenaName);

        closeInventory(p.getInventory());
        p.closeInventory();

        updateInventories();
    }

    public void removeVote(String p, int removeVotes) {
        String arenaName = playerVotes.remove(p);
        if (arenaName != null) {
            int votesCount = arenaVotes.get(arenaName);
            arenaVotes.put(arenaName, votesCount - removeVotes);

            updateInventories();
        }
    }

    public void moveVote(String p, String newArenaName, int addVotes) {
        String arenaName = playerVotes.remove(p);
        if (arenaName != null) {
            int votesCount = arenaVotes.get(arenaName);
            arenaVotes.put(arenaName, votesCount - addVotes);

            votesCount = arenaVotes.get(newArenaName);
            arenaVotes.put(newArenaName, votesCount + addVotes);

            playerVotes.put(p, newArenaName);

            updateInventories();
        }
    }

    public String getWinner() {
        int maxvotes = -1;
        List<String> max = new ArrayList<>();

        for (Map.Entry<String, Integer> ent : arenaVotes.entrySet()) {
            if (ent.getValue() == maxvotes) {
                max.add(ent.getKey());
            } else if (ent.getValue() > maxvotes) {
                maxvotes = ent.getValue();
                max.clear();
                max.add(ent.getKey());
            }
        }

        if (max.size() == 1) {
            return max.get(0);
        } else {
            return max.get(ThreadLocalRandom.current().nextInt(max.size()));
        }
    }

    public void closeInventory(Inventory inv) {
        invs.remove(inv);
    }

    public boolean hasVoted(String p) {
        return playerVotes.containsKey(p);
    }

    public String getPlayerVote(String p) {
        return playerVotes.get(p);
    }

    public Inventory createInventory() {
        Inventory inv = Bukkit.createInventory(null, ((this.arenaVotes.size() + 8) / 9) * 9, Message.VOTEINV.toString());

        for (Map.Entry<String, Integer> ent : arenaVotes.entrySet()) {
            ItemStack item = new ItemStack(Material.PAPER);
            ItemMeta meta = item.getItemMeta();

            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', ent.getKey()));
            meta.setLore(Message.VOTES.toList("votes", ent.getValue()));

            item.setItemMeta(meta);
            inv.addItem(item);
        }

        invs.add(inv);
        return inv;
    }

    private void updateInventories() {
        for (Inventory inv : invs) {
            for (ItemStack item : inv.getContents()) {
                if (item != null) {
                    ItemMeta meta = item.getItemMeta();
                    String arena = meta.getDisplayName();
                    meta.setLore(Message.VOTES.toList("votes", arenaVotes.get(arena)));
                    item.setItemMeta(meta);
                }
            }
        }
    }

}
