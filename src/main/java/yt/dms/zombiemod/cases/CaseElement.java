package yt.dms.zombiemod.cases;

import org.bukkit.inventory.ItemStack;

public abstract class CaseElement {

    protected final ItemData item;

    public CaseElement(ItemData item) {
        this.item = item;
    }

    public ItemData getItem() {
        return item;
    }

    public ItemStack create() {
        return item.create();
    }

    public abstract void apply(String p);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CaseElement that = (CaseElement) o;

        return item.equals(that.item);
    }

    @Override
    public int hashCode() {
        return item.hashCode();
    }
}
