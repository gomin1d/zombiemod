package yt.dms.zombiemod.cases;

import org.bukkit.Bukkit;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import yt.dms.zombiemod.ZombieModGameLaunchPoint;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PlayerOpeningController {

    private final String player;
    private final Inventory inv;
    private final CaseElement prize;
    private final GUIFormat gui;

    private int itemsShift = 0;
    private List<CaseElement> elements = new ArrayList<>();

    private final AnimationRunnable runnable;
    private boolean done = false;

    public PlayerOpeningController(Player player, Case openedCase, int elementsCount) {
        if (elementsCount < 10) {
            elementsCount = 10;
        }

        this.player = player.getName();
        this.gui = openedCase.getGUI();

        inv = gui.buildBaseInventory(Case.OPEN_INVENTORY_MARK);

        int multiplier = 1;
        while (openedCase.size() * multiplier < elementsCount) {
            multiplier++;
        }

        final int finalMultiplier = multiplier;
        ((ZMCase) openedCase).getItems().forEach(e -> {
            for (int i = 0; i < finalMultiplier; i++) {
                for (int j = 0; j < e.getWeight(); j++) {
                    elements.add(e);
                }
            }
        });

        Collections.shuffle(elements);
        prize = elements.get(elements.size() - 7);

        while(elements.size() > elementsCount){
            int i = 0;
            for(; i < elements.size(); i++) {
                ZMCaseElement el = (ZMCaseElement) elements.get(i);

                if (i != elements.size() - 7 && elements.stream().filter(e -> e.equals(el)).count() > 1) {
                    elements.remove(i);
                    break;
                }
            }

            if(i == elements.size()){
                break;
            }
        }

        if(elements.size() > elementsCount){
            elements = elements.stream()
                    .skip(elements.size() - elementsCount)
                    .collect(Collectors.toList());
        }

        runnable = new AnimationRunnable(this);
        runnable.start();

        player.openInventory(inv);
    }

    /**
     * @return Нужно ли останавливать прокрутку
     */
    public boolean shiftItems() {
        int y = gui.getItemsRow();
        boolean started = false;
        boolean first = false;

        for (int x = 0; x < 9; x++) {
            GUISlot slot = gui.getSlot(y, x);
            if (slot.getType() == GUISlotType.ROULETTE && !started) {
                started = true;
                first = true;
            }

            if (!first && gui.getSlot(y, x).getType() != GUISlotType.BORDER) {
                inv.setItem(gui.getSlotNum(y, x - 1), inv.getItem(gui.getSlotNum(y, x)));
            }

            first = false;

            if (x < 8 && gui.getSlot(y, x + 1).getType() == GUISlotType.BORDER) {
                CaseElement el = getElementAtShift(itemsShift + 1);
                if (el != null) {
                    inv.setItem(gui.getSlotNum(y, x), el.create());
                }
            }
        }

        Player p = Bukkit.getPlayerExact(player);
        p.playNote(p.getLocation(), Instrument.STICKS, Note.natural(1, Note.Tone.A));

        itemsShift++;
        return itemsShift == elements.size() - 4;
    }

    public void stop(boolean invClosed) {
        if (!done) {
            prize.apply(player);
            runnable.cancel();
            done = true;

            Player p = Bukkit.getPlayerExact(player);
            if (p != null && !invClosed) {
                Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
                    if (p.isOnline())
                        p.closeInventory();
                }, 30);
                p.playSound(p.getLocation(), Sound.LEVEL_UP, 10, 0);
            }
        }

        ZombieModGameLaunchPoint.cases.remove(player);
    }

    public int getShiftDelay() {
        double procent = Math.floor(((double) itemsShift / (elements.size() - 3)) * 100);
        int i = (int) Math.floorDiv(Math.round(procent), 20);

        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
                return 3;
            case 4:
                return 7;
            default:
                return 10;
        }
    }

    public CaseElement getElementAtShift(int shift) {
        if (shift < 0 || shift >= elements.size())
            return null;

        return elements.get(shift);
    }

}
