package yt.dms.zombiemod.cases;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.Map;

public class GUIFormat {

    private final String title;
    private final GUISlot[][] gui;
    private int itemsRow = -1;

    public GUIFormat(String title, Map<Character, ItemData> items, List<String> border) throws IllegalArgumentException {
        this.title = title;

        gui = new GUISlot[border.size()][9];
        for (int y = 0; y < 3; y++) {
            String line = border.get(y);

            if (line.length() != 9) {
                throw new IllegalArgumentException("GUI row must have 9 chars");
            }

            if (line.contains("*")) {
                if (itemsRow != -1) {
                    throw new IllegalArgumentException("Only one row can contains case items");
                }

                itemsRow = y;
            }

            for (int x = 0; x < 9; x++) {
                char ch = line.charAt(x);


                if (ch == '*') {
                    gui[y][x] = GUISlot.ROULETTE_SLOT;
                } else {
                    if (!items.containsKey(ch))
                        throw new IllegalArgumentException("No item found for char " + ch);

                    ItemData material = items.get(ch);
                    gui[y][x] = new BorderGUISlot(material);
                }
            }
        }
    }

    /**
     * Отсчет начинается с 0
     */
    public int getItemsRow() {
        return itemsRow;
    }

    public int getSlotsCount() {
        return gui.length * 9;
    }

    public GUISlot getSlot(int y, int x) {
        return gui[y][x];
    }

    public Inventory buildBaseInventory(String prefix) {
        Inventory inv = Bukkit.createInventory(null, getSlotsCount(), prefix + title);

        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                GUISlot slot = getSlot(y, x);

                if (slot != null && slot.getType() == GUISlotType.BORDER) {
                    ItemData data = ((BorderGUISlot) slot).getItem();
                    inv.setItem(getSlotNum(y, x), data.create());
                }
            }
        }

        return inv;
    }

    public int getSlotNum(int y, int x) {
        return y * 9 + x;
    }
}
