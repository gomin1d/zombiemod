package yt.dms.zombiemod.cases;

public class BorderGUISlot extends GUISlot {

    private final ItemData item;

    public BorderGUISlot(ItemData item) {
        super(GUISlotType.BORDER);
        this.item = item;
    }

    public ItemData getItem() {
        return item;
    }
}
