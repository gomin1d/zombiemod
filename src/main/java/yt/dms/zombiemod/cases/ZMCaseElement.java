package yt.dms.zombiemod.cases;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import yt.dms.zombiemod.Message;
import yt.dms.zombiemod.ShopElement;
import yt.dms.zombiemod.ZMItemData;
import yt.dms.zombiemod.ZombieModGameLaunchPoint;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.gamer.Gamer;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;

import java.util.Optional;
import java.util.UUID;

public class ZMCaseElement extends CaseElement {

    private int weight;
    private final NBTTagCompound itemNBT;

    public ZMCaseElement(ItemData item, int weight, NBTTagCompound itemNBT) {
        super(item);
        this.weight = weight;
        this.itemNBT = itemNBT;
    }

    public int getWeight() {
        return weight;
    }

    public NBTTagCompound getItemNBT() {
        return itemNBT != null ? (NBTTagCompound) itemNBT.clone() : null;
    }

    @Override
    public ItemStack create() {
        ItemStack i = item.create();

        if (itemNBT != null) {
            net.minecraft.server.v1_8_R3.ItemStack cis = CraftItemStack.asNMSCopy(i);

            cis.setTag(itemNBT);

            return CraftItemStack.asBukkitCopy(cis);
        }

        return i;
    }

    @Override
    public void apply(String p) {
        Player pl = Bukkit.getPlayer(p);
        UUID uuid = pl.getUniqueId();

        ShopElement.ShopElementType type = ShopElement.ShopElementType.valueOf(itemNBT.getString("ztype"));
        int cid = itemNBT.getInt("zcid");

        Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(pl);

        final Optional<ZMItemData> optional = ZombieModGameLaunchPoint.getPlugin().getGameItems().stream().filter(i -> i.getType() == type && i.getContent() == cid).findAny();
        if (!optional.isPresent())
            return;

        ZMItemData item = optional.get();

        switch (type) {
            case ARMOR:
                gamer.addArmor();

                MinigamesAPI.debug(String.format("%s won an armor", p));
                Message.WONARMOR.broadcast("player", p);
                break;
            case TURRET:
//                gamer.addTurret();
//
//                MinigamesAPI.debug(String.format("%s won a turret", p));
//                ZombieModGameLaunchPoint.broadcastMsg("zm.wonturret", ImmutableMap.of("%player", p));
//                break;
            case GUN:
                gamer.buyGun(cid);

                MinigamesAPI.debug(String.format("%s won a gun with id %d", p, cid));
                Message.WONGUN.broadcast("player", p, "weapon", item.getDisplayName());
                break;
            case ZOMBIE:
                gamer.buyZombie(cid);

                MinigamesAPI.debug(String.format("%s won a class with id %d", p, cid));
                Message.WONZOMBIE.broadcast("player", p, "zombie", item.getDisplayName());
                break;
        }
    }

    @Override
    public String toString() {
        return item.getNameToDisplay();
    }
}
