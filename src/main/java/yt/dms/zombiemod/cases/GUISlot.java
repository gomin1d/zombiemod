package yt.dms.zombiemod.cases;

public abstract class GUISlot {

    public static final GUISlot ROULETTE_SLOT = new GUISlot(GUISlotType.ROULETTE) {
    };

    private final GUISlotType type;

    public GUISlot(GUISlotType type) {
        this.type = type;
    }

    public GUISlotType getType() {
        return type;
    }

}
