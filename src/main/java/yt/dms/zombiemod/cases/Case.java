package yt.dms.zombiemod.cases;

import org.bukkit.ChatColor;

public abstract class Case {

    public static final String OPEN_INVENTORY_MARK = ChatColor.RED.toString() + ChatColor.DARK_PURPLE.toString() + ChatColor.RESET.toString();

    private GUIFormat gui;

    public Case(GUIFormat gui) {
        this.gui = gui;
    }

    public GUIFormat getGUI() {
        return gui;
    }

    public abstract int size();

}
