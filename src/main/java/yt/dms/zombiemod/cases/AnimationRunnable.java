package yt.dms.zombiemod.cases;

import org.bukkit.scheduler.BukkitRunnable;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;

public class AnimationRunnable extends BukkitRunnable {

    private final PlayerOpeningController controller;

    private int run = 0;

    public AnimationRunnable(PlayerOpeningController controller) {
        this.controller = controller;
    }

    public void start() {
        runTaskTimer(ZombieModBootstrap.getInstance(), 1, 1);
    }

    @Override
    public void run() {
        if (run == controller.getShiftDelay()) {
            run = 0;
            if (controller.shiftItems()) {
                controller.stop(false);
                return;
            }
        }

        run++;
    }
}
