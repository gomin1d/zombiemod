package yt.dms.zombiemod.cases;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class ZMCase extends Case {

    public static final GUIFormat gui;

    static {
        Map<Character, ItemData> map = new HashMap<>();
        map.put('g', new ItemData(Material.STAINED_GLASS_PANE, (short) 0, 1, ChatColor.RESET.toString()));
        map.put('h', new ItemData(Material.STAINED_GLASS_PANE, (short) 5, 1, ChatColor.RESET.toString()));
        gui = new GUIFormat("ZombieMod", map, Arrays.asList("gggghgggg", "g*******g", "gggghgggg"));
    }

    private final List<ZMCaseElement> items;

    public ZMCase(List<ZMCaseElement> items) {
        super(gui);
        this.items = items;
    }

    public ZMCase() {
        super(gui);
        this.items = new ArrayList<>();
    }

    public void addItem(ItemStack i, int weight) {
        ItemData data = new ItemData(i);

        net.minecraft.server.v1_8_R3.ItemStack cis = CraftItemStack.asNMSCopy(i);
        NBTTagCompound tag;
        if (!cis.hasTag())
            cis.setTag(new NBTTagCompound());
        tag = cis.getTag();

        items.add(new ZMCaseElement(data, weight, tag));
    }

    public List<ZMCaseElement> getItems() {
        return items;
    }

    @Override
    public int size() {
        return items.stream().mapToInt(ZMCaseElement::getWeight).sum();
    }

}
