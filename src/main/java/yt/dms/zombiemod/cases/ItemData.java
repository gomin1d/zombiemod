package yt.dms.zombiemod.cases;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ItemData {

    private final Material material;
    private final short data;
    private int amount;
    private String title;
    private List<String> lore;

    public ItemData(Material material) {
        this.material = material;
        this.data = 0;
        this.amount = 1;
    }

    public ItemData(Material material, short data) {
        this.material = material;
        this.data = data;
        this.amount = 1;
    }

    public ItemData(Material material, short data, int amount) {
        this.material = material;
        this.data = data;
        this.amount = amount;
    }

    public ItemData(Material material, short data, int amount, String title) {
        this.material = material;
        this.data = data;
        this.amount = amount;
        this.title = title;
    }

    public ItemData(Material material, short data, int amount, String title, List<String> lore) {
        this.material = material;
        this.data = data;
        this.amount = amount;
        this.title = title;
        this.lore = lore;
    }

    public ItemData(ItemStack i) {
        this.material = i.getType();
        this.amount = i.getAmount();
        this.data = i.getDurability();

        if (i.hasItemMeta()) {
            ItemMeta meta = i.getItemMeta();

            if (meta.hasDisplayName()) {
                this.title = meta.getDisplayName();
            }

            if (meta.hasLore()) {
                this.lore = meta.getLore();
            }
        }
    }

    public Material getMaterial() {
        return material;
    }

    public String getNameToDisplay() {
        return title != null ? title : material.name().toLowerCase();
    }

    public ItemStack create() {
        ItemStack is = new ItemStack(material, amount, data);

        if (title != null || lore != null) {
            ItemMeta meta = is.getItemMeta();

            if (meta != null) {
                if (title != null)
                    meta.setDisplayName(title);

                if (lore != null)
                    meta.setLore(lore);

                is.setItemMeta(meta);
            }
        }

        return is;
    }

    public static ItemData parseString(String s) {
        String[] spl = s.split(":");

        switch (spl.length) {
            case 1:
                return new ItemData(Material.getMaterial(Integer.valueOf(s)));
            case 2:
                //material, data
                return new ItemData(Material.getMaterial(Integer.valueOf(spl[0])), Short.valueOf(spl[1]));
            case 3:
                //material, data, amount
                return new ItemData(Material.getMaterial(Integer.valueOf(spl[0])), Short.valueOf(spl[1]), Integer.valueOf(spl[2]));
            case 4:
                //material, data, amount, title
                return new ItemData(Material.getMaterial(Integer.valueOf(spl[0])), Short.valueOf(spl[1]), Integer.valueOf(spl[2]), ChatColor.translateAlternateColorCodes('&', spl[3]));
            default:
                List<String> lore = new ArrayList<>();

                lore.addAll(Arrays.asList(spl).subList(4, spl.length));

                return new ItemData(Material.getMaterial(Integer.valueOf(spl[0])), Short.valueOf(spl[1]), Integer.valueOf(spl[2]), ChatColor.translateAlternateColorCodes('&', spl[3]), lore);
        }
    }

    @Override
    public String toString() {
        List<String> list = new ArrayList<>();

        list.add(String.valueOf(material.getId()));
        list.add(String.valueOf(data));
        list.add(String.valueOf(amount));

        if (title != null) {
            list.add(title);

            if (lore != null)
                list.addAll(lore);
        }

        return StringUtils.join(list, ":");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemData itemData = (ItemData) o;

        if (data != itemData.data) return false;
        if (amount != itemData.amount) return false;
        if (material != itemData.material) return false;
        if (title != null ? !title.equals(itemData.title) : itemData.title != null) return false;
        return lore != null ? lore.equals(itemData.lore) : itemData.lore == null;
    }

    @Override
    public int hashCode() {
        int result = material.hashCode();
        result = 31 * result + (int) data;
        result = 31 * result + amount;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (lore != null ? lore.hashCode() : 0);
        return result;
    }
}
