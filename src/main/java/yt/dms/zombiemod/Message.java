package yt.dms.zombiemod;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.entity.Player;
import yt.dms.api.DMS;
import yt.dms.api.spigot.SBar;
import yt.dms.zombiemod.utils.Utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static yt.dms.zombiemod.ZombieModGameLaunchPoint.NOCSAPI;

public enum Message {
    EMPTY(""),
    JOIN("§2{player} §7присоединился к игре"),
    PLAYERSTATS("§a   Статистика игрока"),
    WONARMOR("§7Игрок {player} получает броню"),
    WONGUN("§7Игрок {player} получает {weapon}"),
    WONZOMBIE("§7Игрок {player} получает класс {zombie}"),
    ITEM_ROULETTE("§6§lРулетка"),
    ITEM_STATISTICS("§a§lСтатистика игрока"),
    ITEM_SHOP("§a§lМагазин"),
    BUYFOR("§eКупить за §6§l{xp} §eXP"),
    PLAYERSTOP("§a   Топ игроков"),
    NOXP("§7У вас недостаточно XP"),
    GOTEVERYTHING("§7Вы уже получили все возможные предметы и классы"),
    AVAILABLE("§aДоступно"),
    NOTAVAILABLE("§cНедоступно"),
    CHOOSEZOMBIE("§7Вы выбрали этого зомби"),
    NOZOMBIE("§7У вас нет этого зомби"),
    CHOOSEGUN("§7Вы выбрали это оружие"),
    NOGUN("§7У вас нет этого оружия"),
    HASARMOR("§7У вас уже есть броня"),
    NOARMOR("§7У вас нет брони"),
    NEARESTPLAYER("§6Ближайший игрок: §b{player}§6. Расстояние: §b{distance}"),
    NOPLAYERSNEAR("§cИгроков поблизости нет! :("),
    VOTEINV("Голосование"),
    VOTES("§2Голосов: §a§l{votes}"),
    //    ITEM_VOTE("§2§lГолосование"),
    ITEM_LOBBY("§4§lВыйти в лобби"),
    NOTENOUGHTPLAYERS("§7§lНедостаточно игроков"),
    START("§7Игра началась!"),
    INFECTED("§4{zombie} §7заражен!"),
    TIMEOUT("§7Время окончено! Выжившие победили!"),
    ZINFECT_ROW_1("§c{player}"),
    ZINFECT_ROW_2("§7инфицировал §b{infected}"),
    ZINFECT("§c{player} §7инфицировал §b{infected}"),

    //yougot1
    YOU_GOT_XP("§7Вы получили §2{xp} §7XP"),

    //yougot2
    YOU_GOT_XP_AND_SCORE("§7Вы получили §2{xp} §7XP и §2{score} §7очков"),

    // youlost
    YOU_LOST_SCORE("§7Вы потеряли §2{score} §7очков"),

    PKILL_ROW_1("§b{player}"),
    PKILL_ROW_2("§7убил §c{killed}"),
    PKILL("§b{player} §7убил §c{killed}"),
    PDIED("§b{player} §7умер и стал §cзомби"),
    ZDIED("§c{player} §7умер"),
    UNTILGAME("§7§lДо начала игры: %time%", false),
    UNTILGAME_ROW_1("§6{seconds} §7{word}"),
    UNTILGAME_ROW_2("§7до старта игры"),
    VOTEEND("§7В голосовании победила карта §2{map}"),
    UNTILEND("§7§lДо конца игры: %time%", false),
    UNTILEND_ROW_1("§6{seconds} §7{word}"),
    UNTILEND_ROW_2("§7§lдо конца игры"),
    TELEPORTINV("Телепортироваться"),
    NOTONLINE("Игрок не в сети"),
    VOTE("§2§lГолосование"),
    ZWON("§cЗомби победили!"),
    ZLEAVED("§7Зомби вышел из игры..."),
    RESTART("§7Через 10 секунд игра будет перезапущена"),
    GAMEOVER("§c§lИгра окончена", false),
    LEAVE("§2{player} §7покинул игру"),
    BECAME_ZOMBIE("§b{player} §7стал зомби");

    private final String text;

    private final Map<String, Object> data;

    Message(String text) {
        this(text, true);
    }

    Message(String text, boolean emptyData) {
        this.text = text;
        this.data = emptyData
                ? Collections.emptyMap()
                : new HashMap<>();
    }

    @Override
    public String toString() {
        return this.text;
    }

    public String toString(Object... replacements) {
        if (replacements.length == 0) {
            return this.text;
        }

        if (replacements.length % 2 != 0) {
            throw new RuntimeException("Replacements length should be even");
        }

        String result = this.text;

        for (int i = 0; i < replacements.length; i += 2) {
            String target = "{" + replacements[i] + "}";
            String replacement = replacements[i + 1].toString();

            result = result.replace(target, replacement);
        }

        return result;
    }

    public void broadcast(Object... replacements) {
        Bukkit.broadcastMessage(this.toString(replacements));
    }

    public void send(Player player, Object... replacements) {
        player.sendMessage(this.toString(replacements));
    }

    public List<String> toList(Object... replacements) {
        return Collections.singletonList(this.toString(replacements));
    }

    public void broadcastActionBar(Object... replacements) {
        String text = this.toString(replacements);

        Bukkit.getOnlinePlayers().forEach(player -> this.sendActionBar0(player, text));
    }

    public void sendActionBar(Player player, Object... replacements) {
        this.sendActionBar0(player, this.toString(replacements));
    }

    public void sendBarIfNotActivated(Color color, int seconds, Object... replacements) {
        if ((Boolean) this.data.getOrDefault("activate", false)) {
            return;
        }

        SBar.Bar bar = DMS.spigot().getBarUtils().createBar(
                this.toString(replacements),
                color.getRed(),
                color.getGreen(),
                color.getBlue(),
                seconds
        );

        DMS.spigot().getBarUtils().broadcast(bar);
        this.data.put("activate", true);
    }

    public void sendBar(Color color, Object... replacements) {
        SBar.Bar bar = DMS.spigot().getBarUtils().createBar(
                this.toString(replacements),
                color.getRed(),
                color.getGreen(),
                color.getBlue(),
                .33F
        );

        DMS.spigot().getBarUtils().broadcast(bar);
    }

    private void sendActionBar0(Player player, String text) {
        if (!NOCSAPI) {
            Utils.sendTitle(player, text, "", 0, 40, 0);
        } else {
            player.sendMessage("[actionbar] " + text);
        }
    }

    public static void clearData() {
        for (Message message : values()) {
            message.data.clear();
        }
    }

}
