package yt.dms.zombiemod;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShopElement {

    private final String itemid;
    private final int contentid;
    private final String name;
    private final List<String> lore;
    private final int slot;
    private final ShopElementType type;

    public ShopElement(String itemid, int contentid, int slot, String name, List<String> lore, ShopElementType type) {
        this.itemid = itemid;
        this.contentid = contentid;
        this.slot = slot;
        this.name = name;
        if (lore != null) {
            this.lore = new ArrayList<>();
            this.lore.addAll(lore
                    .stream()
                    .map(l -> ChatColor.translateAlternateColorCodes('&', l))
                    .collect(Collectors.toList())
            );
        } else
            this.lore = null;
        this.type = type;
    }

    public String getItemId() {
        return itemid;
    }

    public int getContentId() {
        return contentid;
    }

    public String getName() {
        return name;
    }

    public List<String> getLore() {
        return lore;
    }

    public int getSlot() {
        return slot;
    }

    public ShopElementType getType() {
        return type;
    }

    public String toString() {
        return slot + ":" + itemid + ":" + name;
    }

    @SuppressWarnings("deprecation")
    public ItemStack create(Player p) {
        String[] itemspl = itemid.split(":");
        int itemid = Integer.parseInt(itemspl[0]);
        short itemdata = itemspl.length > 1 ? Short.parseShort(itemspl[1]) : 0;

        ItemStack i = new ItemStack(itemid, 1, itemdata);

        ItemMeta meta = i.getItemMeta();

        meta.setDisplayName(getNameForItem(p));
        if (lore != null) {
            meta.setLore(lore);
        }
        i.setItemMeta(meta);

        net.minecraft.server.v1_8_R3.ItemStack cis = CraftItemStack.asNMSCopy(i);
        NBTTagCompound tag = null;

        if (!cis.hasTag())
            cis.setTag(new NBTTagCompound());
        tag = cis.getTag();

        tag.setString("ztype", type.toString());
        tag.setString("zcid", String.valueOf(contentid));

        return CraftItemStack.asCraftMirror(cis);
    }

    private String getNameForItem(Player player) {
        // Old code
//        Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);
//
//        if (type == ShopElementType.GUN) {
//            ZGun gun = ZombieModGameLaunchPoint.getPlugin().gunById(contentid);
//            if (gun != null) {
//                if (gamer.hasGun(contentid))
//                    return name + " " + Message.AVAILABLE;
//                else
//                    return name + " " + Message.NOTAVAILABLE;
//            }
//        } else if (type == ShopElementType.ZOMBIE) {
//            if (gamer.hasZombie(contentid))
//                return name + " " + Message.AVAILABLE;
//            else
//                return name + " " + Message.NOTAVAILABLE;
//        }
        return name;
    }

    public enum ShopElementType {

        DECORATE, ZOMBIE, GUN, ARMOR, TURRET;

        public static ShopElementType fromString(String s) {
            switch (s) {
                case "zombie":
                    return ShopElementType.ZOMBIE;
                case "gun":
                    return ShopElementType.GUN;
                case "armor":
                    return ShopElementType.ARMOR;
                case "turret":
                    return TURRET;
                default:
                    return ShopElementType.DECORATE;
            }
        }

    }

}
