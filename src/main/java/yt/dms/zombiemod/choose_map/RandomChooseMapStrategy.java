package yt.dms.zombiemod.choose_map;

import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

/**
 * Рандомный выбор карты
 * Реализовано, но не используется
 * (А еще оно не тестировалось)
 */
public class RandomChooseMapStrategy implements ChooseMapStrategy {

    private String chosenMap;

    private boolean loaded = false;

    private static final Random RANDOM = ThreadLocalRandom.current();

    private static final String MAP_NAME = "zombiemod";

    private static final String MAPS_FOLDER = "maps";

    /*
    Че тут вообще происходит? Прокомментирую на русском, хотя бы для себя, чтобы не забыть
    Суть такова, что нам надо из папки $MAPS_FOLDER выбрать один мир (пока рандомно) и загрузить его
    Ниже происходит следующее:
    - Удаляем папку $MAP_NAME в корне сервера, если таковая есть
    - Ищем папку $MAPS_FOLDER, если её нет, или это не папка, то выбиваем ошибки
    - Фильтруем файлы в папке $MAPS_FOLDER, нам нужны только папки
    - Рандомно выбираем папку, это и будет нашей картой
    - Пытаемся скопировать выбранную карту в корень нашего сервера. В случае ошибки - бросаем RuntimeException
    - Создаем карту средствами баккита
    - ???
    - PROFIT
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void load() {
        if (this.loaded) {
            return;
        }

        File serverFolder = ZombieModBootstrap.getServerFolder();
        File mapFolder = new File(serverFolder, MAP_NAME);

        if (mapFolder.exists()) {
            mapFolder.delete();
        }

        try {
            mapFolder.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException("Could not create a folder '" + MAP_NAME + "' in server folder", e);
        }

        File directory = new File(serverFolder, MAPS_FOLDER);

        if (!directory.exists()) {
            throw new RuntimeException("Please, create folder '" + MAPS_FOLDER +
                    "' in server folder and place the maps in the folder '" + MAPS_FOLDER + "'");
        }

        if (!directory.isDirectory()) {
            throw new RuntimeException("Please, place the maps in the folder '" + MAPS_FOLDER + "'");
        }

        // Filter the files, we need only folders.
        File[] maps = Stream.of(Objects.requireNonNull(directory.listFiles()))
                .filter(File::isDirectory)
                .toArray(File[]::new);
        // Random choose the map
        File chosen = maps[RANDOM.nextInt(maps.length)];
        this.chosenMap = chosen.getName();

        try {
            Files.copy(chosen.toPath(), new FileOutputStream(mapFolder));
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Could not found the folder '" + MAP_NAME + "' in server folder", e);
        } catch (IOException e) {
            throw new RuntimeException("Could not copy a folder '/" + MAPS_FOLDER + "/" + this.chosenMap + "' to '/" + MAP_NAME + "'", e);
        }

        Bukkit.createWorld(new WorldCreator(MAP_NAME));
        this.loaded = true;

        ZombieModBootstrap.debug("Successfully load a map '" + this.chosenMap + "'");
    }

    @Override
    public String choose() {
        return this.chosenMap;
    }

}
