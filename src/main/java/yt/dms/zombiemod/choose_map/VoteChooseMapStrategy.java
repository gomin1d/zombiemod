package yt.dms.zombiemod.choose_map;

import yt.dms.api.DMS;

public class VoteChooseMapStrategy implements ChooseMapStrategy {

    @Override
    public void load() {
        DMS.spigot().getMapSelector().startVoting();
    }

    @Override
    public String choose() {
        return DMS.spigot().getMapSelector().endVoting().getName();
    }

}
