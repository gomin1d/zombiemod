package yt.dms.zombiemod.choose_map;

public interface ChooseMapStrategy {

    /**
     * Загрузить карты для игры
     */
    void load();

    /**
     * Выбрать карту для игры
     * @return название карты для игры
     */
    String choose();

}
