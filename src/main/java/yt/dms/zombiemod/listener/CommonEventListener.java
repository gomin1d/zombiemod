package yt.dms.zombiemod.listener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import yt.dms.api.DMS;
import yt.dms.api.player.DmsPlayer;
import yt.dms.api.spigot.item.DmsItemStack;
import yt.dms.api.util.DmsFutures;
import yt.dms.zombiemod.Message;
import yt.dms.zombiemod.ZMItemData;
import yt.dms.zombiemod.ZombieModGameLaunchPoint;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.cases.PlayerOpeningController;
import yt.dms.zombiemod.cases.ZMCase;
import yt.dms.zombiemod.gamer.Gamer;
import yt.dms.zombiemod.menu.shop.ShopOpenStrategy;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CommonEventListener implements Listener {

    private final String CASEBUY_INVENTORY_MARK = ChatColor.RED.toString() + ChatColor.DARK_GREEN.toString() + ChatColor.RESET.toString();

    private final ShopOpenStrategy shopOpenStrategy;

    public CommonEventListener(ShopOpenStrategy shopOpenStrategy) {
        this.shopOpenStrategy = shopOpenStrategy;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
//        Bukkit.getScheduler().runTaskLaterAsynchronously(ZombieModBootstrap.getInstance(), () -> {
            Player player = event.getPlayer();

            if (!player.isOnline()) {
                return;
            }

            Inventory inventory = player.getInventory();
            inventory.clear();

            // Предмет рулетки
            inventory.setItem(
                    ZombieModBootstrap.getInstance().isLobby() ? 4 : 1,
                    new DmsItemStack(Material.NETHER_STALK, Message.ITEM_ROULETTE.toString())
            );

            // Предмет статистики
            inventory.setItem(
                    ZombieModBootstrap.getInstance().isLobby() ? 5 : 7,
                    new DmsItemStack(Material.GOLD_NUGGET, Message.ITEM_STATISTICS.toString())
            );

            // Предмет магазина
            inventory.setItem(
                    ZombieModBootstrap.getInstance().isLobby() ? 3 : 0,
                    new DmsItemStack(Material.QUARTZ, Message.ITEM_SHOP.toString())
            );
//        }, 2);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItem();

        if (itemStack == null || event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        switch (itemStack.getType()) {
            case NETHER_STALK: {
                Inventory inventory = ZMCase.gui.buildBaseInventory(CASEBUY_INVENTORY_MARK);

                inventory.setItem(13, new DmsItemStack(
                        Material.NETHER_STALK,
                        Message.BUYFOR.toString("xp", ZombieModBootstrap.getSettings().getCaseCost())
                ));
                player.openInventory(inventory);

                break;
            }

            case QUARTZ: {
                this.shopOpenStrategy.open(player);

                break;
            }

            case GOLD_NUGGET: {
                DmsFutures.addSpigotSyncCallback(
                        DMS.spigot().getMinigame().getTopByStat("points"),
                        result -> {
                            List<String> message = ZombieModBootstrap.getInstance().getInfo(player.getName());
                            Iterator<Map.Entry<DmsPlayer, Integer>> entries = result.entrySet().iterator();

                            message.add("");
                            message.add(Message.PLAYERSTOP.toString());

                            for (int i = 0; i < result.size() && i < 5; i++) {
                                Map.Entry<DmsPlayer, Integer> entry = entries.next();

                                if (entry.getValue() == 0) {
                                    break;
                                }

                                message.add((i + 1) + ". " + entry.getKey().getName() + " - " + entry.getValue());
                            }

                            player.sendMessage(message.toArray(new String[0]));
                        }
                );

                break;
            }
        }

    }

    @EventHandler
    // Да простит меня Бог кода (Ринес) за копипаст этого кода без изменений :c
    public void onInventoryClick(InventoryClickEvent event) {
        event.setCancelled(true); // Энивей отменяем

        ItemStack itemStack = event.getCurrentItem();

        if (itemStack == null) {
            return;
        }

        Player player = (Player) event.getWhoClicked();

        if (!event.getInventory().getTitle().startsWith(CASEBUY_INVENTORY_MARK) || itemStack.getType() != Material.NETHER_STALK) {
            return;
        }

        ZMCase zmcase = new ZMCase();
//        ShopStorage shopStorage = ZombieModGameLaunchPoint.getPlugin().getShopStorage();
//        shopStorage.prepare(player.getUniqueId());
        Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);

        if (gamer.getXp() < ZombieModBootstrap.getSettings().getCaseCost()) {
            Message.NOXP.send(player);

            return;
        }

        for (ZMItemData data : ZombieModGameLaunchPoint.getPlugin().getGameItems()) {
            switch (data.getType()) {
                case ARMOR: {
                    if (!gamer.haveArmor()) {
                        zmcase.addItem(data.create(), data.getWeight());
                    }

                    break;
                }
                case GUN: {
                    if (!gamer.hasGun(data.getContent())) {
                        zmcase.addItem(data.create(), data.getWeight());
                    }
                    break;
                }
                case ZOMBIE: {
                    if (!gamer.hasZombie(data.getContent())) {
                        zmcase.addItem(data.create(), data.getWeight());
                    }
                    break;
                }
            }
        }

        if (zmcase.getItems().isEmpty()) {
            Message.GOTEVERYTHING.send(player);

            return;
        }

        gamer.setXp(gamer.getXp() - ZombieModBootstrap.getSettings().getCaseCost());
        ZombieModGameLaunchPoint.cases.put(
                player.getName(),
                new PlayerOpeningController(player, zmcase, ZombieModBootstrap.getSettings().getCaseItems())
        );
    }

}
