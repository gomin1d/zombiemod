package yt.dms.zombiemod;

import com.shampaggon.crackshot.CSUtility;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ZGun {

    private final int gunid;
    private final String name;
    private final int group;

    public ZGun(int gunid, String name, int group) {
        this.gunid = gunid;
        this.name = name;
        this.group = group;
    }

    public int getGunid() {
        return gunid;
    }

    public String getName() {
        return name;
    }

    public int getGroup() {
        return group;
    }

    public ItemStack create() {
        ItemStack item = new CSUtility().generateWeapon(name);
        ItemMeta meta = item.getItemMeta();

        meta.spigot().setUnbreakable(true);

        item.setItemMeta(meta);
        return item;
    }

}
