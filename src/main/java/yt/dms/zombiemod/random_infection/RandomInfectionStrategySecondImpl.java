package yt.dms.zombiemod.random_infection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;
import yt.dms.zombiemod.Message;
import yt.dms.zombiemod.ZTeam;
import yt.dms.zombiemod.ZombieModGameLaunchPoint;
import yt.dms.zombiemod.data.GamePlayerData;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.utils.Utils;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/*
Вместо остановки и возобновления шедуляра, как в первой реализации, мы запускаем один шедуляр на 20 тиков
каждую его итерацию инкрементим таймер. Если таймер == 60, то заражаем рандомного игрока, и устанавливаем
таймер на 0. Так же сбрасываем таймер, когда зомби ударяет игрока.

Эта реализация сделана из-за подозрения в том, что в первой есть баг, связанный с тем, что заражаются два
игрока, а не один, как задумано
 */
public class RandomInfectionStrategySecondImpl implements RandomInfectionStrategy, Listener {

    private final Plugin plugin;

    private BukkitTask task;

    private int timer = 0;

    private static final Random RANDOM = ThreadLocalRandom.current();

    public RandomInfectionStrategySecondImpl(Plugin plugin) {
        Bukkit.getPluginManager().registerEvents(this, this.plugin = plugin);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDamage(EntityDamageByEntityEvent event) {
        Entity damaged = event.getEntity();
        Entity damager = event.getDamager();

        if (!(damaged instanceof Player) || !(damager instanceof Player)) {
            return;
        }

        GamePlayer gamePlayer = MinigamesAPI.getInstance().getGamePlayer(damager.getName());

        if (gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM) == ZTeam.ZOMBIE) {
            this.timer = 0;
        }
    }

    @Override
    public void start() {
        this.stop();

        this.task = Bukkit.getScheduler().runTaskTimer(this.plugin, () -> {
            List<GamePlayer> gamePlayers = Utils.getPlayersInTeam(ZTeam.SURVIVOR);

            if (gamePlayers.isEmpty() || Utils.getPlayersInTeam(ZTeam.ZOMBIE).size() > 4) {
                this.stop();

                return;
            }

            if (++this.timer == 60) {
                GamePlayer random = gamePlayers.get(RANDOM.nextInt(gamePlayers.size()));
                Utils.makeZombie(random, true);
                Utils.broadcastTitleMsg(Message.BECAME_ZOMBIE, null, "player", random.getName());
                ZombieModGameLaunchPoint.getPlugin().getListener().checkZWin(ZombieModGameLaunchPoint.getPlugin().getSession());

                this.timer = 0;
                MinigamesAPI.debug("Random infected: " + random.getName());
            }
        }, 20, 20);
    }

    private void stop() {
        if (this.task != null) {
            this.task.cancel();
            this.task = null;
        }
    }

}
