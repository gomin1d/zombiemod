package yt.dms.zombiemod.random_infection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;
import yt.dms.zombiemod.Message;
import yt.dms.zombiemod.ZTeam;
import yt.dms.zombiemod.ZombieModGameLaunchPoint;
import yt.dms.zombiemod.data.GamePlayerData;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.utils.Utils;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomInfectionStrategyImpl implements RandomInfectionStrategy, Listener {

    private final Plugin plugin;

    private BukkitTask randomInfectionTask;

    private Runnable randomInfectionRunnable = () -> {
        List<GamePlayer> gamePlayers = Utils.getPlayersInTeam(ZTeam.SURVIVOR);

        if (gamePlayers.isEmpty()) {
            this.stop();
            return;
        }

        Bukkit.broadcastMessage("Размер игроков: " + gamePlayers.size());
        GamePlayer random = gamePlayers.get(RANDOM.nextInt(gamePlayers.size()));
        Bukkit.broadcastMessage(random.getName() + " заражен.");
        Utils.makeZombie(random, true);
        ZombieModGameLaunchPoint.getPlugin().getListener().checkZWin(ZombieModGameLaunchPoint.getPlugin().getSession());
        ZombieModGameLaunchPoint.getPlugin().getListener().checkSWin();

        Utils.broadcastTitleMsg(Message.BECAME_ZOMBIE, null, "player", random.getName());

        this.start();
    };

    private static final Random RANDOM = ThreadLocalRandom.current();

    public RandomInfectionStrategyImpl(Plugin plugin) {
        Bukkit.getPluginManager().registerEvents(this, this.plugin = plugin);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDamage(EntityDamageByEntityEvent event) {
        Entity damaged = event.getEntity();
        Entity damager = event.getDamager();

        if (!(damaged instanceof Player) || !(damager instanceof Player)) {
            return;
        }

        GamePlayer gamePlayer = MinigamesAPI.getInstance().getGamePlayer(damager.getName());

        if (gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM) == ZTeam.ZOMBIE) {
            this.start();
        }
    }

    @Override
    public void start() {
        this.stop(); // Останавливаем предыдущий таск

        if (Utils.getPlayersInTeam(ZTeam.ZOMBIE).size() < 5) {
            this.randomInfectionTask = Bukkit.getScheduler().runTaskLater(this.plugin, this.randomInfectionRunnable, 60 * 20);
        }
    }

    private void stop() {
        if (this.randomInfectionTask != null) {
            this.randomInfectionTask.cancel();
            this.randomInfectionTask = null;
        }
    }

}
