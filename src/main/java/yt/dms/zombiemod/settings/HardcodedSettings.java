package yt.dms.zombiemod.settings;

import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;

import static yt.dms.zombiemod.bootstrap.ZombieModBootstrap.STAGING_SERVER;

public class HardcodedSettings implements Settings {

    @Override
    public int getMinPlayers() {
        return STAGING_SERVER ? 2 : 12;
    }

    @Override
    public int getMaxPlayers() {
        return 26;
    }

    @Override
    public int getPrepareTime() {
        return STAGING_SERVER ? 10 : 60;
    }

    @Override
    public int getGameTime() {
        return 360;
    }

    @Override
    public int getXpForZombieKill() {
        return 4;
    }

    @Override
    public int getXpForPlayerKill() {
        return 1;
    }

    @Override
    public int getXpForWin() {
        return 15;
    }

    @Override
    public int getPointsForZombieKill() {
        return 4;
    }

    @Override
    public int getPointsForPlayerKill() {
        return 1;
    }

    @Override
    public int getPointsForWin() {
        return 10;
    }

    @Override
    public int getCaseCost() {
        return 1250;
    }

    @Override
    public int getCaseItems() {
        return 20;
    }

    @Override
    public boolean isSetupMode() {
        return ZombieModBootstrap.getInstance().getMinigameConfig().getBoolean("setup-mode", false, true);
    }

    @Override
    public long getSpiderJumpCooldown() {
        return 45000;
    }

    @Override
    public void reload() {
        // do nothing
    }
}
