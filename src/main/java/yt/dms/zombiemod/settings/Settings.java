package yt.dms.zombiemod.settings;

public interface Settings {

    /**
     * @return минимальное количество игроков, нужное для начала игры
     */
    int getMinPlayers();

    /**
     * @return максимальное количество игроков
     */
    int getMaxPlayers();

    /**
     * @return время подготовки к игре
     */
    int getPrepareTime();

    /**
     * @return время игры
     */
    int getGameTime();

    /**
     * @return количество опыта за убийство зомби
     */
    int getXpForZombieKill();

    /**
     * @return количество опыта за убийство игрока
     */
    int getXpForPlayerKill();

    /**
     * @return количество опыта за победу
     */
    int getXpForWin();

    /**
     * @return количество очков за убийство зомби
     */
    int getPointsForZombieKill();

    /**
     * @return количество опыта за убийство игрока
     */
    int getPointsForPlayerKill();

    /**
     * @return количество очков за победу
     */
    int getPointsForWin();

    /**
     * @return стоимость кейса
     */
    int getCaseCost();

    /**
     * @return количество предметов в кейсе
     */
    int getCaseItems();

    /**
     * @return находится ли сервер в режиме настройки
     */
    boolean isSetupMode();

    /**
     * @return время задержки (в миллисекундах) на прыжок у зомби-паука
     */
    long getSpiderJumpCooldown();

    /**
     * Перезагрузить конфигурацию
     */
    void reload();

}
