package yt.dms.zombiemod.npc;

public interface ManageNpcStrategy {

    /**
     * Заспавнить NPC
     */
    void spawn();

}
