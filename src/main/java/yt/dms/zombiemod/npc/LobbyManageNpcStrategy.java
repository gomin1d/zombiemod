package yt.dms.zombiemod.npc;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import yt.dms.api.DMS;
import yt.dms.api.spigot.phantom.entity.PhantomEntity;
import yt.dms.api.spigot.phantom.entity.PhantomHologram;
import yt.dms.api.spigot.phantom.entity.PhantomPlayer;
import yt.dms.zombiemod.lobby.ZombieModLobbyLaunchPoint;
import yt.dms.zombiemod.menu.OpenMenuStrategy;

public class LobbyManageNpcStrategy implements ManageNpcStrategy {

    private final PhantomPlayer military = DMS.spigot().getPhantomEntityFactory().createPlayer("Solder", "MegaPirla1");
    private final PhantomEntity zombie = DMS.spigot().getPhantomEntityFactory().createEntity(EntityType.ZOMBIE);
    private final PhantomHologram zombieName = DMS.spigot().getPhantomEntityFactory().createHologram("Зомби");

    private static final Location MILITARY_SPAWN_LOCATION = new Location(null, -172.5, 65, 89.5);
    private static final Location ZOMBIE_SPAWN_LOCATION = new Location(null, -172.5, 65, 97.5);
    private static final Location ZOMBIE_NAME_LOCATION = new Location(null, -172.5, 65, 97.5);

    public LobbyManageNpcStrategy(OpenMenuStrategy weaponShop, OpenMenuStrategy zombieShop) {
        Location spawnLocation = ZombieModLobbyLaunchPoint.SPAWN_LOCATION;
        World world = spawnLocation.getWorld();

        this.military.setLocation(new Location(
                world,
                MILITARY_SPAWN_LOCATION.getX(),
                MILITARY_SPAWN_LOCATION.getY(),
                MILITARY_SPAWN_LOCATION.getZ()
        ));
        this.zombie.setLocation(new Location(
                world,
                ZOMBIE_SPAWN_LOCATION.getX(),
                ZOMBIE_SPAWN_LOCATION.getY(),
                ZOMBIE_SPAWN_LOCATION.getZ()
        ));
        this.zombieName.setLocation(new Location(
                world,
                ZOMBIE_NAME_LOCATION.getX(),
                ZOMBIE_NAME_LOCATION.getY(),
                ZOMBIE_NAME_LOCATION.getZ()
        ));

        this.military.lookAt(spawnLocation);
        this.zombie.lookAt(spawnLocation);
        this.zombieName.lookAt(spawnLocation);

        this.military.setInteraction(new SimplePhantomEntityInteraction(weaponShop::open));
        this.zombie.setInteraction(new SimplePhantomEntityInteraction(zombieShop::open));
    }

    @Override
    public void spawn() {
        this.military.spawn(true);
        this.zombie.spawn(true);
        this.zombieName.spawn(true);
    }

}
