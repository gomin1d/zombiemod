package yt.dms.zombiemod.npc;

import org.bukkit.entity.Player;
import yt.dms.api.spigot.phantom.entity.PhantomEntityInteraction;

import java.util.function.Consumer;

/**
 * Простая реализация PhantomEntityInteraction
 * На левый и правый клик назначается один обработчик
 */
public class SimplePhantomEntityInteraction implements PhantomEntityInteraction {

    private final Consumer<Player> consumer;

    public SimplePhantomEntityInteraction(Consumer<Player> consumer) {
        this.consumer = consumer;
    }

    @Override
    public void onLeftClick(Player player) {
        this.consumer.accept(player);
    }

    @Override
    public void onRightClick(Player player) {
        this.consumer.accept(player);
    }
}
