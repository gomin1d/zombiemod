package yt.dms.zombiemod.lobby;

import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import yt.dms.api.DMS;
import yt.dms.api.spigot.SGuiButtons;

@AllArgsConstructor
public class EventListener implements Listener {

    private final SGuiButtons.GuiButton queueButton;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        DMS.spigot().getGuiButtonsUtils().showButton(event.getPlayer(), this.queueButton);
    }

}
