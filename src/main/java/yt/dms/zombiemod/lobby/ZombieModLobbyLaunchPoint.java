package yt.dms.zombiemod.lobby;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import yt.dms.api.DMS;
import yt.dms.api.minigame.plugin.DmsMinigameConfig;
import yt.dms.api.spigot.SGuiButtons;
import yt.dms.api.spigot.table.Table;
import yt.dms.api.spigot.table.TableColumn;
import yt.dms.api.util.DmsFutures;
import yt.dms.zombiemod.bootstrap.LaunchPoint;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.command.SetupCommand;
import yt.dms.zombiemod.menu.ShopsStorage;
import yt.dms.zombiemod.npc.LobbyManageNpcStrategy;
import yt.dms.zombiemod.sideboard.lobby.LobbySideboardUpdateStrategy;
import yt.dms.zombiemod.sideboard.lobby.LobbySideboardUpdateStrategyImpl;
import yt.dms.zombiemod.sideboard.lobby.LobbySideboardUpdater;

import java.util.Arrays;

public class ZombieModLobbyLaunchPoint implements LaunchPoint {

    public static final Location SPAWN_LOCATION = new Location(Bukkit.getWorld("world"), -166.5, 65, 93.5);

    @Getter
    private static Table table;

    @Override
    public void enabling() {
        LobbySideboardUpdateStrategy sideboardUpdateStrategy = new LobbySideboardUpdateStrategyImpl();
        LobbySideboardUpdater sideboardUpdater = new LobbySideboardUpdater(sideboardUpdateStrategy);

        Bukkit.getPluginManager().registerEvents(sideboardUpdater, ZombieModBootstrap.getInstance());

        SGuiButtons guiButtonsUtils = DMS.spigot().getGuiButtonsUtils();
        SGuiButtons.GuiButton queueButton = guiButtonsUtils.createQueueButton(guiButtonsUtils
                .builder("Войти в очередь")
                .color(255, 0, 0, 50)
                .queueName("zm")
        );

        Bukkit.getPluginManager().registerEvents(
                new EventListener(queueButton),
                ZombieModBootstrap.getInstance()
        );

        ShopsStorage shopsStorage = ZombieModBootstrap.getInstance().getShopsStorage();
        new LobbyManageNpcStrategy(shopsStorage.getWeaponShopOpenStrategy(), shopsStorage.getChooseZombieShopOpenStrategy())
                .spawn();

        this.initTop();
        this.initGoldChanger();
        new SetupCommand(null);
    }

    @Override
    public void disabling() {
        //
    }

    private void initGoldChanger() {
        Location location = ZombieModBootstrap.getInstance().getMinigameConfig().getLocation("gold_changer_location");

        if (location == null) {
            return;
        }

        DMS.spigot().getPhantomEntityFactory().spawnAutoGoldChanger(location, 2);
    }

    private void initTop() {
        DmsMinigameConfig config = ZombieModBootstrap.getInstance().getMinigameConfig();
        table = DMS.spigot().getTableUtils().constructNew();
        Location location = config.getLocation("top_table_location");

        if (location == null) {
            return;
        }

        table.setLocation(location);
        table.setRotationX(config.getFloat("top_table_rotation_x", 0));
        table.setRotationY(config.getFloat("top_table_rotation_y", 0));
        table.setRotationZ(config.getFloat("top_table_rotation_z", 0));
        table.addColumn(new TableColumn("Ник игрока", 128));
        table.addColumn(new TableColumn("Рейтинг", 32));
        table.create();

        Bukkit.getScheduler().scheduleSyncRepeatingTask(ZombieModBootstrap.getInstance(), () -> {
            DmsFutures.addSpigotSyncCallback(
                    DMS.spigot().getMinigame().getTopByStat("points"),
                    result -> {
                        table.clearRows();
                        result.forEach((player, points) -> {
                            table.addRow(Arrays.asList(DMS.getChatUtil().colorize(player.getVisibleFullNameUncolored()), Integer.toString(points)));
                        });
                        table.update();
                    }
            );
        }, 1L, 20L * 60);
    }

}
