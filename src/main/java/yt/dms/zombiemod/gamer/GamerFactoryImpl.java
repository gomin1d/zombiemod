package yt.dms.zombiemod.gamer;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class GamerFactoryImpl implements GamerFactory {

    private final Map<String, Gamer> gamers = new HashMap<>();

    @Override
    public Gamer getGamer(String name) {
        return this.gamers.computeIfAbsent(name.toLowerCase(), key -> new GamerImpl(name));
    }

    @Override
    public Gamer getGamer(Player player) {
        return this.getGamer(player.getName());
    }

    @Override
    public void inactiveGamer(String name) {
        this.gamers.remove(name.toLowerCase());
    }

}
