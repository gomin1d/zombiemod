package yt.dms.zombiemod.gamer;

import org.bukkit.entity.Player;

public interface GamerFactory {

    Gamer getGamer(String name);

    Gamer getGamer(Player player);

    void inactiveGamer(String name);

}
