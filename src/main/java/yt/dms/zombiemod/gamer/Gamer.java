package yt.dms.zombiemod.gamer;

import yt.dms.api.player.DmsPlayer;
import yt.dms.zombiemod.ZombieType;

import java.util.UUID;

public interface Gamer {

    UUID getUniqueId();

    String getName();

    int getXp();

    int getPoints();

    ZombieType getZombieType();

    void setXp(int xp);

    void setPoints(int points);

    void setZombieType(ZombieType zombieType);

    int getGun(int gunNumber);

    void setGun(int gunNumber, int gunId);

    int getZombieKilled();

    void incrementZombieKilled();

    int getSurvivorsKilled();

    void incrementSurvivorsKilled();

    int getGamesPlayed();

    void incrementGamesPlayed();

    boolean haveArmor();

    void addArmor();

    /**
     * Куплен ли тип зомби у игрока
     * @param id тип зомби
     * @return {@code true} если куплен, {@code false} в противном случае
     */
    boolean hasZombie(int id);

    /**
     * Куплено ли оружие у игрока
     * @param id оружие
     * @return {@code true} если куплено, {@code false} в противном случае
     */
    boolean hasGun(int id);

    /**
     * Добавить игроку купленного зомби
     * @param id тип зомби
     */
    void buyZombie(int id);

    /**
     * Добавить игроку купленное оружие
     * @param id оружие
     */
    void buyGun(int id);

    DmsPlayer toDmsPlayer();

}
