package yt.dms.zombiemod.gamer;

import yt.dms.api.DMS;
import yt.dms.api.player.DmsPlayer;
import yt.dms.api.player.DmsPlayerMinigameStats;
import yt.dms.zombiemod.ZombieType;
import yt.dms.zombiemod.data.GamePlayerData;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

public class GamerImpl implements Gamer {

    private final String name;

    private static final Collection<Integer> DEFAULT_GUNS = Arrays.asList(1, 2),
            DEFAULT_ZOMBIES = Collections.singleton(1);

    public GamerImpl(String name) {
        this.name = name;
    }

    @Override
    public UUID getUniqueId() {
        return this.toDmsPlayer().spigot().getSpigotPlayer().getUniqueId();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getXp() {
        return this.getMinigameStats().getStat("xp");
    }

    @Override
    public int getPoints() {
        return this.getMinigameStats().getStat("points");
    }

    @Override
    public ZombieType getZombieType() {
        int zombieType = this.getMinigameStats().getStat("zombie_type");

        return zombieType == 0
                ? ZombieType.DEFAULT
                : ZombieType.byId(zombieType);
    }

    @Override
    public void setXp(int xp) {
        this.getMinigameStats().setStat("xp", xp);
    }

    @Override
    public void setPoints(int points) {
        this.getMinigameStats().setStat("points", points < 0 ? 0 : points);
    }

    @Override
    public void setZombieType(ZombieType zombieType) {
        this.getMinigameStats().setStat("zombie_type", zombieType.getId());
    }

    @Override
    public int getGun(int gunNumber) {
        int id = this.getMinigameStats().getStat("gun." + gunNumber);

        if (id == 0) {
            if (gunNumber == 1) { // Если номер оружия 1 и у игрока нет купленного оружия этого класса, то выдаем стандартное оружие (его айди 1)
                return 1;
            }

            if (gunNumber == 2) { // Если номер оружия 2 и у игрока нет купленного оружия этого класса, то выдаем стандартное оружие (его айди 2)
                return 2;
            }
        }

        return id;
    }

    @Override
    public void setGun(int gunNumber, int gunId) {
        this.getMinigameStats().setStat("gun." + gunNumber, gunId);
    }

    @Override
    public int getZombieKilled() {
        return this.getMinigameStats().getStat("zombie_killed");
    }

    @Override
    public void incrementZombieKilled() {
        this.getMinigameStats().setStat("zombie_killder", this.getZombieKilled() + 1);
    }

    @Override
    public int getSurvivorsKilled() {
        return this.getMinigameStats().getStat("survivors_killed");
    }

    @Override
    public void incrementSurvivorsKilled() {
        this.getMinigameStats().setStat("survivors_killed", this.getSurvivorsKilled() + 1);

        this.toGamePlayer().<Integer>computeData(
                GamePlayerData.SURVIVORS_KILLED,
                (key, value) -> (value == null ? 1 : value + 1)
        );
    }

    @Override
    public int getGamesPlayed() {
        return this.getMinigameStats().getStat("games_played");
    }

    @Override
    public void incrementGamesPlayed() {
        this.getMinigameStats().setStat("games_played", this.getGamesPlayed() + 1);
    }

    @Override
    public boolean haveArmor() {
        return this.getMinigameStats().hasFeature("armor");
    }

    @Override
    public void addArmor() {
        this.getMinigameStats().addFeature("armor");
    }

    @Override
    public boolean hasZombie(int id) {
        return DEFAULT_ZOMBIES.contains(id) || this.hasInShop("purchased_zombies", id);
    }

    @Override
    public boolean hasGun(int id) {
        return DEFAULT_GUNS.contains(id) || this.hasInShop("purchased_weapons", id);
    }

    @Override
    public void buyZombie(int id) {
        this.addToShop("purchased_zombies", id);
    }

    @Override
    public void buyGun(int id) {
        this.addToShop("purchased_weapons", id);
    }

    @Override
    public DmsPlayer toDmsPlayer() {
        return DMS.getPlayerRegistry().get(this.name);
    }

    private boolean hasInShop(String key, int id) {
        id = (int) Math.pow(2, id); // Возводим 2 в степень айдишки

        return (this.getShopBitMask(key) & id) == id; // Проверяем, есть ли в бит маске бит с нашим айди
    }

    private void addToShop(String key, int id) {
        id = (int) Math.pow(2, id); // Возводим 2 в степень айдишки
        int mask = this.getShopBitMask(key); // Получаем бит маску

        mask = mask | id; // Устанавливаем бит в бит маске
        this.getMinigameStats().setStat(key, mask); // Обновляем бит маску
    }

    private int getShopBitMask(String key) {
        return this.getMinigameStats().getStat(key);
    }

    public DmsPlayerMinigameStats getMinigameStats() {
        return this.toDmsPlayer().getCurrentMinigameStats();
    }

    private GamePlayer toGamePlayer() {
        return MinigamesAPI.getInstance().getGamePlayer(this.name);
    }


}
