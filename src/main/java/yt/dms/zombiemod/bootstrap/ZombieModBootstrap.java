package yt.dms.zombiemod.bootstrap;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.scoreboard.Objective;
import yt.dms.api.DMS;
import yt.dms.api.DmsSpigot;
import yt.dms.api.minigame.DmsMinigameType;
import yt.dms.api.minigame.plugin.DmsMinigamePlugin;
import yt.dms.zombiemod.Message;
import yt.dms.zombiemod.ZombieModGameLaunchPoint;
import yt.dms.zombiemod.gamer.Gamer;
import yt.dms.zombiemod.gamer.GamerFactory;
import yt.dms.zombiemod.gamer.GamerFactoryImpl;
import yt.dms.zombiemod.gun.GunRegistry;
import yt.dms.zombiemod.gun.HardcodedGunRegistry;
import yt.dms.zombiemod.listener.CommonEventListener;
import yt.dms.zombiemod.lobby.ZombieModLobbyLaunchPoint;
import yt.dms.zombiemod.menu.ShopsStorage;
import yt.dms.zombiemod.settings.HardcodedSettings;
import yt.dms.zombiemod.settings.Settings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ZombieModBootstrap extends DmsMinigamePlugin {

    @Getter
    private static ZombieModBootstrap instance;

    @Getter
    private boolean lobby;

    private LaunchPoint launchPoint;

    @Getter
    private ShopsStorage shopsStorage;

    @Getter
    private GamerFactory gamerFactory;

    @Getter
    private GunRegistry gunRegistry;

    @Getter
    private static Settings settings;

    @Getter
    private static File serverFolder;

    public static final boolean STAGING_SERVER = false;

    private static final boolean DEBUG = true;

    public ZombieModBootstrap() {
        super(DmsMinigameType.ZOMBIEMOD);
    }

    @Override
    protected void enabling() {
        this.saveDefaultConfig();
        instance = this;

        label0:
        {
            String path = this.getDataFolder().getAbsolutePath();
            debug("datafolder: " + path);
            debug("separator: " + File.separator);
            File pluginsFolder = new File(path.substring(0, path.lastIndexOf(File.separator)));

            if (!pluginsFolder.exists()) {
                debug("plugins folder is not exists");

                break label0;
            }

            debug("plugins: " + (path = pluginsFolder.getAbsolutePath()));
            File serverFolder = new File(path.substring(0, path.lastIndexOf(File.separator)));

            if (!serverFolder.exists()) {
                debug("server folder is not exists");

                break label0;
            }
            debug("server folder: " + serverFolder.getAbsolutePath());

            File[] files = serverFolder.listFiles();

            if (files != null) {
                debug("Files in server folder:");

                for (File file : files) {
                    debug(" - " + file.getName());
                }
            }

            ZombieModBootstrap.serverFolder = serverFolder;
        }

        //region load guns
        {
            this.gunRegistry = new HardcodedGunRegistry();
            int guns = this.gunRegistry.load();

            debug("Load " + guns + " guns");
        }
        //endregion

        settings = new HardcodedSettings();
        this.gamerFactory = new GamerFactoryImpl();
        this.shopsStorage = new ShopsStorage();
        this.launchPoint = (this.lobby = DMS.spigot().getInternalServerTypeAsEnum() == DmsSpigot.ServerType.LOBBY)
                ? new ZombieModLobbyLaunchPoint()
                : new ZombieModGameLaunchPoint();

        debug("Start as " + (this.lobby ? "lobby" : "game"));

        this.launchPoint.enabling();

        Bukkit.getPluginManager().registerEvents(new CommonEventListener(this.shopsStorage.getShopOpenStrategy()), this);

        Bukkit.getScoreboardManager().getMainScoreboard().getObjectives().forEach(Objective::unregister);
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "op kasdo");
    }

    @Override
    public FileConfiguration getConfig() {
        // Да я заебался искать все эти getConfig()
        return this.getFileConfig();
    }

    @Override
    protected void disabling() {
        this.launchPoint.disabling();
    }

    public List<String> getInfo(String name) {
        Gamer gamer = this.gamerFactory.getGamer(name);
        ArrayList<String> message = new ArrayList<>();

        message.add(Message.PLAYERSTATS.toString());
        message.add("§eXP: §r" + gamer.getXp());
        message.add("§eОчков: §r" + gamer.getPoints());
        message.add("§eСыграно матчей: §r" + gamer.getGamesPlayed());
        message.add("§eУбито зомби: §r" + gamer.getZombieKilled());
        message.add("§eИнифицировано выживших: §r" + gamer.getSurvivorsKilled());

        return message;
    }

    public static void debug(String message) {
        if (DEBUG) {
            instance.getLogger().info("[Debug] " + message);
        }
    }

}
