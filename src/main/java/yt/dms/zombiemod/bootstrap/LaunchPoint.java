package yt.dms.zombiemod.bootstrap;

public interface LaunchPoint {

    void enabling();

    void disabling();

}
