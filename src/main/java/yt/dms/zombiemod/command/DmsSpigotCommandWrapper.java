package yt.dms.zombiemod.command;

import yt.dms.api.command.DmsCommandAccessCheckResult;
import yt.dms.api.command.DmsCommandExecutor;
import yt.dms.api.command.DmsSpigotCommand;
import yt.dms.api.command.exception.DmsCommandException;
import yt.dms.api.player.DmsPlayer;

/**
 * Враппер над DmsSpigotCommand
 * Используется, когда поведение нашей подкоманды можно описать поведением другой подкоманды
 */
public class DmsSpigotCommandWrapper extends DmsSpigotCommand {

    private final DmsSpigotCommand parent;

    /**
     * @see DmsSpigotCommand
     */
    protected DmsSpigotCommandWrapper(DmsSpigotCommand parent, String name, String description, String... aliases) {
        super(name, description, aliases);
        this.parent = parent;
    }

    @Override
    public DmsCommandAccessCheckResult hasAccess(DmsPlayer player) {
        return this.parent.hasAccess(player);
    }

    @Override
    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
        this.parent.execute(executor, args);
    }

}
