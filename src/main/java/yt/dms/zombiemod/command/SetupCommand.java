package yt.dms.zombiemod.command;

import org.apache.commons.lang3.math.NumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import yt.dms.api.command.DmsCommandAccessCheckResult;
import yt.dms.api.command.DmsCommandExecutor;
import yt.dms.api.command.DmsSpigotCommandParent;
import yt.dms.api.command.exception.DmsCommandException;
import yt.dms.api.command.exception.DmsCommandNotEnoughArgumentsException;
import yt.dms.api.player.DmsPlayer;
import yt.dms.api.player.PermissionGroup;
import yt.dms.zombiemod.arena.ArenasManager;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;

import java.util.List;
import java.util.stream.Collectors;

public class SetupCommand extends DmsSpigotCommandParent {

    private static final DmsCommandAccessCheckResult REQUIRED_GROUP = new DmsCommandAccessCheckResult(PermissionGroup.ADMINISTRATOR);

    public SetupCommand(ArenasManager arenasManager) {
        super("zmsetup", "настройка режима");

        // Вынесено в отдельные методы, чтобы сделать меньше вложенность
        if (ZombieModBootstrap.getInstance().isLobby()) {
            subcommandsForLobby();
        } else {
            subcommandsForGame(arenasManager);
        }
    }

    private void subcommandsForLobby() {
        subcommand(new DmsSpigotCommandWrapper(this, "top_table", "управление таблицей топа") {

            {
                subcommand(new DmsSpigotCommandWrapper(this, "location", "установить позицию для таблицы топа") {

                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        checkPlayer(executor);
                        ZombieModBootstrap.getInstance().getMinigameConfig().setLocation(
                                "top_table_location",
                                executor.getPlayer().spigot().getSpigotPlayer().getLocation()
                        );
                        executor.sendCommandMessage("&aПозиция таблицы топа обновлена. Перезагрузите сервер.");
                    }

                });

                subcommand(new DmsSpigotCommandWrapper(this, "rotation", "установить поворот таблицы") {

                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        String rotation;

                        if (args.length < 2
                                || !((rotation = args[0]).equalsIgnoreCase("x")
                                && rotation.equalsIgnoreCase("y")
                                && rotation.equalsIgnoreCase("z"))
                                && !NumberUtils.isNumber(args[1])) {
                            throw new DmsCommandNotEnoughArgumentsException("одна из осей - x или y или z, значение [float]");
                        }

                        float value = Float.parseFloat(args[1]);

                        ZombieModBootstrap.getInstance().getMinigameConfig().setFloat(
                                "top_table_rotation_" + rotation,
                                value
                        );
                        executor.sendCommandMessage("&aПоворот таблицы топа по оси " + rotation + " обновлен. Перезагрузите сервер.");
                    }

                });

            }

        });

        subcommand(new DmsSpigotCommandWrapper(this, "gold_changer", "управление обменивателем золота") {
            {
                subcommand(new DmsSpigotCommandWrapper(this, "location", "установить локацию обменивателя") {
                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        checkPlayer(executor);
                        ZombieModBootstrap.getInstance().getMinigameConfig().setLocation(
                                "gold_changer_location",
                                executor.getPlayer().spigot().getSpigotPlayer().getLocation()
                        );
                        executor.sendCommandMessage("&aПозиция обменивателя золота обновлена. Перезагрузите сервер.");
                    }
                });
            }
        });
    }

    private void subcommandsForGame(ArenasManager arenasManager) {
        subcommand(new DmsSpigotCommandWrapper(this, "set_lobby", "установить точку лобби") {

            @Override
            public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                checkPlayer(executor);
                ZombieModBootstrap.getInstance().getMinigameConfig().setLocation(
                        "lobby",
                        executor.getPlayer().spigot().getSpigotPlayer().getLocation()
                );
                executor.sendCommandMessage("&aПозиция точки спавна обновлена. Перезагрузите сервер для обновления.");
            }

        });

        subcommand(new DmsSpigotCommandWrapper(this, "world", "управление мирами") {

            {
                subcommand(new DmsSpigotCommandWrapper(this, "list", "список миров") {
                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        executor.sendCommandMessage("§fСписок карт: " + String.join("§f, ", Bukkit.getWorlds().stream()
                                .map(World::getName)
                                .sorted(String.CASE_INSENSITIVE_ORDER)
                                .collect(Collectors.toList())
                        ));
                    }
                });

                subcommand(new DmsSpigotCommandWrapper(this, "tp", "переместиться на карту") {

                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        checkPlayer(executor);

                        if (args.length < 1) {
                            throw new DmsCommandNotEnoughArgumentsException("название карты");
                        }

                        String mapName = args[0];
                        World world = Bukkit.getWorld(mapName);

                        if (world == null) {
                            throw new DmsCommandException("Карта '" + mapName + "' не найдена!");
                        }

                        executor.getPlayer().spigot().getSpigotPlayer().teleport(world.getSpawnLocation());
                        executor.sendCommandMessage("§aТелепортация на арену '" + mapName + "'");
                    }

                });
            }

        });

        subcommand(new DmsSpigotCommandWrapper(this, "arena", "работа с аренами") {

            {
                subcommand(new DmsSpigotCommandWrapper(this, "load", "загрузить арену(арены)") {

                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        if (args.length < 1) {
                            throw new DmsCommandNotEnoughArgumentsException("название_арены|all");
                        }

                        String arena = args[0];

                        if (arena.equalsIgnoreCase("all")) {
                            List<String> toLoad = arenasManager.getAllArenas().stream()
                                    .filter(s -> !arenasManager.isLoaded(s))
                                    .collect(Collectors.toList());

                            if (toLoad.isEmpty()) {
                                throw new DmsCommandException("§cВсе карты уже загружены!");
                            }

                            toLoad.forEach(arenasManager::load);
                            executor.sendCommandMessage("§aЗагруженные карты: " + String.join(", ", toLoad));
                            return;
                        }

                        if (arenasManager.isLoaded(arena)) {
                            throw new DmsCommandException("Арена '" + arena + "' уже загружена!");
                        }

                        arenasManager.load(arena);
                        executor.sendCommandMessage("§aАрена '" + arena + "' успешно загружена!");
                    }

                });

                subcommand(new DmsSpigotCommandWrapper(this, "list", "список арен") {

                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        List<String> arenas = arenasManager.getAllArenas();

                        if (arenas.isEmpty()) {
                            throw new DmsCommandException("Список арен пуст.");
                        }

                        executor.sendCommandMessage("§fСписок арен: " + String.join("§f, ", arenas.stream()
                                .sorted(String.CASE_INSENSITIVE_ORDER)
                                .map(arena -> (arenasManager.isLoaded(arena) ? "§e" : "§c") + arena)
                                .collect(Collectors.toList())
                        ));
                    }

                });

                subcommand(new DmsSpigotCommandWrapper(this, "tp", "переместиться на арену") {

                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        checkPlayer(executor);

                        if (args.length < 1) {
                            throw new DmsCommandNotEnoughArgumentsException("название_арены");
                        }

                        String arenaName = args[0];

                        if (!arenasManager.hasArena(arenaName)) {
                            throw new DmsCommandException("Арена '" + arenaName + "' не найдена!");
                        }

                        World world = Bukkit.getWorld(arenaName);
                        Location spawnLocation = arenasManager.getSpawn(arenaName);

                        executor.getPlayer().spigot().getSpigotPlayer()
                                .teleport(spawnLocation == null ? world.getSpawnLocation() : spawnLocation);
                        executor.sendCommandMessage("§aТелепортация на арену '" + arenaName + "'");
                    }

                });

                subcommand(new DmsSpigotCommandWrapper(this, "setspawn", "установить точку спавна для этой арены") {

                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        checkPlayer(executor);

                        Player player = executor.getPlayer().spigot().getSpigotPlayer();
                        World world = player.getWorld();

                        if (!arenasManager.hasArena(world.getName())) {
                            throw new DmsCommandException("Вы должны находиться на арене!");
                        }

                        arenasManager.setSpawn(world.getName(), player.getLocation());
                        executor.sendCommandMessage("§aТочка спавна успешно установлена!");
                    }

                });

                subcommand(new DmsSpigotCommandWrapper(this, "rename", "переименовать арену") {

                    @Override
                    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                        if (args.length < 2) {
                            throw new DmsCommandNotEnoughArgumentsException("название", "отображаемое название");
                        }

                        String name = args[0], displayName = args[1];

                        if (!arenasManager.hasArena(name)) {
                            throw new DmsCommandException("§cАрена '§r" + name + "§c' не найдена!");
                        }

                        arenasManager.setDisplayName(name, displayName);
                        executor.sendCommandMessage("§aВы успешно установили арене '§r" + name +
                                "§a' отображаемое название '§r" + displayName + "§a'");
                    }

                });
            }

            @Override
            public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
                showHelp(executor);
            }

        });
    }

    @Override
    public DmsCommandAccessCheckResult hasAccess(DmsPlayer player) {
        return player.getPermissions().isAdministrator()
                ? null
                : REQUIRED_GROUP;
    }

    @Override
    public void execute(DmsCommandExecutor executor, String[] args) throws DmsCommandException {
        checkPlayer(executor);
        showHelp(executor);
    }

}
