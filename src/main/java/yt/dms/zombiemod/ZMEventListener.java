package yt.dms.zombiemod;

import com.shampaggon.crackshot.CSUtility;
import com.shampaggon.crackshot.events.WeaponDamageEntityEvent;
import com.shampaggon.crackshot.events.WeaponPreShootEvent;
import com.shampaggon.crackshot.events.WeaponScopeEvent;
import com.shampaggon.crackshot.events.WeaponShootEvent;
import me.libraryaddict.disguise.DisguiseAPI;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;
import yt.dms.api.DMS;
import yt.dms.api.player.DmsPlayerPermissions;
import yt.dms.api.spigot.item.DmsItemStack;
import yt.dms.zombiemod.arena.ArenasManager;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.cases.Case;
import yt.dms.zombiemod.cases.PlayerOpeningController;
import yt.dms.zombiemod.data.DataKey;
import yt.dms.zombiemod.data.GamePlayerData;
import yt.dms.zombiemod.data.GameSessionData;
import yt.dms.zombiemod.gamer.Gamer;
import yt.dms.zombiemod.gun.Gun;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.lib.mgapi.arena.GameArena;
import yt.dms.zombiemod.lib.mgapi.events.*;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;
import yt.dms.zombiemod.random_infection.RandomInfectionStrategySecondImpl;
import yt.dms.zombiemod.sideboard.SideboardUpdateStrategy;
import yt.dms.zombiemod.sideboard.game.GameSideboardUpdateStrategy;
import yt.dms.zombiemod.sideboard.game.GameSideboardUpdateStrategyImpl;
import yt.dms.zombiemod.sideboard.wait.WaitGameSideboardUpdateStrategy;
import yt.dms.zombiemod.sideboard.wait.WaitGameSideboardUpdateStrategyImpl;
import yt.dms.zombiemod.totem.Totem;
import yt.dms.zombiemod.totem.TotemManager;
import yt.dms.zombiemod.totem.toxic.ToxicTotemManager;
import yt.dms.zombiemod.utils.Utils;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static yt.dms.zombiemod.ZombieModGameLaunchPoint.NOCSAPI;

@SuppressWarnings("unused")
public class ZMEventListener implements Listener {

    private static String waitfor = "";
    private static Location spawn, corner1, corner2;
    private final Random rand = ThreadLocalRandom.current();
    private final GamePlayer[] lastzombs = new GamePlayer[3];
    private final HashMap<String, Integer> invzombies = new HashMap<>();
    private final HashMap<String, Integer> brokenGuns = new HashMap<>();
    private int lastzombspos = 0;
    private List<String> GGs = new ArrayList<>();
    private boolean shoutUsed = false;
    private boolean timeout = false;
    private boolean stopping = false;
    private VoteController voteController;
    private WaitGameSideboardUpdateStrategy waitGameSideboardUpdateStrategy;
    private GameSideboardUpdateStrategy gameSideboardUpdateStrategy;

    private TotemManager<?> toxicTotemManager;

    public ZMEventListener() {
        this.toxicTotemManager = new ToxicTotemManager(ZombieModBootstrap.getInstance());

        this.changeSideboard();

        Bukkit.getScheduler().runTaskTimer(ZombieModBootstrap.getInstance(), () -> {
            this.getCurrentSideboardUpdateStrategy().update();
        }, 20, 20);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(ZombieModBootstrap.getInstance(), new ZombieNearRunnable(), 10L, 10L);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(ZombieModBootstrap.getInstance(), new ZombieInvisibleRunnable(), 20L, 20L);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(ZombieModBootstrap.getInstance(), new WebCooldownRunnable(), 20L, 20L);

        voteController = new VoteController(ZombieModGameLaunchPoint.getPlugin().getMAPI().getArenaManager().getArenas());

        if (!NOCSAPI) {
            ZombieModGameLaunchPoint.getPlugin().getChooseMapStrategy().load();

            DMS.spigot().getQueueUtils().awaitPlayers(
                    ZombieModBootstrap.getSettings().getMinPlayers(),
                    ZombieModBootstrap.getSettings().getMaxPlayers(),
                    "zm",
                    this::startTheGameFromQueue
            );
        }
    }

    private void changeSideboard() {
        if (this.waitGameSideboardUpdateStrategy == null) {
            this.gameSideboardUpdateStrategy = null;
            this.waitGameSideboardUpdateStrategy = new WaitGameSideboardUpdateStrategyImpl();
        } else {
            this.waitGameSideboardUpdateStrategy = null;
            this.gameSideboardUpdateStrategy = new GameSideboardUpdateStrategyImpl();
        }
    }

    public static void setWaitFor(String wait) {
        waitfor = wait;
    }

    public static void save(Player player, String arenaName) {
        if (spawn == null || corner1 == null || corner2 == null) {
            player.sendMessage("Вы указали не все точки");

            return;
        }

        if (ZombieModGameLaunchPoint.getPlugin().getMAPI().getArenaManager().hasArena(arenaName)) {
            player.sendMessage("Арена с таким именем уже существует");

            return;
        }

        ZombieModGameLaunchPoint.getPlugin().getMAPI().getArenaManager().createArena(arenaName, corner1, corner2, spawn);
        player.sendMessage("Сохранено.");
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(p);

        prepareJoined(p, gamer);
    }

    private void prepareJoined(Player player, Gamer gamer) {
        player.getInventory().setHelmet(new ItemStack(Material.AIR));
        DisguiseAPI.undisguiseToAll(player);

//        player.getInventory().clear();

        if (gamer.haveArmor()) {
            player.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
        }

        player.setMaxHealth(20);
        player.setHealth(20);

        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }

        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500000, 2));

        for (Player player1 : Bukkit.getOnlinePlayers()) {
            player1.showPlayer(player);
        }

        GameSession gameSession = ZombieModGameLaunchPoint.getPlugin().getSession();

        if (gameSession == null) {
            return;
        }

        Inventory inventory = player.getInventory();

        if (!gameSession.isStarted() && gameSession.getPlayersCount() < ZombieModBootstrap.getSettings().getMaxPlayers()) {
            player.setGameMode(GameMode.ADVENTURE);

            gameSession.joinPlayer(player.getName());

            this.getCurrentSideboardUpdateStrategy().send(player);

            //broadcastMsg("zm.join", ImmutableMap.of("%player", player.getName()));

            player.setFlying(false);
            player.setAllowFlight(false);

            inventory.setItem(8, new DmsItemStack(Material.MAGMA_CREAM, Message.ITEM_LOBBY.toString()));

            if (gameSession.getData(GamePlayerData.VOTE_ENDED) != null && (Boolean) gameSession.getData(GamePlayerData.VOTE_ENDED)) {
                player.teleport((Location) gameSession.getData(GameSessionData.LOBBY));

                player.sendMessage("Телепортируем на арену...");

                Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> player.teleport(gameSession.getArena().getSpawns()[0]), 20);
            } else {
                for (PotionEffect effect : player.getActivePotionEffects()) {
                    player.removePotionEffect(effect.getType());
                }

                player.teleport((Location) gameSession.getData(GameSessionData.LOBBY));

                Message.NOTENOUGHTPLAYERS.sendActionBar(player);
            }
        } else {
            DMS.spigot().getBungeeUtils().sendPlayer(player, "@zmlobby");
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        GamePlayer gamePlayer = MinigamesAPI.getInstance().getGamePlayer(event.getPlayer().getName());
        Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(event.getPlayer());
        ZTeam zteam = gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM);

        Message.LEAVE.broadcast(
                "player", event.getPlayer().getName(),
                "color", zteam == ZTeam.SURVIVOR ? ChatColor.BLUE : zteam == ZTeam.ZOMBIE ? ChatColor.RED : ChatColor.GRAY
        );
        ZombieModBootstrap.getInstance().getGamerFactory().inactiveGamer(gamePlayer.getName());

        GameSession gameSession = ZombieModGameLaunchPoint.getPlugin().getSession();
        if (gameSession.isStarted()) {
            gamer.setPoints(gamer.getPoints() - 10);
            checkZWin(gameSession);
        }

        PlayerOpeningController controller = ZombieModGameLaunchPoint.cases.get(event.getPlayer().getName());

        if (controller != null) {
            controller.stop(true);
        }

        Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
            this.getCurrentSideboardUpdateStrategy().update();
        }, 1);
    }

    @EventHandler
    public void onLeave(PlayerLeaveGameSessionEvent event) {
        if (event.getSession().isStarted()) {
            checkSWin();
        } else if (voteController != null) {
            Player player = event.getPlayer().getEntity();
            player.getInventory().remove(Material.PAPER);

            int votes = 1;

            if (!NOCSAPI) {
                DmsPlayerPermissions perms = DMS.getPlayerRegistry().get(player.getName()).getPermissions();
                votes = perms.isRich() ? 5 : perms.isVipPlus() ? 3 : perms.isVip() ? 2 : 1;
            }

            voteController.removeVote(player.getName(), votes);
        }

        if (event.getSession().getPlayersCount() == 1 &&  //if everyone but one player left and map had already chosen
                (event.getSession().getData(GamePlayerData.VOTE_ENDED) != null && (Boolean) event.getSession().getData(GamePlayerData.VOTE_ENDED))) {
            event.getSession().addData(GamePlayerData.VOTE_ENDED, false);
            voteController = new VoteController(ZombieModGameLaunchPoint.getPlugin().getMAPI().getArenaManager().getArenas());
        }
    }

    @EventHandler
    public void onArenaLeave(GamePlayerCrossArenaBorderEvent event) {
        if (event.getSession().getData(GamePlayerData.VOTE_ENDED) != null && (Boolean) event.getSession().getData(GamePlayerData.VOTE_ENDED)) {
            event.getPlayer().getEntity().teleport(event.getSession().getArena().getSpawns()[0]);
        }
    }

    @EventHandler
    public void onSessionStart(GameSessionStartEvent event) {
        this.changeSideboard();
        GamePlayer[] players = event.getSession().getAllPlayers();
        GamePlayer zombie = players[rand.nextInt(players.length)];
        zombie.addData(GamePlayerData.ZOMBIE_TEAM, ZTeam.ZOMBIE);

        MinigamesAPI.debug("Game started");

        if (!NOCSAPI) {
            DMS.spigot().getQueueUtils().stopAwaitingPlayers();
        }

        for (GamePlayer gamePlayer : players) {
            Player player = gamePlayer.getEntity();
            player.playSound(player.getLocation(), Sound.NOTE_PIANO, 10, 0);

            Inventory inventory = player.getInventory();
            inventory.clear();
            inventory.setItem(8, new DmsItemStack(Material.GOLD_NUGGET, Message.ITEM_STATISTICS.toString()));

            for (PotionEffect effect : player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }

            if (gamePlayer.equals(zombie)) {
                Utils.makeZombie(gamePlayer, true);
            } else {
                player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20000, 2));
                gamePlayer.addData(GamePlayerData.ZOMBIE_TEAM, ZTeam.SURVIVOR);

                Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);

                for (int i = 0; i < 5; i++) {
                    Gun gun = ZombieModBootstrap.getInstance().getGunRegistry().get(gamer.getGun(i + 1));

                    if (gun != null) {
                        inventory.setItem(i, gun.buildItem());
                    }
                }

                if (gamer.haveArmor()) {
                    player.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
                }
            }

            this.gameSideboardUpdateStrategy.update(player);
        }

        Utils.broadcastTitleMsg(Message.START, Message.INFECTED, "zombie", zombie.getName());

        new RandomInfectionStrategySecondImpl(ZombieModBootstrap.getInstance()).start();
    }

    @EventHandler
    public void onSessionStop(GameSessionStopEvent event) {
        if (stopping) {
            return;
        }

        if (timeout) {
            sWin(event.getSession());
        }
    }

    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent event) {
        event.setFoodLevel(20);
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        if (event.toWeatherState()) {
            event.setCancelled(true);
        }
    }

    @SuppressWarnings("UnusedParameters")
    @EventHandler
    public void onSessionTimeout(GameSessionTimeoutEvent event) {
        Utils.broadcastTitleMsg(Message.TIMEOUT, null);
        MinigamesAPI.debug("Time is over");
        timeout = true;
    }

    @EventHandler
    public void onGamePlayerKill(GamePlayerKillEvent event) {
        GamePlayer victim = event.getVictim();
        GamePlayer killer = event.getKiller();
        ZTeam victimZTeam = victim.getData(GamePlayerData.ZOMBIE_TEAM);
        ZTeam killerZTeam = killer.getData(GamePlayerData.ZOMBIE_TEAM);

        if (victimZTeam != null && killerZTeam != null) {
            if (victimZTeam.equals(killerZTeam)) return;

            Player victimPlayer = victim.getEntity();
            Player killerPlayer = killer.getEntity();

            Gamer killerGamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(killerPlayer);
            Gamer victimGamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(victimPlayer);

            int mod = getModForXP(killerPlayer);
            victimGamer.setPoints(victimGamer.getPoints() - ZombieModBootstrap.getSettings().getPointsForPlayerKill());

            if (victimZTeam == ZTeam.SURVIVOR && killerZTeam == ZTeam.ZOMBIE) {   //z killed p
                Utils.broadcastTitleMsg(
                        Message.ZINFECT_ROW_1,
                        Message.ZINFECT_ROW_2,
                        "player", killer.getName(),
                        "infected", victim.getName()
                );
                Message.ZINFECT.broadcast("player", killer.getName(), "infected", victim.getName());

                MinigamesAPI.debug(killer.getName() + " turned " + victim.getName() + " into zombie");
                Utils.makeZombie(victim, true);

                ZombieType ztype = killer.getData(GamePlayerData.ZOMBIE_TYPE);
                if (ztype == ZombieType.SHADOW) {
                    victimPlayer.showPlayer(killerPlayer);
                }

                for (GamePlayer gamePlayer : event.getSession().getAllPlayers()) {
                    Player player = gamePlayer.getEntity();
                    player.playSound(player.getLocation(), Sound.NOTE_PIANO, 10, 0);
                }

                int xp = ZombieModBootstrap.getSettings().getXpForPlayerKill() * mod;

                killerGamer.setXp(killerGamer.getXp() + xp);
                killerGamer.setPoints(killerGamer.getPoints() + ZombieModBootstrap.getSettings().getPointsForPlayerKill());

                Message.YOU_GOT_XP_AND_SCORE.sendActionBar(
                        killerPlayer,
                        "xp", xp,
                        "score", ZombieModBootstrap.getSettings().getPointsForPlayerKill()
                );
                Message.YOU_LOST_SCORE.sendActionBar(
                        victimPlayer,
                        "score", ZombieModBootstrap.getSettings().getPointsForPlayerKill()
                );

                if (lastzombspos < 2)
                    lastzombs[++lastzombspos] = victim;
                else {
                    lastzombs[0] = victim;
                    lastzombspos = 0;
                }

                Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(killerPlayer);
                gamer.incrementSurvivorsKilled();

                checkZWin(event.getSession());
            } else if (victimZTeam.equals(ZTeam.ZOMBIE) && killerZTeam.equals(ZTeam.SURVIVOR)) {   //p killed z
                Object[] replacements = {
                        "player", killer.getName(),
                        "killed", victim.getName()
                };
                Utils.broadcastTitleMsg(Message.PKILL_ROW_1, Message.PKILL_ROW_2, replacements);
                Message.PKILL.broadcast(replacements);
                int xp = ZombieModBootstrap.getSettings().getXpForZombieKill() * mod;

                killerGamer.setXp(killerGamer.getXp() + xp);
                killerGamer.setPoints(killerGamer.getPoints() + ZombieModBootstrap.getSettings().getPointsForZombieKill());
                Message.YOU_GOT_XP_AND_SCORE.sendActionBar(
                        killerPlayer,
                        "xp", xp,
                        "score", ZombieModBootstrap.getSettings().getPointsForZombieKill()
                );
                Message.YOU_LOST_SCORE.sendActionBar(
                        victimPlayer,
                        "score", ZombieModBootstrap.getSettings().getPointsForZombieKill()
                );

                for (Player p : Bukkit.getOnlinePlayers()) {
                    p.playSound(p.getLocation(), Sound.NOTE_PIANO, 10, 0);
                }

                Utils.makeZombie(victim, false); // Full health
                victimPlayer.teleport(event.getSession().getArena().getSpawns()[0]);

                Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(killerPlayer);
                gamer.incrementZombieKilled();

                checkSWin();
            }
        }

        this.getCurrentSideboardUpdateStrategy().update();
    }

    @EventHandler
    public void onGamePlayerDeath(GamePlayerDeathEvent event) {
        GamePlayer gamePlayer = event.getPlayer();
        ZTeam zteam = gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM);

        if (zteam != null) {
            Player player = gamePlayer.getEntity();

            Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);

            gamer.setPoints(gamer.getPoints() - ZombieModBootstrap.getSettings().getPointsForPlayerKill());

            if (zteam == ZTeam.SURVIVOR) { //z killed gamePlayer
                Utils.broadcastTitleMsg(Message.PDIED, null, "player", gamePlayer.getName());

                MinigamesAPI.debug(gamePlayer.getName() + " died & turned into zombie");
                Utils.makeZombie(gamePlayer, true);

                for (PotionEffect effect : player.getActivePotionEffects()) {
                    player.removePotionEffect(effect.getType());
                }

                for (GamePlayer gamePlayer1 : event.getSession().getAllPlayers()) {
                    Player player1 = gamePlayer1.getEntity();
                    player1.playSound(player1.getLocation(), Sound.NOTE_PIANO, 10, 0);
                }

                player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 20), true);

                Message.YOU_LOST_SCORE.sendActionBar(player, "score", ZombieModBootstrap.getSettings().getPointsForPlayerKill());

                if (lastzombspos < 2)
                    lastzombs[++lastzombspos] = gamePlayer;
                else {
                    lastzombs[0] = gamePlayer;
                    lastzombspos = 0;
                }

                checkZWin(event.getSession());
            } else if (zteam == ZTeam.ZOMBIE) { //gamePlayer killed z
                Message.ZDIED.broadcast("player", gamePlayer.getName());

                Message.YOU_LOST_SCORE.sendActionBar(player, "score", ZombieModBootstrap.getSettings().getPointsForZombieKill());

                for (Player player1 : Bukkit.getOnlinePlayers()) {
                    player1.playSound(player1.getLocation(), Sound.NOTE_PIANO, 10, 0);
                }

                Utils.makeZombie(gamePlayer, false);                           // Full health
                player.teleport(event.getSession().getArena().getSpawns()[0]);

                checkSWin();
            }
        }

        this.getCurrentSideboardUpdateStrategy().update();
    }

    @EventHandler
    public void onGamePlayerHit(GamePlayerHitEvent event) {
        ZTeam damagerZTeam = event.getDamager().getData(GamePlayerData.ZOMBIE_TEAM);
        ZTeam victimZTeam = event.getVictim().getData(GamePlayerData.ZOMBIE_TEAM);
        EntityDamageByEntityEvent bukkitEvent = event.getBukkitEvent();

        if (damagerZTeam != null && victimZTeam == null) {    //spectator
            event.getVictim().getEntity().teleport(event.getSession().getArena().getSpawns()[0]);
            bukkitEvent.setCancelled(true);

            return;
        }

        if (damagerZTeam == null || damagerZTeam.equals(victimZTeam)) { //spectator or teamdamage
            bukkitEvent.setCancelled(true);

            return;
        }

        if (damagerZTeam.equals(ZTeam.ZOMBIE) && victimZTeam.equals(ZTeam.SURVIVOR)) {  // z -> s
            bukkitEvent.setDamage(8);
            ZombieType zombieType = event.getDamager().getData(GamePlayerData.ZOMBIE_TYPE);

            if (zombieType == ZombieType.SHADOW) {
                showInvisibleZombie(event.getDamager().getEntity());
            }

            return;
        }
        if (damagerZTeam.equals(ZTeam.SURVIVOR) && victimZTeam.equals(ZTeam.ZOMBIE)) {    // s -> z
            ZombieType zombieType = event.getVictim().getData(GamePlayerData.ZOMBIE_TYPE);

            if (zombieType == ZombieType.SHADOW) {
                showInvisibleZombie(event.getDamager().getEntity());
            }

            if (event.getDamager().getEntity().getItemInHand() == null) {
                bukkitEvent.setDamage(1.0);
            } else if (event.getDamager().getEntity().getItemInHand().getType() == Material.CACTUS) {   //ULTIMATE secret admin weapon!
                bukkitEvent.setDamage(100);
            }

            GamePlayer gamePlayer = MinigamesAPI.getInstance().getGamePlayer(event.getDamager().getEntity().getName());
            double zombieDamage = gamePlayer.hasData(GamePlayerData.ZOMBIE_DAMAGE)
                    ? (double) gamePlayer.getData(GamePlayerData.ZOMBIE_DAMAGE)
                    : 0;

            gamePlayer.addData(GamePlayerData.ZOMBIE_DAMAGE, zombieDamage + bukkitEvent.getFinalDamage());
        }
    }

    @EventHandler
    public void onShotByWeapon(WeaponDamageEntityEvent event) {
        GameSession session = ZombieModGameLaunchPoint.getPlugin().getSession();

        if (session == null) {
            return;
        }

        Player damager = event.getPlayer();

        if (damager == null || !(event.getVictim() instanceof Player)) {
            return;
        }

        Player victim = (Player) event.getVictim();
        GamePlayer damagerGamePlayer = ZombieModGameLaunchPoint.getPlugin().getMAPI().getAPI().getGamePlayer(damager.getName());
        GamePlayer victimGamePlayer = ZombieModGameLaunchPoint.getPlugin().getMAPI().getAPI().getGamePlayer(victim.getName());

        ZTeam damagerZTeam = damagerGamePlayer.getData(GamePlayerData.ZOMBIE_TEAM);
        ZTeam victimZTeam = victimGamePlayer.getData(GamePlayerData.ZOMBIE_TEAM);

        if (damagerZTeam == victimZTeam || event.getWeaponTitle().equals("Bomb")) {
            event.setCancelled(true);
            return;
        }

        if (event.getWeaponTitle().equals("Needler") && victimGamePlayer.getData(GamePlayerData.ZOMBIE_TYPE) == ZombieType.ALIEN) {
            Bukkit.getScheduler().runTaskLater(
                    ZombieModBootstrap.getInstance(),
                    () -> victim.removePotionEffect(PotionEffectType.BLINDNESS),
                    1
            );
        }

        if (victimGamePlayer.getData(GamePlayerData.ZOMBIE_TYPE) == ZombieType.ALIEN && new Random().nextInt(4) == 0) {
            damager.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 20));
        }

        if (victimGamePlayer.getData(GamePlayerData.ZOMBIE_TYPE) == ZombieType.SHADOW) {
            showInvisibleZombie(victim);
        }
    }

    @EventHandler
    public void onPreShoot(WeaponPreShootEvent e) {
        if ((e.getWeaponTitle().equals("Bomb") &&
                (!ZombieModGameLaunchPoint.getPlugin().getSession().isStarted() || stopping))
                || brokenGuns.containsKey(e.getPlayer().getName())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onShoot(final WeaponShootEvent event) {
        if (event.getWeaponTitle().equals("Bomb")) {
            ZombieType type = MinigamesAPI.getInstance().getGamePlayer(event.getPlayer().getName()).getData(GamePlayerData.ZOMBIE_TYPE);

            if (type == ZombieType.PUDGE) {
                Bukkit.getScheduler().runTaskLater(
                        ZombieModBootstrap.getInstance(),
                        () -> new CSUtility().giveWeapon(event.getPlayer(), "Bomb", 1),
                        60 * 20
                );
            }
        }
    }

    @EventHandler
    public void onScope(WeaponScopeEvent event) {
        if (!event.isZoomIn()) {
            Bukkit.getScheduler().runTaskLater(
                    ZombieModBootstrap.getInstance(),
                    () -> event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20000, 2)),
                    20
            );
        }
    }

    @EventHandler
    // TODO:
    public void onPotionSplashEvent(PotionSplashEvent event) {
        event.setCancelled(true);
        ProjectileSource projectileSource = event.getEntity().getShooter();

        if (!(projectileSource instanceof Player)) {
            return;
        }

        Player shooter = (Player) projectileSource;
        GamePlayer shooterAsGamePlayer = MinigamesAPI.getInstance().getGamePlayer(shooter.getName());

        if (shooterAsGamePlayer.getData(GamePlayerData.ZOMBIE_TEAM) != ZTeam.ZOMBIE) {
            return;
        }

        Consumer<Player> forEachConsumer = player -> {
            player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 8 * 20, 2));
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 8 * 20, 1));

            brokenGuns.put(player.getName(), 8);

            String title = "§4Ваше оружие все в паутине!";
            String subtitle = "Вы не можете стрелять или бить";
            if (!NOCSAPI) {
                Utils.sendTitle(player, title, subtitle, 0, 40, 0);
            } else {
                player.sendMessage("[title]" + title + "/" + subtitle);
            }

            Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> player.setVelocity(new Vector()), 1);
        };

        event.getAffectedEntities().stream()
                .filter(entity -> entity instanceof Player)
                .map(entity -> (Player) entity)
                .map(player -> MinigamesAPI.getInstance().getGamePlayer(player.getName()))
                .filter(gamePlayer -> gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM) == ZTeam.SURVIVOR)
                .map(GamePlayer::getEntity)
                .forEach(forEachConsumer);
    }

    @EventHandler
    public void onPlayerDamageByEntity(EntityDamageByEntityEvent event) {
        if (!ZombieModGameLaunchPoint.getPlugin().getSession().isStarted() || stopping || event.getEntityType() != EntityType.PLAYER) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event) {
        if (event.getCause() == DamageCause.VOID) {
            event.getEntity().teleport(event.getEntity().getLocation().add(0, 50, 0));
        }

        if (!ZombieModGameLaunchPoint.getPlugin().getSession().isStarted()
                || event.getCause() == DamageCause.FALL
                || event.getCause() == DamageCause.DROWNING
                || event.getCause() == DamageCause.SUFFOCATION) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onRegen(EntityRegainHealthEvent event) {
        if (event.getRegainReason() != EntityRegainHealthEvent.RegainReason.MAGIC_REGEN) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        GamePlayer gamePlayer = ZombieModGameLaunchPoint.getPlugin().getMAPI().getAPI().getGamePlayer(event.getPlayer().getName());
        Player player = gamePlayer.getEntity();
        GameSession session = ZombieModGameLaunchPoint.getPlugin().getSession();

        if (session == null || player == null) {
            return;
        }

        player.teleport(session.getArena().getSpawns()[0]);
    }

    @EventHandler
    public void onCountDown(GameSessionCountDownTickEvent e) {
        GameSession sess = e.getSession();
        if (sess.getPlayersCount() < ZombieModBootstrap.getSettings().getMinPlayers()) {
            e.setTime(0);

            //RMCSAPI.getBarAPI().clearForAll();
            return;
        }

        if (sess.getData(GamePlayerData.VOTE_ENDED) == null || !((Boolean) sess.getData(GamePlayerData.VOTE_ENDED))) {
            e.setTime(0);
            return;
        }

        Message.EMPTY.broadcastActionBar();

        Sound[] scarysounds = new Sound[]{Sound.ZOMBIE_HURT, Sound.ZOMBIE_IDLE, Sound.ZOMBIE_INFECT,
                Sound.ZOMBIE_METAL, Sound.ZOMBIE_REMEDY, Sound.ZOMBIE_UNFECT,
                Sound.ZOMBIE_WALK, Sound.AMBIENCE_CAVE};

        Message.UNTILGAME.sendBarIfNotActivated(Color.fromRGB(175, 85, 228), ZombieModBootstrap.getSettings().getPrepareTime());

        for (Player p : Bukkit.getOnlinePlayers()) {
            p.playSound(p.getLocation(), scarysounds[new Random().nextInt(scarysounds.length)], 10, 0);
        }

        int time = ZombieModBootstrap.getSettings().getPrepareTime() - e.getTime();

        if (time == 0) {
            Utils.broadcastTitleMsg(null, null);
        } else if (time == 30 || time == 10 || time < 6) {
            Utils.broadcastTitleMsg(
                    Message.UNTILGAME_ROW_1,
                    Message.UNTILGAME_ROW_2,
                    "seconds", time,
                    "word", DMS.getChatUtil().transformByCount(time, "секунда", "секунды", "секунд")
            );
        }
    }

    public void endVoting(GameSession gameSession) {
        String choose = ZombieModGameLaunchPoint.getPlugin().getChooseMapStrategy().choose();
        ArenasManager arenasManager = ZombieModGameLaunchPoint.getPlugin().getArenasManager();
        Location spawn = arenasManager.getSpawn(choose);

        if (spawn == null) {
            throw new RuntimeException("Spawn in arena '" + choose + "' is not setup.");
        }

        GameArena arena = new GameArena(choose, arenasManager.getDisplayName(choose), spawn);

        Message.VOTEEND.broadcast("map", arena.getDisplayName());
        gameSession.addData(GamePlayerData.VOTE_ENDED, true);
        ZombieModGameLaunchPoint.getPlugin().setNewArena(arena);
        voteController = null;
        gameSession = ZombieModGameLaunchPoint.getPlugin().getSession();

        for (GamePlayer gamePlayer : gameSession.getAllPlayers()) {
            gamePlayer.getEntity().teleport(gameSession.getArena().getSpawns()[0].clone().add(0, 1, 0));
            gamePlayer.getEntity().getInventory().remove(Material.PAPER);
            gamePlayer.getEntity().getInventory().remove(Material.NETHER_STALK);

            gamePlayer.getEntity().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500000, 2));
        }

        Message.EMPTY.broadcastActionBar();

        World world = gameSession.getArena().getSpawns()[0].getWorld();
        world.setTime(18000);
        world.setGameRuleValue("doDaylightCycle", "false");
    }

    @EventHandler
    public void onGameTick(GameSessionTickEvent event) {
        if (stopping) {
            return;
        }

        int time = ZombieModBootstrap.getSettings().getGameTime() - event.getTime();

        Message.UNTILEND.sendBarIfNotActivated(Color.fromRGB(175, 85, 228), ZombieModBootstrap.getSettings().getGameTime());
//        setBar("§7§lДо конца игры: %time%", GAMETIME, event.getTime(), ImmutableMap.of(/*"%d", String.valueOf(time)*/), Color.fromRGB(175, 85, 228));
        if (time == 30 || time == 10 || (time < 6 && time > 0)) {
            Utils.broadcastTitleMsg(
                    Message.UNTILEND_ROW_1,
                    Message.UNTILEND_ROW_2,
                    "seconds", time,
                    "word", DMS.getChatUtil().transformByCount(time, "секунда", "секунды", "секунд")
            );
        }
//        }

        if (ZombieModBootstrap.getSettings().getGameTime() - event.getTime() == 130) {
            ItemStack itemStack = new ItemStack(3095);
            ItemMeta meta = itemStack.getItemMeta();
            meta.setDisplayName("§7Сумерки");
            itemStack.setItemMeta(meta);

            Utils.getPlayersInTeam(ZTeam.ZOMBIE).stream()
                    .filter(gamePlayer -> gamePlayer.getData(GamePlayerData.ZOMBIE_TYPE) == ZombieType.ALIEN)
                    .map(GamePlayer::getEntity)
                    .forEach(player -> player.getInventory().setItem(3, itemStack));
        }

        if (ZombieModBootstrap.getSettings().getGameTime() - event.getTime() == 60) {
            ItemStack shout = new ItemStack(3092);
            ItemMeta meta = shout.getItemMeta();
            meta.setDisplayName("§4КРИК ЗОМБИ");
            shout.setItemMeta(meta);

            Arrays.stream(ZombieModGameLaunchPoint.getPlugin().getSession().getAllPlayers())
                    .filter(gp -> gp.getData(GamePlayerData.ZOMBIE_TEAM) == ZTeam.ZOMBIE)
                    .filter(gp -> gp.getData(GamePlayerData.ZOMBIE_TYPE) == ZombieType.PUDGE)
                    .map(GamePlayer::getEntity)
                    .forEach(p -> p.getInventory().addItem(shout));
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        event.getDrops().clear();
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();

        if (ZombieModGameLaunchPoint.getPlugin().getSession().isStarted()) {
            GamePlayer gamePlayer = MinigamesAPI.getInstance().getGamePlayer(player.getName());
            ZTeam team = gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM);

            event.setFormat(event.getFormat().replace("%zteam", team == ZTeam.SURVIVOR ? "§9Выживший" : "§cЗомби"));
        }

        if (stopping
                && !GGs.contains(event.getPlayer().getName())
                && (event.getMessage().equalsIgnoreCase("gg") || event.getMessage().equalsIgnoreCase("гг"))
        ) {
            Gamer data = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);
            data.setXp(data.getXp() + 1);

            Message.YOU_GOT_XP.send(event.getPlayer(), "xp", 1);
            GGs.add(event.getPlayer().getName());
        }
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    public void onXPChange(PlayerExpChangeEvent event) {
        event.setAmount(0);
    }

    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        ItemStack itemStack = event.getItem();
        final Player player = event.getPlayer();

        if (itemStack == null) {
            return;
        }

        if (event.getAction() == Action.LEFT_CLICK_BLOCK &&
                ZombieModGameLaunchPoint.getPlugin().getSession().isStarted() &&
                MinigamesAPI.getInstance().getGamePlayer(event.getPlayer().getName()).getData(GamePlayerData.ZOMBIE_TEAM) == null) {
            event.setCancelled(true);

            return;
        }

        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            switch (itemStack.getType()) {
                case PAPER: {
                    if (voteController != null) {
                        player.openInventory(voteController.createInventory());
                    }
                    break;
                }
                case MAGMA_CREAM: {
                    DMS.spigot().getBungeeUtils().sendPlayer(player, "@zmlobby");

                    break;
                }
                case SLIME_BALL: {
                    DMS.spigot().getBungeeUtils().sendPlayer(player, "@zmlobby");
                    break;
                }
                case CLAY_BALL: {       //SPIDER
                    player.getInventory().remove(Material.CLAY_BALL);

                    Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
                        if (ZombieModGameLaunchPoint.getPlugin().getSession().isStarted() &&
                                MinigamesAPI.getInstance().getGamePlayer(player.getName()).getData(GamePlayerData.ZOMBIE_TYPE) == ZombieType.SPIDER) {
                            player.getInventory().setItem(1, new DmsItemStack(Material.CLAY_BALL, "§aПаутина"));
                        }
                    }, 30 * 20);

                    Vector direction = player.getEyeLocation().getDirection();
                    double y = direction.getY();

                    player.launchProjectile(ThrownPotion.class, direction.clone().multiply(2).setY(y));
                    break;
                }
                case RECORD_10: { // Toxic
                    player.getInventory().remove(Material.RECORD_10);

                    Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
                        if (ZombieModGameLaunchPoint.getPlugin().getSession().isStarted() &&
                                MinigamesAPI.getInstance().getGamePlayer(player.getName()).getData(GamePlayerData.ZOMBIE_TYPE) == ZombieType.TOXIC) {
                            player.getInventory().setItem(1, new DmsItemStack(Material.RECORD_10, "§с§lТоксичное облако"));
                        }
                    }, 80 * 20);

                    MinigamesAPI.debug("Player " + player.getName() + " try to place a toxic totem");

                    this.toxicTotemManager.create(player.getLocation()).place();

                    break;
                }
            }

            if (itemStack.getTypeId() == 3095) {
                player.getInventory().remove(3095);

                Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
                    if (ZombieModGameLaunchPoint.getPlugin().getSession().isStarted() &&
                            MinigamesAPI.getInstance().getGamePlayer(player.getName()).getData(GamePlayerData.ZOMBIE_TYPE) == ZombieType.ALIEN) {
                        player.getInventory().setItem(3, itemStack);
                    }
                }, 30 * 20);

                Utils.getPlayersInTeam(ZTeam.SURVIVOR).stream()
                        .map(GamePlayer::getEntity)
                        .forEach(player1 -> {
                            player1.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 6 * 20, 1));
                        });
            }

            if (itemStack.getTypeId() == 3092) {
                player.getInventory().remove(3092);

                Bukkit.getOnlinePlayers().forEach(p2 -> p2.playSound(p2.getLocation(), Sound.GHAST_SCREAM, 10, 0));
                Bukkit.broadcastMessage(String.format("§eИгрок §4%s §eактивировал способность §4§lКРИК ЗОМБИ", player.getName()));

                if (!shoutUsed) {
                    Arrays.stream(ZombieModGameLaunchPoint.getPlugin().getSession().getAllPlayers())
                            .filter(gp -> gp.getData(GamePlayerData.ZOMBIE_TEAM) == ZTeam.ZOMBIE)
                            .map(GamePlayer::getEntity)
                            .forEach(p2 -> {
                                p2.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60, 1, true, false));
                                p2.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20 * 60, 0, true, false));
                            });

                    shoutUsed = true;
                }
            }
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        GamePlayer gamePlayer = MinigamesAPI.getInstance().getGamePlayer(player.getName());

        if (gamePlayer.getData(GamePlayerData.ZOMBIE_TYPE) != ZombieType.SPIDER) {
            return;
        }

        if (gamePlayer.hasData(GamePlayerData.SPIDER__JUMP_LAST_ACTIVATED)) {
            long last = gamePlayer.getData(GamePlayerData.SPIDER__JUMP_LAST_ACTIVATED);

            if (System.currentTimeMillis() - last < ZombieModBootstrap.getSettings().getSpiderJumpCooldown()) {
                return;
            }
        }

        if (player.isFlying()) {
            return;
        }

        if (player.getGameMode() == GameMode.SPECTATOR || player.getGameMode() == GameMode.CREATIVE) {
            return;
        }

        if (!player.isOnGround()) {
            return;
        }

        if (player.getWorld().getBlockAt(player.getLocation().add(0, -2, 0)).getType() == Material.AIR) {
            return;
        }

        player.setAllowFlight(true);
    }

    @EventHandler
    public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();

        if (player.getGameMode() == GameMode.SPECTATOR || player.getGameMode() == GameMode.CREATIVE) {
            return;
        }

        GamePlayer gamePlayer = MinigamesAPI.getInstance().getGamePlayer(player.getName());
        gamePlayer.addData(GamePlayerData.SPIDER__JUMP_LAST_ACTIVATED, System.currentTimeMillis());

        event.setCancelled(true);
        player.setAllowFlight(false);
        player.setFlying(false);

        double rad = Math.toRadians(player.getLocation().getYaw());
        double sin = Math.sin(rad);
        double cos = Math.cos(rad);

        player.setVelocity(new Vector(-sin, 0, cos).multiply(player.isOnGround() ? 2.3 : 1).setY(0.6));
        player.setFallDistance(-256);
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (event.getInventory().getTitle().equals(Message.VOTEINV.toString()) && voteController != null) {
            voteController.closeInventory(event.getInventory());
        } else if (event.getInventory().getTitle().startsWith(Case.OPEN_INVENTORY_MARK)) {
            PlayerOpeningController controller = ZombieModGameLaunchPoint.cases.get(event.getPlayer().getName());

            if (controller != null) {
                controller.stop(true);
            }
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        event.setCancelled(true);
        ItemStack item = event.getCurrentItem();

        if (item == null || item.getAmount() == 0) {
            return;
        }

        Player player = (Player) event.getWhoClicked();

        if (event.getInventory().getTitle().equals(Message.TELEPORTINV.toString())) {
            Player target = Bukkit.getPlayerExact(
                    item.getItemMeta().getDisplayName().startsWith(ChatColor.RED.toString()) ?
                            item.getItemMeta().getDisplayName().substring(2) :
                            item.getItemMeta().getDisplayName());
            if (target != null) {
                player.teleport(target);
            } else {
                Message.NOTONLINE.send(player);
            }
        } else if (event.getInventory().getTitle().equals(Message.VOTEINV.toString())) {
            String arenaName = item.getItemMeta().getDisplayName();

            if (voteController != null && !arenaName.equals(voteController.getPlayerVote(player.getName()))) {
                int votes = 1;

                if (!NOCSAPI) {
                    DmsPlayerPermissions perms = DMS.getPlayerRegistry().get(player.getName()).getPermissions();
                    votes = perms.isRich() ? 5 : perms.isVipPlus() ? 3 : perms.isVip() ? 2 : 1;
                }

                if (!voteController.hasVoted(player.getName())) {
                    voteController.vote(player, arenaName, votes);
                } else {
                    voteController.moveVote(player.getName(), arenaName, votes);
                }

                Message.VOTE.send(player, "map", arenaName);
            }

            player.closeInventory();
        }
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        String[] parts = event.getMessage().split(" ");

        if (parts[0].equalsIgnoreCase("/kill") || parts[0].equalsIgnoreCase("/minecraft:kill")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if (event.getEntityType() != EntityType.PLAYER && event.getEntityType() != EntityType.ARMOR_STAND) {
            event.setCancelled(true);
        }
    }

    public void checkZWin(GameSession gameSession) {
        if (!stopping && Utils.getPlayersInTeam(ZTeam.SURVIVOR).size() == 0) {
            Message.ZWON.broadcast();
            zWin(gameSession);
        }
    }

    private void zWin(GameSession gameSession) {
        for (GamePlayer gp : gameSession.getAllPlayers()) {
            ZTeam zteam = gp.getData(GamePlayerData.ZOMBIE_TEAM);
            Player player = gp.getEntity();
            Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);

            if (zteam == null) {
                continue;
            }

            if (zteam.equals(ZTeam.ZOMBIE)) {
                player.getLocation().getWorld().playSound(player.getLocation(), Sound.WITHER_SPAWN, 10, 0);

                if (!contains(lastzombs, gp)) {
                    int xp = ZombieModBootstrap.getSettings().getXpForWin() * getModForXP(player);

                    gamer.setXp(gamer.getXp() + xp);
                    gamer.setPoints(gamer.getPoints() + ZombieModBootstrap.getSettings().getPointsForWin());
                    Message.YOU_GOT_XP_AND_SCORE.sendActionBar(
                            player,
                            "xp", xp,
                            "score", ZombieModBootstrap.getSettings().getPointsForWin()
                    );
                    this.takeScoreAndChest(gamer);
                }
            } else {
                gamer.setPoints(gamer.getPoints() - ZombieModBootstrap.getSettings().getPointsForWin());
                Message.YOU_LOST_SCORE.sendActionBar(
                        player,
                        "score", ZombieModBootstrap.getSettings().getPointsForWin()
                );
            }
        }

        MinigamesAPI.debug("Zombies won!");
        printTopPlayers(true);
        restartGame(gameSession);
    }

    public void checkSWin() {
        if (!stopping && Utils.getPlayersInTeam(ZTeam.ZOMBIE).size() == 0) {
            List<GamePlayer> survivors = Utils.getPlayersInTeam(ZTeam.SURVIVOR);

            int position = rand.nextInt(survivors.size());
            survivors.get(position).getEntity().getInventory().clear();
            Utils.makeZombie(survivors.get(position), true);

            Utils.broadcastTitleMsg(Message.ZLEAVED, Message.INFECTED, "zombie", survivors.get(position).getName());

            survivors.get(position).getEntity().removePotionEffect(PotionEffectType.JUMP);
        }
    }

    private void sWin(GameSession gameSession) {
        if (stopping) {
            return;
        }

        for (GamePlayer gamePlayer : gameSession.getAllPlayers()) {
            ZTeam zteam = gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM);
            Player player = gamePlayer.getEntity();
            Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);

            if (zteam == null) {
                continue;
            }

            if (zteam.equals(ZTeam.SURVIVOR)) {
                player.getLocation().getWorld().playSound(player.getLocation(), Sound.WITHER_DEATH, 10, 0);
                int xp = ZombieModBootstrap.getSettings().getXpForWin() * getModForXP(player);
                gamer.setXp(gamer.getXp() + xp);
                gamer.setPoints(gamer.getPoints() + ZombieModBootstrap.getSettings().getPointsForWin());
                Message.YOU_GOT_XP_AND_SCORE.sendActionBar(
                        player,
                        "xp", xp,
                        "score", ZombieModBootstrap.getSettings().getPointsForWin()
                );
                this.takeScoreAndChest(gamer);
            } else {
                gamer.setPoints(gamer.getPoints() - ZombieModBootstrap.getSettings().getPointsForWin());
                Message.YOU_LOST_SCORE.sendActionBar(
                        player,
                        "score", ZombieModBootstrap.getSettings().getPointsForWin()
                );
            }
        }

        MinigamesAPI.debug("Survivors won!");
        if (timeout) {
            timeout = false;
        }

        printTopPlayers(false);
        restartGame(gameSession);
    }

    private void takeScoreAndChest(Gamer gamer) {
        // Идея в том, что шанс выпада любого числа на отрезке от 0 до 19 равен 5 процентам (по теории вероятности - 1/20)
        if (ThreadLocalRandom.current().nextInt(20) == 5) {
            gamer.toDmsPlayer().getCosmetics().addRandomChest();
        }

        gamer.toDmsPlayer().getNetworkLeveling().addExperience(25);
    }

    private void printTopPlayers(boolean zombieWin) {
        DataKey dataKey = zombieWin
                ? GamePlayerData.SURVIVORS_KILLED
                : GamePlayerData.ZOMBIE_DAMAGE;
        Comparator<GamePlayer> comparator = zombieWin
                ? Comparator.comparingInt(gamePlayer -> gamePlayer.getData(dataKey))
                : Comparator.comparingDouble(gamePlayer -> gamePlayer.getData(dataKey));
        List<GamePlayer> top = Stream.of(ZombieModGameLaunchPoint.getPlugin().getSession().getAllPlayers())
                .filter(gamePlayer -> gamePlayer.hasData(dataKey))
                .sorted(Collections.reverseOrder(comparator))
                .limit(3)
                .collect(Collectors.toList());

        if (top.isEmpty()) {
            top = Utils.getPlayersInTeam(zombieWin ? ZTeam.ZOMBIE : ZTeam.SURVIVOR).stream()
                    .limit(3)
                    .collect(Collectors.toList());
        }

        Bukkit.broadcastMessage(Utils.repeat("§2▀", 34));
        Bukkit.broadcastMessage(" ");

        for (int i = 0; i < top.size(); i++) {
            GamePlayer gamePlayer = top.get(i);

            Bukkit.broadcastMessage(Utils.onCenter(
                    (i == 0 ? "§b" : (i == 1 ? "§c" : "§6"))
                            + "Топ-"
                            + (i + 1)
                            + (zombieWin ? " убийца" : " по урону")
                            + ": "
                            + DMS.getChatUtil().colorize(gamePlayer.toGamer().toDmsPlayer().getVisibleFullNameUncolored())
                            + " §7- "
                            + gamePlayer.getData(dataKey),
                    " ", 85
            ));
        }

        Bukkit.broadcastMessage(" ");
        Bukkit.broadcastMessage(Utils.repeat("§2▄", 34));

        MinigamesAPI.debug("Print a top players [" + (zombieWin ? "zombie, " : "survivors, ") + top.size() + "]");
    }

    private void restartGame(final GameSession gameSession) {
        MinigamesAPI.debug("Restarting game");
        stopping = true;

        Message.clearData();

        this.toxicTotemManager.getTotems().stream()
                .filter(Totem::isPlaced)
                .forEach(Totem::remove);

        for (Player player : Bukkit.getOnlinePlayers()) {
            Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(player);
            gamer.incrementGamesPlayed();
            player.setExp(0);
            player.setLevel(0);
            player.setPlayerListName(player.getName());
            player.setDisplayName(player.getName());
            player.getActivePotionEffects().forEach(effect -> player.removePotionEffect(effect.getType()));
            MinigamesAPI.getInstance().getGamePlayer(player.getName()).clearData();
        }

        Message.RESTART.broadcast();
        Message.GAMEOVER.sendBar(Color.fromRGB(175, 85, 228));

        Message.GAMEOVER.broadcastActionBar();

        Bukkit.getScheduler().runTaskLater(ZombieModBootstrap.getInstance(), () -> {
            gameSession.restart();

            gameSession.addData(GamePlayerData.VOTE_ENDED, false);
            voteController = new VoteController(ZombieModGameLaunchPoint.getPlugin().getMAPI().getArenaManager().getArenas());
            ZombieModGameLaunchPoint.getPlugin().getChooseMapStrategy().load();
            this.changeSideboard();

            shoutUsed = false;
            stopping = false;
            timeout = false;
            invzombies.clear();
            GGs.clear();

            for (int i = 0; i < lastzombs.length; i++) {
                lastzombs[i] = null;
            }
            lastzombspos = 0;

            DMS.spigot().getBungeeUtils().sendAllPlayers("@zmlobby");
            DMS.spigot().getWorldRestorer().restore();

            if (!NOCSAPI) {
                DMS.spigot().getQueueUtils().awaitPlayers(
                        ZombieModBootstrap.getSettings().getMinPlayers(),
                        ZombieModBootstrap.getSettings().getMaxPlayers(),
                        "zm",
                        this::startTheGameFromQueue
                );
            }
        }, 20 * 10);
    }

    private void startTheGameFromQueue() {
        endVoting(ZombieModGameLaunchPoint.getPlugin().getSession());
    }

    private void showInvisibleZombie(Player player) {
        for (Player player1 : Bukkit.getOnlinePlayers()) {
            player1.showPlayer(player);
        }

        invzombies.put(player.getName(), 3);
    }

    private int getModForXP(Player player) {
        if (!NOCSAPI) {
            DmsPlayerPermissions perms = DMS.getPlayerRegistry().get(player.getName()).getPermissions();

            return perms.isRich() ? 5 : perms.isVipPlus() ? 3 : perms.isVip() ? 2 : 1;
        }

        return 1;
    }

    private <T> boolean contains(T[] array, T object) {
        for (T o2 : array) {
            if (object.equals(o2)) {
                return true;
            }
        }

        return false;
    }

    private SideboardUpdateStrategy getCurrentSideboardUpdateStrategy() {
        return this.waitGameSideboardUpdateStrategy == null
                ? this.gameSideboardUpdateStrategy
                : this.waitGameSideboardUpdateStrategy;
    }

    static class ArenaSetupListener implements Listener {

        @EventHandler
        public void onInteract(PlayerInteractEvent event) {
            ItemStack itemStack = event.getItem();

            if (event.getClickedBlock() == null) {
                return;
            }

            Location location = event.getClickedBlock().getLocation();

            if (event.getPlayer().isOp() &&
                    itemStack != null &&
                    itemStack.getType() == Material.STICK &&
                    itemStack.hasItemMeta() &&
                    itemStack.getItemMeta().hasDisplayName() &&
                    itemStack.getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Choose the location") &&
                    event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                switch (waitfor) {
                    case "spawn": {
                        spawn = location;
                        event.getPlayer().sendMessage("Done");

                        break;
                    }
                    case "corner1": {
                        corner1 = location;
                        event.getPlayer().sendMessage("Done");

                        break;
                    }
                    case "corner2": {
                        corner2 = location;
                        event.getPlayer().sendMessage("Done");

                        break;
                    }
                    default: {
                        break;
                    }
                }

                event.setCancelled(true);
            }
        }

    }

    private class ZombieNearRunnable implements Runnable {

        @Override
        public void run() {
            for (GamePlayer gamePlayer : ZombieModGameLaunchPoint.getPlugin().getSession().getAllPlayers()) {
                Player player = gamePlayer.getEntity();
                ZTeam zteam = gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM);

                if (!(zteam == ZTeam.SURVIVOR)) {
                    continue;
                }

                player.getNearbyEntities(10, 4, 10)
                        .stream()
                        .filter(entity -> entity.getType() == EntityType.PLAYER)
                        .forEach(entity -> {
                            GamePlayer gamePlayer1 = MinigamesAPI.getInstance().getGamePlayer(entity.getName());
                            ZTeam zteam1 = gamePlayer1.getData(GamePlayerData.ZOMBIE_TEAM);
                            ZombieType zombieType = gamePlayer1.getData(GamePlayerData.ZOMBIE_TYPE);

                            if (zteam1 == ZTeam.ZOMBIE && zombieType == ZombieType.SHADOW) {
                                player.playSound(player.getLocation(), Sound.NOTE_BASS_DRUM, 1000, 0);
                            }
                        });
            }
        }

    }

    private class ZombieInvisibleRunnable implements Runnable {

        @Override
        public void run() {
            Iterator<Entry<String, Integer>> iterator = invzombies.entrySet().iterator();
            while (iterator.hasNext()) {
                Entry<String, Integer> ent = iterator.next();
                Player p = Bukkit.getPlayerExact(ent.getKey());
                if (ent.getValue() > 0) {
                    ent.setValue(ent.getValue() - 1);
                } else
                    iterator.remove();
                for (Player p2 : Bukkit.getOnlinePlayers()) {
                    GamePlayer gp = MinigamesAPI.getInstance().getGamePlayer(p2.getName());
                    if (gp.getData(GamePlayerData.ZOMBIE_TEAM) == ZTeam.SURVIVOR)
                        p2.hidePlayer(p);
                }
            }
        }
    }

    private class WebCooldownRunnable implements Runnable {

        @Override
        public void run() {
            processCooldowns(brokenGuns.entrySet().iterator());
        }

        private void processCooldowns(Iterator<Entry<String, Integer>> iterator) {
            while (iterator.hasNext()) {
                Entry<String, Integer> entry = iterator.next();

                if (entry.getValue() > 0) {
                    entry.setValue(entry.getValue() - 1);
                } else {
                    iterator.remove();
                }
            }
        }
    }

}
