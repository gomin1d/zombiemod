package yt.dms.zombiemod;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import yt.dms.zombiemod.data.GamePlayerData;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.lib.mgapi.player.GamePlayer;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

public class CompassUpdater extends BukkitRunnable {
    public void run() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            GameSession session = MinigamesAPI.getInstance().getPlayerSession(p.getName());

            if (session != null && p.getInventory().getItemInHand().getType() == Material.COMPASS) {
                GamePlayer gamePlayer = MinigamesAPI.getInstance().getGamePlayer(p.getName());
                ZTeam pteam = gamePlayer.getData(GamePlayerData.ZOMBIE_TEAM);

                if (pteam == ZTeam.ZOMBIE) {
                    updateCompases(p);
                }
            }
        }
    }

    public static void updateCompases(Player player) {
        Location location = player.getLocation();
        double shortestDistance = 99999;
        Player nearestPlayer = player;

        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            GameSession gameSession = MinigamesAPI.getInstance().getPlayerSession(onlinePlayer.getName());

            if (gameSession != null) {
                GamePlayer gp = MinigamesAPI.getInstance().getGamePlayer(onlinePlayer.getName());
                ZTeam zteam = gp.getData(GamePlayerData.ZOMBIE_TEAM);

                if (zteam == ZTeam.SURVIVOR) {
                    double distanceToPlayer = location.distance(onlinePlayer.getLocation());

                    if (distanceToPlayer < shortestDistance && player != onlinePlayer) {
                        nearestPlayer = onlinePlayer;
                        shortestDistance = distanceToPlayer;
                    }
                }
            }
        }

        player.setCompassTarget(nearestPlayer.getLocation());
        ItemStack itemStack = player.getItemInHand();
        ItemMeta itemMeta = itemStack.getItemMeta();

        if (player != nearestPlayer) {
            itemMeta.setDisplayName(Message.NEARESTPLAYER.toString("player", nearestPlayer.getName(), "distance", (int) shortestDistance));
        } else {
            itemMeta.setDisplayName(Message.NOPLAYERSNEAR.toString());
        }

        itemStack.setItemMeta(itemMeta);
        player.setItemInHand(itemStack);
    }
}
