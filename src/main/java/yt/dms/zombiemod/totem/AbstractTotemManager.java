package yt.dms.zombiemod.totem;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractTotemManager<T extends Totem> implements TotemManager<T> {

    // Радиус действия тотема в квадрате
    private final int workRadius;

    private final Collection<T> totems = new ArrayList<>();

    /**
     * Конструктор
     * @param workRadius радиус действия тотема
     */
    public AbstractTotemManager(int workRadius) {
        this.workRadius = workRadius * workRadius;
    }

    @Override
    public Collection<T> getTotems() {
        return Collections.unmodifiableCollection(this.totems);
    }

    @Override
    public Optional<T> findTotem(int id) {
        return this.totems.stream()
                .filter(toxicTotem -> toxicTotem.getEntityId() == id)
                .findAny();
    }

    @Override
    public int getTotemsSize() {
        return this.totems.size();
    }

    @Override
    public boolean hasTotemNear(Location location) {
        return this.totems.size() != 0 && this.totems.stream()
                .filter(Totem::isPlaced)
                .anyMatch(totem -> totem.getLocation().distanceSquared(location) <= this.workRadius);
    }

    @Override
    public Collection<T> getTotemsInWorkRadius(Location location) {
        return this.totems.stream()
                .filter(Totem::isPlaced)
                .filter(totem -> totem.getLocation().distanceSquared(location) <= this.workRadius)
                .collect(Collectors.toList());
    }

    protected T addAndReturn(T totem) {
        this.totems.add(totem);

        return totem;
    }

}
