package yt.dms.zombiemod.totem.toxic;

import com.shampaggon.crackshot.events.WeaponDamageEntityEvent;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import yt.dms.zombiemod.ZTeam;
import yt.dms.zombiemod.data.GamePlayerData;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.totem.Totem;
import yt.dms.zombiemod.totem.TotemManager;

import java.util.Optional;
import java.util.stream.Collectors;

public class ToxicTotemListener implements Listener {

    private final TotemManager<ToxicTotem> toxicTotemManager;

    public ToxicTotemListener(TotemManager<ToxicTotem> toxicTotemManager) {
        this.toxicTotemManager = toxicTotemManager;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamage(WeaponDamageEntityEvent event) {
        this.toxicTotemManager.findTotem(event.getVictim().getEntityId()).ifPresent(totem -> {
            event.setCancelled(true);
        });
    }

    @EventHandler
    public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent event) {
        this.toxicTotemManager.findTotem(event.getRightClicked().getEntityId()).ifPresent(totem -> {
            event.setCancelled(true);
        });
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) {
            return;
        }

        if (MinigamesAPI.getInstance().getGamePlayer(event.getDamager().getName()).getData(GamePlayerData.ZOMBIE_TEAM) == ZTeam.ZOMBIE) {
            return;
        }

        Optional<ToxicTotem> toxicTotemOptional = this.toxicTotemManager.findTotem(event.getEntity().getEntityId());

        if (!toxicTotemOptional.isPresent()) {
            if (event.getEntityType() == EntityType.ARMOR_STAND) {
                MinigamesAPI.debug("Player " + event.getDamager().getName() + " damage a armor stand with id "
                        + event.getEntity().getEntityId() + ", but it is not a totem"
                );
                MinigamesAPI.debug("All placed totems: " +
                        this.toxicTotemManager.getTotems().stream()
                                .filter(Totem::isPlaced)
                                .map(totem -> String.valueOf(totem.getEntityId()))
                                .collect(Collectors.toList())
                        .toString()
                );
            }

            return;
        }

        Damageable damageable = (Damageable) event.getEntity();
        double health = damageable.getHealth() - event.getFinalDamage();

        if (health <= 0) {
            toxicTotemOptional.get().remove();
        } else {
            damageable.setHealth(health);
        }

        event.setCancelled(true);
    }

}
