package yt.dms.zombiemod.totem.toxic;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import yt.dms.zombiemod.ZTeam;
import yt.dms.zombiemod.data.GamePlayerData;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.totem.AbstractTotemManager;
import yt.dms.zombiemod.totem.Totem;

import java.util.HashMap;
import java.util.Map;

public class ToxicTotemManager extends AbstractTotemManager<ToxicTotem> {

    private final Plugin plugin;

    private final Map<Player, BukkitTask> tasks = new HashMap<>();

    public ToxicTotemManager(Plugin plugin) {
        super(10);

        Bukkit.getPluginManager().registerEvents(new ToxicTotemListener(this), this.plugin = plugin);
        Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            Bukkit.getOnlinePlayers().forEach(player -> {
                Location location = player.getLocation();
                double y = location.getY();

                boolean apply = this.getTotemsInWorkRadius(location).stream().map(Totem::getLocation).anyMatch(totemLocation -> {
                    Location clone = totemLocation.clone();
                    clone.setY(y);

                    return clone.distanceSquared(location) <= 49;
                });

                if (apply) {
                    this.apply(player);
                }
            });
        }, 0, 5);
    }

    @Override
    public ToxicTotem create(Location location) {
        ToxicTotem toxicTotem = new ToxicTotem(location);

        Bukkit.getScheduler().runTaskLater(this.plugin, toxicTotem::remove, 20 * 35);

        return this.addAndReturn(toxicTotem);
    }

    @Override
    public void apply(Player player) {
        if (MinigamesAPI.getInstance().getGamePlayer(player.getName()).getData(GamePlayerData.ZOMBIE_TEAM) == ZTeam.ZOMBIE) {
            return;
        }

        if (this.tasks.containsKey(player)) {
            this.tasks.get(player).cancel();
        }

        if (!player.hasPotionEffect(PotionEffectType.CONFUSION)) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, Integer.MAX_VALUE, 1));
        }

        this.tasks.put(player, Bukkit.getScheduler().runTaskLater(this.plugin, () -> {
            player.removePotionEffect(PotionEffectType.CONFUSION);
        }, 20 * 10));
    }

}
