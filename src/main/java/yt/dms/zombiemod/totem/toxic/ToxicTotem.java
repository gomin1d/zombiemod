package yt.dms.zombiemod.totem.toxic;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.totem.AbstractTotem;

public class ToxicTotem extends AbstractTotem {

    public ToxicTotem(Location location) {
        super(location);
    }

    @Override
    public void place() {
        super.place();

        this.armorStand.setVisible(false);
        this.armorStand.setGravity(false);
        this.armorStand.setMaxHealth(40);
        this.armorStand.setHelmet(new ItemStack(Material.SLIME_BLOCK));
        this.armorStand.setCustomName("§c§lТоксичное облако");
        this.armorStand.setCustomNameVisible(true);

        MinigamesAPI.debug(" Totem type: toxic");
    }

}
