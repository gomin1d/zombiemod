package yt.dms.zombiemod.totem;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Optional;

public interface TotemManager<T extends Totem> {

    /**
     * Создать тотем
     * @param location локация тотема
     * @return тотем
     */
    T create(Location location);

    /**
     * Получить количество тотемов
     * @return количество тотемов
     */
    int getTotemsSize();

    /**
     * Получить все зарегистрированные тотемы
     * @return неизменяемый список зарегистрированных тотемов
     */
    Collection<T> getTotems();

    /**
     * Найти тотем по айди его арморстенда
     * @param id айди арморстенда
     * @return тотем
     */
    Optional<T> findTotem(int id);

    /**
     * Есть ли тотемы, действие которых распространяется на указанную локацию
     * @param location локация
     * @return {@code true} если локация задевает радиус действия какого-то тотема
     */
    boolean hasTotemNear(Location location);

    /**
     * Получить все тотемы, действие которых распространяется на указанную локацию
     * @param location локация
     * @return все тотемы, действие которых распространяется на указанную локацию
     */
    Collection<T> getTotemsInWorkRadius(Location location);

    /**
     * Применить эффекты тотема к игроку
     * @param player игрок
     */
    void apply(Player player);

    /**
     * Применить эффекты тотема к игроку, если он находится в радиусе действия какого-то тотема
     * @param player игрок
     */
    default void applyIfInWorkRadius(Player player) {
        if (this.hasTotemNear(player.getLocation())) {
            this.apply(player);
        }
    }

}
