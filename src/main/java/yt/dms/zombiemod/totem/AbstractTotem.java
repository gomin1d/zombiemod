package yt.dms.zombiemod.totem;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;

public abstract class AbstractTotem implements Totem {

    private final Location location;

    protected ArmorStand armorStand;

    public AbstractTotem(Location location) {
        this.location = location;
    }

    @Override
    public Integer getEntityId() {
        return this.armorStand == null
                ? null
                : this.armorStand.getEntityId();
    }

    @Override
    public Location getLocation() {
        return this.location;
    }

    @Override
    public void place() throws IllegalStateException {
        if (this.isPlaced()) {
            throw new IllegalStateException("You can't place a totem that is already placed");
        }

        this.armorStand = (ArmorStand) this.location.getWorld().spawnEntity(this.location, EntityType.ARMOR_STAND);

        MinigamesAPI.debug("Successfully placed a totem with id " + this.getEntityId());
    }

    @Override
    public void remove() throws IllegalStateException {
        if (!this.isPlaced()) {
            throw new IllegalStateException("You can't remove a totem that is not placed");
        }

        this.armorStand.remove();
        this.armorStand = null;

        MinigamesAPI.debug("Successfully removed a totem with id " + this.getEntityId());
    }

    @Override
    public boolean isPlaced() {
        return this.armorStand != null;
    }

}
