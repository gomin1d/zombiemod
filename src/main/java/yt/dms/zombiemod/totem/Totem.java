package yt.dms.zombiemod.totem;

import org.bukkit.Location;

public interface Totem {

    /**
     * Получить айди арморстенда
     * @return айди арморстенда или null, если тотем не поставлен
     */
    Integer getEntityId();

    /**
     * Получить локацию тотема
     * @return локация тотема
     */
    Location getLocation();

    /**
     * Поставить тотем
     * @throws IllegalStateException если тотем уже установлен
     */
    void place() throws IllegalStateException;

    /**
     * Убрать тотем
     * @throws IllegalStateException если тотем еще не установлен
     */
    void remove() throws IllegalStateException;

    /**
     * @return установлен ли тотем
     */
    boolean isPlaced();

}
