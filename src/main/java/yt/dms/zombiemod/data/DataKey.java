package yt.dms.zombiemod.data;

public interface DataKey {

    String getKey();

}
