package yt.dms.zombiemod.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum GameSessionData implements DataKey {
    LOBBY;

    @Getter
    private final String key;

    GameSessionData() {
        this.key = this.name().toLowerCase().replace("__", ".");
    }

}
