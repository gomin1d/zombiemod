package yt.dms.zombiemod.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum GamePlayerData implements DataKey {
    SURVIVORS_KILLED,
    ZOMBIE_DAMAGE,
    ZOMBIE_TEAM,
    ZOMBIE_TYPE,
    VOTE_ENDED,
    SPIDER__JUMP_LAST_ACTIVATED
    ;

    @Getter
    private final String key;

    GamePlayerData() {
        this.key = this.name().toLowerCase().replace("__", ".");
    }

}
