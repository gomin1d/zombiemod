package yt.dms.zombiemod;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import yt.dms.api.DMS;
import yt.dms.zombiemod.ShopElement.ShopElementType;
import yt.dms.zombiemod.arena.ArenasManager;
import yt.dms.zombiemod.arena.ArenasManagerImpl;
import yt.dms.zombiemod.bootstrap.LaunchPoint;
import yt.dms.zombiemod.bootstrap.ZombieModBootstrap;
import yt.dms.zombiemod.cases.ItemData;
import yt.dms.zombiemod.cases.PlayerOpeningController;
import yt.dms.zombiemod.choose_map.ChooseMapStrategy;
import yt.dms.zombiemod.choose_map.VoteChooseMapStrategy;
import yt.dms.zombiemod.command.SetupCommand;
import yt.dms.zombiemod.data.GameSessionData;
import yt.dms.zombiemod.gamer.Gamer;
import yt.dms.zombiemod.lib.mgapi.MinigamePluginController;
import yt.dms.zombiemod.lib.mgapi.MinigamesAPI;
import yt.dms.zombiemod.lib.mgapi.arena.GameArena;
import yt.dms.zombiemod.lib.mgapi.session.GameSession;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ZombieModGameLaunchPoint implements LaunchPoint {
    
    private MinigamePluginController mapi;
    private GameSession sess = null;

    private List<ZMItemData> gameItems = new ArrayList<>();

    private Location lobby;

    private static ZombieModGameLaunchPoint that;
    private BufferedWriter sqlLogWriter;

    public static Map<String, PlayerOpeningController> cases = new HashMap<>();

    public static boolean NOCSAPI = false;   // for debug purposes

    @Getter
    private ZMEventListener listener;

    @Getter
    private ChooseMapStrategy chooseMapStrategy;

    @Getter
    private ArenasManager arenasManager;

    @Override
    public void enabling() {
        that = this;
        this.chooseMapStrategy = new VoteChooseMapStrategy();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        final String dateString = format.format(new Date());
        File sqlLogFile = new File(ZombieModBootstrap.getInstance().getDataFolder(), "logs/" + dateString + ".log");
        try {
            sqlLogFile.getParentFile().mkdirs();
            sqlLogFile.createNewFile();
            sqlLogWriter = new BufferedWriter(new FileWriter(sqlLogFile, true));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (Bukkit.getPluginManager().getPlugin("LibsDisguises") == null) {
            ZombieModBootstrap.getInstance().getLogger().warning("LibsDisguises not found! Plugin will be disabled");
            Bukkit.getPluginManager().disablePlugin(ZombieModBootstrap.getInstance());
            return;
        }

        if (Bukkit.getPluginManager().getPlugin("CrackShot") == null) {
            ZombieModBootstrap.getInstance().getLogger().warning("CrackShot not found! Plugin will be disabled");
            Bukkit.getPluginManager().disablePlugin(ZombieModBootstrap.getInstance());
            return;
        }

        this.arenasManager = new ArenasManagerImpl();
        new SetupCommand(this.arenasManager);
        lobby = ZombieModBootstrap.getInstance().getMinigameConfig().getLocation("lobby");

        loadCase();
        initMAPI();

        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard objectives remove HUB");

        if (mapi.getArenaManager().getArenas().length > 0 && lobby != null && !ZombieModBootstrap.getSettings().isSetupMode()) {
            sess = mapi.getSessionManager().createSession(mapi.getArenaManager().getArena(mapi.getArenaManager().getArenas()[0]));
            sess.setPlayersLimit(ZombieModBootstrap.getSettings().getMinPlayers(), ZombieModBootstrap.getSettings().getMaxPlayers());
            sess.setTime(ZombieModBootstrap.getSettings().getPrepareTime(), ZombieModBootstrap.getSettings().getGameTime());
            sess.addData(GameSessionData.LOBBY, lobby);
            for (int i = 0; i < ZombieModBootstrap.getSettings().getMaxPlayers(); i++)
                sess.addTeam();

            listener = new ZMEventListener();
            Bukkit.getPluginManager().registerEvents(listener, ZombieModBootstrap.getInstance());
            
            Bukkit.getMessenger().registerOutgoingPluginChannel(ZombieModBootstrap.getInstance(), "BungeeCord");
            new CompassUpdater().runTaskTimer(ZombieModBootstrap.getInstance(), 20L, 20L);

            DMS.spigot().getWorldRestorer().prepare(lobby.getWorld().getName());
        } else {
            Bukkit.getPluginManager().registerEvents(new ZMEventListener.ArenaSetupListener(), ZombieModBootstrap.getInstance());
            ZombieModBootstrap.getInstance().getLogger().info("ZombieMod is ready for setup");
        }
    }

    @Override
    public void disabling() {
        try {
            sqlLogWriter.flush();
            sqlLogWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (mapi != null)
            mapi.getAPI().stop();
    }

    private void initMAPI() {
        mapi = new MinigamesAPI().run(ZombieModBootstrap.getInstance());
    }

    private void loadCase() {
        File fcase = new File(ZombieModBootstrap.getInstance().getDataFolder(), "case.yml");
        if (checkFile(fcase))
            return;

        YamlConfiguration ccase = YamlConfiguration.loadConfiguration(fcase);

        for (Object o : ccase.getValues(false).values()) {
            ConfigurationSection sec = (ConfigurationSection) o;

            ShopElementType type = ShopElementType.fromString(sec.getString("type"));
            String displayName = ChatColor.translateAlternateColorCodes('&', sec.getString("displayName"));
            ItemData item = ItemData.parseString(sec.getString("item"));
            int weight = sec.getInt("weight");
            int content = sec.getInt("content");

            gameItems.add(new ZMItemData(type, displayName, item, weight, content));
        }
    }

    private boolean checkFile(File f) {
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    public void setNewArena(GameArena arena) {
        sess = mapi.getSessionManager().setNewArena(sess, arena);
    }

    public List<ZMItemData> getGameItems() {
        return gameItems;
    }

    public boolean onCommand(CommandSender sender, Command command, String title, String[] args) {
        if (args.length == 0)
            return true;
        if (!sender.isOp()) return true;
        if (args[0].equalsIgnoreCase("set") && sender instanceof Player) {
            if (args.length >= 2) {
                ZMEventListener.setWaitFor(args[1]);
                ItemStack item = new ItemStack(Material.STICK);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName(ChatColor.GREEN + "Choose the location");
                item.setItemMeta(meta);
                ((HumanEntity) sender).getInventory().setItemInHand(item);
            } else
                ZMEventListener.setWaitFor("");
        } else if (args[0].equalsIgnoreCase("setlobby") && sender instanceof Player) {
            sender.sendMessage("don't work");
        } else if (args.length > 1 && args[0].equalsIgnoreCase("save") && sender instanceof Player) {
            ZMEventListener.save((Player) sender, args[1]);
        } else if (args[0].equalsIgnoreCase("addxp")) {
            if (args.length >= 3) {
                Gamer gamer = ZombieModBootstrap.getInstance().getGamerFactory().getGamer(args[1]);

                gamer.setXp(gamer.getXp() + Integer.parseInt(args[2]));
                sender.sendMessage("Добавлено");
            } else
                sender.sendMessage("Укажите игрока и XP");
        } else if (args[0].equalsIgnoreCase("info")) {
            if (args.length >= 2) {
                List<String> message = ZombieModBootstrap.getInstance().getInfo(args[1]);
                sender.sendMessage(message.toArray(new String[0]));
            } else
                sender.sendMessage("Укажите игрока");
        } else if (args[0].equalsIgnoreCase("start")) {
            if(sess.getPlayersCount() < 2) {
                sender.sendMessage("Нужно хотя бы два игрока");
                return true;
            }

            if (!sess.isStarted()) {
                try {
                    listener.endVoting(sess);
                    sess.startGame();
                    sender.sendMessage("Игра начата");
                } catch (IllegalStateException e) {
                    sender.sendMessage(e.getMessage());
                }
            } else {
                sender.sendMessage("Игра уже началась");
            }
        }
//        else if (args[0].equalsIgnoreCase("fixtop")) {
//            usersStorage.addNicknames();
//        }
        else if (args[0].equalsIgnoreCase("reload")) {
            ZombieModBootstrap.getSettings().reload();
            loadCase();

            sender.sendMessage("Перезагружено");
        } else {
            sender.sendMessage("Неизвестная команда");
        }
        return true;
    }

    public MinigamePluginController getMAPI() {
        return mapi;
    }

    public GameSession getSession() {
        return sess;
    }

    public static ZombieModGameLaunchPoint getPlugin() {
        return that;
    }

}
