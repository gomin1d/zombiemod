package yt.dms.zombiemod;

import lombok.Getter;
import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.Disguise;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import me.libraryaddict.disguise.disguisetypes.watchers.LivingWatcher;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import yt.dms.zombiemod.gamer.Gamer;

public enum ZombieType implements NameBuildable {

    DEFAULT(1, "§c§lКласический", DisguiseType.ZOMBIE, 200),
    SPEED(2, "§c§lОхотник", DisguiseType.ENDERMAN, 140, new PotionEffect(PotionEffectType.SPEED, 6000000, 3)),
    SHADOW(3, "§c§lСкелет", DisguiseType.SKELETON, 20),
    JUMP(4, "§c§lПрыгун", DisguiseType.ZOMBIE_VILLAGER, 120, new PotionEffect(PotionEffectType.JUMP, 6000000, 1)),
    PUDGE(5, "§c§lПудж", DisguiseType.PIG_ZOMBIE, 240),
    ALIEN(6, "§c§lАлиен", DisguiseType.WITHER_SKELETON, 160),
    TOXIC(7, "§c§lТоксик", DisguiseType.ZOMBIE, 160),
//    BLOODSEEKER(7, DisguiseType.SKELETON, 100),
    SPIDER(8, "§c§lПаук", DisguiseType.SPIDER, 140);

    private final int id;

    @Getter
    private final String displayName;

    private final PotionEffect[] effects;
    private final DisguiseType disguiseType;
    private final double health;

    ZombieType(int id, String displayName, DisguiseType disguiseType, double health, PotionEffect... effects) {
        this.id = id;
        this.displayName = displayName;
        this.disguiseType = disguiseType;
        this.health = health;
        this.effects = effects;
    }

    public int getId() {
        return id;
    }

    public PotionEffect[] getEffects() {
        return effects;
    }

    public void disguise(Player p) {
        Disguise disguise = new MobDisguise(disguiseType);
        DisguiseAPI.disguiseToAll(p, disguise);
        LivingWatcher watcher = (LivingWatcher) disguise.getWatcher();
        watcher.setCustomName(ChatColor.RED + p.getName());
        watcher.setCustomNameVisible(true);
    }

    public double getHealth() {
        return health;
    }

    public static ZombieType byId(int id) {
        for (ZombieType type : values())
            if(type.getId() == id)
                return type;

        return DEFAULT;
    }

    @Override
    public boolean has(Gamer gamer) {
        return gamer.hasZombie(this.id);
    }

}
